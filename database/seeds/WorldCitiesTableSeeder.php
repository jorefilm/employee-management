<?php

use Illuminate\Database\Seeder;

class WorldCitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'state_id' => NULL,
                'name' => 'Elbasan',
            ),
            1 => 
            array (
                'id' => 2,
                'state_id' => NULL,
                'name' => 'Diber',
            ),
            2 => 
            array (
                'id' => 3,
                'state_id' => NULL,
                'name' => 'Tirane',
            ),
            3 => 
            array (
                'id' => 4,
                'state_id' => NULL,
                'name' => 'Durres',
            ),
            4 => 
            array (
                'id' => 5,
                'state_id' => NULL,
                'name' => 'Vlore',
            ),
            5 => 
            array (
                'id' => 6,
                'state_id' => NULL,
                'name' => 'Fier',
            ),
            6 => 
            array (
                'id' => 7,
                'state_id' => NULL,
                'name' => 'Gjirokaster',
            ),
            7 => 
            array (
                'id' => 8,
                'state_id' => NULL,
                'name' => 'Korce',
            ),
            8 => 
            array (
                'id' => 9,
                'state_id' => NULL,
                'name' => 'Kukes',
            ),
            9 => 
            array (
                'id' => 10,
                'state_id' => NULL,
                'name' => 'Lezhe',
            ),
            10 => 
            array (
                'id' => 11,
                'state_id' => NULL,
                'name' => 'Berat',
            ),
            11 => 
            array (
                'id' => 12,
                'state_id' => NULL,
                'name' => 'Shkoder',
            ),
            12 => 
            array (
                'id' => 13,
                'state_id' => NULL,
                'name' => 'Adrar',
              ),
            13 => 
            array (
                'id' => 14,
                'state_id' => NULL,
                'name' => 'Alger',
              ),
            14 => 
            array (
                'id' => 15,
                'state_id' => NULL,
                'name' => 'Ain Defla',
              ),
            15 => 
            array (
                'id' => 16,
                'state_id' => NULL,
                'name' => 'Ain Temouchent',
              ),
            16 => 
            array (
                'id' => 17,
                'state_id' => NULL,
                'name' => 'Annaba',
              ),
            17 => 
            array (
                'id' => 18,
                'state_id' => NULL,
                'name' => 'Oran',
              ),
            18 => 
            array (
                'id' => 19,
                'state_id' => NULL,
                'name' => 'Batna',
              ),
            19 => 
            array (
                'id' => 20,
                'state_id' => NULL,
                'name' => 'Bejaia',
              ),
            20 => 
            array (
                'id' => 21,
                'state_id' => NULL,
                'name' => 'Bechar',
              ),
            21 => 
            array (
                'id' => 22,
                'state_id' => NULL,
                'name' => 'El Bayadh',
              ),
            22 => 
            array (
                'id' => 23,
                'state_id' => NULL,
                'name' => 'Biskra',
              ),
            23 => 
            array (
                'id' => 24,
                'state_id' => NULL,
                'name' => 'Bordj Bou Arreridj',
              ),
            24 => 
            array (
                'id' => 25,
                'state_id' => NULL,
                'name' => 'Blida',
              ),
            25 => 
            array (
                'id' => 26,
                'state_id' => NULL,
                'name' => 'Boumerdes',
              ),
            26 => 
            array (
                'id' => 27,
                'state_id' => NULL,
                'name' => 'Bouira',
              ),
            27 => 
            array (
                'id' => 28,
                'state_id' => NULL,
                'name' => 'Tipaza',
              ),
            28 => 
            array (
                'id' => 29,
                'state_id' => NULL,
                'name' => 'Tissemsilt',
              ),
            29 => 
            array (
                'id' => 30,
                'state_id' => NULL,
                'name' => 'Ghardaia',
              ),
            30 => 
            array (
                'id' => 31,
                'state_id' => NULL,
                'name' => 'Guelma',
              ),
            31 => 
            array (
                'id' => 32,
                'state_id' => NULL,
                'name' => 'Khenchela',
              ),
            32 => 
            array (
                'id' => 33,
                'state_id' => NULL,
                'name' => 'Relizane',
              ),
            33 => 
            array (
                'id' => 34,
                'state_id' => NULL,
                'name' => 'Jijel',
              ),
            34 => 
            array (
                'id' => 35,
                'state_id' => NULL,
                'name' => 'Djelfa',
              ),
            35 => 
            array (
                'id' => 36,
                'state_id' => NULL,
                'name' => 'Constantine',
              ),
            36 => 
            array (
                'id' => 37,
                'state_id' => NULL,
                'name' => 'Laghouat',
              ),
            37 => 
            array (
                'id' => 38,
                'state_id' => NULL,
                'name' => 'Mascara',
              ),
            38 => 
            array (
                'id' => 39,
                'state_id' => NULL,
                'name' => 'Medea',
              ),
            39 => 
            array (
                'id' => 40,
                'state_id' => NULL,
                'name' => 'Mila',
              ),
            40 => 
            array (
                'id' => 41,
                'state_id' => NULL,
                'name' => 'Mostaganem',
              ),
            41 => 
            array (
                'id' => 42,
                'state_id' => NULL,
                'name' => 'Msila',
              ),
            42 => 
            array (
                'id' => 43,
                'state_id' => NULL,
                'name' => 'Naama',
              ),
            43 => 
            array (
                'id' => 44,
                'state_id' => NULL,
                'name' => 'Setif',
              ),
            44 => 
            array (
                'id' => 45,
                'state_id' => NULL,
                'name' => 'Saida',
              ),
            45 => 
            array (
                'id' => 46,
                'state_id' => NULL,
                'name' => 'Skikda',
              ),
            46 => 
            array (
                'id' => 47,
                'state_id' => NULL,
                'name' => 'Souk Ahras',
              ),
            47 => 
            array (
                'id' => 48,
                'state_id' => NULL,
                'name' => 'El Tarf',
              ),
            48 => 
            array (
                'id' => 49,
                'state_id' => NULL,
                'name' => 'Tamanghasset',
              ),
            49 => 
            array (
                'id' => 50,
                'state_id' => NULL,
                'name' => 'Tebessa',
              ),
            50 => 
            array (
                'id' => 51,
                'state_id' => NULL,
                'name' => 'Tlemcen',
              ),
            51 => 
            array (
                'id' => 52,
                'state_id' => NULL,
                'name' => 'Tizi Ouzou',
              ),
            52 => 
            array (
                'id' => 53,
                'state_id' => NULL,
                'name' => 'Tiaret',
              ),
            53 => 
            array (
                'id' => 54,
                'state_id' => NULL,
                'name' => 'Tindouf',
              ),
            54 => 
            array (
                'id' => 55,
                'state_id' => NULL,
                'name' => 'El Oued',
              ),
            55 => 
            array (
                'id' => 56,
                'state_id' => NULL,
                'name' => 'Ouargla',
              ),
            56 => 
            array (
                'id' => 57,
                'state_id' => NULL,
                'name' => 'Oum el Bouaghi',
              ),
            57 => 
            array (
                'id' => 58,
                'state_id' => NULL,
                'name' => 'Sidi Bel Abbes',
              ),
            58 => 
            array (
                'id' => 59,
                'state_id' => NULL,
                'name' => 'Chlef',
              ),
            59 => 
            array (
                'id' => 60,
                'state_id' => NULL,
                'name' => 'Illizi',
              ),
            60 => 
            array (
                'id' => 61,
                'state_id' => NULL,
                'name' => 'Herat',
              ),
            61 => 
            array (
                'id' => 62,
                'state_id' => NULL,
                'name' => 'Kabul',
              ),
            62 => 
            array (
                'id' => 63,
                'state_id' => NULL,
                'name' => 'Kandahar',
              ),
            63 => 
            array (
                'id' => 64,
                'state_id' => NULL,
                'name' => 'Mazar-i Sharif',
              ),
            64 => 
            array (
                'id' => 65,
                'state_id' => NULL,
                'name' => 'Parana',
              ),
            65 => 
            array (
                'id' => 66,
                'state_id' => NULL,
                'name' => 'Viedma',
              ),
            66 => 
            array (
                'id' => 67,
                'state_id' => NULL,
                'name' => 'Posadas',
              ),
            67 => 
            array (
                'id' => 68,
                'state_id' => NULL,
                'name' => 'Bahia Blanca',
              ),
            68 => 
            array (
                'id' => 69,
                'state_id' => NULL,
                'name' => 'Buenos Aires',
              ),
            69 => 
            array (
                'id' => 70,
                'state_id' => NULL,
                'name' => 'Formosa',
              ),
            70 => 
            array (
                'id' => 71,
                'state_id' => NULL,
                'name' => 'Jujuy',
              ),
            71 => 
            array (
                'id' => 72,
                'state_id' => NULL,
                'name' => 'Catamarca',
              ),
            72 => 
            array (
                'id' => 73,
                'state_id' => NULL,
                'name' => 'Cordoba',
              ),
            73 => 
            array (
                'id' => 74,
                'state_id' => NULL,
                'name' => 'Corrientes',
              ),
            74 => 
            array (
                'id' => 75,
                'state_id' => NULL,
                'name' => 'Villa Krause',
              ),
            75 => 
            array (
                'id' => 76,
                'state_id' => NULL,
                'name' => 'Concordia',
              ),
            76 => 
            array (
                'id' => 77,
                'state_id' => NULL,
                'name' => 'La Rioja',
              ),
            77 => 
            array (
                'id' => 78,
                'state_id' => NULL,
                'name' => 'La Plata',
              ),
            78 => 
            array (
                'id' => 79,
                'state_id' => NULL,
                'name' => 'Resistencia',
              ),
            79 => 
            array (
                'id' => 80,
                'state_id' => NULL,
                'name' => 'Rio Gallegos',
              ),
            80 => 
            array (
                'id' => 81,
                'state_id' => NULL,
                'name' => 'Rio Cuarto',
              ),
            81 => 
            array (
                'id' => 82,
                'state_id' => NULL,
                'name' => 'Comodoro Rivadavia',
              ),
            82 => 
            array (
                'id' => 83,
                'state_id' => NULL,
                'name' => 'Rosario',
              ),
            83 => 
            array (
                'id' => 84,
                'state_id' => NULL,
                'name' => 'Rawson',
              ),
            84 => 
            array (
                'id' => 85,
                'state_id' => NULL,
                'name' => 'Mar del Plata',
              ),
            85 => 
            array (
                'id' => 86,
                'state_id' => NULL,
                'name' => 'Mendoza',
              ),
            86 => 
            array (
                'id' => 87,
                'state_id' => NULL,
                'name' => 'Neuquen',
              ),
            87 => 
            array (
                'id' => 88,
                'state_id' => NULL,
                'name' => 'Salta',
              ),
            88 => 
            array (
                'id' => 89,
                'state_id' => NULL,
                'name' => 'Santiago del Estero',
              ),
            89 => 
            array (
                'id' => 90,
                'state_id' => NULL,
                'name' => 'Santa Fe',
              ),
            90 => 
            array (
                'id' => 91,
                'state_id' => NULL,
                'name' => 'San Juan',
              ),
            91 => 
            array (
                'id' => 92,
                'state_id' => NULL,
                'name' => 'San Rafael',
              ),
            92 => 
            array (
                'id' => 93,
                'state_id' => NULL,
                'name' => 'San Luis',
              ),
            93 => 
            array (
                'id' => 94,
                'state_id' => NULL,
                'name' => 'Santa Rosa',
              ),
            94 => 
            array (
                'id' => 95,
                'state_id' => NULL,
                'name' => 'San Miguel de Tucuman',
              ),
            95 => 
            array (
                'id' => 96,
                'state_id' => NULL,
                'name' => 'San Nicolas',
              ),
            96 => 
            array (
                'id' => 97,
                'state_id' => NULL,
                'name' => 'Trelew',
              ),
            97 => 
            array (
                'id' => 98,
                'state_id' => NULL,
                'name' => 'Ushuaia',
              ),
            98 => 
            array (
                'id' => 99,
                'state_id' => NULL,
                'name' => 'Abu Dhabi',
            ),
            99 => 
            array (
                'id' => 100,
                'state_id' => NULL,
                'name' => 'Al l\'Ayn',
            ),
            100 => 
            array (
                'id' => 101,
                'state_id' => NULL,
                'name' => 'Dubai',
            ),
            101 => 
            array (
                'id' => 102,
                'state_id' => NULL,
                'name' => 'Ash Shariqah',
            ),
            102 => 
            array (
                'id' => 103,
                'state_id' => NULL,
                'name' => 'Al-Batinah',
            ),
            103 => 
            array (
                'id' => 104,
                'state_id' => NULL,
                'name' => 'Az-Zahirah',
            ),
            104 => 
            array (
                'id' => 105,
                'state_id' => NULL,
                'name' => 'Ash-Sharqiyah',
            ),
            105 => 
            array (
                'id' => 106,
                'state_id' => NULL,
                'name' => 'Masqat',
            ),
            106 => 
            array (
                'id' => 107,
                'state_id' => NULL,
                'name' => 'Musandam',
            ),
            107 => 
            array (
                'id' => 108,
                'state_id' => NULL,
                'name' => 'Ad-Dakhiliyah',
            ),
            108 => 
            array (
                'id' => 109,
                'state_id' => NULL,
                'name' => 'Al-Wusta',
            ),
            109 => 
            array (
                'id' => 110,
                'state_id' => NULL,
                'name' => 'Zufar',
            ),
            110 => 
            array (
                'id' => 111,
                'state_id' => NULL,
                'name' => 'Abseron',
              ),
            111 => 
            array (
                'id' => 112,
                'state_id' => NULL,
                'name' => 'Xacmaz',
              ),
            112 => 
            array (
                'id' => 113,
                'state_id' => NULL,
                'name' => 'Kalbacar',
              ),
            113 => 
            array (
                'id' => 114,
                'state_id' => NULL,
                'name' => 'Qazax',
              ),
            114 => 
            array (
                'id' => 115,
                'state_id' => NULL,
                'name' => 'Lankaran',
              ),
            115 => 
            array (
                'id' => 116,
                'state_id' => NULL,
                'name' => 'Mil-Qarabax',
              ),
            116 => 
            array (
                'id' => 117,
                'state_id' => NULL,
                'name' => 'Mugan-Salyan',
              ),
            117 => 
            array (
                'id' => 118,
                'state_id' => NULL,
                'name' => 'Nagorni-Qarabax',
              ),
            118 => 
            array (
                'id' => 119,
                'state_id' => NULL,
                'name' => 'Naxcivan',
            ),
            119 => 
            array (
                'id' => 120,
                'state_id' => NULL,
                'name' => 'Priaraks',
              ),
            120 => 
            array (
                'id' => 121,
                'state_id' => NULL,
                'name' => 'Saki',
            ),
            121 => 
            array (
                'id' => 122,
                'state_id' => NULL,
                'name' => 'Sumqayit',
              ),
            122 => 
            array (
                'id' => 123,
                'state_id' => NULL,
                'name' => 'Sirvan',
              ),
            123 => 
            array (
                'id' => 124,
                'state_id' => NULL,
                'name' => 'Ganca',
            ),
            124 => 
            array (
                'id' => 125,
                'state_id' => NULL,
                'name' => 'Aswan',
              ),
            125 => 
            array (
                'id' => 126,
                'state_id' => NULL,
                'name' => 'Al Ghurdaqah',
              ),
            126 => 
            array (
                'id' => 127,
                'state_id' => NULL,
                'name' => 'Cairo',
              ),
            127 => 
            array (
                'id' => 128,
                'state_id' => NULL,
                'name' => 'Shubra al Khaymah',
              ),
            128 => 
            array (
                'id' => 129,
                'state_id' => NULL,
                'name' => 'Alexandria',
              ),
            129 => 
            array (
                'id' => 130,
                'state_id' => NULL,
                'name' => 'Afar',
            ),
            130 => 
            array (
                'id' => 131,
                'state_id' => NULL,
                'name' => 'Amara',
            ),
            131 => 
            array (
                'id' => 132,
                'state_id' => NULL,
                'name' => 'Oromiya',
            ),
            132 => 
            array (
                'id' => 133,
                'state_id' => NULL,
                'name' => 'Binshangul Gumuz',
            ),
            133 => 
            array (
                'id' => 134,
                'state_id' => NULL,
                'name' => 'Dire Dawa',
            ),
            134 => 
            array (
                'id' => 135,
                'state_id' => NULL,
                'name' => 'Gambela Hizboch',
            ),
            135 => 
            array (
                'id' => 136,
                'state_id' => NULL,
                'name' => 'Hareri  Hizb',
            ),
            136 => 
            array (
                'id' => 137,
                'state_id' => NULL,
                'name' => 'YeDebub Biheroch',
            ),
            137 => 
            array (
                'id' => 138,
                'state_id' => NULL,
                'name' => 'Sumale',
            ),
            138 => 
            array (
                'id' => 139,
                'state_id' => NULL,
                'name' => 'Tigray',
            ),
            139 => 
            array (
                'id' => 140,
                'state_id' => NULL,
                'name' => 'Adis abeba',
            ),
            140 => 
            array (
                'id' => 141,
                'state_id' => NULL,
                'name' => 'Offaly',
            ),
            141 => 
            array (
                'id' => 142,
                'state_id' => NULL,
                'name' => 'Tipperary',
            ),
            142 => 
            array (
                'id' => 143,
                'state_id' => NULL,
                'name' => 'Dublin',
            ),
            143 => 
            array (
                'id' => 144,
                'state_id' => NULL,
                'name' => 'Donegal',
            ),
            144 => 
            array (
                'id' => 145,
                'state_id' => NULL,
                'name' => 'Galway',
            ),
            145 => 
            array (
                'id' => 146,
                'state_id' => NULL,
                'name' => 'Kildare',
            ),
            146 => 
            array (
                'id' => 147,
                'state_id' => NULL,
                'name' => 'Kilkenny',
            ),
            147 => 
            array (
                'id' => 148,
                'state_id' => NULL,
                'name' => 'Cavan',
            ),
            148 => 
            array (
                'id' => 149,
                'state_id' => NULL,
                'name' => 'Carlow',
            ),
            149 => 
            array (
                'id' => 150,
                'state_id' => NULL,
                'name' => 'Kerry',
            ),
            150 => 
            array (
                'id' => 151,
                'state_id' => NULL,
                'name' => 'Cork',
            ),
            151 => 
            array (
                'id' => 152,
                'state_id' => NULL,
                'name' => 'Clare',
            ),
            152 => 
            array (
                'id' => 153,
                'state_id' => NULL,
                'name' => 'Longford',
            ),
            153 => 
            array (
                'id' => 154,
                'state_id' => NULL,
                'name' => 'Louth',
            ),
            154 => 
            array (
                'id' => 155,
                'state_id' => NULL,
                'name' => 'Laois',
            ),
            155 => 
            array (
                'id' => 156,
                'state_id' => NULL,
                'name' => 'Limerick',
            ),
            156 => 
            array (
                'id' => 157,
                'state_id' => NULL,
                'name' => 'Leitrim',
            ),
            157 => 
            array (
                'id' => 158,
                'state_id' => NULL,
                'name' => 'Roscommon',
            ),
            158 => 
            array (
                'id' => 159,
                'state_id' => NULL,
                'name' => 'Mayo',
            ),
            159 => 
            array (
                'id' => 160,
                'state_id' => NULL,
                'name' => 'Meath',
            ),
            160 => 
            array (
                'id' => 161,
                'state_id' => NULL,
                'name' => 'Monaghan',
            ),
            161 => 
            array (
                'id' => 162,
                'state_id' => NULL,
                'name' => 'Sligo',
            ),
            162 => 
            array (
                'id' => 163,
                'state_id' => NULL,
                'name' => 'Wicklow',
            ),
            163 => 
            array (
                'id' => 164,
                'state_id' => NULL,
                'name' => 'Wexford',
            ),
            164 => 
            array (
                'id' => 165,
                'state_id' => NULL,
                'name' => 'Waterford',
            ),
            165 => 
            array (
                'id' => 166,
                'state_id' => NULL,
                'name' => 'Westmeath',
            ),
            166 => 
            array (
                'id' => 167,
                'state_id' => NULL,
                'name' => 'Polva',
            ),
            167 => 
            array (
                'id' => 168,
                'state_id' => NULL,
                'name' => 'Harju',
            ),
            168 => 
            array (
                'id' => 169,
                'state_id' => NULL,
                'name' => 'Rapla',
            ),
            169 => 
            array (
                'id' => 170,
                'state_id' => NULL,
                'name' => 'Laane',
            ),
            170 => 
            array (
                'id' => 171,
                'state_id' => NULL,
                'name' => 'Parnu',
            ),
            171 => 
            array (
                'id' => 172,
                'state_id' => NULL,
                'name' => 'Saare',
            ),
            172 => 
            array (
                'id' => 173,
                'state_id' => NULL,
                'name' => 'Tartu',
            ),
            173 => 
            array (
                'id' => 174,
                'state_id' => NULL,
                'name' => 'Valga',
            ),
            174 => 
            array (
                'id' => 175,
                'state_id' => NULL,
                'name' => 'Viljandi',
            ),
            175 => 
            array (
                'id' => 176,
                'state_id' => NULL,
                'name' => 'Laane-Viru',
            ),
            176 => 
            array (
                'id' => 177,
                'state_id' => NULL,
                'name' => 'Voru',
            ),
            177 => 
            array (
                'id' => 178,
                'state_id' => NULL,
                'name' => 'Hiiu',
            ),
            178 => 
            array (
                'id' => 179,
                'state_id' => NULL,
                'name' => 'Jarva',
            ),
            179 => 
            array (
                'id' => 180,
                'state_id' => NULL,
                'name' => 'Jogeva',
            ),
            180 => 
            array (
                'id' => 181,
                'state_id' => NULL,
                'name' => 'Ida-Viru',
            ),
            181 => 
            array (
                'id' => 182,
                'state_id' => NULL,
                'name' => 'Andorra la Vella',
            ),
            182 => 
            array (
                'id' => 183,
                'state_id' => NULL,
                'name' => 'Ordino',
            ),
            183 => 
            array (
                'id' => 184,
                'state_id' => NULL,
                'name' => 'Encamp',
            ),
            184 => 
            array (
                'id' => 185,
                'state_id' => NULL,
                'name' => 'Canillo',
            ),
            185 => 
            array (
                'id' => 186,
                'state_id' => NULL,
                'name' => 'Escaldes-Engordany',
            ),
            186 => 
            array (
                'id' => 187,
                'state_id' => NULL,
                'name' => 'La Massana',
            ),
            187 => 
            array (
                'id' => 188,
                'state_id' => NULL,
                'name' => 'Sant Julia de Laria',
            ),
            188 => 
            array (
                'id' => 189,
                'state_id' => NULL,
                'name' => 'Cuanza Norte',
              ),
            189 => 
            array (
                'id' => 190,
                'state_id' => NULL,
                'name' => 'Lunda Norte',
              ),
            190 => 
            array (
                'id' => 191,
                'state_id' => NULL,
                'name' => 'Bengo',
              ),
            191 => 
            array (
                'id' => 192,
                'state_id' => NULL,
                'name' => 'Benguela',
              ),
            192 => 
            array (
                'id' => 193,
                'state_id' => NULL,
                'name' => 'Bie',
              ),
            193 => 
            array (
                'id' => 194,
                'state_id' => NULL,
                'name' => 'Cabinda',
              ),
            194 => 
            array (
                'id' => 195,
                'state_id' => NULL,
                'name' => 'Cunene',
              ),
            195 => 
            array (
                'id' => 196,
                'state_id' => NULL,
                'name' => 'Cuando Cubango',
              ),
            196 => 
            array (
                'id' => 197,
                'state_id' => NULL,
                'name' => 'Luanda',
              ),
            197 => 
            array (
                'id' => 198,
                'state_id' => NULL,
                'name' => 'Malanje',
              ),
            198 => 
            array (
                'id' => 199,
                'state_id' => NULL,
                'name' => 'Moxico',
              ),
            199 => 
            array (
                'id' => 200,
                'state_id' => NULL,
                'name' => 'Namibe',
              ),
            200 => 
            array (
                'id' => 201,
                'state_id' => NULL,
                'name' => 'Cuanza Sul',
              ),
            201 => 
            array (
                'id' => 202,
                'state_id' => NULL,
                'name' => 'Lunda Sul',
              ),
            202 => 
            array (
                'id' => 203,
                'state_id' => NULL,
                'name' => 'Huambo',
              ),
            203 => 
            array (
                'id' => 204,
                'state_id' => NULL,
                'name' => 'Huila',
              ),
            204 => 
            array (
                'id' => 205,
                'state_id' => NULL,
                'name' => 'Uige',
              ),
            205 => 
            array (
                'id' => 206,
                'state_id' => NULL,
                'name' => 'Zaire',
              ),
            206 => 
            array (
                'id' => 207,
                'state_id' => NULL,
                'name' => 'Burgenland',
              ),
            207 => 
            array (
                'id' => 208,
                'state_id' => NULL,
                'name' => 'Tyrol',
              ),
            208 => 
            array (
                'id' => 209,
                'state_id' => NULL,
                'name' => 'Vorarlberg',
              ),
            209 => 
            array (
                'id' => 210,
                'state_id' => NULL,
                'name' => 'Carinthia',
              ),
            210 => 
            array (
                'id' => 211,
                'state_id' => NULL,
                'name' => 'Salzburg',
              ),
            211 => 
            array (
                'id' => 212,
                'state_id' => NULL,
                'name' => 'Upper Austria',
              ),
            212 => 
            array (
                'id' => 213,
                'state_id' => NULL,
                'name' => 'Styria',
              ),
            213 => 
            array (
                'id' => 214,
                'state_id' => NULL,
                'name' => 'Vienna',
              ),
            214 => 
            array (
                'id' => 215,
                'state_id' => NULL,
                'name' => 'Lower Austria',
              ),
            215 => 
            array (
                'id' => 216,
                'state_id' => NULL,
                'name' => 'Northern',
            ),
            216 => 
            array (
                'id' => 217,
                'state_id' => NULL,
                'name' => 'Bougainville',
            ),
            217 => 
            array (
                'id' => 218,
                'state_id' => NULL,
                'name' => 'Eastern Highlands',
            ),
            218 => 
            array (
                'id' => 219,
                'state_id' => NULL,
                'name' => 'East Sepik',
            ),
            219 => 
            array (
                'id' => 220,
                'state_id' => NULL,
                'name' => 'East New Britain',
            ),
            220 => 
            array (
                'id' => 221,
                'state_id' => NULL,
                'name' => 'Enga',
            ),
            221 => 
            array (
                'id' => 222,
                'state_id' => NULL,
                'name' => 'Gulf',
            ),
            222 => 
            array (
                'id' => 223,
                'state_id' => NULL,
                'name' => 'Madang',
            ),
            223 => 
            array (
                'id' => 224,
                'state_id' => NULL,
                'name' => 'Manus',
            ),
            224 => 
            array (
                'id' => 225,
                'state_id' => NULL,
                'name' => 'Milne Bay',
            ),
            225 => 
            array (
                'id' => 226,
                'state_id' => NULL,
                'name' => 'Port Moresby',
            ),
            226 => 
            array (
                'id' => 227,
                'state_id' => NULL,
                'name' => 'Morobe',
            ),
            227 => 
            array (
                'id' => 228,
                'state_id' => NULL,
                'name' => 'Southern Highlands',
            ),
            228 => 
            array (
                'id' => 229,
                'state_id' => NULL,
                'name' => 'Simbu',
            ),
            229 => 
            array (
                'id' => 230,
                'state_id' => NULL,
                'name' => 'Sandaun',
            ),
            230 => 
            array (
                'id' => 231,
                'state_id' => NULL,
                'name' => 'Western',
            ),
            231 => 
            array (
                'id' => 232,
                'state_id' => NULL,
                'name' => 'Western Highlands',
            ),
            232 => 
            array (
                'id' => 233,
                'state_id' => NULL,
                'name' => 'West New Britain',
            ),
            233 => 
            array (
                'id' => 234,
                'state_id' => NULL,
                'name' => 'New Ireland',
            ),
            234 => 
            array (
                'id' => 235,
                'state_id' => NULL,
                'name' => 'Peshawar',
              ),
            235 => 
            array (
                'id' => 236,
                'state_id' => NULL,
                'name' => 'Faisalabad',
              ),
            236 => 
            array (
                'id' => 237,
                'state_id' => NULL,
                'name' => 'Gujranwala',
              ),
            237 => 
            array (
                'id' => 238,
                'state_id' => NULL,
                'name' => 'Hyderabad',
              ),
            238 => 
            array (
                'id' => 239,
                'state_id' => NULL,
                'name' => 'Karachi',
              ),
            239 => 
            array (
                'id' => 240,
                'state_id' => NULL,
                'name' => 'Lahore',
              ),
            240 => 
            array (
                'id' => 241,
                'state_id' => NULL,
                'name' => 'Rawalpindi',
              ),
            241 => 
            array (
                'id' => 242,
                'state_id' => NULL,
                'name' => 'Multan',
              ),
            242 => 
            array (
                'id' => 243,
                'state_id' => NULL,
                'name' => 'Islamabad',
              ),
            243 => 
            array (
                'id' => 244,
                'state_id' => NULL,
                'name' => 'Amambay',
            ),
            244 => 
            array (
                'id' => 245,
                'state_id' => NULL,
                'name' => 'Presidente Hayes',
            ),
            245 => 
            array (
                'id' => 246,
                'state_id' => NULL,
                'name' => 'Paraguari',
            ),
            246 => 
            array (
                'id' => 247,
                'state_id' => NULL,
                'name' => 'Boqueron',
            ),
            247 => 
            array (
                'id' => 248,
                'state_id' => NULL,
                'name' => 'Guaira',
            ),
            248 => 
            array (
                'id' => 249,
                'state_id' => NULL,
                'name' => 'Caaguazu',
            ),
            249 => 
            array (
                'id' => 250,
                'state_id' => NULL,
                'name' => 'Canindeyu',
            ),
            250 => 
            array (
                'id' => 251,
                'state_id' => NULL,
                'name' => 'Caazapa',
            ),
            251 => 
            array (
                'id' => 252,
                'state_id' => NULL,
                'name' => 'Concepcion',
            ),
            252 => 
            array (
                'id' => 253,
                'state_id' => NULL,
                'name' => 'Cordillera',
            ),
            253 => 
            array (
                'id' => 254,
                'state_id' => NULL,
                'name' => 'Misiones',
            ),
            254 => 
            array (
                'id' => 255,
                'state_id' => NULL,
                'name' => 'Neembucu',
            ),
            255 => 
            array (
                'id' => 256,
                'state_id' => NULL,
                'name' => 'Alto Paraguay',
            ),
            256 => 
            array (
                'id' => 257,
                'state_id' => NULL,
                'name' => 'Alto Parana',
            ),
            257 => 
            array (
                'id' => 258,
                'state_id' => NULL,
                'name' => 'San Pedro',
            ),
            258 => 
            array (
                'id' => 259,
                'state_id' => NULL,
                'name' => 'Asuncion',
            ),
            259 => 
            array (
                'id' => 260,
                'state_id' => NULL,
                'name' => 'Itapua',
            ),
            260 => 
            array (
                'id' => 261,
                'state_id' => NULL,
                'name' => 'Central',
            ),
            261 => 
            array (
                'id' => 262,
                'state_id' => NULL,
                'name' => 'Gaza Strip',
            ),
            262 => 
            array (
                'id' => 263,
                'state_id' => NULL,
                'name' => 'West Bank',
            ),
            263 => 
            array (
                'id' => 264,
                'state_id' => NULL,
                'name' => 'Ash-Shamaliyah',
            ),
            264 => 
            array (
                'id' => 265,
                'state_id' => NULL,
                'name' => 'Al-Hadd',
            ),
            265 => 
            array (
                'id' => 266,
                'state_id' => NULL,
                'name' => 'Hammad',
            ),
            266 => 
            array (
                'id' => 267,
                'state_id' => NULL,
                'name' => 'Ar-Rifa',
            ),
            267 => 
            array (
                'id' => 268,
                'state_id' => NULL,
                'name' => 'Al-Manamah',
            ),
            268 => 
            array (
                'id' => 269,
                'state_id' => NULL,
                'name' => 'Al-Muharraq',
            ),
            269 => 
            array (
                'id' => 270,
                'state_id' => NULL,
                'name' => 'Al-Gharbiyah',
            ),
            270 => 
            array (
                'id' => 271,
                'state_id' => NULL,
                'name' => 'Isa',
            ),
            271 => 
            array (
                'id' => 272,
                'state_id' => NULL,
                'name' => 'Al-Wusta',
            ),
            272 => 
            array (
                'id' => 273,
                'state_id' => NULL,
                'name' => 'Acre',
            ),
            273 => 
            array (
                'id' => 274,
                'state_id' => NULL,
                'name' => 'Alagoas',
            ),
            274 => 
            array (
                'id' => 275,
                'state_id' => NULL,
                'name' => 'Amapa',
            ),
            275 => 
            array (
                'id' => 276,
                'state_id' => NULL,
                'name' => 'Parana',
            ),
            276 => 
            array (
                'id' => 277,
                'state_id' => NULL,
                'name' => 'Brasilia',
              ),
            277 => 
            array (
                'id' => 278,
                'state_id' => NULL,
                'name' => 'Bahia',
            ),
            278 => 
            array (
                'id' => 279,
                'state_id' => NULL,
                'name' => 'Rio Grande do Norte',
            ),
            279 => 
            array (
                'id' => 280,
                'state_id' => NULL,
                'name' => 'Pernambuco',
            ),
            280 => 
            array (
                'id' => 281,
                'state_id' => NULL,
                'name' => 'Goias',
            ),
            281 => 
            array (
                'id' => 282,
                'state_id' => NULL,
                'name' => 'Rondonia',
            ),
            282 => 
            array (
                'id' => 283,
                'state_id' => NULL,
                'name' => 'Rio de Janeiro',
            ),
            283 => 
            array (
                'id' => 284,
                'state_id' => NULL,
                'name' => 'Roraima',
            ),
            284 => 
            array (
                'id' => 285,
                'state_id' => NULL,
                'name' => 'Maranhao',
            ),
            285 => 
            array (
                'id' => 286,
                'state_id' => NULL,
                'name' => 'Mato Grosso',
            ),
            286 => 
            array (
                'id' => 287,
                'state_id' => NULL,
                'name' => 'Minas Gerais',
            ),
            287 => 
            array (
                'id' => 288,
                'state_id' => NULL,
                'name' => 'Rio Grande do Sul',
            ),
            288 => 
            array (
                'id' => 289,
                'state_id' => NULL,
                'name' => 'Mato Grosso do Sul',
            ),
            289 => 
            array (
                'id' => 290,
                'state_id' => NULL,
                'name' => 'Para',
            ),
            290 => 
            array (
                'id' => 291,
                'state_id' => NULL,
                'name' => 'Paraiba',
            ),
            291 => 
            array (
                'id' => 292,
                'state_id' => NULL,
                'name' => 'Piaui',
            ),
            292 => 
            array (
                'id' => 293,
                'state_id' => NULL,
                'name' => 'Ceara',
            ),
            293 => 
            array (
                'id' => 294,
                'state_id' => NULL,
                'name' => 'Sergipe',
            ),
            294 => 
            array (
                'id' => 295,
                'state_id' => NULL,
                'name' => 'Espirito Santo',
            ),
            295 => 
            array (
                'id' => 296,
                'state_id' => NULL,
                'name' => 'Sao Paulo',
            ),
            296 => 
            array (
                'id' => 297,
                'state_id' => NULL,
                'name' => 'Santa Catarina',
            ),
            297 => 
            array (
                'id' => 298,
                'state_id' => NULL,
                'name' => 'Tocantins',
            ),
            298 => 
            array (
                'id' => 299,
                'state_id' => NULL,
                'name' => 'Amazonas',
            ),
            299 => 
            array (
                'id' => 300,
                'state_id' => NULL,
                'name' => 'Bresckaja',
            ),
            300 => 
            array (
                'id' => 301,
                'state_id' => NULL,
                'name' => 'Homelskaja',
            ),
            301 => 
            array (
                'id' => 302,
                'state_id' => NULL,
                'name' => 'Hrodzenskaja',
            ),
            302 => 
            array (
                'id' => 303,
                'state_id' => NULL,
                'name' => 'Minsk',
            ),
            303 => 
            array (
                'id' => 304,
                'state_id' => NULL,
                'name' => 'Mahileuskaja',
            ),
            304 => 
            array (
                'id' => 305,
                'state_id' => NULL,
                'name' => 'Vicebskaja',
            ),
            305 => 
            array (
                'id' => 306,
                'state_id' => NULL,
                'name' => 'Burgas',
              ),
            306 => 
            array (
                'id' => 307,
                'state_id' => NULL,
                'name' => 'Khaskovo',
              ),
            307 => 
            array (
                'id' => 308,
                'state_id' => NULL,
                'name' => 'Ruse',
              ),
            308 => 
            array (
                'id' => 309,
                'state_id' => NULL,
                'name' => 'Lovech',
              ),
            309 => 
            array (
                'id' => 310,
                'state_id' => NULL,
                'name' => 'Montana',
              ),
            310 => 
            array (
                'id' => 311,
                'state_id' => NULL,
                'name' => 'Plovdiv',
              ),
            311 => 
            array (
                'id' => 312,
                'state_id' => NULL,
                'name' => 'Sofiya',
              ),
            312 => 
            array (
                'id' => 313,
                'state_id' => NULL,
                'name' => 'Grad Sofiya',
              ),
            313 => 
            array (
                'id' => 314,
                'state_id' => NULL,
                'name' => 'Varna',
              ),
            314 => 
            array (
                'id' => 315,
                'state_id' => NULL,
                'name' => 'Alibori',
            ),
            315 => 
            array (
                'id' => 316,
                'state_id' => NULL,
                'name' => 'Atakora',
            ),
            316 => 
            array (
                'id' => 317,
                'state_id' => NULL,
                'name' => 'Littoral',
            ),
            317 => 
            array (
                'id' => 318,
                'state_id' => NULL,
                'name' => 'Bohicon',
              ),
            318 => 
            array (
                'id' => 319,
                'state_id' => NULL,
                'name' => 'Borgou',
            ),
            319 => 
            array (
                'id' => 320,
                'state_id' => NULL,
                'name' => 'Atlantique',
            ),
            320 => 
            array (
                'id' => 321,
                'state_id' => NULL,
                'name' => 'Plateau',
            ),
            321 => 
            array (
                'id' => 322,
                'state_id' => NULL,
                'name' => 'Kouffo',
            ),
            322 => 
            array (
                'id' => 323,
                'state_id' => NULL,
                'name' => 'Mono',
            ),
            323 => 
            array (
                'id' => 324,
                'state_id' => NULL,
                'name' => 'Collines',
            ),
            324 => 
            array (
                'id' => 325,
                'state_id' => NULL,
                'name' => 'Oueme',
            ),
            325 => 
            array (
                'id' => 326,
                'state_id' => NULL,
                'name' => 'Donga',
            ),
            326 => 
            array (
                'id' => 327,
                'state_id' => NULL,
                'name' => 'Zou',
            ),
            327 => 
            array (
                'id' => 328,
                'state_id' => NULL,
                'name' => 'Hainaut',
              ),
            328 => 
            array (
                'id' => 329,
                'state_id' => NULL,
                'name' => 'Antwerpen',
              ),
            329 => 
            array (
                'id' => 330,
                'state_id' => NULL,
                'name' => 'Brabant-Wallone',
              ),
            330 => 
            array (
                'id' => 331,
                'state_id' => NULL,
                'name' => 'Brussels',
              ),
            331 => 
            array (
                'id' => 332,
                'state_id' => NULL,
                'name' => 'Oost-Vlaanderen',
              ),
            332 => 
            array (
                'id' => 333,
                'state_id' => NULL,
                'name' => 'Vlaams-Brabant',
              ),
            333 => 
            array (
                'id' => 334,
                'state_id' => NULL,
                'name' => 'Liege',
              ),
            334 => 
            array (
                'id' => 335,
                'state_id' => NULL,
                'name' => 'Limburg',
              ),
            335 => 
            array (
                'id' => 336,
                'state_id' => NULL,
                'name' => 'Luxembourg',
              ),
            336 => 
            array (
                'id' => 337,
                'state_id' => NULL,
                'name' => 'Namur',
              ),
            337 => 
            array (
                'id' => 338,
                'state_id' => NULL,
                'name' => 'West-Vlaanderen',
              ),
            338 => 
            array (
                'id' => 339,
                'state_id' => NULL,
                'name' => 'Elbląg',
              ),
            339 => 
            array (
                'id' => 340,
                'state_id' => NULL,
                'name' => 'Olsztyn',
              ),
            340 => 
            array (
                'id' => 341,
                'state_id' => NULL,
                'name' => 'Ostrołeka',
              ),
            341 => 
            array (
                'id' => 342,
                'state_id' => NULL,
                'name' => 'Bydgoszcz',
              ),
            342 => 
            array (
                'id' => 343,
                'state_id' => NULL,
                'name' => 'Piotrkow',
              ),
            343 => 
            array (
                'id' => 344,
                'state_id' => NULL,
                'name' => 'Bytom',
              ),
            344 => 
            array (
                'id' => 345,
                'state_id' => NULL,
                'name' => 'Biała Podlaska',
              ),
            345 => 
            array (
                'id' => 346,
                'state_id' => NULL,
                'name' => 'Białystok',
              ),
            346 => 
            array (
                'id' => 347,
                'state_id' => NULL,
                'name' => 'Opole',
              ),
            347 => 
            array (
                'id' => 348,
                'state_id' => NULL,
                'name' => 'Poznan',
              ),
            348 => 
            array (
                'id' => 349,
                'state_id' => NULL,
                'name' => 'Dabrowa Gornicza',
              ),
            349 => 
            array (
                'id' => 350,
                'state_id' => NULL,
                'name' => 'Gorzow Wlkp',
              ),
            350 => 
            array (
                'id' => 351,
                'state_id' => NULL,
                'name' => 'Wroclaw',
              ),
            351 => 
            array (
                'id' => 352,
                'state_id' => NULL,
                'name' => 'Wlocławek',
              ),
            352 => 
            array (
                'id' => 353,
                'state_id' => NULL,
                'name' => 'Gdansk',
              ),
            353 => 
            array (
                'id' => 354,
                'state_id' => NULL,
                'name' => 'Gdynia',
              ),
            354 => 
            array (
                'id' => 355,
                'state_id' => NULL,
                'name' => 'Gliwice',
              ),
            355 => 
            array (
                'id' => 356,
                'state_id' => NULL,
                'name' => 'Grudziadz',
              ),
            356 => 
            array (
                'id' => 357,
                'state_id' => NULL,
                'name' => 'Chełm',
              ),
            357 => 
            array (
                'id' => 358,
                'state_id' => NULL,
                'name' => 'Warszawa',
              ),
            358 => 
            array (
                'id' => 359,
                'state_id' => NULL,
                'name' => 'Chorzow',
              ),
            359 => 
            array (
                'id' => 360,
                'state_id' => NULL,
                'name' => 'Kalisz',
              ),
            360 => 
            array (
                'id' => 361,
                'state_id' => NULL,
                'name' => 'Katowice',
              ),
            361 => 
            array (
                'id' => 362,
                'state_id' => NULL,
                'name' => 'Kielce',
              ),
            362 => 
            array (
                'id' => 363,
                'state_id' => NULL,
                'name' => 'Konin',
              ),
            363 => 
            array (
                'id' => 364,
                'state_id' => NULL,
                'name' => 'Koszalin',
              ),
            364 => 
            array (
                'id' => 365,
                'state_id' => NULL,
                'name' => 'Krakow',
              ),
            365 => 
            array (
                'id' => 366,
                'state_id' => NULL,
                'name' => 'Krosno',
              ),
            366 => 
            array (
                'id' => 367,
                'state_id' => NULL,
                'name' => 'Radom',
              ),
            367 => 
            array (
                'id' => 368,
                'state_id' => NULL,
                'name' => 'Legnica',
              ),
            368 => 
            array (
                'id' => 369,
                'state_id' => NULL,
                'name' => 'Leszno',
              ),
            369 => 
            array (
                'id' => 370,
                'state_id' => NULL,
                'name' => 'Lublin',
              ),
            370 => 
            array (
                'id' => 371,
                'state_id' => NULL,
                'name' => 'Ruda Sl',
              ),
            371 => 
            array (
                'id' => 372,
                'state_id' => NULL,
                'name' => 'Lodz',
              ),
            372 => 
            array (
                'id' => 373,
                'state_id' => NULL,
                'name' => 'Zielona Gora',
              ),
            373 => 
            array (
                'id' => 374,
                'state_id' => NULL,
                'name' => 'Mysłowice',
              ),
            374 => 
            array (
                'id' => 375,
                'state_id' => NULL,
                'name' => 'Piła',
              ),
            375 => 
            array (
                'id' => 376,
                'state_id' => NULL,
                'name' => 'Przemysl',
              ),
            376 => 
            array (
                'id' => 377,
                'state_id' => NULL,
                'name' => 'Plock',
              ),
            377 => 
            array (
                'id' => 378,
                'state_id' => NULL,
                'name' => 'Ciechanow',
              ),
            378 => 
            array (
                'id' => 379,
                'state_id' => NULL,
                'name' => 'Rzeszow',
              ),
            379 => 
            array (
                'id' => 380,
                'state_id' => NULL,
                'name' => 'Szczecin',
              ),
            380 => 
            array (
                'id' => 381,
                'state_id' => NULL,
                'name' => 'Skierniewice',
              ),
            381 => 
            array (
                'id' => 382,
                'state_id' => NULL,
                'name' => 'Slupsk',
              ),
            382 => 
            array (
                'id' => 383,
                'state_id' => NULL,
                'name' => 'Suwałki',
              ),
            383 => 
            array (
                'id' => 384,
                'state_id' => NULL,
                'name' => 'Sopot',
              ),
            384 => 
            array (
                'id' => 385,
                'state_id' => NULL,
                'name' => 'Sosnowiec',
              ),
            385 => 
            array (
                'id' => 386,
                'state_id' => NULL,
                'name' => 'Tarnow',
              ),
            386 => 
            array (
                'id' => 387,
                'state_id' => NULL,
                'name' => 'Tarnobrzeg',
              ),
            387 => 
            array (
                'id' => 388,
                'state_id' => NULL,
                'name' => 'Tychy',
              ),
            388 => 
            array (
                'id' => 389,
                'state_id' => NULL,
                'name' => 'Torun',
              ),
            389 => 
            array (
                'id' => 390,
                'state_id' => NULL,
                'name' => 'Walbrzych',
              ),
            390 => 
            array (
                'id' => 391,
                'state_id' => NULL,
                'name' => 'Lomza',
              ),
            391 => 
            array (
                'id' => 392,
                'state_id' => NULL,
                'name' => 'Siemianowice Sl',
              ),
            392 => 
            array (
                'id' => 393,
                'state_id' => NULL,
                'name' => 'Swinoujscie',
              ),
            393 => 
            array (
                'id' => 394,
                'state_id' => NULL,
                'name' => 'Swietochłowice',
              ),
            394 => 
            array (
                'id' => 395,
                'state_id' => NULL,
                'name' => 'Siedlce',
              ),
            395 => 
            array (
                'id' => 396,
                'state_id' => NULL,
                'name' => 'Sieradz',
              ),
            396 => 
            array (
                'id' => 397,
                'state_id' => NULL,
                'name' => 'Nowy Sacz',
              ),
            397 => 
            array (
                'id' => 398,
                'state_id' => NULL,
                'name' => 'Jaworzno',
              ),
            398 => 
            array (
                'id' => 399,
                'state_id' => NULL,
                'name' => 'Jelenia Gora',
              ),
            399 => 
            array (
                'id' => 400,
                'state_id' => NULL,
                'name' => 'Zabrze',
              ),
            400 => 
            array (
                'id' => 401,
                'state_id' => NULL,
                'name' => 'Zamosc',
              ),
            401 => 
            array (
                'id' => 402,
                'state_id' => NULL,
                'name' => 'El Alto',
              ),
            402 => 
            array (
                'id' => 403,
                'state_id' => NULL,
                'name' => 'Oruro',
              ),
            403 => 
            array (
                'id' => 404,
                'state_id' => NULL,
                'name' => 'El Beni',
              ),
            404 => 
            array (
                'id' => 405,
                'state_id' => NULL,
                'name' => 'Potosi',
              ),
            405 => 
            array (
                'id' => 406,
                'state_id' => NULL,
                'name' => 'Quillacollo',
              ),
            406 => 
            array (
                'id' => 407,
                'state_id' => NULL,
                'name' => 'Cochabamba',
              ),
            407 => 
            array (
                'id' => 408,
                'state_id' => NULL,
                'name' => 'La Paz',
              ),
            408 => 
            array (
                'id' => 409,
                'state_id' => NULL,
                'name' => 'Pando',
              ),
            409 => 
            array (
                'id' => 410,
                'state_id' => NULL,
                'name' => 'Chuquisaca',
              ),
            410 => 
            array (
                'id' => 411,
                'state_id' => NULL,
                'name' => 'Sacaba',
              ),
            411 => 
            array (
                'id' => 412,
                'state_id' => NULL,
                'name' => 'Santa Cruz',
              ),
            412 => 
            array (
                'id' => 413,
                'state_id' => NULL,
                'name' => 'Tarija',
              ),
            413 => 
            array (
                'id' => 414,
                'state_id' => NULL,
                'name' => 'Posavski',
              ),
            414 => 
            array (
                'id' => 415,
                'state_id' => NULL,
                'name' => 'Bosansko-Podrinjski',
              ),
            415 => 
            array (
                'id' => 416,
                'state_id' => NULL,
                'name' => 'Tomislavgrad',
              ),
            416 => 
            array (
                'id' => 417,
                'state_id' => NULL,
                'name' => 'Hercegovacko-Neretvanski',
              ),
            417 => 
            array (
                'id' => 418,
                'state_id' => NULL,
                'name' => 'Sarajevo',
              ),
            418 => 
            array (
                'id' => 419,
                'state_id' => NULL,
                'name' => 'Tuzlanski-Podrinjski',
              ),
            419 => 
            array (
                'id' => 420,
                'state_id' => NULL,
                'name' => 'Unsko-Sanski',
              ),
            420 => 
            array (
                'id' => 421,
                'state_id' => NULL,
                'name' => 'Hercegovacko-Bosanski',
              ),
            421 => 
            array (
                'id' => 422,
                'state_id' => NULL,
                'name' => 'Zapadno-Hercegovaki',
              ),
            422 => 
            array (
                'id' => 423,
                'state_id' => NULL,
                'name' => 'Zenicko-Dobojski',
              ),
            423 => 
            array (
                'id' => 424,
                'state_id' => NULL,
                'name' => 'Srednjo-Bosanski',
              ),
            424 => 
            array (
                'id' => 425,
                'state_id' => NULL,
                'name' => 'Belize',
            ),
            425 => 
            array (
                'id' => 426,
                'state_id' => NULL,
                'name' => 'Orange Walk',
            ),
            426 => 
            array (
                'id' => 427,
                'state_id' => NULL,
                'name' => 'Cayo',
            ),
            427 => 
            array (
                'id' => 428,
                'state_id' => NULL,
                'name' => 'Corozal',
            ),
            428 => 
            array (
                'id' => 429,
                'state_id' => NULL,
                'name' => 'Stann Creek',
            ),
            429 => 
            array (
                'id' => 430,
                'state_id' => NULL,
                'name' => 'Toledo',
            ),
            430 => 
            array (
                'id' => 431,
                'state_id' => NULL,
                'name' => 'Bale',
              ),
            431 => 
            array (
                'id' => 432,
                'state_id' => NULL,
                'name' => 'Bam',
              ),
            432 => 
            array (
                'id' => 433,
                'state_id' => NULL,
                'name' => 'Banwa',
              ),
            433 => 
            array (
                'id' => 434,
                'state_id' => NULL,
                'name' => 'Bazega',
              ),
            434 => 
            array (
                'id' => 435,
                'state_id' => NULL,
                'name' => 'Poni',
              ),
            435 => 
            array (
                'id' => 436,
                'state_id' => NULL,
                'name' => 'Boulgou',
              ),
            436 => 
            array (
                'id' => 437,
                'state_id' => NULL,
                'name' => 'Boulkiemde',
              ),
            437 => 
            array (
                'id' => 438,
                'state_id' => NULL,
                'name' => 'Bougouriba',
              ),
            438 => 
            array (
                'id' => 439,
                'state_id' => NULL,
                'name' => 'Ganzourgou',
              ),
            439 => 
            array (
                'id' => 440,
                'state_id' => NULL,
                'name' => 'Gourma',
              ),
            440 => 
            array (
                'id' => 441,
                'state_id' => NULL,
                'name' => 'Ziro',
              ),
            441 => 
            array (
                'id' => 442,
                'state_id' => NULL,
                'name' => 'Kadiogo',
              ),
            442 => 
            array (
                'id' => 443,
                'state_id' => NULL,
                'name' => 'Kenedougou',
              ),
            443 => 
            array (
                'id' => 444,
                'state_id' => NULL,
                'name' => 'Komondjari',
              ),
            444 => 
            array (
                'id' => 445,
                'state_id' => NULL,
                'name' => 'Comoe',
              ),
            445 => 
            array (
                'id' => 446,
                'state_id' => NULL,
                'name' => 'Kompienga',
              ),
            446 => 
            array (
                'id' => 447,
                'state_id' => NULL,
                'name' => 'Kossi',
              ),
            447 => 
            array (
                'id' => 448,
                'state_id' => NULL,
                'name' => 'Koulpelogo',
              ),
            448 => 
            array (
                'id' => 449,
                'state_id' => NULL,
                'name' => 'Kourweogo',
              ),
            449 => 
            array (
                'id' => 450,
                'state_id' => NULL,
                'name' => 'Kouritenga',
              ),
            450 => 
            array (
                'id' => 451,
                'state_id' => NULL,
                'name' => 'Leraba',
              ),
            451 => 
            array (
                'id' => 452,
                'state_id' => NULL,
                'name' => 'Loroum',
              ),
            452 => 
            array (
                'id' => 453,
                'state_id' => NULL,
                'name' => 'Mouhoun',
              ),
            453 => 
            array (
                'id' => 454,
                'state_id' => NULL,
                'name' => 'Namentenga',
              ),
            454 => 
            array (
                'id' => 455,
                'state_id' => NULL,
                'name' => 'Nahouri',
              ),
            455 => 
            array (
                'id' => 456,
                'state_id' => NULL,
                'name' => 'Nayala',
              ),
            456 => 
            array (
                'id' => 457,
                'state_id' => NULL,
                'name' => 'Gnagna',
              ),
            457 => 
            array (
                'id' => 458,
                'state_id' => NULL,
                'name' => 'Noumbiel',
              ),
            458 => 
            array (
                'id' => 459,
                'state_id' => NULL,
                'name' => 'Passore',
              ),
            459 => 
            array (
                'id' => 460,
                'state_id' => NULL,
                'name' => 'Seno',
              ),
            460 => 
            array (
                'id' => 461,
                'state_id' => NULL,
                'name' => 'Sanguie',
              ),
            461 => 
            array (
                'id' => 462,
                'state_id' => NULL,
                'name' => 'Sanmatenga',
              ),
            462 => 
            array (
                'id' => 463,
                'state_id' => NULL,
                'name' => 'Sourou',
              ),
            463 => 
            array (
                'id' => 464,
                'state_id' => NULL,
                'name' => 'Soum',
              ),
            464 => 
            array (
                'id' => 465,
                'state_id' => NULL,
                'name' => 'Tapoa',
              ),
            465 => 
            array (
                'id' => 466,
                'state_id' => NULL,
                'name' => 'Tuy',
              ),
            466 => 
            array (
                'id' => 467,
                'state_id' => NULL,
                'name' => 'Houet',
              ),
            467 => 
            array (
                'id' => 468,
                'state_id' => NULL,
                'name' => 'Oubritenga',
              ),
            468 => 
            array (
                'id' => 469,
                'state_id' => NULL,
                'name' => 'Oudalan',
              ),
            469 => 
            array (
                'id' => 470,
                'state_id' => NULL,
                'name' => 'Sissili',
              ),
            470 => 
            array (
                'id' => 471,
                'state_id' => NULL,
                'name' => 'Yagha',
              ),
            471 => 
            array (
                'id' => 472,
                'state_id' => NULL,
                'name' => 'Yatenga',
              ),
            472 => 
            array (
                'id' => 473,
                'state_id' => NULL,
                'name' => 'Ioba',
              ),
            473 => 
            array (
                'id' => 474,
                'state_id' => NULL,
                'name' => 'Zoundweogo',
              ),
            474 => 
            array (
                'id' => 475,
                'state_id' => NULL,
                'name' => 'Zondoma',
              ),
            475 => 
            array (
                'id' => 476,
                'state_id' => NULL,
                'name' => 'Bubanza',
            ),
            476 => 
            array (
                'id' => 477,
                'state_id' => NULL,
                'name' => 'Bururi',
            ),
            477 => 
            array (
                'id' => 478,
                'state_id' => NULL,
                'name' => 'Bujumbura Mairie',
            ),
            478 => 
            array (
                'id' => 479,
                'state_id' => NULL,
                'name' => 'Bujumbura Rural',
            ),
            479 => 
            array (
                'id' => 480,
                'state_id' => NULL,
                'name' => 'Ngozi',
            ),
            480 => 
            array (
                'id' => 481,
                'state_id' => NULL,
                'name' => 'Kirundo',
            ),
            481 => 
            array (
                'id' => 482,
                'state_id' => NULL,
                'name' => 'Gitega',
            ),
            482 => 
            array (
                'id' => 483,
                'state_id' => NULL,
                'name' => 'Karuzi',
            ),
            483 => 
            array (
                'id' => 484,
                'state_id' => NULL,
                'name' => 'Kayanza',
            ),
            484 => 
            array (
                'id' => 485,
                'state_id' => NULL,
                'name' => 'Cankuzo',
            ),
            485 => 
            array (
                'id' => 486,
                'state_id' => NULL,
                'name' => 'Rutana',
            ),
            486 => 
            array (
                'id' => 487,
                'state_id' => NULL,
                'name' => 'Ruyigi',
            ),
            487 => 
            array (
                'id' => 488,
                'state_id' => NULL,
                'name' => 'Makamba',
            ),
            488 => 
            array (
                'id' => 489,
                'state_id' => NULL,
                'name' => 'Muramvya',
            ),
            489 => 
            array (
                'id' => 490,
                'state_id' => NULL,
                'name' => 'Mwaro',
            ),
            490 => 
            array (
                'id' => 491,
                'state_id' => NULL,
                'name' => 'Muyinga',
            ),
            491 => 
            array (
                'id' => 492,
                'state_id' => NULL,
                'name' => 'Cibitoke',
            ),
            492 => 
            array (
                'id' => 493,
                'state_id' => NULL,
                'name' => 'Haeju',
              ),
            493 => 
            array (
                'id' => 494,
                'state_id' => NULL,
                'name' => 'Hyesan',
              ),
            494 => 
            array (
                'id' => 495,
                'state_id' => NULL,
                'name' => 'Kanggye',
              ),
            495 => 
            array (
                'id' => 496,
                'state_id' => NULL,
                'name' => 'Kaesong',
              ),
            496 => 
            array (
                'id' => 497,
                'state_id' => NULL,
                'name' => 'Naseon',
              ),
            497 => 
            array (
                'id' => 498,
                'state_id' => NULL,
                'name' => 'Namp\'o',
              ),
            498 => 
            array (
                'id' => 499,
                'state_id' => NULL,
                'name' => 'Pyongyang',
              ),
            499 => 
            array (
                'id' => 500,
                'state_id' => NULL,
                'name' => 'Ch\'ongjin',
              ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 501,
                'state_id' => NULL,
                'name' => 'Sariwon',
              ),
            1 => 
            array (
                'id' => 502,
                'state_id' => NULL,
                'name' => 'Hamhung',
              ),
            2 => 
            array (
                'id' => 503,
                'state_id' => NULL,
                'name' => 'Sinuiju',
              ),
            3 => 
            array (
                'id' => 504,
                'state_id' => NULL,
                'name' => 'Wonsan',
              ),
            4 => 
            array (
                'id' => 505,
                'state_id' => NULL,
                'name' => 'Aarhus',
            ),
            5 => 
            array (
                'id' => 506,
                'state_id' => NULL,
                'name' => 'Nordjylland',
              ),
            6 => 
            array (
                'id' => 507,
                'state_id' => NULL,
                'name' => 'Bornholm',
            ),
            7 => 
            array (
                'id' => 508,
                'state_id' => NULL,
                'name' => 'Frederiksborg',
              ),
            8 => 
            array (
                'id' => 509,
                'state_id' => NULL,
                'name' => 'Fyn',
            ),
            9 => 
            array (
                'id' => 510,
                'state_id' => NULL,
                'name' => 'Copenhagen',
              ),
            10 => 
            array (
                'id' => 511,
                'state_id' => NULL,
                'name' => 'Ribe',
              ),
            11 => 
            array (
                'id' => 512,
                'state_id' => NULL,
                'name' => 'Ringkoebing',
              ),
            12 => 
            array (
                'id' => 513,
                'state_id' => NULL,
                'name' => 'Roskilde',
              ),
            13 => 
            array (
                'id' => 514,
                'state_id' => NULL,
                'name' => 'Soenderjylland',
              ),
            14 => 
            array (
                'id' => 515,
                'state_id' => NULL,
                'name' => 'Storstroem',
            ),
            15 => 
            array (
                'id' => 516,
                'state_id' => NULL,
                'name' => 'Viborg',
              ),
            16 => 
            array (
                'id' => 517,
                'state_id' => NULL,
                'name' => 'Vejle',
              ),
            17 => 
            array (
                'id' => 518,
                'state_id' => NULL,
                'name' => 'Vestsjaelland',
            ),
            18 => 
            array (
                'id' => 519,
                'state_id' => NULL,
                'name' => 'Arnsberg',
              ),
            19 => 
            array (
                'id' => 520,
                'state_id' => NULL,
                'name' => 'Erfurt',
              ),
            20 => 
            array (
                'id' => 521,
                'state_id' => NULL,
                'name' => 'Ansbach',
              ),
            21 => 
            array (
                'id' => 522,
                'state_id' => NULL,
                'name' => 'Augsburg',
              ),
            22 => 
            array (
                'id' => 523,
                'state_id' => NULL,
                'name' => 'Berlin',
            ),
            23 => 
            array (
                'id' => 524,
                'state_id' => NULL,
                'name' => 'Bayreuth',
              ),
            24 => 
            array (
                'id' => 525,
                'state_id' => NULL,
                'name' => 'Bielefeld',
              ),
            25 => 
            array (
                'id' => 526,
                'state_id' => NULL,
                'name' => 'Potsdam',
              ),
            26 => 
            array (
                'id' => 527,
                'state_id' => NULL,
                'name' => 'Bochum',
              ),
            27 => 
            array (
                'id' => 528,
                'state_id' => NULL,
                'name' => 'Bremen',
            ),
            28 => 
            array (
                'id' => 529,
                'state_id' => NULL,
                'name' => 'Brunswick',
              ),
            29 => 
            array (
                'id' => 530,
                'state_id' => NULL,
                'name' => 'Darmstadt',
              ),
            30 => 
            array (
                'id' => 531,
                'state_id' => NULL,
                'name' => 'Detmold',
              ),
            31 => 
            array (
                'id' => 532,
                'state_id' => NULL,
                'name' => 'Dresden',
              ),
            32 => 
            array (
                'id' => 533,
                'state_id' => NULL,
                'name' => 'Dessau',
              ),
            33 => 
            array (
                'id' => 534,
                'state_id' => NULL,
                'name' => 'Dusseldorf',
              ),
            34 => 
            array (
                'id' => 535,
                'state_id' => NULL,
                'name' => 'Frankfurt',
              ),
            35 => 
            array (
                'id' => 536,
                'state_id' => NULL,
                'name' => 'Freiburg',
              ),
            36 => 
            array (
                'id' => 537,
                'state_id' => NULL,
                'name' => 'Halle',
              ),
            37 => 
            array (
                'id' => 538,
                'state_id' => NULL,
                'name' => 'Hamburg',
            ),
            38 => 
            array (
                'id' => 539,
                'state_id' => NULL,
                'name' => 'Hannover',
              ),
            39 => 
            array (
                'id' => 540,
                'state_id' => NULL,
                'name' => 'Kiel',
              ),
            40 => 
            array (
                'id' => 541,
                'state_id' => NULL,
                'name' => 'GieBen',
              ),
            41 => 
            array (
                'id' => 542,
                'state_id' => NULL,
                'name' => 'Karlsruhe',
              ),
            42 => 
            array (
                'id' => 543,
                'state_id' => NULL,
                'name' => 'Kassel',
              ),
            43 => 
            array (
                'id' => 544,
                'state_id' => NULL,
                'name' => 'Chemnitz',
              ),
            44 => 
            array (
                'id' => 545,
                'state_id' => NULL,
                'name' => 'Koblenz',
              ),
            45 => 
            array (
                'id' => 546,
                'state_id' => NULL,
                'name' => 'Cologne',
              ),
            46 => 
            array (
                'id' => 547,
                'state_id' => NULL,
                'name' => 'Leipzig',
              ),
            47 => 
            array (
                'id' => 548,
                'state_id' => NULL,
                'name' => 'Landshut',
              ),
            48 => 
            array (
                'id' => 549,
                'state_id' => NULL,
                'name' => 'Luneburg',
              ),
            49 => 
            array (
                'id' => 550,
                'state_id' => NULL,
                'name' => 'Magdeburg',
              ),
            50 => 
            array (
                'id' => 551,
                'state_id' => NULL,
                'name' => 'Mannheim',
              ),
            51 => 
            array (
                'id' => 552,
                'state_id' => NULL,
                'name' => 'Mainz',
              ),
            52 => 
            array (
                'id' => 553,
                'state_id' => NULL,
                'name' => 'Muenster',
              ),
            53 => 
            array (
                'id' => 554,
                'state_id' => NULL,
                'name' => 'Munich',
              ),
            54 => 
            array (
                'id' => 555,
                'state_id' => NULL,
                'name' => 'Nuremberg',
              ),
            55 => 
            array (
                'id' => 556,
                'state_id' => NULL,
                'name' => 'Schwerin',
              ),
            56 => 
            array (
                'id' => 557,
                'state_id' => NULL,
                'name' => 'Stuttgart',
              ),
            57 => 
            array (
                'id' => 558,
                'state_id' => NULL,
                'name' => 'Trier',
              ),
            58 => 
            array (
                'id' => 559,
                'state_id' => NULL,
                'name' => 'Wiesbaden',
              ),
            59 => 
            array (
                'id' => 560,
                'state_id' => NULL,
                'name' => 'Wuerzburg',
              ),
            60 => 
            array (
                'id' => 561,
                'state_id' => NULL,
                'name' => 'Aileu',
            ),
            61 => 
            array (
                'id' => 562,
                'state_id' => NULL,
                'name' => 'Ainaro',
            ),
            62 => 
            array (
                'id' => 563,
                'state_id' => NULL,
                'name' => 'Ermera',
            ),
            63 => 
            array (
                'id' => 564,
                'state_id' => NULL,
                'name' => 'Ambeno',
            ),
            64 => 
            array (
                'id' => 565,
                'state_id' => NULL,
                'name' => 'Baucau',
            ),
            65 => 
            array (
                'id' => 566,
                'state_id' => NULL,
                'name' => 'Bobonaro',
            ),
            66 => 
            array (
                'id' => 567,
                'state_id' => NULL,
                'name' => 'Dili',
            ),
            67 => 
            array (
                'id' => 568,
                'state_id' => NULL,
                'name' => 'Kovalima',
            ),
            68 => 
            array (
                'id' => 569,
                'state_id' => NULL,
                'name' => 'Lautem',
            ),
            69 => 
            array (
                'id' => 570,
                'state_id' => NULL,
                'name' => 'Liquica',
            ),
            70 => 
            array (
                'id' => 571,
                'state_id' => NULL,
                'name' => 'Manatuto',
            ),
            71 => 
            array (
                'id' => 572,
                'state_id' => NULL,
                'name' => 'Manofahi',
            ),
            72 => 
            array (
                'id' => 573,
                'state_id' => NULL,
                'name' => 'Viqueque',
            ),
            73 => 
            array (
                'id' => 574,
                'state_id' => NULL,
                'name' => 'Maritime',
            ),
            74 => 
            array (
                'id' => 575,
                'state_id' => NULL,
                'name' => 'Savanes',
            ),
            75 => 
            array (
                'id' => 576,
                'state_id' => NULL,
                'name' => 'Plateaux',
            ),
            76 => 
            array (
                'id' => 577,
                'state_id' => NULL,
                'name' => 'Kara',
            ),
            77 => 
            array (
                'id' => 578,
                'state_id' => NULL,
                'name' => 'Centre',
            ),
            78 => 
            array (
                'id' => 579,
                'state_id' => NULL,
                'name' => 'Abakan',
              ),
            79 => 
            array (
                'id' => 580,
                'state_id' => NULL,
                'name' => 'Arkhangelsk',
              ),
            80 => 
            array (
                'id' => 581,
                'state_id' => NULL,
                'name' => 'Aginskoye',
              ),
            81 => 
            array (
                'id' => 582,
                'state_id' => NULL,
                'name' => 'Anadyr',
              ),
            82 => 
            array (
                'id' => 583,
                'state_id' => NULL,
                'name' => 'Astrakhan',
              ),
            83 => 
            array (
                'id' => 584,
                'state_id' => NULL,
                'name' => 'Elista',
              ),
            84 => 
            array (
                'id' => 585,
                'state_id' => NULL,
                'name' => 'Orel',
              ),
            85 => 
            array (
                'id' => 586,
                'state_id' => NULL,
                'name' => 'Orenburg',
              ),
            86 => 
            array (
                'id' => 587,
                'state_id' => NULL,
                'name' => 'Barnaul',
              ),
            87 => 
            array (
                'id' => 588,
                'state_id' => NULL,
                'name' => 'Penza',
              ),
            88 => 
            array (
                'id' => 589,
                'state_id' => NULL,
                'name' => 'Petropavlovsk-Kamchatskiy',
              ),
            89 => 
            array (
                'id' => 590,
                'state_id' => NULL,
                'name' => 'Petrozavodsk',
              ),
            90 => 
            array (
                'id' => 591,
                'state_id' => NULL,
                'name' => 'Perm',
              ),
            91 => 
            array (
                'id' => 592,
                'state_id' => NULL,
                'name' => 'Birobidzan',
              ),
            92 => 
            array (
                'id' => 593,
                'state_id' => NULL,
                'name' => 'Belgorod',
              ),
            93 => 
            array (
                'id' => 594,
                'state_id' => NULL,
                'name' => 'Chabarovsk',
              ),
            94 => 
            array (
                'id' => 595,
                'state_id' => NULL,
                'name' => 'Blagoveshchensk',
              ),
            95 => 
            array (
                'id' => 596,
                'state_id' => NULL,
                'name' => 'Bryansk',
              ),
            96 => 
            array (
                'id' => 597,
                'state_id' => NULL,
                'name' => 'Chelyabinsk',
              ),
            97 => 
            array (
                'id' => 598,
                'state_id' => NULL,
                'name' => 'Chita',
              ),
            98 => 
            array (
                'id' => 599,
                'state_id' => NULL,
                'name' => 'Rostov-na-Donu',
              ),
            99 => 
            array (
                'id' => 600,
                'state_id' => NULL,
                'name' => 'Omsk',
              ),
            100 => 
            array (
                'id' => 601,
                'state_id' => NULL,
                'name' => 'Volgograd',
              ),
            101 => 
            array (
                'id' => 602,
                'state_id' => NULL,
                'name' => 'Vladimir',
              ),
            102 => 
            array (
                'id' => 603,
                'state_id' => NULL,
                'name' => 'Vladikavkaz',
              ),
            103 => 
            array (
                'id' => 604,
                'state_id' => NULL,
                'name' => 'Gorno-Altajsk',
              ),
            104 => 
            array (
                'id' => 605,
                'state_id' => NULL,
                'name' => 'Grozny',
              ),
            105 => 
            array (
                'id' => 606,
                'state_id' => NULL,
                'name' => 'Vladivostok',
              ),
            106 => 
            array (
                'id' => 607,
                'state_id' => NULL,
                'name' => 'Khanty-Mansiysk',
              ),
            107 => 
            array (
                'id' => 608,
                'state_id' => NULL,
                'name' => 'Kirov',
              ),
            108 => 
            array (
                'id' => 609,
                'state_id' => NULL,
                'name' => 'Kaliningrad',
              ),
            109 => 
            array (
                'id' => 610,
                'state_id' => NULL,
                'name' => 'Kazan',
              ),
            110 => 
            array (
                'id' => 611,
                'state_id' => NULL,
                'name' => 'Kaluga',
              ),
            111 => 
            array (
                'id' => 612,
                'state_id' => NULL,
                'name' => 'Kostroma',
              ),
            112 => 
            array (
                'id' => 613,
                'state_id' => NULL,
                'name' => 'Krasnodar',
              ),
            113 => 
            array (
                'id' => 614,
                'state_id' => NULL,
                'name' => 'Krasnojarsk',
              ),
            114 => 
            array (
                'id' => 615,
                'state_id' => NULL,
                'name' => 'Kemerovo',
              ),
            115 => 
            array (
                'id' => 616,
                'state_id' => NULL,
                'name' => 'Kyzyl',
              ),
            116 => 
            array (
                'id' => 617,
                'state_id' => NULL,
                'name' => 'Kudymkar',
              ),
            117 => 
            array (
                'id' => 618,
                'state_id' => NULL,
                'name' => 'Kurgan',
              ),
            118 => 
            array (
                'id' => 619,
                'state_id' => NULL,
                'name' => 'Kursk',
              ),
            119 => 
            array (
                'id' => 620,
                'state_id' => NULL,
                'name' => 'Lipeck',
              ),
            120 => 
            array (
                'id' => 621,
                'state_id' => NULL,
                'name' => 'Ryazan',
              ),
            121 => 
            array (
                'id' => 622,
                'state_id' => NULL,
                'name' => 'Makhachkala',
              ),
            122 => 
            array (
                'id' => 623,
                'state_id' => NULL,
                'name' => 'Magadan',
              ),
            123 => 
            array (
                'id' => 624,
                'state_id' => NULL,
                'name' => 'Magas',
            ),
            124 => 
            array (
                'id' => 625,
                'state_id' => NULL,
                'name' => 'Maykop',
              ),
            125 => 
            array (
                'id' => 626,
                'state_id' => NULL,
                'name' => 'Murmansk',
              ),
            126 => 
            array (
                'id' => 627,
                'state_id' => NULL,
                'name' => 'Moscow',
              ),
            127 => 
            array (
                'id' => 628,
                'state_id' => NULL,
                'name' => 'Nalchik',
              ),
            128 => 
            array (
                'id' => 629,
                'state_id' => NULL,
                'name' => 'Naryan-Mar',
              ),
            129 => 
            array (
                'id' => 630,
                'state_id' => NULL,
                'name' => 'Juzno-Sachalinsk',
              ),
            130 => 
            array (
                'id' => 631,
                'state_id' => NULL,
                'name' => 'Velikij Novgorod',
              ),
            131 => 
            array (
                'id' => 632,
                'state_id' => NULL,
                'name' => 'Palana',
              ),
            132 => 
            array (
                'id' => 633,
                'state_id' => NULL,
                'name' => 'Pskov',
              ),
            133 => 
            array (
                'id' => 634,
                'state_id' => NULL,
                'name' => 'Cheboksary',
              ),
            134 => 
            array (
                'id' => 635,
                'state_id' => NULL,
                'name' => 'Cherkessk',
              ),
            135 => 
            array (
                'id' => 636,
                'state_id' => NULL,
                'name' => 'Tyumen',
              ),
            136 => 
            array (
                'id' => 637,
                'state_id' => NULL,
                'name' => 'Saratov',
              ),
            137 => 
            array (
                'id' => 638,
                'state_id' => NULL,
                'name' => 'Saransk',
              ),
            138 => 
            array (
                'id' => 639,
                'state_id' => NULL,
                'name' => 'Salekhard',
              ),
            139 => 
            array (
                'id' => 640,
                'state_id' => NULL,
                'name' => 'Samara',
              ),
            140 => 
            array (
                'id' => 641,
                'state_id' => NULL,
                'name' => 'Syktyvkar',
              ),
            141 => 
            array (
                'id' => 642,
                'state_id' => NULL,
                'name' => 'St. Peterburg',
              ),
            142 => 
            array (
                'id' => 643,
                'state_id' => NULL,
                'name' => 'Smolensk',
              ),
            143 => 
            array (
                'id' => 644,
                'state_id' => NULL,
                'name' => 'Stavropol',
              ),
            144 => 
            array (
                'id' => 645,
                'state_id' => NULL,
                'name' => 'Tambov',
              ),
            145 => 
            array (
                'id' => 646,
                'state_id' => NULL,
                'name' => 'Tver',
              ),
            146 => 
            array (
                'id' => 647,
                'state_id' => NULL,
                'name' => 'Tula',
              ),
            147 => 
            array (
                'id' => 648,
                'state_id' => NULL,
                'name' => 'Tomsk',
              ),
            148 => 
            array (
                'id' => 649,
                'state_id' => NULL,
                'name' => 'Voronezh',
              ),
            149 => 
            array (
                'id' => 650,
                'state_id' => NULL,
                'name' => 'Vologda',
              ),
            150 => 
            array (
                'id' => 651,
                'state_id' => NULL,
                'name' => 'Ufa',
              ),
            151 => 
            array (
                'id' => 652,
                'state_id' => NULL,
                'name' => 'Ulan-Ude',
              ),
            152 => 
            array (
                'id' => 653,
                'state_id' => NULL,
                'name' => 'Uljanovsk',
              ),
            153 => 
            array (
                'id' => 654,
                'state_id' => NULL,
                'name' => 'Ust-Ordynsky',
              ),
            154 => 
            array (
                'id' => 655,
                'state_id' => NULL,
                'name' => 'Niznij Novgorod',
              ),
            155 => 
            array (
                'id' => 656,
                'state_id' => NULL,
                'name' => 'Novosibirsk',
              ),
            156 => 
            array (
                'id' => 657,
                'state_id' => NULL,
                'name' => 'Jakutsk',
              ),
            157 => 
            array (
                'id' => 658,
                'state_id' => NULL,
                'name' => 'Jaroslavl',
              ),
            158 => 
            array (
                'id' => 659,
                'state_id' => NULL,
                'name' => 'Jekaterinburg',
              ),
            159 => 
            array (
                'id' => 660,
                'state_id' => NULL,
                'name' => 'Irkutsk',
              ),
            160 => 
            array (
                'id' => 661,
                'state_id' => NULL,
                'name' => 'Izhevsk',
              ),
            161 => 
            array (
                'id' => 662,
                'state_id' => NULL,
                'name' => 'Ivanovo',
              ),
            162 => 
            array (
                'id' => 663,
                'state_id' => NULL,
                'name' => 'Yoshkar-Ola',
              ),
            163 => 
            array (
                'id' => 664,
                'state_id' => NULL,
                'name' => 'Azuay',
            ),
            164 => 
            array (
                'id' => 665,
                'state_id' => NULL,
                'name' => 'El Oro',
            ),
            165 => 
            array (
                'id' => 666,
                'state_id' => NULL,
                'name' => 'Esmeraldas',
            ),
            166 => 
            array (
                'id' => 667,
                'state_id' => NULL,
                'name' => 'Bolivar',
            ),
            167 => 
            array (
                'id' => 668,
                'state_id' => NULL,
                'name' => 'Guayas',
            ),
            168 => 
            array (
                'id' => 669,
                'state_id' => NULL,
                'name' => 'Galapagos',
            ),
            169 => 
            array (
                'id' => 670,
                'state_id' => NULL,
                'name' => 'Carchi',
            ),
            170 => 
            array (
                'id' => 671,
                'state_id' => NULL,
                'name' => 'Canar',
            ),
            171 => 
            array (
                'id' => 672,
                'state_id' => NULL,
                'name' => 'Cotopaxi',
            ),
            172 => 
            array (
                'id' => 673,
                'state_id' => NULL,
                'name' => 'Loja',
            ),
            173 => 
            array (
                'id' => 674,
                'state_id' => NULL,
                'name' => 'Los Rios',
            ),
            174 => 
            array (
                'id' => 675,
                'state_id' => NULL,
                'name' => 'Manabi',
            ),
            175 => 
            array (
                'id' => 676,
                'state_id' => NULL,
                'name' => 'Morona-Santiago',
            ),
            176 => 
            array (
                'id' => 677,
                'state_id' => NULL,
                'name' => 'Napo, Orellana',
            ),
            177 => 
            array (
                'id' => 678,
                'state_id' => NULL,
                'name' => 'Pastaza',
            ),
            178 => 
            array (
                'id' => 679,
                'state_id' => NULL,
                'name' => 'Pichincha',
            ),
            179 => 
            array (
                'id' => 680,
                'state_id' => NULL,
                'name' => 'Chimborazo',
            ),
            180 => 
            array (
                'id' => 681,
                'state_id' => NULL,
                'name' => 'Zamora-Chinchipe',
            ),
            181 => 
            array (
                'id' => 682,
                'state_id' => NULL,
                'name' => 'Sucumbios',
            ),
            182 => 
            array (
                'id' => 683,
                'state_id' => NULL,
                'name' => 'Tungurahua',
            ),
            183 => 
            array (
                'id' => 684,
                'state_id' => NULL,
                'name' => 'Imbabura',
            ),
            184 => 
            array (
                'id' => 685,
                'state_id' => NULL,
                'name' => 'Anseba',
            ),
            185 => 
            array (
                'id' => 686,
                'state_id' => NULL,
                'name' => 'Semenawi Keyih Bahri',
            ),
            186 => 
            array (
                'id' => 687,
                'state_id' => NULL,
                'name' => 'Gash Barka',
            ),
            187 => 
            array (
                'id' => 688,
                'state_id' => NULL,
                'name' => 'Debub',
            ),
            188 => 
            array (
                'id' => 689,
                'state_id' => NULL,
                'name' => 'Debubawi Keyih Bahri',
            ),
            189 => 
            array (
                'id' => 690,
                'state_id' => NULL,
                'name' => 'Maekel',
            ),
            190 => 
            array (
                'id' => 691,
                'state_id' => NULL,
                'name' => 'Arles',
              ),
            191 => 
            array (
                'id' => 692,
                'state_id' => NULL,
                'name' => 'Ajaccio',
              ),
            192 => 
            array (
                'id' => 693,
                'state_id' => NULL,
                'name' => 'Aix-en-Provence',
              ),
            193 => 
            array (
                'id' => 694,
                'state_id' => NULL,
                'name' => 'Orleans',
              ),
            194 => 
            array (
                'id' => 695,
                'state_id' => NULL,
                'name' => 'Paris',
              ),
            195 => 
            array (
                'id' => 696,
                'state_id' => NULL,
                'name' => 'Besancon',
              ),
            196 => 
            array (
                'id' => 697,
                'state_id' => NULL,
                'name' => 'Dijon',
              ),
            197 => 
            array (
                'id' => 698,
                'state_id' => NULL,
                'name' => 'Frejus',
              ),
            198 => 
            array (
                'id' => 699,
                'state_id' => NULL,
                'name' => 'Caen',
              ),
            199 => 
            array (
                'id' => 700,
                'state_id' => NULL,
                'name' => 'Rennes',
              ),
            200 => 
            array (
                'id' => 701,
                'state_id' => NULL,
                'name' => 'Lyon',
              ),
            201 => 
            array (
                'id' => 702,
                'state_id' => NULL,
                'name' => 'Lille',
              ),
            202 => 
            array (
                'id' => 703,
                'state_id' => NULL,
                'name' => 'Limoges',
              ),
            203 => 
            array (
                'id' => 704,
                'state_id' => NULL,
                'name' => 'Rouen',
              ),
            204 => 
            array (
                'id' => 705,
                'state_id' => NULL,
                'name' => 'Marseille',
              ),
            205 => 
            array (
                'id' => 706,
                'state_id' => NULL,
                'name' => 'Metz',
              ),
            206 => 
            array (
                'id' => 707,
                'state_id' => NULL,
                'name' => 'Montpellier',
              ),
            207 => 
            array (
                'id' => 708,
                'state_id' => NULL,
                'name' => 'Nantes',
              ),
            208 => 
            array (
                'id' => 709,
                'state_id' => NULL,
                'name' => 'Nice',
              ),
            209 => 
            array (
                'id' => 710,
                'state_id' => NULL,
                'name' => 'Chalons-en-Champagne',
              ),
            210 => 
            array (
                'id' => 711,
                'state_id' => NULL,
                'name' => 'Toulouse',
              ),
            211 => 
            array (
                'id' => 712,
                'state_id' => NULL,
                'name' => 'Valence',
              ),
            212 => 
            array (
                'id' => 713,
                'state_id' => NULL,
                'name' => 'Amiens',
              ),
            213 => 
            array (
                'id' => 714,
                'state_id' => NULL,
                'name' => 'Davao',
              ),
            214 => 
            array (
                'id' => 715,
                'state_id' => NULL,
                'name' => 'Caloocan',
              ),
            215 => 
            array (
                'id' => 716,
                'state_id' => NULL,
                'name' => 'Manila',
              ),
            216 => 
            array (
                'id' => 717,
                'state_id' => NULL,
                'name' => 'Cebu',
              ),
            217 => 
            array (
                'id' => 718,
                'state_id' => NULL,
                'name' => 'Espoo',
              ),
            218 => 
            array (
                'id' => 719,
                'state_id' => NULL,
                'name' => 'Oulu',
              ),
            219 => 
            array (
                'id' => 720,
                'state_id' => NULL,
                'name' => 'Pori',
              ),
            220 => 
            array (
                'id' => 721,
                'state_id' => NULL,
                'name' => 'Porvoo',
              ),
            221 => 
            array (
                'id' => 722,
                'state_id' => NULL,
                'name' => 'Hameenlinna',
              ),
            222 => 
            array (
                'id' => 723,
                'state_id' => NULL,
                'name' => 'Helsinki',
              ),
            223 => 
            array (
                'id' => 724,
                'state_id' => NULL,
                'name' => 'Kajaani',
              ),
            224 => 
            array (
                'id' => 725,
                'state_id' => NULL,
                'name' => 'Kokkola',
              ),
            225 => 
            array (
                'id' => 726,
                'state_id' => NULL,
                'name' => 'Kotka',
              ),
            226 => 
            array (
                'id' => 727,
                'state_id' => NULL,
                'name' => 'Kuopio',
              ),
            227 => 
            array (
                'id' => 728,
                'state_id' => NULL,
                'name' => 'Lahti',
              ),
            228 => 
            array (
                'id' => 729,
                'state_id' => NULL,
                'name' => 'Lappeenranta',
              ),
            229 => 
            array (
                'id' => 730,
                'state_id' => NULL,
                'name' => 'Rovaniemi',
              ),
            230 => 
            array (
                'id' => 731,
                'state_id' => NULL,
                'name' => 'Mariehamn',
              ),
            231 => 
            array (
                'id' => 732,
                'state_id' => NULL,
                'name' => 'Mikkeli',
              ),
            232 => 
            array (
                'id' => 733,
                'state_id' => NULL,
                'name' => 'Tampere',
              ),
            233 => 
            array (
                'id' => 734,
                'state_id' => NULL,
                'name' => 'Turku',
              ),
            234 => 
            array (
                'id' => 735,
                'state_id' => NULL,
                'name' => 'Vaasa',
              ),
            235 => 
            array (
                'id' => 736,
                'state_id' => NULL,
                'name' => 'Vantaa',
              ),
            236 => 
            array (
                'id' => 737,
                'state_id' => NULL,
                'name' => 'Joensuu',
              ),
            237 => 
            array (
                'id' => 738,
                'state_id' => NULL,
                'name' => 'Paul',
            ),
            238 => 
            array (
                'id' => 739,
                'state_id' => NULL,
                'name' => 'Porto Novo',
            ),
            239 => 
            array (
                'id' => 740,
                'state_id' => NULL,
                'name' => 'Boa Vista',
            ),
            240 => 
            array (
                'id' => 741,
                'state_id' => NULL,
                'name' => 'Brava',
            ),
            241 => 
            array (
                'id' => 742,
                'state_id' => NULL,
                'name' => 'Ribeira Grande',
            ),
            242 => 
            array (
                'id' => 743,
                'state_id' => NULL,
                'name' => 'Fogo',
            ),
            243 => 
            array (
                'id' => 744,
                'state_id' => NULL,
                'name' => 'Maio',
            ),
            244 => 
            array (
                'id' => 745,
                'state_id' => NULL,
                'name' => 'Mosteiros',
            ),
            245 => 
            array (
                'id' => 746,
                'state_id' => NULL,
                'name' => 'Praia',
            ),
            246 => 
            array (
                'id' => 747,
                'state_id' => NULL,
                'name' => 'Sal',
            ),
            247 => 
            array (
                'id' => 748,
                'state_id' => NULL,
                'name' => 'Santo Antao',
            ),
            248 => 
            array (
                'id' => 749,
                'state_id' => NULL,
                'name' => 'Santiago',
            ),
            249 => 
            array (
                'id' => 750,
                'state_id' => NULL,
                'name' => 'Sao Domingos',
            ),
            250 => 
            array (
                'id' => 751,
                'state_id' => NULL,
                'name' => 'Sao Filipe',
            ),
            251 => 
            array (
                'id' => 752,
                'state_id' => NULL,
                'name' => 'Santa Catarina',
            ),
            252 => 
            array (
                'id' => 753,
                'state_id' => NULL,
                'name' => 'Santa Cruz',
            ),
            253 => 
            array (
                'id' => 754,
                'state_id' => NULL,
                'name' => 'Sao Miguel',
            ),
            254 => 
            array (
                'id' => 755,
                'state_id' => NULL,
                'name' => 'Sao Nicolau',
            ),
            255 => 
            array (
                'id' => 756,
                'state_id' => NULL,
                'name' => 'Sao Vicente',
            ),
            256 => 
            array (
                'id' => 757,
                'state_id' => NULL,
                'name' => 'Tarrafal',
            ),
            257 => 
            array (
                'id' => 758,
                'state_id' => NULL,
                'name' => 'Arauca',
              ),
            258 => 
            array (
                'id' => 759,
                'state_id' => NULL,
                'name' => 'Antioquia',
              ),
            259 => 
            array (
                'id' => 760,
                'state_id' => NULL,
                'name' => 'Norte de Santander',
              ),
            260 => 
            array (
                'id' => 761,
                'state_id' => NULL,
                'name' => 'Bogota',
              ),
            261 => 
            array (
                'id' => 762,
                'state_id' => NULL,
                'name' => 'Bolivar',
              ),
            262 => 
            array (
                'id' => 763,
                'state_id' => NULL,
                'name' => 'Boyaca',
              ),
            263 => 
            array (
                'id' => 764,
                'state_id' => NULL,
                'name' => 'Atlantico',
              ),
            264 => 
            array (
                'id' => 765,
                'state_id' => NULL,
                'name' => 'Guaviare',
              ),
            265 => 
            array (
                'id' => 766,
                'state_id' => NULL,
                'name' => 'La Guajira',
              ),
            266 => 
            array (
                'id' => 767,
                'state_id' => NULL,
                'name' => 'Guainia',
              ),
            267 => 
            array (
                'id' => 768,
                'state_id' => NULL,
                'name' => 'Quindio',
              ),
            268 => 
            array (
                'id' => 769,
                'state_id' => NULL,
                'name' => 'Caldas',
              ),
            269 => 
            array (
                'id' => 770,
                'state_id' => NULL,
                'name' => 'Caqueta',
              ),
            270 => 
            array (
                'id' => 771,
                'state_id' => NULL,
                'name' => 'Casanare',
              ),
            271 => 
            array (
                'id' => 772,
                'state_id' => NULL,
                'name' => 'Cauca',
              ),
            272 => 
            array (
                'id' => 773,
                'state_id' => NULL,
                'name' => 'Valle del Cauca',
              ),
            273 => 
            array (
                'id' => 774,
                'state_id' => NULL,
                'name' => 'Cordoba',
              ),
            274 => 
            array (
                'id' => 775,
                'state_id' => NULL,
                'name' => 'Cundinamarca',
              ),
            275 => 
            array (
                'id' => 776,
                'state_id' => NULL,
                'name' => 'Risaralda',
              ),
            276 => 
            array (
                'id' => 777,
                'state_id' => NULL,
                'name' => 'Magdalena',
              ),
            277 => 
            array (
                'id' => 778,
                'state_id' => NULL,
                'name' => 'Meta',
              ),
            278 => 
            array (
                'id' => 779,
                'state_id' => NULL,
                'name' => 'Narino',
              ),
            279 => 
            array (
                'id' => 780,
                'state_id' => NULL,
                'name' => 'Putumayo',
              ),
            280 => 
            array (
                'id' => 781,
                'state_id' => NULL,
                'name' => 'Choco',
              ),
            281 => 
            array (
                'id' => 782,
                'state_id' => NULL,
                'name' => 'Cesar',
              ),
            282 => 
            array (
                'id' => 783,
                'state_id' => NULL,
                'name' => 'Santander',
              ),
            283 => 
            array (
                'id' => 784,
                'state_id' => NULL,
                'name' => 'San Andres y Providencia',
              ),
            284 => 
            array (
                'id' => 785,
                'state_id' => NULL,
                'name' => 'Sucre',
              ),
            285 => 
            array (
                'id' => 786,
                'state_id' => NULL,
                'name' => 'Tolima',
              ),
            286 => 
            array (
                'id' => 787,
                'state_id' => NULL,
                'name' => 'Vichada',
              ),
            287 => 
            array (
                'id' => 788,
                'state_id' => NULL,
                'name' => 'Vaupes',
              ),
            288 => 
            array (
                'id' => 789,
                'state_id' => NULL,
                'name' => 'Huila',
              ),
            289 => 
            array (
                'id' => 790,
                'state_id' => NULL,
                'name' => 'Amazonas',
              ),
            290 => 
            array (
                'id' => 791,
                'state_id' => NULL,
                'name' => 'Alajuela',
            ),
            291 => 
            array (
                'id' => 792,
                'state_id' => NULL,
                'name' => 'Heredia',
            ),
            292 => 
            array (
                'id' => 793,
                'state_id' => NULL,
                'name' => 'Guanacaste',
            ),
            293 => 
            array (
                'id' => 794,
                'state_id' => NULL,
                'name' => 'Cartago',
            ),
            294 => 
            array (
                'id' => 795,
                'state_id' => NULL,
                'name' => 'Limon',
            ),
            295 => 
            array (
                'id' => 796,
                'state_id' => NULL,
                'name' => 'Puntarenas',
            ),
            296 => 
            array (
                'id' => 797,
                'state_id' => NULL,
                'name' => 'San Jose',
            ),
            297 => 
            array (
                'id' => 798,
                'state_id' => NULL,
                'name' => 'Holguin',
            ),
            298 => 
            array (
                'id' => 799,
                'state_id' => NULL,
                'name' => 'Pinar del Rio',
            ),
            299 => 
            array (
                'id' => 800,
                'state_id' => NULL,
                'name' => 'Villa Clara',
            ),
            300 => 
            array (
                'id' => 801,
                'state_id' => NULL,
                'name' => 'Granma',
            ),
            301 => 
            array (
                'id' => 802,
                'state_id' => NULL,
                'name' => 'Guantanamo',
            ),
            302 => 
            array (
                'id' => 803,
                'state_id' => NULL,
                'name' => 'La Habana',
            ),
            303 => 
            array (
                'id' => 804,
                'state_id' => NULL,
                'name' => 'Ciudad de la Habana',
            ),
            304 => 
            array (
                'id' => 805,
                'state_id' => NULL,
                'name' => 'Camaguey',
            ),
            305 => 
            array (
                'id' => 806,
                'state_id' => NULL,
                'name' => 'Las Tunas',
            ),
            306 => 
            array (
                'id' => 807,
                'state_id' => NULL,
                'name' => 'Matanzas',
            ),
            307 => 
            array (
                'id' => 808,
                'state_id' => NULL,
                'name' => 'Mayari',
              ),
            308 => 
            array (
                'id' => 809,
                'state_id' => NULL,
                'name' => 'Manzanillo',
              ),
            309 => 
            array (
                'id' => 810,
                'state_id' => NULL,
                'name' => 'Isla de la Juventud',
            ),
            310 => 
            array (
                'id' => 811,
                'state_id' => NULL,
                'name' => 'Santiago de Cuba',
            ),
            311 => 
            array (
                'id' => 812,
                'state_id' => NULL,
                'name' => 'Sancti Spiritus',
            ),
            312 => 
            array (
                'id' => 813,
                'state_id' => NULL,
                'name' => 'Cienfuegos',
            ),
            313 => 
            array (
                'id' => 814,
                'state_id' => NULL,
                'name' => 'Ciego de Avila',
            ),
            314 => 
            array (
                'id' => 815,
                'state_id' => NULL,
                'name' => 'Essequibo Islands-West Demerara',
            ),
            315 => 
            array (
                'id' => 816,
                'state_id' => NULL,
                'name' => 'Barima-Waini',
            ),
            316 => 
            array (
                'id' => 817,
                'state_id' => NULL,
                'name' => 'Pomeroon-Supenaam',
            ),
            317 => 
            array (
                'id' => 818,
                'state_id' => NULL,
                'name' => 'Potaro-Siparuni',
            ),
            318 => 
            array (
                'id' => 819,
                'state_id' => NULL,
                'name' => 'Demerara-Mahaica',
            ),
            319 => 
            array (
                'id' => 820,
                'state_id' => NULL,
                'name' => 'East Berbice-Corentyne',
            ),
            320 => 
            array (
                'id' => 821,
                'state_id' => NULL,
                'name' => 'Cuyuni-Mazaruni',
            ),
            321 => 
            array (
                'id' => 822,
                'state_id' => NULL,
                'name' => 'Mahaica-Berbice',
            ),
            322 => 
            array (
                'id' => 823,
                'state_id' => NULL,
                'name' => 'Upper Demerara-Berbice',
            ),
            323 => 
            array (
                'id' => 824,
                'state_id' => NULL,
                'name' => 'Upper Takutu-Upper Essequibo',
            ),
            324 => 
            array (
                'id' => 825,
                'state_id' => NULL,
                'name' => 'Arkalyk',
              ),
            325 => 
            array (
                'id' => 826,
                'state_id' => NULL,
                'name' => 'Aqmola',
              ),
            326 => 
            array (
                'id' => 827,
                'state_id' => NULL,
                'name' => 'Aksu',
              ),
            327 => 
            array (
                'id' => 828,
                'state_id' => NULL,
                'name' => 'Aqtobe',
              ),
            328 => 
            array (
                'id' => 829,
                'state_id' => NULL,
                'name' => 'Almaty',
              ),
            329 => 
            array (
                'id' => 830,
                'state_id' => NULL,
                'name' => 'Arys',
              ),
            330 => 
            array (
                'id' => 831,
                'state_id' => NULL,
                'name' => 'Astana',
              ),
            331 => 
            array (
                'id' => 832,
                'state_id' => NULL,
                'name' => 'Atyrau',
              ),
            332 => 
            array (
                'id' => 833,
                'state_id' => NULL,
                'name' => 'Ekibastuz',
              ),
            333 => 
            array (
                'id' => 834,
                'state_id' => NULL,
                'name' => 'Balkhash',
              ),
            334 => 
            array (
                'id' => 835,
                'state_id' => NULL,
                'name' => 'Pavlodar',
              ),
            335 => 
            array (
                'id' => 836,
                'state_id' => NULL,
                'name' => 'Soltustik Qazaqstan',
              ),
            336 => 
            array (
                'id' => 837,
                'state_id' => NULL,
                'name' => 'Shyghys Qazaqstan',
              ),
            337 => 
            array (
                'id' => 838,
                'state_id' => NULL,
                'name' => 'Zyryanovsk',
              ),
            338 => 
            array (
                'id' => 839,
                'state_id' => NULL,
                'name' => 'Zhambyl',
              ),
            339 => 
            array (
                'id' => 840,
                'state_id' => NULL,
                'name' => 'Zhezkazgan',
              ),
            340 => 
            array (
                'id' => 841,
                'state_id' => NULL,
                'name' => 'Qaraghandy',
              ),
            341 => 
            array (
                'id' => 842,
                'state_id' => NULL,
                'name' => 'Karazhal',
              ),
            342 => 
            array (
                'id' => 843,
                'state_id' => NULL,
                'name' => 'Kapchagay',
              ),
            343 => 
            array (
                'id' => 844,
                'state_id' => NULL,
                'name' => 'Qostanay',
              ),
            344 => 
            array (
                'id' => 845,
                'state_id' => NULL,
                'name' => 'Qyzylorda',
              ),
            345 => 
            array (
                'id' => 846,
                'state_id' => NULL,
                'name' => 'Kentau',
              ),
            346 => 
            array (
                'id' => 847,
                'state_id' => NULL,
                'name' => 'Kurchatov',
              ),
            347 => 
            array (
                'id' => 848,
                'state_id' => NULL,
                'name' => 'Lisakovsk',
              ),
            348 => 
            array (
                'id' => 849,
                'state_id' => NULL,
                'name' => 'Leninogorsk',
              ),
            349 => 
            array (
                'id' => 850,
                'state_id' => NULL,
                'name' => 'Rudny',
              ),
            350 => 
            array (
                'id' => 851,
                'state_id' => NULL,
                'name' => 'Mangghystau',
              ),
            351 => 
            array (
                'id' => 852,
                'state_id' => NULL,
                'name' => 'Ongtustik Qazaqstan',
              ),
            352 => 
            array (
                'id' => 853,
                'state_id' => NULL,
                'name' => 'Saran',
              ),
            353 => 
            array (
                'id' => 854,
                'state_id' => NULL,
                'name' => 'Semey',
              ),
            354 => 
            array (
                'id' => 855,
                'state_id' => NULL,
                'name' => 'Shakhtinsk',
              ),
            355 => 
            array (
                'id' => 856,
                'state_id' => NULL,
                'name' => 'Stepnogorsk',
              ),
            356 => 
            array (
                'id' => 857,
                'state_id' => NULL,
                'name' => 'Tekeli',
              ),
            357 => 
            array (
                'id' => 858,
                'state_id' => NULL,
                'name' => 'Temirtau',
              ),
            358 => 
            array (
                'id' => 859,
                'state_id' => NULL,
                'name' => 'Turkestan',
              ),
            359 => 
            array (
                'id' => 860,
                'state_id' => NULL,
                'name' => 'Batys Qazaqstan',
              ),
            360 => 
            array (
                'id' => 861,
                'state_id' => NULL,
                'name' => 'Zhanaozen',
              ),
            361 => 
            array (
                'id' => 862,
                'state_id' => NULL,
                'name' => 'Almere',
              ),
            362 => 
            array (
                'id' => 863,
                'state_id' => NULL,
                'name' => 'Amersfoort',
              ),
            363 => 
            array (
                'id' => 864,
                'state_id' => NULL,
                'name' => 'Amsterdam',
              ),
            364 => 
            array (
                'id' => 865,
                'state_id' => NULL,
                'name' => 'Arnhem',
              ),
            365 => 
            array (
                'id' => 866,
                'state_id' => NULL,
                'name' => 'Apeldoorn',
              ),
            366 => 
            array (
                'id' => 867,
                'state_id' => NULL,
                'name' => 'Assen',
              ),
            367 => 
            array (
                'id' => 868,
                'state_id' => NULL,
                'name' => 'Ede',
              ),
            368 => 
            array (
                'id' => 869,
                'state_id' => NULL,
                'name' => 'Emmen',
              ),
            369 => 
            array (
                'id' => 870,
                'state_id' => NULL,
                'name' => 'Eindhoven',
              ),
            370 => 
            array (
                'id' => 871,
                'state_id' => NULL,
                'name' => 'Breda',
              ),
            371 => 
            array (
                'id' => 872,
                'state_id' => NULL,
                'name' => 'Tilburg',
              ),
            372 => 
            array (
                'id' => 873,
                'state_id' => NULL,
                'name' => 'Dordrecht',
              ),
            373 => 
            array (
                'id' => 874,
                'state_id' => NULL,
                'name' => 'Enschede',
              ),
            374 => 
            array (
                'id' => 875,
                'state_id' => NULL,
                'name' => 'Groningen',
              ),
            375 => 
            array (
                'id' => 876,
                'state_id' => NULL,
                'name' => 'Haarlem',
              ),
            376 => 
            array (
                'id' => 877,
                'state_id' => NULL,
                'name' => 'Hague',
              ),
            377 => 
            array (
                'id' => 878,
                'state_id' => NULL,
                'name' => 'Hoofddorp',
              ),
            378 => 
            array (
                'id' => 879,
                'state_id' => NULL,
                'name' => 'Leiden',
              ),
            379 => 
            array (
                'id' => 880,
                'state_id' => NULL,
                'name' => 'Lelystad',
              ),
            380 => 
            array (
                'id' => 881,
                'state_id' => NULL,
                'name' => 'Rotterdam',
              ),
            381 => 
            array (
                'id' => 882,
                'state_id' => NULL,
                'name' => 'Leeuwarden',
              ),
            382 => 
            array (
                'id' => 883,
                'state_id' => NULL,
                'name' => 'Maastricht',
              ),
            383 => 
            array (
                'id' => 884,
                'state_id' => NULL,
                'name' => 'Middelburg',
              ),
            384 => 
            array (
                'id' => 885,
                'state_id' => NULL,
                'name' => 'Nijmegen',
              ),
            385 => 
            array (
                'id' => 886,
                'state_id' => NULL,
                'name' => '\'s-Hertogenbosch',
              ),
            386 => 
            array (
                'id' => 887,
                'state_id' => NULL,
                'name' => 'Utrecht',
              ),
            387 => 
            array (
                'id' => 888,
                'state_id' => NULL,
                'name' => 'Zwolle',
              ),
            388 => 
            array (
                'id' => 889,
                'state_id' => NULL,
                'name' => 'Zoetermeer',
              ),
            389 => 
            array (
                'id' => 890,
                'state_id' => NULL,
                'name' => 'Atlantida',
            ),
            390 => 
            array (
                'id' => 891,
                'state_id' => NULL,
                'name' => 'El Paraiso',
            ),
            391 => 
            array (
                'id' => 892,
                'state_id' => NULL,
                'name' => 'Ocotepeque',
            ),
            392 => 
            array (
                'id' => 893,
                'state_id' => NULL,
                'name' => 'Olancho',
            ),
            393 => 
            array (
                'id' => 894,
                'state_id' => NULL,
                'name' => 'Francisco Morazan',
            ),
            394 => 
            array (
                'id' => 895,
                'state_id' => NULL,
                'name' => 'Gracias a Dios',
            ),
            395 => 
            array (
                'id' => 896,
                'state_id' => NULL,
                'name' => 'Islas de la Bahia',
            ),
            396 => 
            array (
                'id' => 897,
                'state_id' => NULL,
                'name' => 'Cortes',
            ),
            397 => 
            array (
                'id' => 898,
                'state_id' => NULL,
                'name' => 'Colon',
            ),
            398 => 
            array (
                'id' => 899,
                'state_id' => NULL,
                'name' => 'Comayagua',
            ),
            399 => 
            array (
                'id' => 900,
                'state_id' => NULL,
                'name' => 'Copan',
            ),
            400 => 
            array (
                'id' => 901,
                'state_id' => NULL,
                'name' => 'La Paz',
            ),
            401 => 
            array (
                'id' => 902,
                'state_id' => NULL,
                'name' => 'Lempira',
            ),
            402 => 
            array (
                'id' => 903,
                'state_id' => NULL,
                'name' => 'Choluteca',
            ),
            403 => 
            array (
                'id' => 904,
                'state_id' => NULL,
                'name' => 'Choloma',
              ),
            404 => 
            array (
                'id' => 905,
                'state_id' => NULL,
                'name' => 'Valle',
            ),
            405 => 
            array (
                'id' => 906,
                'state_id' => NULL,
                'name' => 'Santa Barbara',
            ),
            406 => 
            array (
                'id' => 907,
                'state_id' => NULL,
                'name' => 'Intibuca',
            ),
            407 => 
            array (
                'id' => 908,
                'state_id' => NULL,
                'name' => 'Yoro',
            ),
            408 => 
            array (
                'id' => 909,
                'state_id' => NULL,
                'name' => 'Phoenix Islands',
              ),
            409 => 
            array (
                'id' => 910,
                'state_id' => NULL,
                'name' => 'Gilberts Islands',
              ),
            410 => 
            array (
                'id' => 911,
                'state_id' => NULL,
                'name' => 'Line Islands',
              ),
            411 => 
            array (
                'id' => 912,
                'state_id' => NULL,
                'name' => 'Ali Sabih',
            ),
            412 => 
            array (
                'id' => 913,
                'state_id' => NULL,
                'name' => 'Obock',
            ),
            413 => 
            array (
                'id' => 914,
                'state_id' => NULL,
                'name' => 'Dikhil',
            ),
            414 => 
            array (
                'id' => 915,
                'state_id' => NULL,
                'name' => 'Tadjoura',
            ),
            415 => 
            array (
                'id' => 916,
                'state_id' => NULL,
                'name' => 'Osh',
            ),
            416 => 
            array (
                'id' => 917,
                'state_id' => NULL,
                'name' => 'Batken',
            ),
            417 => 
            array (
                'id' => 918,
                'state_id' => NULL,
                'name' => 'Bishkek',
            ),
            418 => 
            array (
                'id' => 919,
                'state_id' => NULL,
                'name' => 'Chuy',
            ),
            419 => 
            array (
                'id' => 920,
                'state_id' => NULL,
                'name' => 'Jalal-Abad',
            ),
            420 => 
            array (
                'id' => 921,
                'state_id' => NULL,
                'name' => 'Karabalta',
              ),
            421 => 
            array (
                'id' => 922,
                'state_id' => NULL,
                'name' => 'Kara-Kol',
              ),
            422 => 
            array (
                'id' => 923,
                'state_id' => NULL,
                'name' => 'Kant',
              ),
            423 => 
            array (
                'id' => 924,
                'state_id' => NULL,
                'name' => 'Kok-Jangak',
            ),
            424 => 
            array (
                'id' => 925,
                'state_id' => NULL,
                'name' => 'Mailuu-Suu',
            ),
            425 => 
            array (
                'id' => 926,
                'state_id' => NULL,
                'name' => 'Naryn',
            ),
            426 => 
            array (
                'id' => 927,
                'state_id' => NULL,
                'name' => 'Suluktu',
            ),
            427 => 
            array (
                'id' => 928,
                'state_id' => NULL,
                'name' => 'Talas',
            ),
            428 => 
            array (
                'id' => 929,
                'state_id' => NULL,
                'name' => 'Tash-Kumyr',
            ),
            429 => 
            array (
                'id' => 930,
                'state_id' => NULL,
                'name' => 'Uzgen',
            ),
            430 => 
            array (
                'id' => 931,
                'state_id' => NULL,
                'name' => 'Ysyk-Kol',
            ),
            431 => 
            array (
                'id' => 932,
                'state_id' => NULL,
                'name' => 'Boke',
              ),
            432 => 
            array (
                'id' => 933,
                'state_id' => NULL,
                'name' => 'Nzerekore',
              ),
            433 => 
            array (
                'id' => 934,
                'state_id' => NULL,
                'name' => 'Faranah',
              ),
            434 => 
            array (
                'id' => 935,
                'state_id' => NULL,
                'name' => 'Kindia',
              ),
            435 => 
            array (
                'id' => 936,
                'state_id' => NULL,
                'name' => 'Kankan',
              ),
            436 => 
            array (
                'id' => 937,
                'state_id' => NULL,
                'name' => 'Conakry',
              ),
            437 => 
            array (
                'id' => 938,
                'state_id' => NULL,
                'name' => 'Labe',
              ),
            438 => 
            array (
                'id' => 939,
                'state_id' => NULL,
                'name' => 'Mamou',
              ),
            439 => 
            array (
                'id' => 940,
                'state_id' => NULL,
                'name' => 'Abbotsford',
              ),
            440 => 
            array (
                'id' => 941,
                'state_id' => NULL,
                'name' => 'Edmonton',
              ),
            441 => 
            array (
                'id' => 942,
                'state_id' => NULL,
                'name' => 'Oshawa',
              ),
            442 => 
            array (
                'id' => 943,
                'state_id' => NULL,
                'name' => 'Barrie',
              ),
            443 => 
            array (
                'id' => 944,
                'state_id' => NULL,
                'name' => 'Cape Breton',
              ),
            444 => 
            array (
                'id' => 945,
                'state_id' => NULL,
                'name' => 'Toronto',
              ),
            445 => 
            array (
                'id' => 946,
                'state_id' => NULL,
                'name' => 'Fredericton',
              ),
            446 => 
            array (
                'id' => 947,
                'state_id' => NULL,
                'name' => 'Guelph',
              ),
            447 => 
            array (
                'id' => 948,
                'state_id' => NULL,
                'name' => 'Halifax',
              ),
            448 => 
            array (
                'id' => 949,
                'state_id' => NULL,
                'name' => 'Hamilton',
              ),
            449 => 
            array (
                'id' => 950,
                'state_id' => NULL,
                'name' => 'Whitehorse',
              ),
            450 => 
            array (
                'id' => 951,
                'state_id' => NULL,
                'name' => 'Kelowna',
              ),
            451 => 
            array (
                'id' => 952,
                'state_id' => NULL,
                'name' => 'Brampton',
              ),
            452 => 
            array (
                'id' => 953,
                'state_id' => NULL,
                'name' => 'Kingston',
              ),
            453 => 
            array (
                'id' => 954,
                'state_id' => NULL,
                'name' => 'Calgary',
              ),
            454 => 
            array (
                'id' => 955,
                'state_id' => NULL,
                'name' => 'Quebec',
              ),
            455 => 
            array (
                'id' => 956,
                'state_id' => NULL,
                'name' => 'Regina',
              ),
            456 => 
            array (
                'id' => 957,
                'state_id' => NULL,
                'name' => 'London',
              ),
            457 => 
            array (
                'id' => 958,
                'state_id' => NULL,
                'name' => 'Montreal',
              ),
            458 => 
            array (
                'id' => 959,
                'state_id' => NULL,
                'name' => 'Sudbury',
              ),
            459 => 
            array (
                'id' => 960,
                'state_id' => NULL,
                'name' => 'Saskatoon',
              ),
            460 => 
            array (
                'id' => 961,
                'state_id' => NULL,
                'name' => 'Trois-Rivieres',
              ),
            461 => 
            array (
                'id' => 962,
                'state_id' => NULL,
                'name' => 'Thunder Bay',
              ),
            462 => 
            array (
                'id' => 963,
                'state_id' => NULL,
                'name' => 'Sherbrooke',
              ),
            463 => 
            array (
                'id' => 964,
                'state_id' => NULL,
                'name' => 'St. Catharines',
              ),
            464 => 
            array (
                'id' => 965,
                'state_id' => NULL,
                'name' => 'Saint-John\'s',
              ),
            465 => 
            array (
                'id' => 966,
                'state_id' => NULL,
                'name' => 'Victoria',
              ),
            466 => 
            array (
                'id' => 967,
                'state_id' => NULL,
                'name' => 'Vancouver',
              ),
            467 => 
            array (
                'id' => 968,
                'state_id' => NULL,
                'name' => 'Winnipeg',
              ),
            468 => 
            array (
                'id' => 969,
                'state_id' => NULL,
                'name' => 'Windsor',
              ),
            469 => 
            array (
                'id' => 970,
                'state_id' => NULL,
                'name' => 'Ottawa',
              ),
            470 => 
            array (
                'id' => 971,
                'state_id' => NULL,
                'name' => 'Charlottetown',
              ),
            471 => 
            array (
                'id' => 972,
                'state_id' => NULL,
                'name' => 'Yellowknife',
              ),
            472 => 
            array (
                'id' => 973,
                'state_id' => NULL,
                'name' => 'Iqaluit',
              ),
            473 => 
            array (
                'id' => 974,
                'state_id' => NULL,
                'name' => 'Ashanti',
            ),
            474 => 
            array (
                'id' => 975,
                'state_id' => NULL,
                'name' => 'Obuasi',
              ),
            475 => 
            array (
                'id' => 976,
                'state_id' => NULL,
                'name' => 'Northern',
            ),
            476 => 
            array (
                'id' => 977,
                'state_id' => NULL,
                'name' => 'Brong Ahafo',
            ),
            477 => 
            array (
                'id' => 978,
                'state_id' => NULL,
                'name' => 'Greater Accra',
            ),
            478 => 
            array (
                'id' => 979,
                'state_id' => NULL,
                'name' => 'Eastern',
            ),
            479 => 
            array (
                'id' => 980,
                'state_id' => NULL,
                'name' => 'Upper East',
            ),
            480 => 
            array (
                'id' => 981,
                'state_id' => NULL,
                'name' => 'Upper West',
            ),
            481 => 
            array (
                'id' => 982,
                'state_id' => NULL,
                'name' => 'Volta',
            ),
            482 => 
            array (
                'id' => 983,
                'state_id' => NULL,
                'name' => 'Western',
            ),
            483 => 
            array (
                'id' => 984,
                'state_id' => NULL,
                'name' => 'Central',
            ),
            484 => 
            array (
                'id' => 985,
                'state_id' => NULL,
                'name' => 'Ogooue-Lolo',
            ),
            485 => 
            array (
                'id' => 986,
                'state_id' => NULL,
                'name' => 'Ogooue-Ivindo',
            ),
            486 => 
            array (
                'id' => 987,
                'state_id' => NULL,
                'name' => 'Ogooue-Maritime',
            ),
            487 => 
            array (
                'id' => 988,
                'state_id' => NULL,
                'name' => 'Ngounie',
            ),
            488 => 
            array (
                'id' => 989,
                'state_id' => NULL,
                'name' => 'Estuaire',
            ),
            489 => 
            array (
                'id' => 990,
                'state_id' => NULL,
                'name' => 'Nyanga',
            ),
            490 => 
            array (
                'id' => 991,
                'state_id' => NULL,
                'name' => 'Haut-Ogooue',
            ),
            491 => 
            array (
                'id' => 992,
                'state_id' => NULL,
                'name' => 'Woleu-Ntem',
            ),
            492 => 
            array (
                'id' => 993,
                'state_id' => NULL,
                'name' => 'Moyen-Ogooue',
            ),
            493 => 
            array (
                'id' => 994,
                'state_id' => NULL,
                'name' => 'Otdar Mean Chey',
            ),
            494 => 
            array (
                'id' => 995,
                'state_id' => NULL,
                'name' => 'Krong Keb',
            ),
            495 => 
            array (
                'id' => 996,
                'state_id' => NULL,
                'name' => 'Preah Vihear',
            ),
            496 => 
            array (
                'id' => 997,
                'state_id' => NULL,
                'name' => 'Krong Pailin',
            ),
            497 => 
            array (
                'id' => 998,
                'state_id' => NULL,
                'name' => 'Banteay Mean Chey',
            ),
            498 => 
            array (
                'id' => 999,
                'state_id' => NULL,
                'name' => 'Kampong Chhnang',
              ),
            499 => 
            array (
                'id' => 1000,
                'state_id' => NULL,
                'name' => 'Kampong Spoe',
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1001,
                'state_id' => NULL,
                'name' => 'Kampong Thum',
              ),
            1 => 
            array (
                'id' => 1002,
                'state_id' => NULL,
                'name' => 'Kampong Cham',
            ),
            2 => 
            array (
                'id' => 1003,
                'state_id' => NULL,
                'name' => 'Prey Veng',
            ),
            3 => 
            array (
                'id' => 1004,
                'state_id' => NULL,
                'name' => 'Takev',
            ),
            4 => 
            array (
                'id' => 1005,
                'state_id' => NULL,
                'name' => 'Svay Rieng',
              ),
            5 => 
            array (
                'id' => 1006,
                'state_id' => NULL,
                'name' => 'Kandal',
            ),
            6 => 
            array (
                'id' => 1007,
                'state_id' => NULL,
                'name' => 'Kaoh Kong',
              ),
            7 => 
            array (
                'id' => 1008,
                'state_id' => NULL,
                'name' => 'Kampot',
              ),
            8 => 
            array (
                'id' => 1009,
                'state_id' => NULL,
                'name' => 'Phnum Penh',
              ),
            9 => 
            array (
                'id' => 1010,
                'state_id' => NULL,
                'name' => 'Kracheh',
            ),
            10 => 
            array (
                'id' => 1011,
                'state_id' => NULL,
                'name' => 'Rotanak Kiri',
              ),
            11 => 
            array (
                'id' => 1012,
                'state_id' => NULL,
                'name' => 'Bat Dambang',
            ),
            12 => 
            array (
                'id' => 1013,
                'state_id' => NULL,
                'name' => 'Mondol Kiri',
              ),
            13 => 
            array (
                'id' => 1014,
                'state_id' => NULL,
                'name' => 'Pouthĭsat',
            ),
            14 => 
            array (
                'id' => 1015,
                'state_id' => NULL,
                'name' => 'Stoeng Treng',
              ),
            15 => 
            array (
                'id' => 1016,
                'state_id' => NULL,
                'name' => 'Krong Preah',
            ),
            16 => 
            array (
                'id' => 1017,
                'state_id' => NULL,
                'name' => 'Siem Reab',
              ),
            17 => 
            array (
                'id' => 1018,
                'state_id' => NULL,
                'name' => 'Olomoucky',
            ),
            18 => 
            array (
                'id' => 1019,
                'state_id' => NULL,
                'name' => 'Plzensky',
            ),
            19 => 
            array (
                'id' => 1020,
                'state_id' => NULL,
                'name' => 'Prague',
            ),
            20 => 
            array (
                'id' => 1021,
                'state_id' => NULL,
                'name' => 'Kralovehradecky',
            ),
            21 => 
            array (
                'id' => 1022,
                'state_id' => NULL,
                'name' => 'Karlovarsky',
            ),
            22 => 
            array (
                'id' => 1023,
                'state_id' => NULL,
                'name' => 'Liberecky',
            ),
            23 => 
            array (
                'id' => 1024,
                'state_id' => NULL,
                'name' => 'Moravskoslezsky',
            ),
            24 => 
            array (
                'id' => 1025,
                'state_id' => NULL,
                'name' => 'Jihomoravsky',
            ),
            25 => 
            array (
                'id' => 1026,
                'state_id' => NULL,
                'name' => 'Pardubicky',
            ),
            26 => 
            array (
                'id' => 1027,
                'state_id' => NULL,
                'name' => 'Vysocina',
            ),
            27 => 
            array (
                'id' => 1028,
                'state_id' => NULL,
                'name' => 'Ustecky',
            ),
            28 => 
            array (
                'id' => 1029,
                'state_id' => NULL,
                'name' => 'Stredocesky',
            ),
            29 => 
            array (
                'id' => 1030,
                'state_id' => NULL,
                'name' => 'Zlinsky',
            ),
            30 => 
            array (
                'id' => 1031,
                'state_id' => NULL,
                'name' => 'Matabeleland North',
            ),
            31 => 
            array (
                'id' => 1032,
                'state_id' => NULL,
                'name' => 'Bulawayo',
            ),
            32 => 
            array (
                'id' => 1033,
                'state_id' => NULL,
                'name' => 'Mashonaland East',
            ),
            33 => 
            array (
                'id' => 1034,
                'state_id' => NULL,
                'name' => 'Harare',
            ),
            34 => 
            array (
                'id' => 1035,
                'state_id' => NULL,
                'name' => 'Manicaland',
            ),
            35 => 
            array (
                'id' => 1036,
                'state_id' => NULL,
                'name' => 'Masvingo',
            ),
            36 => 
            array (
                'id' => 1037,
                'state_id' => NULL,
                'name' => 'Matabeleland South',
            ),
            37 => 
            array (
                'id' => 1038,
                'state_id' => NULL,
                'name' => 'Mashonaland West',
            ),
            38 => 
            array (
                'id' => 1039,
                'state_id' => NULL,
                'name' => 'Midlands',
            ),
            39 => 
            array (
                'id' => 1040,
                'state_id' => NULL,
                'name' => 'Mashonaland Central',
            ),
            40 => 
            array (
                'id' => 1041,
                'state_id' => NULL,
                'name' => 'Adamaoua',
              ),
            41 => 
            array (
                'id' => 1042,
                'state_id' => NULL,
                'name' => 'Nord',
              ),
            42 => 
            array (
                'id' => 1043,
                'state_id' => NULL,
                'name' => 'Extreme-Nord',
              ),
            43 => 
            array (
                'id' => 1044,
                'state_id' => NULL,
                'name' => 'Littoral',
              ),
            44 => 
            array (
                'id' => 1045,
                'state_id' => NULL,
                'name' => 'Est',
              ),
            45 => 
            array (
                'id' => 1046,
                'state_id' => NULL,
                'name' => 'Sud',
              ),
            46 => 
            array (
                'id' => 1047,
                'state_id' => NULL,
                'name' => 'Nord-Oueste',
              ),
            47 => 
            array (
                'id' => 1048,
                'state_id' => NULL,
                'name' => 'Ouest',
              ),
            48 => 
            array (
                'id' => 1049,
                'state_id' => NULL,
                'name' => 'Sud-Oueste',
              ),
            49 => 
            array (
                'id' => 1050,
                'state_id' => NULL,
                'name' => 'Centre',
              ),
            50 => 
            array (
                'id' => 1051,
                'state_id' => NULL,
                'name' => 'Madinat ach Shamal',
            ),
            51 => 
            array (
                'id' => 1052,
                'state_id' => NULL,
                'name' => 'Ad Dawhah',
            ),
            52 => 
            array (
                'id' => 1053,
                'state_id' => NULL,
                'name' => 'Al Ghuwariyah',
            ),
            53 => 
            array (
                'id' => 1054,
                'state_id' => NULL,
                'name' => 'Al Khawr',
            ),
            54 => 
            array (
                'id' => 1055,
                'state_id' => NULL,
                'name' => 'Jariyan al Batnah',
            ),
            55 => 
            array (
                'id' => 1056,
                'state_id' => NULL,
                'name' => 'Ar Rayyan',
            ),
            56 => 
            array (
                'id' => 1057,
                'state_id' => NULL,
                'name' => 'Al Wakrah',
            ),
            57 => 
            array (
                'id' => 1058,
                'state_id' => NULL,
                'name' => 'Umm Salal',
            ),
            58 => 
            array (
                'id' => 1059,
                'state_id' => NULL,
                'name' => 'Al Jumaliyah',
            ),
            59 => 
            array (
                'id' => 1060,
                'state_id' => NULL,
                'name' => 'Agnebi',
            ),
            60 => 
            array (
                'id' => 1061,
                'state_id' => NULL,
                'name' => 'Bafing',
            ),
            61 => 
            array (
                'id' => 1062,
                'state_id' => NULL,
                'name' => 'Vallee du Bandama',
            ),
            62 => 
            array (
                'id' => 1063,
                'state_id' => NULL,
                'name' => 'Denguele',
            ),
            63 => 
            array (
                'id' => 1064,
                'state_id' => NULL,
                'name' => 'Nzi-Comoe',
            ),
            64 => 
            array (
                'id' => 1065,
                'state_id' => NULL,
                'name' => 'Fromager',
            ),
            65 => 
            array (
                'id' => 1066,
                'state_id' => NULL,
                'name' => 'Lacs',
            ),
            66 => 
            array (
                'id' => 1067,
                'state_id' => NULL,
                'name' => 'Marahoue',
            ),
            67 => 
            array (
                'id' => 1068,
                'state_id' => NULL,
                'name' => 'Sud-Bandama',
            ),
            68 => 
            array (
                'id' => 1069,
                'state_id' => NULL,
                'name' => 'Sud-Comoe',
            ),
            69 => 
            array (
                'id' => 1070,
                'state_id' => NULL,
                'name' => 'Haut-Sassandra',
            ),
            70 => 
            array (
                'id' => 1071,
                'state_id' => NULL,
                'name' => 'Savanes',
            ),
            71 => 
            array (
                'id' => 1072,
                'state_id' => NULL,
                'name' => 'Montagnes',
            ),
            72 => 
            array (
                'id' => 1073,
                'state_id' => NULL,
                'name' => 'Worodougou',
            ),
            73 => 
            array (
                'id' => 1074,
                'state_id' => NULL,
                'name' => 'Bas-Sassandra',
            ),
            74 => 
            array (
                'id' => 1075,
                'state_id' => NULL,
                'name' => 'Lagunes',
            ),
            75 => 
            array (
                'id' => 1076,
                'state_id' => NULL,
                'name' => 'Zanzan',
            ),
            76 => 
            array (
                'id' => 1077,
                'state_id' => NULL,
                'name' => 'Moyen-Cavally',
            ),
            77 => 
            array (
                'id' => 1078,
                'state_id' => NULL,
                'name' => 'Moyen-Comoe',
            ),
            78 => 
            array (
                'id' => 1079,
                'state_id' => NULL,
                'name' => 'Osjecko-Baranjska',
            ),
            79 => 
            array (
                'id' => 1080,
                'state_id' => NULL,
                'name' => 'Bjelovarsko-Bilogorska',
            ),
            80 => 
            array (
                'id' => 1081,
                'state_id' => NULL,
                'name' => 'Primorsko-Goranska',
            ),
            81 => 
            array (
                'id' => 1082,
                'state_id' => NULL,
                'name' => 'Pozega-Slavonia',
            ),
            82 => 
            array (
                'id' => 1083,
                'state_id' => NULL,
                'name' => 'Brodsko-Posavska',
            ),
            83 => 
            array (
                'id' => 1084,
                'state_id' => NULL,
                'name' => 'Dubrovacko-Neretvanska',
            ),
            84 => 
            array (
                'id' => 1085,
                'state_id' => NULL,
                'name' => 'Karlovacka',
            ),
            85 => 
            array (
                'id' => 1086,
                'state_id' => NULL,
                'name' => 'Koprivnicko-Krizevacka',
            ),
            86 => 
            array (
                'id' => 1087,
                'state_id' => NULL,
                'name' => 'Krapinsko-Zagorska',
            ),
            87 => 
            array (
                'id' => 1088,
                'state_id' => NULL,
                'name' => 'Licko-Senjska',
            ),
            88 => 
            array (
                'id' => 1089,
                'state_id' => NULL,
                'name' => 'Medimurska',
            ),
            89 => 
            array (
                'id' => 1090,
                'state_id' => NULL,
                'name' => 'Zagrebacka',
            ),
            90 => 
            array (
                'id' => 1091,
                'state_id' => NULL,
                'name' => 'Grad Zagreb',
            ),
            91 => 
            array (
                'id' => 1092,
                'state_id' => NULL,
                'name' => 'Splitsko-Dalmatinska',
            ),
            92 => 
            array (
                'id' => 1093,
                'state_id' => NULL,
                'name' => 'Varazdinska',
            ),
            93 => 
            array (
                'id' => 1094,
                'state_id' => NULL,
                'name' => 'Viroviticko-Podravska',
            ),
            94 => 
            array (
                'id' => 1095,
                'state_id' => NULL,
                'name' => 'Vukovarsko-Srijemska',
            ),
            95 => 
            array (
                'id' => 1096,
                'state_id' => NULL,
                'name' => 'Sibensko-Kninska',
            ),
            96 => 
            array (
                'id' => 1097,
                'state_id' => NULL,
                'name' => 'Sisacko-Moslavacka',
            ),
            97 => 
            array (
                'id' => 1098,
                'state_id' => NULL,
                'name' => 'Istarska',
            ),
            98 => 
            array (
                'id' => 1099,
                'state_id' => NULL,
                'name' => 'Zadarska',
            ),
            99 => 
            array (
                'id' => 1100,
                'state_id' => NULL,
                'name' => 'Elgeyo-Marakwet',
              ),
            100 => 
            array (
                'id' => 1101,
                'state_id' => NULL,
                'name' => 'Baringo',
              ),
            101 => 
            array (
                'id' => 1102,
                'state_id' => NULL,
                'name' => 'Bungoma',
              ),
            102 => 
            array (
                'id' => 1103,
                'state_id' => NULL,
                'name' => 'Bomet',
              ),
            103 => 
            array (
                'id' => 1104,
                'state_id' => NULL,
                'name' => 'Busia',
              ),
            104 => 
            array (
                'id' => 1105,
                'state_id' => NULL,
                'name' => 'Embu',
              ),
            105 => 
            array (
                'id' => 1106,
                'state_id' => NULL,
                'name' => 'Homa Bay',
              ),
            106 => 
            array (
                'id' => 1107,
                'state_id' => NULL,
                'name' => 'Kiambu',
              ),
            107 => 
            array (
                'id' => 1108,
                'state_id' => NULL,
                'name' => 'Kilifi',
              ),
            108 => 
            array (
                'id' => 1109,
                'state_id' => NULL,
                'name' => 'Kirinyaga',
              ),
            109 => 
            array (
                'id' => 1110,
                'state_id' => NULL,
                'name' => 'Kisumu',
              ),
            110 => 
            array (
                'id' => 1111,
                'state_id' => NULL,
                'name' => 'Kitui',
              ),
            111 => 
            array (
                'id' => 1112,
                'state_id' => NULL,
                'name' => 'Kisii',
              ),
            112 => 
            array (
                'id' => 1113,
                'state_id' => NULL,
                'name' => 'Garissa',
              ),
            113 => 
            array (
                'id' => 1114,
                'state_id' => NULL,
                'name' => 'Kakamega',
              ),
            114 => 
            array (
                'id' => 1115,
                'state_id' => NULL,
                'name' => 'Kajiado',
              ),
            115 => 
            array (
                'id' => 1116,
                'state_id' => NULL,
                'name' => 'Kericho',
              ),
            116 => 
            array (
                'id' => 1117,
                'state_id' => NULL,
                'name' => 'Kwale',
              ),
            117 => 
            array (
                'id' => 1118,
                'state_id' => NULL,
                'name' => 'Lamu',
              ),
            118 => 
            array (
                'id' => 1119,
                'state_id' => NULL,
                'name' => 'Laikipia',
              ),
            119 => 
            array (
                'id' => 1120,
                'state_id' => NULL,
                'name' => 'Machakos',
              ),
            120 => 
            array (
                'id' => 1121,
                'state_id' => NULL,
                'name' => 'Makueni',
              ),
            121 => 
            array (
                'id' => 1122,
                'state_id' => NULL,
                'name' => 'Marsabit',
              ),
            122 => 
            array (
                'id' => 1123,
                'state_id' => NULL,
                'name' => 'Mandera',
              ),
            123 => 
            array (
                'id' => 1124,
                'state_id' => NULL,
                'name' => 'Meru',
              ),
            124 => 
            array (
                'id' => 1125,
                'state_id' => NULL,
                'name' => 'Mombasa',
              ),
            125 => 
            array (
                'id' => 1126,
                'state_id' => NULL,
                'name' => 'Migori',
              ),
            126 => 
            array (
                'id' => 1127,
                'state_id' => NULL,
                'name' => 'Muranga',
              ),
            127 => 
            array (
                'id' => 1128,
                'state_id' => NULL,
                'name' => 'Nakuru',
              ),
            128 => 
            array (
                'id' => 1129,
                'state_id' => NULL,
                'name' => 'Narok',
              ),
            129 => 
            array (
                'id' => 1130,
                'state_id' => NULL,
                'name' => 'Nandi',
              ),
            130 => 
            array (
                'id' => 1131,
                'state_id' => NULL,
                'name' => 'Nairobi',
            ),
            131 => 
            array (
                'id' => 1132,
                'state_id' => NULL,
                'name' => 'Nithi',
              ),
            132 => 
            array (
                'id' => 1133,
                'state_id' => NULL,
                'name' => 'Nyamira',
              ),
            133 => 
            array (
                'id' => 1134,
                'state_id' => NULL,
                'name' => 'Nyandarua',
              ),
            134 => 
            array (
                'id' => 1135,
                'state_id' => NULL,
                'name' => 'Nyeri',
              ),
            135 => 
            array (
                'id' => 1136,
                'state_id' => NULL,
                'name' => 'Samburu',
              ),
            136 => 
            array (
                'id' => 1137,
                'state_id' => NULL,
                'name' => 'Tana River',
              ),
            137 => 
            array (
                'id' => 1138,
                'state_id' => NULL,
                'name' => 'Taita-Taveta',
              ),
            138 => 
            array (
                'id' => 1139,
                'state_id' => NULL,
                'name' => 'Trans-Nzoia',
              ),
            139 => 
            array (
                'id' => 1140,
                'state_id' => NULL,
                'name' => 'Turkana',
              ),
            140 => 
            array (
                'id' => 1141,
                'state_id' => NULL,
                'name' => 'Wajir',
              ),
            141 => 
            array (
                'id' => 1142,
                'state_id' => NULL,
                'name' => 'Uasin Gishu',
              ),
            142 => 
            array (
                'id' => 1143,
                'state_id' => NULL,
                'name' => 'Vihiga',
              ),
            143 => 
            array (
                'id' => 1144,
                'state_id' => NULL,
                'name' => 'West Pokot',
              ),
            144 => 
            array (
                'id' => 1145,
                'state_id' => NULL,
                'name' => 'Siaya',
              ),
            145 => 
            array (
                'id' => 1146,
                'state_id' => NULL,
                'name' => 'Isiolo',
              ),
            146 => 
            array (
                'id' => 1147,
                'state_id' => NULL,
                'name' => 'Central',
            ),
            147 => 
            array (
                'id' => 1148,
                'state_id' => NULL,
                'name' => 'Aluksnes',
              ),
            148 => 
            array (
                'id' => 1149,
                'state_id' => NULL,
                'name' => 'Aizkraukles',
              ),
            149 => 
            array (
                'id' => 1150,
                'state_id' => NULL,
                'name' => 'Ogres',
              ),
            150 => 
            array (
                'id' => 1151,
                'state_id' => NULL,
                'name' => 'Balvu',
              ),
            151 => 
            array (
                'id' => 1152,
                'state_id' => NULL,
                'name' => 'Bauskas',
              ),
            152 => 
            array (
                'id' => 1153,
                'state_id' => NULL,
                'name' => 'Cesu',
              ),
            153 => 
            array (
                'id' => 1154,
                'state_id' => NULL,
                'name' => 'Dobeles',
              ),
            154 => 
            array (
                'id' => 1155,
                'state_id' => NULL,
                'name' => 'Gulbenes',
              ),
            155 => 
            array (
                'id' => 1156,
                'state_id' => NULL,
                'name' => 'Jekabpils',
              ),
            156 => 
            array (
                'id' => 1157,
                'state_id' => NULL,
                'name' => 'Kraslavas',
              ),
            157 => 
            array (
                'id' => 1158,
                'state_id' => NULL,
                'name' => 'Kuldigas',
              ),
            158 => 
            array (
                'id' => 1159,
                'state_id' => NULL,
                'name' => 'Rezeknes',
              ),
            159 => 
            array (
                'id' => 1160,
                'state_id' => NULL,
                'name' => 'Rigas',
              ),
            160 => 
            array (
                'id' => 1161,
                'state_id' => NULL,
                'name' => 'Liepajas',
              ),
            161 => 
            array (
                'id' => 1162,
                'state_id' => NULL,
                'name' => 'Limbazu',
              ),
            162 => 
            array (
                'id' => 1163,
                'state_id' => NULL,
                'name' => 'Ludzas',
              ),
            163 => 
            array (
                'id' => 1164,
                'state_id' => NULL,
                'name' => 'Madonas',
              ),
            164 => 
            array (
                'id' => 1165,
                'state_id' => NULL,
                'name' => 'Preilu',
              ),
            165 => 
            array (
                'id' => 1166,
                'state_id' => NULL,
                'name' => 'Saldus',
              ),
            166 => 
            array (
                'id' => 1167,
                'state_id' => NULL,
                'name' => 'Talsu',
              ),
            167 => 
            array (
                'id' => 1168,
                'state_id' => NULL,
                'name' => 'Daugavpils',
              ),
            168 => 
            array (
                'id' => 1169,
                'state_id' => NULL,
                'name' => 'Tukuma',
              ),
            169 => 
            array (
                'id' => 1170,
                'state_id' => NULL,
                'name' => 'Valkas',
              ),
            170 => 
            array (
                'id' => 1171,
                'state_id' => NULL,
                'name' => 'Valmieras',
              ),
            171 => 
            array (
                'id' => 1172,
                'state_id' => NULL,
                'name' => 'Ventspils',
              ),
            172 => 
            array (
                'id' => 1173,
                'state_id' => NULL,
                'name' => 'Jelgavas',
              ),
            173 => 
            array (
                'id' => 1174,
                'state_id' => NULL,
                'name' => 'Berea',
            ),
            174 => 
            array (
                'id' => 1175,
                'state_id' => NULL,
                'name' => 'Butha-Buthe',
            ),
            175 => 
            array (
                'id' => 1176,
                'state_id' => NULL,
                'name' => 'Quthing',
            ),
            176 => 
            array (
                'id' => 1177,
                'state_id' => NULL,
                'name' => 'Qachas Nek',
            ),
            177 => 
            array (
                'id' => 1178,
                'state_id' => NULL,
                'name' => 'Leribe',
            ),
            178 => 
            array (
                'id' => 1179,
                'state_id' => NULL,
                'name' => 'Mafeteng',
            ),
            179 => 
            array (
                'id' => 1180,
                'state_id' => NULL,
                'name' => 'Maseru',
            ),
            180 => 
            array (
                'id' => 1181,
                'state_id' => NULL,
                'name' => 'Mohales Hoek',
            ),
            181 => 
            array (
                'id' => 1182,
                'state_id' => NULL,
                'name' => 'Mokhotlong',
            ),
            182 => 
            array (
                'id' => 1183,
                'state_id' => NULL,
                'name' => 'Thaba-Tseka',
            ),
            183 => 
            array (
                'id' => 1184,
                'state_id' => NULL,
                'name' => 'Attapu',
            ),
            184 => 
            array (
                'id' => 1185,
                'state_id' => NULL,
                'name' => 'Bolikhamxai',
            ),
            185 => 
            array (
                'id' => 1186,
                'state_id' => NULL,
                'name' => 'Bokeo',
            ),
            186 => 
            array (
                'id' => 1187,
                'state_id' => NULL,
                'name' => 'Xiangkhoang',
            ),
            187 => 
            array (
                'id' => 1188,
                'state_id' => NULL,
                'name' => 'Phongsali',
            ),
            188 => 
            array (
                'id' => 1189,
                'state_id' => NULL,
                'name' => 'Khammouan',
            ),
            189 => 
            array (
                'id' => 1190,
                'state_id' => NULL,
                'name' => 'Houaphan',
            ),
            190 => 
            array (
                'id' => 1191,
                'state_id' => NULL,
                'name' => 'Louangphrabang',
            ),
            191 => 
            array (
                'id' => 1192,
                'state_id' => NULL,
                'name' => 'Louang Namtha',
            ),
            192 => 
            array (
                'id' => 1193,
                'state_id' => NULL,
                'name' => 'Xaisomboun',
            ),
            193 => 
            array (
                'id' => 1194,
                'state_id' => NULL,
                'name' => 'Xekong',
            ),
            194 => 
            array (
                'id' => 1195,
                'state_id' => NULL,
                'name' => 'Saravan',
            ),
            195 => 
            array (
                'id' => 1196,
                'state_id' => NULL,
                'name' => 'Savannakhet',
            ),
            196 => 
            array (
                'id' => 1197,
                'state_id' => NULL,
                'name' => 'Xaignabouri',
            ),
            197 => 
            array (
                'id' => 1198,
                'state_id' => NULL,
                'name' => 'Vientiane',
            ),
            198 => 
            array (
                'id' => 1199,
                'state_id' => NULL,
                'name' => 'Oudomxai',
            ),
            199 => 
            array (
                'id' => 1200,
                'state_id' => NULL,
                'name' => 'Champasak',
            ),
            200 => 
            array (
                'id' => 1201,
                'state_id' => NULL,
                'name' => 'Ash-Shamal',
            ),
            201 => 
            array (
                'id' => 1202,
                'state_id' => NULL,
                'name' => 'Al-Biqa',
            ),
            202 => 
            array (
                'id' => 1203,
                'state_id' => NULL,
                'name' => 'Bayrut',
            ),
            203 => 
            array (
                'id' => 1204,
                'state_id' => NULL,
                'name' => 'Jabal Lubnan',
            ),
            204 => 
            array (
                'id' => 1205,
                'state_id' => NULL,
                'name' => 'An-Nabatiyah',
            ),
            205 => 
            array (
                'id' => 1206,
                'state_id' => NULL,
                'name' => 'Al-Janub',
            ),
            206 => 
            array (
                'id' => 1207,
                'state_id' => NULL,
                'name' => 'Gbarpolu',
              ),
            207 => 
            array (
                'id' => 1208,
                'state_id' => NULL,
                'name' => 'Bong',
            ),
            208 => 
            array (
                'id' => 1209,
                'state_id' => NULL,
                'name' => 'Bopolu',
              ),
            209 => 
            array (
                'id' => 1210,
                'state_id' => NULL,
                'name' => 'Bomi',
            ),
            210 => 
            array (
                'id' => 1211,
                'state_id' => NULL,
                'name' => 'Grand Bassa',
            ),
            211 => 
            array (
                'id' => 1212,
                'state_id' => NULL,
                'name' => 'Grand Gedeh',
            ),
            212 => 
            array (
                'id' => 1213,
                'state_id' => NULL,
                'name' => 'Grand Cape Mount',
            ),
            213 => 
            array (
                'id' => 1214,
                'state_id' => NULL,
                'name' => 'Grand Kru',
            ),
            214 => 
            array (
                'id' => 1215,
                'state_id' => NULL,
                'name' => 'Fish Town',
            ),
            215 => 
            array (
                'id' => 1216,
                'state_id' => NULL,
                'name' => 'River Gee',
            ),
            216 => 
            array (
                'id' => 1217,
                'state_id' => NULL,
                'name' => 'River Cess',
            ),
            217 => 
            array (
                'id' => 1218,
                'state_id' => NULL,
                'name' => 'Lofa',
            ),
            218 => 
            array (
                'id' => 1219,
                'state_id' => NULL,
                'name' => 'Margibi',
            ),
            219 => 
            array (
                'id' => 1220,
                'state_id' => NULL,
                'name' => 'Maryland',
            ),
            220 => 
            array (
                'id' => 1221,
                'state_id' => NULL,
                'name' => 'Montserrado',
            ),
            221 => 
            array (
                'id' => 1222,
                'state_id' => NULL,
                'name' => 'Nimba',
            ),
            222 => 
            array (
                'id' => 1223,
                'state_id' => NULL,
                'name' => 'Sinoe',
            ),
            223 => 
            array (
                'id' => 1224,
                'state_id' => NULL,
                'name' => 'Alytus',
            ),
            224 => 
            array (
                'id' => 1225,
                'state_id' => NULL,
                'name' => 'Kaunas',
            ),
            225 => 
            array (
                'id' => 1226,
                'state_id' => NULL,
                'name' => 'Klaipeda',
            ),
            226 => 
            array (
                'id' => 1227,
                'state_id' => NULL,
                'name' => 'Marijampole',
            ),
            227 => 
            array (
                'id' => 1228,
                'state_id' => NULL,
                'name' => 'Panevezys',
            ),
            228 => 
            array (
                'id' => 1229,
                'state_id' => NULL,
                'name' => 'Taurages',
            ),
            229 => 
            array (
                'id' => 1230,
                'state_id' => NULL,
                'name' => 'Telsiu',
            ),
            230 => 
            array (
                'id' => 1231,
                'state_id' => NULL,
                'name' => 'Vilnius',
            ),
            231 => 
            array (
                'id' => 1232,
                'state_id' => NULL,
                'name' => 'Utenos',
            ),
            232 => 
            array (
                'id' => 1233,
                'state_id' => NULL,
                'name' => 'Siauliai',
            ),
            233 => 
            array (
                'id' => 1234,
                'state_id' => NULL,
                'name' => 'Akmenes',
              ),
            234 => 
            array (
                'id' => 1235,
                'state_id' => NULL,
                'name' => 'Diekirch',
            ),
            235 => 
            array (
                'id' => 1236,
                'state_id' => NULL,
                'name' => 'Grevenmacher',
            ),
            236 => 
            array (
                'id' => 1237,
                'state_id' => NULL,
                'name' => 'Luxembourg',
            ),
            237 => 
            array (
                'id' => 1238,
                'state_id' => NULL,
                'name' => 'Byumba',
            ),
            238 => 
            array (
                'id' => 1239,
                'state_id' => NULL,
                'name' => 'Butare',
            ),
            239 => 
            array (
                'id' => 1240,
                'state_id' => NULL,
                'name' => 'Nyanza',
            ),
            240 => 
            array (
                'id' => 1241,
                'state_id' => NULL,
                'name' => 'Kibungo',
            ),
            241 => 
            array (
                'id' => 1242,
                'state_id' => NULL,
                'name' => 'Kibuye',
            ),
            242 => 
            array (
                'id' => 1243,
                'state_id' => NULL,
                'name' => 'Kigali-Ngali',
            ),
            243 => 
            array (
                'id' => 1244,
                'state_id' => NULL,
                'name' => 'Kigali-Ville',
            ),
            244 => 
            array (
                'id' => 1245,
                'state_id' => NULL,
                'name' => 'Gikongoro',
            ),
            245 => 
            array (
                'id' => 1246,
                'state_id' => NULL,
                'name' => 'Gisenyi',
            ),
            246 => 
            array (
                'id' => 1247,
                'state_id' => NULL,
                'name' => 'Gitarama',
            ),
            247 => 
            array (
                'id' => 1248,
                'state_id' => NULL,
                'name' => 'Kabuga',
            ),
            248 => 
            array (
                'id' => 1249,
                'state_id' => NULL,
                'name' => 'Rwamagana',
            ),
            249 => 
            array (
                'id' => 1250,
                'state_id' => NULL,
                'name' => 'Ruhango',
            ),
            250 => 
            array (
                'id' => 1251,
                'state_id' => NULL,
                'name' => 'Ruhengeri',
            ),
            251 => 
            array (
                'id' => 1252,
                'state_id' => NULL,
                'name' => 'Cyangugu',
            ),
            252 => 
            array (
                'id' => 1253,
                'state_id' => NULL,
                'name' => 'Umutara',
            ),
            253 => 
            array (
                'id' => 1254,
                'state_id' => NULL,
                'name' => 'Alba Iulia',
            ),
            254 => 
            array (
                'id' => 1255,
                'state_id' => NULL,
                'name' => 'Arad',
            ),
            255 => 
            array (
                'id' => 1256,
                'state_id' => NULL,
                'name' => 'Oradea',
            ),
            256 => 
            array (
                'id' => 1257,
                'state_id' => NULL,
                'name' => 'Bacau',
            ),
            257 => 
            array (
                'id' => 1258,
                'state_id' => NULL,
                'name' => 'Baia Mare',
            ),
            258 => 
            array (
                'id' => 1259,
                'state_id' => NULL,
                'name' => 'Bistrita',
            ),
            259 => 
            array (
                'id' => 1260,
                'state_id' => NULL,
                'name' => 'Botosani',
            ),
            260 => 
            array (
                'id' => 1261,
                'state_id' => NULL,
                'name' => 'Bucuresti',
            ),
            261 => 
            array (
                'id' => 1262,
                'state_id' => NULL,
                'name' => 'Brasov',
            ),
            262 => 
            array (
                'id' => 1263,
                'state_id' => NULL,
                'name' => 'Braila',
            ),
            263 => 
            array (
                'id' => 1264,
                'state_id' => NULL,
                'name' => 'Buzau',
            ),
            264 => 
            array (
                'id' => 1265,
                'state_id' => NULL,
                'name' => 'Drobeta-Turnu Severin',
            ),
            265 => 
            array (
                'id' => 1266,
                'state_id' => NULL,
                'name' => 'Deva',
            ),
            266 => 
            array (
                'id' => 1267,
                'state_id' => NULL,
                'name' => 'Timisoara',
            ),
            267 => 
            array (
                'id' => 1268,
                'state_id' => NULL,
                'name' => 'Focsani',
            ),
            268 => 
            array (
                'id' => 1269,
                'state_id' => NULL,
                'name' => 'Galati',
            ),
            269 => 
            array (
                'id' => 1270,
                'state_id' => NULL,
                'name' => 'Giurgiu',
            ),
            270 => 
            array (
                'id' => 1271,
                'state_id' => NULL,
                'name' => 'Constanta',
            ),
            271 => 
            array (
                'id' => 1272,
                'state_id' => NULL,
                'name' => 'Craiova',
            ),
            272 => 
            array (
                'id' => 1273,
                'state_id' => NULL,
                'name' => 'Calarasi',
            ),
            273 => 
            array (
                'id' => 1274,
                'state_id' => NULL,
                'name' => 'Cluj-Napoca',
            ),
            274 => 
            array (
                'id' => 1275,
                'state_id' => NULL,
                'name' => 'XRimnicu Vilcea',
            ),
            275 => 
            array (
                'id' => 1276,
                'state_id' => NULL,
                'name' => 'Resita',
            ),
            276 => 
            array (
                'id' => 1277,
                'state_id' => NULL,
                'name' => 'Miercurea-Ciuc',
            ),
            277 => 
            array (
                'id' => 1278,
                'state_id' => NULL,
                'name' => 'Pitesti',
            ),
            278 => 
            array (
                'id' => 1279,
                'state_id' => NULL,
                'name' => 'Piatra Neamt',
            ),
            279 => 
            array (
                'id' => 1280,
                'state_id' => NULL,
                'name' => 'Ploiesti',
            ),
            280 => 
            array (
                'id' => 1281,
                'state_id' => NULL,
                'name' => 'Satu Mare',
            ),
            281 => 
            array (
                'id' => 1282,
                'state_id' => NULL,
                'name' => 'Sfantu-Gheorghe',
            ),
            282 => 
            array (
                'id' => 1283,
                'state_id' => NULL,
                'name' => 'Slatina',
            ),
            283 => 
            array (
                'id' => 1284,
                'state_id' => NULL,
                'name' => 'Slobozia',
            ),
            284 => 
            array (
                'id' => 1285,
                'state_id' => NULL,
                'name' => 'Suceava',
            ),
            285 => 
            array (
                'id' => 1286,
                'state_id' => NULL,
                'name' => 'Targovişte',
            ),
            286 => 
            array (
                'id' => 1287,
                'state_id' => NULL,
                'name' => 'Tirgu Mures',
            ),
            287 => 
            array (
                'id' => 1288,
                'state_id' => NULL,
                'name' => 'Tirgu-Jiu',
            ),
            288 => 
            array (
                'id' => 1289,
                'state_id' => NULL,
                'name' => 'Tulcea',
            ),
            289 => 
            array (
                'id' => 1290,
                'state_id' => NULL,
                'name' => 'Vaslui',
            ),
            290 => 
            array (
                'id' => 1291,
                'state_id' => NULL,
                'name' => 'Sibiu',
            ),
            291 => 
            array (
                'id' => 1292,
                'state_id' => NULL,
                'name' => 'Iasi',
            ),
            292 => 
            array (
                'id' => 1293,
                'state_id' => NULL,
                'name' => 'Alexandria',
            ),
            293 => 
            array (
                'id' => 1294,
                'state_id' => NULL,
                'name' => 'Zalau',
            ),
            294 => 
            array (
                'id' => 1295,
                'state_id' => NULL,
                'name' => 'Antsiranana',
            ),
            295 => 
            array (
                'id' => 1296,
                'state_id' => NULL,
                'name' => 'Fianarantsoa',
            ),
            296 => 
            array (
                'id' => 1297,
                'state_id' => NULL,
                'name' => 'Mahajanga',
            ),
            297 => 
            array (
                'id' => 1298,
                'state_id' => NULL,
                'name' => 'Antananarivo',
            ),
            298 => 
            array (
                'id' => 1299,
                'state_id' => NULL,
                'name' => 'Toamasina',
            ),
            299 => 
            array (
                'id' => 1300,
                'state_id' => NULL,
                'name' => 'Toliary',
            ),
            300 => 
            array (
                'id' => 1301,
                'state_id' => NULL,
                'name' => 'Addu Atoll',
              ),
            301 => 
            array (
                'id' => 1302,
                'state_id' => NULL,
                'name' => 'North Ari Atoll',
              ),
            302 => 
            array (
                'id' => 1303,
                'state_id' => NULL,
                'name' => 'North Thiladhunmathi',
              ),
            303 => 
            array (
                'id' => 1304,
                'state_id' => NULL,
                'name' => 'North Maalhosmadhulu',
              ),
            304 => 
            array (
                'id' => 1305,
                'state_id' => NULL,
                'name' => 'North Miladhunmadhulu',
              ),
            305 => 
            array (
                'id' => 1306,
                'state_id' => NULL,
                'name' => 'North Nilandhe Atoll',
              ),
            306 => 
            array (
                'id' => 1307,
                'state_id' => NULL,
                'name' => 'North Huvadhu Atoll',
              ),
            307 => 
            array (
                'id' => 1308,
                'state_id' => NULL,
                'name' => 'Faadhippolhu',
              ),
            308 => 
            array (
                'id' => 1309,
                'state_id' => NULL,
                'name' => 'Felidhu Atoll',
              ),
            309 => 
            array (
                'id' => 1310,
                'state_id' => NULL,
                'name' => 'Foammulah',
              ),
            310 => 
            array (
                'id' => 1311,
                'state_id' => NULL,
                'name' => 'Hadhdhunmathi',
              ),
            311 => 
            array (
                'id' => 1312,
                'state_id' => NULL,
                'name' => 'Kolhumadulu',
              ),
            312 => 
            array (
                'id' => 1313,
                'state_id' => NULL,
                'name' => 'Male',
              ),
            313 => 
            array (
                'id' => 1314,
                'state_id' => NULL,
                'name' => 'Male Atoll',
              ),
            314 => 
            array (
                'id' => 1315,
                'state_id' => NULL,
                'name' => 'Mulakatholhu',
              ),
            315 => 
            array (
                'id' => 1316,
                'state_id' => NULL,
                'name' => 'South Ari Atoll',
              ),
            316 => 
            array (
                'id' => 1317,
                'state_id' => NULL,
                'name' => 'South Thiladhunmathi',
              ),
            317 => 
            array (
                'id' => 1318,
                'state_id' => NULL,
                'name' => 'South Maalhosmadulu',
              ),
            318 => 
            array (
                'id' => 1319,
                'state_id' => NULL,
                'name' => 'South Miladhunmadhulu',
              ),
            319 => 
            array (
                'id' => 1320,
                'state_id' => NULL,
                'name' => 'South Nilandhe Atoll',
              ),
            320 => 
            array (
                'id' => 1321,
                'state_id' => NULL,
                'name' => 'South Huvadhu Atoll',
              ),
            321 => 
            array (
                'id' => 1322,
                'state_id' => NULL,
                'name' => 'Northern',
            ),
            322 => 
            array (
                'id' => 1323,
                'state_id' => NULL,
                'name' => 'Southern',
            ),
            323 => 
            array (
                'id' => 1324,
                'state_id' => NULL,
                'name' => 'Central',
            ),
            324 => 
            array (
                'id' => 1325,
                'state_id' => NULL,
                'name' => 'Bamako',
            ),
            325 => 
            array (
                'id' => 1326,
                'state_id' => NULL,
                'name' => 'Kidal',
            ),
            326 => 
            array (
                'id' => 1327,
                'state_id' => NULL,
                'name' => 'Gao',
            ),
            327 => 
            array (
                'id' => 1328,
                'state_id' => NULL,
                'name' => 'Kayes',
            ),
            328 => 
            array (
                'id' => 1329,
                'state_id' => NULL,
                'name' => 'Koulikoro',
            ),
            329 => 
            array (
                'id' => 1330,
                'state_id' => NULL,
                'name' => 'Mopti',
            ),
            330 => 
            array (
                'id' => 1331,
                'state_id' => NULL,
                'name' => 'Segou',
            ),
            331 => 
            array (
                'id' => 1332,
                'state_id' => NULL,
                'name' => 'Tombouctou',
            ),
            332 => 
            array (
                'id' => 1333,
                'state_id' => NULL,
                'name' => 'Sikasso',
            ),
            333 => 
            array (
                'id' => 1334,
                'state_id' => NULL,
                'name' => 'Adrar',
            ),
            334 => 
            array (
                'id' => 1335,
                'state_id' => NULL,
                'name' => 'El-Acaba',
            ),
            335 => 
            array (
                'id' => 1336,
                'state_id' => NULL,
                'name' => 'Brakna',
            ),
            336 => 
            array (
                'id' => 1337,
                'state_id' => NULL,
                'name' => 'Hodh el-Gharbi',
            ),
            337 => 
            array (
                'id' => 1338,
                'state_id' => NULL,
                'name' => 'Gorgol',
            ),
            338 => 
            array (
                'id' => 1339,
                'state_id' => NULL,
                'name' => 'Guidimaka',
            ),
            339 => 
            array (
                'id' => 1340,
                'state_id' => NULL,
                'name' => 'Dakhlet Nouadhibou',
            ),
            340 => 
            array (
                'id' => 1341,
                'state_id' => NULL,
                'name' => 'Nouakchott',
            ),
            341 => 
            array (
                'id' => 1342,
                'state_id' => NULL,
                'name' => 'Tagant',
            ),
            342 => 
            array (
                'id' => 1343,
                'state_id' => NULL,
                'name' => 'Trarza',
            ),
            343 => 
            array (
                'id' => 1344,
                'state_id' => NULL,
                'name' => 'Tiris Zemmour',
            ),
            344 => 
            array (
                'id' => 1345,
                'state_id' => NULL,
                'name' => 'Hodh ech-Chargui',
            ),
            345 => 
            array (
                'id' => 1346,
                'state_id' => NULL,
                'name' => 'Inchiri',
            ),
            346 => 
            array (
                'id' => 1347,
                'state_id' => NULL,
                'name' => 'Bayanhongor',
            ),
            347 => 
            array (
                'id' => 1348,
                'state_id' => NULL,
                'name' => 'Bayan-Ulgiy',
            ),
            348 => 
            array (
                'id' => 1349,
                'state_id' => NULL,
                'name' => 'Bulgan',
            ),
            349 => 
            array (
                'id' => 1350,
                'state_id' => NULL,
                'name' => 'Darhan-Uul',
            ),
            350 => 
            array (
                'id' => 1351,
                'state_id' => NULL,
                'name' => 'Dornod',
            ),
            351 => 
            array (
                'id' => 1352,
                'state_id' => NULL,
                'name' => 'Dornogovi',
            ),
            352 => 
            array (
                'id' => 1353,
                'state_id' => NULL,
                'name' => 'Orhon',
            ),
            353 => 
            array (
                'id' => 1354,
                'state_id' => NULL,
                'name' => 'Govi-Altay',
            ),
            354 => 
            array (
                'id' => 1355,
                'state_id' => NULL,
                'name' => 'Govisumber',
            ),
            355 => 
            array (
                'id' => 1356,
                'state_id' => NULL,
                'name' => 'Arhangay',
            ),
            356 => 
            array (
                'id' => 1357,
                'state_id' => NULL,
                'name' => 'Hovd',
            ),
            357 => 
            array (
                'id' => 1358,
                'state_id' => NULL,
                'name' => 'Hentiy',
            ),
            358 => 
            array (
                'id' => 1359,
                'state_id' => NULL,
                'name' => 'Hovsgol',
            ),
            359 => 
            array (
                'id' => 1360,
                'state_id' => NULL,
                'name' => 'Umnogovi',
              ),
            360 => 
            array (
                'id' => 1361,
                'state_id' => NULL,
                'name' => 'Uvorhangay',
              ),
            361 => 
            array (
                'id' => 1362,
                'state_id' => NULL,
                'name' => 'Selenge',
            ),
            362 => 
            array (
                'id' => 1363,
                'state_id' => NULL,
                'name' => 'Suhbaatar',
            ),
            363 => 
            array (
                'id' => 1364,
                'state_id' => NULL,
                'name' => 'Uvs',
            ),
            364 => 
            array (
                'id' => 1365,
                'state_id' => NULL,
                'name' => 'Ulaanbaatar hot',
            ),
            365 => 
            array (
                'id' => 1366,
                'state_id' => NULL,
                'name' => 'Dzavhan',
            ),
            366 => 
            array (
                'id' => 1367,
                'state_id' => NULL,
                'name' => 'Dundgovi',
            ),
            367 => 
            array (
                'id' => 1368,
                'state_id' => NULL,
                'name' => 'Tov',
            ),
            368 => 
            array (
                'id' => 1369,
                'state_id' => NULL,
                'name' => 'Dhaka',
              ),
            369 => 
            array (
                'id' => 1370,
                'state_id' => NULL,
                'name' => 'Chittagong',
              ),
            370 => 
            array (
                'id' => 1371,
                'state_id' => NULL,
                'name' => 'Khulna',
              ),
            371 => 
            array (
                'id' => 1372,
                'state_id' => NULL,
                'name' => 'Arequipa',
            ),
            372 => 
            array (
                'id' => 1373,
                'state_id' => NULL,
                'name' => 'Apurimac',
            ),
            373 => 
            array (
                'id' => 1374,
                'state_id' => NULL,
                'name' => 'Ayacucho',
            ),
            374 => 
            array (
                'id' => 1375,
                'state_id' => NULL,
                'name' => 'Ancash',
            ),
            375 => 
            array (
                'id' => 1376,
                'state_id' => NULL,
                'name' => 'Juliaca',
              ),
            376 => 
            array (
                'id' => 1377,
                'state_id' => NULL,
                'name' => 'Junin',
            ),
            377 => 
            array (
                'id' => 1378,
                'state_id' => NULL,
                'name' => 'Cajamarca',
            ),
            378 => 
            array (
                'id' => 1379,
                'state_id' => NULL,
                'name' => 'Callao',
            ),
            379 => 
            array (
                'id' => 1380,
                'state_id' => NULL,
                'name' => 'Cusco',
            ),
            380 => 
            array (
                'id' => 1381,
                'state_id' => NULL,
                'name' => 'La Libertad',
            ),
            381 => 
            array (
                'id' => 1382,
                'state_id' => NULL,
                'name' => 'Lambayeque',
            ),
            382 => 
            array (
                'id' => 1383,
                'state_id' => NULL,
                'name' => 'Lima',
            ),
            383 => 
            array (
                'id' => 1384,
                'state_id' => NULL,
                'name' => 'Loreto',
            ),
            384 => 
            array (
                'id' => 1385,
                'state_id' => NULL,
                'name' => 'Madre de Dios',
            ),
            385 => 
            array (
                'id' => 1386,
                'state_id' => NULL,
                'name' => 'Moquegua',
            ),
            386 => 
            array (
                'id' => 1387,
                'state_id' => NULL,
                'name' => 'Pasco',
            ),
            387 => 
            array (
                'id' => 1388,
                'state_id' => NULL,
                'name' => 'Piura',
            ),
            388 => 
            array (
                'id' => 1389,
                'state_id' => NULL,
                'name' => 'Puno',
            ),
            389 => 
            array (
                'id' => 1390,
                'state_id' => NULL,
                'name' => 'Chimbote',
              ),
            390 => 
            array (
                'id' => 1391,
                'state_id' => NULL,
                'name' => 'Chincha Alta',
              ),
            391 => 
            array (
                'id' => 1392,
                'state_id' => NULL,
                'name' => 'San Martin',
            ),
            392 => 
            array (
                'id' => 1393,
                'state_id' => NULL,
                'name' => 'Sullana',
              ),
            393 => 
            array (
                'id' => 1394,
                'state_id' => NULL,
                'name' => 'Tacna',
            ),
            394 => 
            array (
                'id' => 1395,
                'state_id' => NULL,
                'name' => 'Tumbes',
            ),
            395 => 
            array (
                'id' => 1396,
                'state_id' => NULL,
                'name' => 'Huanuco',
            ),
            396 => 
            array (
                'id' => 1397,
                'state_id' => NULL,
                'name' => 'Huancavelica',
            ),
            397 => 
            array (
                'id' => 1398,
                'state_id' => NULL,
                'name' => 'Ucayali',
            ),
            398 => 
            array (
                'id' => 1399,
                'state_id' => NULL,
                'name' => 'Amazonas',
            ),
            399 => 
            array (
                'id' => 1400,
                'state_id' => NULL,
                'name' => 'Ica',
            ),
            400 => 
            array (
                'id' => 1401,
                'state_id' => NULL,
                'name' => 'Bago',
            ),
            401 => 
            array (
                'id' => 1402,
                'state_id' => NULL,
                'name' => 'Shan',
            ),
            402 => 
            array (
                'id' => 1403,
                'state_id' => NULL,
                'name' => 'Tanintharyi',
            ),
            403 => 
            array (
                'id' => 1404,
                'state_id' => NULL,
                'name' => 'Kayin',
            ),
            404 => 
            array (
                'id' => 1405,
                'state_id' => NULL,
                'name' => 'Kachin',
            ),
            405 => 
            array (
                'id' => 1406,
                'state_id' => NULL,
                'name' => 'Kayah',
            ),
            406 => 
            array (
                'id' => 1407,
                'state_id' => NULL,
                'name' => 'Magway',
            ),
            407 => 
            array (
                'id' => 1408,
                'state_id' => NULL,
                'name' => 'Mandalay',
            ),
            408 => 
            array (
                'id' => 1409,
                'state_id' => NULL,
                'name' => 'Mon',
            ),
            409 => 
            array (
                'id' => 1410,
                'state_id' => NULL,
                'name' => 'Chin',
            ),
            410 => 
            array (
                'id' => 1411,
                'state_id' => NULL,
                'name' => 'Rakhine',
            ),
            411 => 
            array (
                'id' => 1412,
                'state_id' => NULL,
                'name' => 'Sagaing',
            ),
            412 => 
            array (
                'id' => 1413,
                'state_id' => NULL,
                'name' => 'Yangon',
            ),
            413 => 
            array (
                'id' => 1414,
                'state_id' => NULL,
                'name' => 'Ayeyarwady',
            ),
            414 => 
            array (
                'id' => 1415,
                'state_id' => NULL,
                'name' => 'Tangier',
              ),
            415 => 
            array (
                'id' => 1416,
                'state_id' => NULL,
                'name' => 'Tetouan',
              ),
            416 => 
            array (
                'id' => 1417,
                'state_id' => NULL,
                'name' => 'Fes',
              ),
            417 => 
            array (
                'id' => 1418,
                'state_id' => NULL,
                'name' => 'Casablanca',
              ),
            418 => 
            array (
                'id' => 1419,
                'state_id' => NULL,
                'name' => 'Rabat',
              ),
            419 => 
            array (
                'id' => 1420,
                'state_id' => NULL,
                'name' => 'Marrakech',
              ),
            420 => 
            array (
                'id' => 1421,
                'state_id' => NULL,
                'name' => 'Meknes',
              ),
            421 => 
            array (
                'id' => 1422,
                'state_id' => NULL,
                'name' => 'Oujda',
              ),
            422 => 
            array (
                'id' => 1423,
                'state_id' => NULL,
                'name' => 'Western Sahara',
              ),
            423 => 
            array (
                'id' => 1424,
                'state_id' => NULL,
                'name' => 'Aguascalientes',
              ),
            424 => 
            array (
                'id' => 1425,
                'state_id' => NULL,
                'name' => 'Acapulco',
              ),
            425 => 
            array (
                'id' => 1426,
                'state_id' => NULL,
                'name' => 'Hermosillo',
              ),
            426 => 
            array (
                'id' => 1427,
                'state_id' => NULL,
                'name' => 'Campeche',
              ),
            427 => 
            array (
                'id' => 1428,
                'state_id' => NULL,
                'name' => 'Obregon',
              ),
            428 => 
            array (
                'id' => 1429,
                'state_id' => NULL,
                'name' => 'Orizaba',
              ),
            429 => 
            array (
                'id' => 1430,
                'state_id' => NULL,
                'name' => 'Valles',
              ),
            430 => 
            array (
                'id' => 1431,
                'state_id' => NULL,
                'name' => 'Puerto Vallarta',
              ),
            431 => 
            array (
                'id' => 1432,
                'state_id' => NULL,
                'name' => 'Villahermosa',
              ),
            432 => 
            array (
                'id' => 1433,
                'state_id' => NULL,
                'name' => 'Poza Rica de Hidalgo',
              ),
            433 => 
            array (
                'id' => 1434,
                'state_id' => NULL,
                'name' => 'Tijuana',
              ),
            434 => 
            array (
                'id' => 1435,
                'state_id' => NULL,
                'name' => 'Durango',
              ),
            435 => 
            array (
                'id' => 1436,
                'state_id' => NULL,
                'name' => 'Ensenada',
              ),
            436 => 
            array (
                'id' => 1437,
                'state_id' => NULL,
                'name' => 'Guadalajara',
              ),
            437 => 
            array (
                'id' => 1438,
                'state_id' => NULL,
                'name' => 'Guanajuato',
              ),
            438 => 
            array (
                'id' => 1439,
                'state_id' => NULL,
                'name' => 'Jalapa',
              ),
            439 => 
            array (
                'id' => 1440,
                'state_id' => NULL,
                'name' => 'Juarez',
              ),
            440 => 
            array (
                'id' => 1441,
                'state_id' => NULL,
                'name' => 'Benito Juare',
              ),
            441 => 
            array (
                'id' => 1442,
                'state_id' => NULL,
                'name' => 'Carmen',
              ),
            442 => 
            array (
                'id' => 1443,
                'state_id' => NULL,
                'name' => 'Colima',
              ),
            443 => 
            array (
                'id' => 1444,
                'state_id' => NULL,
                'name' => 'Queretaro',
              ),
            444 => 
            array (
                'id' => 1445,
                'state_id' => NULL,
                'name' => 'Cuernavaca',
              ),
            445 => 
            array (
                'id' => 1446,
                'state_id' => NULL,
                'name' => 'Culiacan',
              ),
            446 => 
            array (
                'id' => 1447,
                'state_id' => NULL,
                'name' => 'Coatzacoalcos',
              ),
            447 => 
            array (
                'id' => 1448,
                'state_id' => NULL,
                'name' => 'La Paz',
              ),
            448 => 
            array (
                'id' => 1449,
                'state_id' => NULL,
                'name' => 'Leon',
              ),
            449 => 
            array (
                'id' => 1450,
                'state_id' => NULL,
                'name' => 'Reynosa',
              ),
            450 => 
            array (
                'id' => 1451,
                'state_id' => NULL,
                'name' => 'Los Mochis',
              ),
            451 => 
            array (
                'id' => 1452,
                'state_id' => NULL,
                'name' => 'Mazatlan',
              ),
            452 => 
            array (
                'id' => 1453,
                'state_id' => NULL,
                'name' => 'Matamoros',
              ),
            453 => 
            array (
                'id' => 1454,
                'state_id' => NULL,
                'name' => 'Merida',
              ),
            454 => 
            array (
                'id' => 1455,
                'state_id' => NULL,
                'name' => 'Monclova',
              ),
            455 => 
            array (
                'id' => 1456,
                'state_id' => NULL,
                'name' => 'Monterrey',
              ),
            456 => 
            array (
                'id' => 1457,
                'state_id' => NULL,
                'name' => 'Morelia',
              ),
            457 => 
            array (
                'id' => 1458,
                'state_id' => NULL,
                'name' => 'Mexico City',
              ),
            458 => 
            array (
                'id' => 1459,
                'state_id' => NULL,
                'name' => 'Mexicali',
              ),
            459 => 
            array (
                'id' => 1460,
                'state_id' => NULL,
                'name' => 'Nogales',
              ),
            460 => 
            array (
                'id' => 1461,
                'state_id' => NULL,
                'name' => 'Pachuca',
              ),
            461 => 
            array (
                'id' => 1462,
                'state_id' => NULL,
                'name' => 'Puebla',
              ),
            462 => 
            array (
                'id' => 1463,
                'state_id' => NULL,
                'name' => 'Chilpancingo',
              ),
            463 => 
            array (
                'id' => 1464,
                'state_id' => NULL,
                'name' => 'Chihuahua',
              ),
            464 => 
            array (
                'id' => 1465,
                'state_id' => NULL,
                'name' => 'Cheturnal',
              ),
            465 => 
            array (
                'id' => 1466,
                'state_id' => NULL,
                'name' => 'Saltillo',
              ),
            466 => 
            array (
                'id' => 1467,
                'state_id' => NULL,
                'name' => 'Zacatecas',
              ),
            467 => 
            array (
                'id' => 1468,
                'state_id' => NULL,
                'name' => 'Celaya',
              ),
            468 => 
            array (
                'id' => 1469,
                'state_id' => NULL,
                'name' => 'San Luis Potosi',
              ),
            469 => 
            array (
                'id' => 1470,
                'state_id' => NULL,
                'name' => 'Tapachula',
              ),
            470 => 
            array (
                'id' => 1471,
                'state_id' => NULL,
                'name' => 'Tampico',
              ),
            471 => 
            array (
                'id' => 1472,
                'state_id' => NULL,
                'name' => 'Tlaxcala',
              ),
            472 => 
            array (
                'id' => 1473,
                'state_id' => NULL,
                'name' => 'Tepic',
              ),
            473 => 
            array (
                'id' => 1474,
                'state_id' => NULL,
                'name' => 'Tehuacan',
              ),
            474 => 
            array (
                'id' => 1475,
                'state_id' => NULL,
                'name' => 'Tuxtla Gutierrez',
              ),
            475 => 
            array (
                'id' => 1476,
                'state_id' => NULL,
                'name' => 'Torreon',
              ),
            476 => 
            array (
                'id' => 1477,
                'state_id' => NULL,
                'name' => 'Toluca',
              ),
            477 => 
            array (
                'id' => 1478,
                'state_id' => NULL,
                'name' => 'Oaxaca',
              ),
            478 => 
            array (
                'id' => 1479,
                'state_id' => NULL,
                'name' => 'Victoria',
              ),
            479 => 
            array (
                'id' => 1480,
                'state_id' => NULL,
                'name' => 'Veracruz',
              ),
            480 => 
            array (
                'id' => 1481,
                'state_id' => NULL,
                'name' => 'Uruapan',
              ),
            481 => 
            array (
                'id' => 1482,
                'state_id' => NULL,
                'name' => 'Nuevo Laredo',
              ),
            482 => 
            array (
                'id' => 1483,
                'state_id' => NULL,
                'name' => 'Irapuato',
              ),
            483 => 
            array (
                'id' => 1484,
                'state_id' => NULL,
                'name' => 'Erongo',
            ),
            484 => 
            array (
                'id' => 1485,
                'state_id' => NULL,
                'name' => 'Ohangwena',
            ),
            485 => 
            array (
                'id' => 1486,
                'state_id' => NULL,
                'name' => 'Okavango',
            ),
            486 => 
            array (
                'id' => 1487,
                'state_id' => NULL,
                'name' => 'Omaheke',
            ),
            487 => 
            array (
                'id' => 1488,
                'state_id' => NULL,
                'name' => 'Omusati',
            ),
            488 => 
            array (
                'id' => 1489,
                'state_id' => NULL,
                'name' => 'Otjozondjupa',
            ),
            489 => 
            array (
                'id' => 1490,
                'state_id' => NULL,
                'name' => 'Oshana',
            ),
            490 => 
            array (
                'id' => 1491,
                'state_id' => NULL,
                'name' => 'Oshikoto',
            ),
            491 => 
            array (
                'id' => 1492,
                'state_id' => NULL,
                'name' => 'Hardap',
            ),
            492 => 
            array (
                'id' => 1493,
                'state_id' => NULL,
                'name' => 'Khomas',
            ),
            493 => 
            array (
                'id' => 1494,
                'state_id' => NULL,
                'name' => 'Karas',
            ),
            494 => 
            array (
                'id' => 1495,
                'state_id' => NULL,
                'name' => 'Caprivi',
            ),
            495 => 
            array (
                'id' => 1496,
                'state_id' => NULL,
                'name' => 'Kunene',
            ),
            496 => 
            array (
                'id' => 1497,
                'state_id' => NULL,
                'name' => 'Upington',
              ),
            497 => 
            array (
                'id' => 1498,
                'state_id' => NULL,
                'name' => 'Mount Ayliff',
              ),
            498 => 
            array (
                'id' => 1499,
                'state_id' => NULL,
                'name' => 'Pietermaritzburg',
              ),
            499 => 
            array (
                'id' => 1500,
                'state_id' => NULL,
                'name' => 'Pietersburg',
              ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1501,
                'state_id' => NULL,
                'name' => 'Pretoria',
              ),
            1 => 
            array (
                'id' => 1502,
                'state_id' => NULL,
                'name' => 'Bisho',
              ),
            2 => 
            array (
                'id' => 1503,
                'state_id' => NULL,
                'name' => 'Bredasdorp',
              ),
            3 => 
            array (
                'id' => 1504,
                'state_id' => NULL,
                'name' => 'Bloemfontein',
              ),
            4 => 
            array (
                'id' => 1505,
                'state_id' => NULL,
                'name' => 'Bronkhorstspruit',
              ),
            5 => 
            array (
                'id' => 1506,
                'state_id' => NULL,
                'name' => 'De Aar',
              ),
            6 => 
            array (
                'id' => 1507,
                'state_id' => NULL,
                'name' => 'Durban',
              ),
            7 => 
            array (
                'id' => 1508,
                'state_id' => NULL,
                'name' => 'Dundee',
              ),
            8 => 
            array (
                'id' => 1509,
                'state_id' => NULL,
                'name' => 'Barkley East',
              ),
            9 => 
            array (
                'id' => 1510,
                'state_id' => NULL,
                'name' => 'East London',
              ),
            10 => 
            array (
                'id' => 1511,
                'state_id' => NULL,
                'name' => 'Vryburg',
              ),
            11 => 
            array (
                'id' => 1512,
                'state_id' => NULL,
                'name' => 'Vereeniging',
              ),
            12 => 
            array (
                'id' => 1513,
                'state_id' => NULL,
                'name' => 'Groblersdal',
              ),
            13 => 
            array (
                'id' => 1514,
                'state_id' => NULL,
                'name' => 'Giyani',
              ),
            14 => 
            array (
                'id' => 1515,
                'state_id' => NULL,
                'name' => 'Kimberley',
              ),
            15 => 
            array (
                'id' => 1516,
                'state_id' => NULL,
                'name' => 'Cape Town',
              ),
            16 => 
            array (
                'id' => 1517,
                'state_id' => NULL,
                'name' => 'Klerksdorp',
              ),
            17 => 
            array (
                'id' => 1518,
                'state_id' => NULL,
                'name' => 'Kuruman',
              ),
            18 => 
            array (
                'id' => 1519,
                'state_id' => NULL,
                'name' => 'Queenstown',
              ),
            19 => 
            array (
                'id' => 1520,
                'state_id' => NULL,
                'name' => 'Ladysmith',
              ),
            20 => 
            array (
                'id' => 1521,
                'state_id' => NULL,
                'name' => 'Randfontein',
              ),
            21 => 
            array (
                'id' => 1522,
                'state_id' => NULL,
                'name' => 'Richards Bay',
              ),
            22 => 
            array (
                'id' => 1523,
                'state_id' => NULL,
                'name' => 'Rustenburg',
              ),
            23 => 
            array (
                'id' => 1524,
                'state_id' => NULL,
                'name' => 'Middelburg',
              ),
            24 => 
            array (
                'id' => 1525,
                'state_id' => NULL,
                'name' => 'Mkuze',
              ),
            25 => 
            array (
                'id' => 1526,
                'state_id' => NULL,
                'name' => 'Moorreesburg',
              ),
            26 => 
            array (
                'id' => 1527,
                'state_id' => NULL,
                'name' => 'Nelspruit',
              ),
            27 => 
            array (
                'id' => 1528,
                'state_id' => NULL,
                'name' => 'Nylstroom',
              ),
            28 => 
            array (
                'id' => 1529,
                'state_id' => NULL,
                'name' => 'Newcastle',
              ),
            29 => 
            array (
                'id' => 1530,
                'state_id' => NULL,
                'name' => 'George',
              ),
            30 => 
            array (
                'id' => 1531,
                'state_id' => NULL,
                'name' => 'Sasolburg',
              ),
            31 => 
            array (
                'id' => 1532,
                'state_id' => NULL,
                'name' => 'Secunda',
              ),
            32 => 
            array (
                'id' => 1533,
                'state_id' => NULL,
                'name' => 'Ixopo',
              ),
            33 => 
            array (
                'id' => 1534,
                'state_id' => NULL,
                'name' => 'Trompsburg',
              ),
            34 => 
            array (
                'id' => 1535,
                'state_id' => NULL,
                'name' => 'Springbok',
              ),
            35 => 
            array (
                'id' => 1536,
                'state_id' => NULL,
                'name' => 'Thulamahashe',
              ),
            36 => 
            array (
                'id' => 1537,
                'state_id' => NULL,
                'name' => 'Thohoyandou',
              ),
            37 => 
            array (
                'id' => 1538,
                'state_id' => NULL,
                'name' => 'Witsieshoek',
              ),
            38 => 
            array (
                'id' => 1539,
                'state_id' => NULL,
                'name' => 'Welkom',
              ),
            39 => 
            array (
                'id' => 1540,
                'state_id' => NULL,
                'name' => 'Ulundi',
              ),
            40 => 
            array (
                'id' => 1541,
                'state_id' => NULL,
                'name' => 'Umtata',
              ),
            41 => 
            array (
                'id' => 1542,
                'state_id' => NULL,
                'name' => 'Worcester',
              ),
            42 => 
            array (
                'id' => 1543,
                'state_id' => NULL,
                'name' => 'Beaufort West',
              ),
            43 => 
            array (
                'id' => 1544,
                'state_id' => NULL,
                'name' => 'Port Shepstone',
              ),
            44 => 
            array (
                'id' => 1545,
                'state_id' => NULL,
                'name' => 'Port Elizabeth',
              ),
            45 => 
            array (
                'id' => 1546,
                'state_id' => NULL,
                'name' => 'Johannesburg',
              ),
            46 => 
            array (
                'id' => 1547,
                'state_id' => NULL,
                'name' => 'Bagmati',
            ),
            47 => 
            array (
                'id' => 1548,
                'state_id' => NULL,
                'name' => 'Dhawalagiri',
            ),
            48 => 
            array (
                'id' => 1549,
                'state_id' => NULL,
                'name' => 'Gandaki',
            ),
            49 => 
            array (
                'id' => 1550,
                'state_id' => NULL,
                'name' => 'Kosi',
            ),
            50 => 
            array (
                'id' => 1551,
                'state_id' => NULL,
                'name' => 'Karnali',
            ),
            51 => 
            array (
                'id' => 1552,
                'state_id' => NULL,
                'name' => 'Janakpur',
            ),
            52 => 
            array (
                'id' => 1553,
                'state_id' => NULL,
                'name' => 'Rapti',
            ),
            53 => 
            array (
                'id' => 1554,
                'state_id' => NULL,
                'name' => 'Lumbini',
            ),
            54 => 
            array (
                'id' => 1555,
                'state_id' => NULL,
                'name' => 'Mahakali',
            ),
            55 => 
            array (
                'id' => 1556,
                'state_id' => NULL,
                'name' => 'Mechi',
            ),
            56 => 
            array (
                'id' => 1557,
                'state_id' => NULL,
                'name' => 'Narayani',
            ),
            57 => 
            array (
                'id' => 1558,
                'state_id' => NULL,
                'name' => 'Bheri',
            ),
            58 => 
            array (
                'id' => 1559,
                'state_id' => NULL,
                'name' => 'Sogarmatha',
            ),
            59 => 
            array (
                'id' => 1560,
                'state_id' => NULL,
                'name' => 'Seti',
            ),
            60 => 
            array (
                'id' => 1561,
                'state_id' => NULL,
                'name' => 'Esteli',
            ),
            61 => 
            array (
                'id' => 1562,
                'state_id' => NULL,
                'name' => 'Atlantico Norte',
            ),
            62 => 
            array (
                'id' => 1563,
                'state_id' => NULL,
                'name' => 'Boaco',
            ),
            63 => 
            array (
                'id' => 1564,
                'state_id' => NULL,
                'name' => 'Granada',
            ),
            64 => 
            array (
                'id' => 1565,
                'state_id' => NULL,
                'name' => 'Carazo',
            ),
            65 => 
            array (
                'id' => 1566,
                'state_id' => NULL,
                'name' => 'Leon',
            ),
            66 => 
            array (
                'id' => 1567,
                'state_id' => NULL,
                'name' => 'Rivas',
            ),
            67 => 
            array (
                'id' => 1568,
                'state_id' => NULL,
                'name' => 'Madriz',
            ),
            68 => 
            array (
                'id' => 1569,
                'state_id' => NULL,
                'name' => 'Managua',
            ),
            69 => 
            array (
                'id' => 1570,
                'state_id' => NULL,
                'name' => 'Masaya',
            ),
            70 => 
            array (
                'id' => 1571,
                'state_id' => NULL,
                'name' => 'Matagalpa',
            ),
            71 => 
            array (
                'id' => 1572,
                'state_id' => NULL,
                'name' => 'Atlantico Sur',
            ),
            72 => 
            array (
                'id' => 1573,
                'state_id' => NULL,
                'name' => 'Chinandega',
            ),
            73 => 
            array (
                'id' => 1574,
                'state_id' => NULL,
                'name' => 'Chontales',
            ),
            74 => 
            array (
                'id' => 1575,
                'state_id' => NULL,
                'name' => 'Rio San Juan',
            ),
            75 => 
            array (
                'id' => 1576,
                'state_id' => NULL,
                'name' => 'Jinotega',
            ),
            76 => 
            array (
                'id' => 1577,
                'state_id' => NULL,
                'name' => 'Nueva Segovia',
            ),
            77 => 
            array (
                'id' => 1578,
                'state_id' => NULL,
                'name' => 'Agadez',
              ),
            78 => 
            array (
                'id' => 1579,
                'state_id' => NULL,
                'name' => 'Diffa',
              ),
            79 => 
            array (
                'id' => 1580,
                'state_id' => NULL,
                'name' => 'Tillaberi',
              ),
            80 => 
            array (
                'id' => 1581,
                'state_id' => NULL,
                'name' => 'Dosso',
              ),
            81 => 
            array (
                'id' => 1582,
                'state_id' => NULL,
                'name' => 'Zinder',
              ),
            82 => 
            array (
                'id' => 1583,
                'state_id' => NULL,
                'name' => 'Maradi',
              ),
            83 => 
            array (
                'id' => 1584,
                'state_id' => NULL,
                'name' => 'Niamey C.U.',
              ),
            84 => 
            array (
                'id' => 1585,
                'state_id' => NULL,
                'name' => 'Tahoua',
              ),
            85 => 
            array (
                'id' => 1586,
                'state_id' => NULL,
                'name' => 'Abuja',
              ),
            86 => 
            array (
                'id' => 1587,
                'state_id' => NULL,
                'name' => 'Ogbomosho',
              ),
            87 => 
            array (
                'id' => 1588,
                'state_id' => NULL,
                'name' => 'Kano',
              ),
            88 => 
            array (
                'id' => 1589,
                'state_id' => NULL,
                'name' => 'Lagos',
              ),
            89 => 
            array (
                'id' => 1590,
                'state_id' => NULL,
                'name' => 'Ibadan',
              ),
            90 => 
            array (
                'id' => 1591,
                'state_id' => NULL,
                'name' => 'Akershus',
            ),
            91 => 
            array (
                'id' => 1592,
                'state_id' => NULL,
                'name' => 'Oppland',
            ),
            92 => 
            array (
                'id' => 1593,
                'state_id' => NULL,
                'name' => 'Oslo',
            ),
            93 => 
            array (
                'id' => 1594,
                'state_id' => NULL,
                'name' => 'Nord-Trondelag',
            ),
            94 => 
            array (
                'id' => 1595,
                'state_id' => NULL,
                'name' => 'Buskerud',
            ),
            95 => 
            array (
                'id' => 1596,
                'state_id' => NULL,
                'name' => 'Aust-Agder',
            ),
            96 => 
            array (
                'id' => 1597,
                'state_id' => NULL,
                'name' => 'Ostfold',
            ),
            97 => 
            array (
                'id' => 1598,
                'state_id' => NULL,
                'name' => 'Finnmark',
            ),
            98 => 
            array (
                'id' => 1599,
                'state_id' => NULL,
                'name' => 'Hedmark',
            ),
            99 => 
            array (
                'id' => 1600,
                'state_id' => NULL,
                'name' => 'Hordaland',
            ),
            100 => 
            array (
                'id' => 1601,
                'state_id' => NULL,
                'name' => 'Rogaland',
            ),
            101 => 
            array (
                'id' => 1602,
                'state_id' => NULL,
                'name' => 'More og Romsdal',
            ),
            102 => 
            array (
                'id' => 1603,
                'state_id' => NULL,
                'name' => 'Sor-Trondelag',
            ),
            103 => 
            array (
                'id' => 1604,
                'state_id' => NULL,
                'name' => 'Nordland',
            ),
            104 => 
            array (
                'id' => 1605,
                'state_id' => NULL,
                'name' => 'Sogn og Fjordane',
            ),
            105 => 
            array (
                'id' => 1606,
                'state_id' => NULL,
                'name' => 'Telemark',
            ),
            106 => 
            array (
                'id' => 1607,
                'state_id' => NULL,
                'name' => 'Troms',
            ),
            107 => 
            array (
                'id' => 1608,
                'state_id' => NULL,
                'name' => 'Vest-Agder',
            ),
            108 => 
            array (
                'id' => 1609,
                'state_id' => NULL,
                'name' => 'Vestfold',
            ),
            109 => 
            array (
                'id' => 1610,
                'state_id' => NULL,
                'name' => 'Alentejo Litoral',
              ),
            110 => 
            array (
                'id' => 1611,
                'state_id' => NULL,
                'name' => 'Pinhal Litoral',
              ),
            111 => 
            array (
                'id' => 1612,
                'state_id' => NULL,
                'name' => 'Porto',
              ),
            112 => 
            array (
                'id' => 1613,
                'state_id' => NULL,
                'name' => 'Douro',
              ),
            113 => 
            array (
                'id' => 1614,
                'state_id' => NULL,
                'name' => 'Entre Douro e Vouga',
              ),
            114 => 
            array (
                'id' => 1615,
                'state_id' => NULL,
                'name' => 'Faro',
              ),
            115 => 
            array (
                'id' => 1616,
                'state_id' => NULL,
                'name' => 'Funchal',
              ),
            116 => 
            array (
                'id' => 1617,
                'state_id' => NULL,
                'name' => 'Cavado',
              ),
            117 => 
            array (
                'id' => 1618,
                'state_id' => NULL,
                'name' => 'Cova da Beira',
              ),
            118 => 
            array (
                'id' => 1619,
                'state_id' => NULL,
                'name' => 'Lisboa',
              ),
            119 => 
            array (
                'id' => 1620,
                'state_id' => NULL,
                'name' => 'Leziria do Tejo',
              ),
            120 => 
            array (
                'id' => 1621,
                'state_id' => NULL,
                'name' => 'Medio Tejo',
              ),
            121 => 
            array (
                'id' => 1622,
                'state_id' => NULL,
                'name' => 'Minho-Lima',
              ),
            122 => 
            array (
                'id' => 1623,
                'state_id' => NULL,
                'name' => 'Beira Interior Norte',
              ),
            123 => 
            array (
                'id' => 1624,
                'state_id' => NULL,
                'name' => 'Beira Interior Sul',
              ),
            124 => 
            array (
                'id' => 1625,
                'state_id' => NULL,
                'name' => 'Pinhal Interior Norte',
              ),
            125 => 
            array (
                'id' => 1626,
                'state_id' => NULL,
                'name' => 'Pinhal Interior Sul',
              ),
            126 => 
            array (
                'id' => 1627,
                'state_id' => NULL,
                'name' => 'Ponta Delgada',
              ),
            127 => 
            array (
                'id' => 1628,
                'state_id' => NULL,
                'name' => 'Peninsula de Setubal',
              ),
            128 => 
            array (
                'id' => 1629,
                'state_id' => NULL,
                'name' => 'Serra da Estrela',
              ),
            129 => 
            array (
                'id' => 1630,
                'state_id' => NULL,
                'name' => 'Alto Alentejo',
              ),
            130 => 
            array (
                'id' => 1631,
                'state_id' => NULL,
                'name' => 'Alto Tros-os-Montes',
              ),
            131 => 
            array (
                'id' => 1632,
                'state_id' => NULL,
                'name' => 'Tamega',
              ),
            132 => 
            array (
                'id' => 1633,
                'state_id' => NULL,
                'name' => 'Ave',
              ),
            133 => 
            array (
                'id' => 1634,
                'state_id' => NULL,
                'name' => 'Oeste',
              ),
            134 => 
            array (
                'id' => 1635,
                'state_id' => NULL,
                'name' => 'Baixo Alentejo',
              ),
            135 => 
            array (
                'id' => 1636,
                'state_id' => NULL,
                'name' => 'Baixo Vouga',
              ),
            136 => 
            array (
                'id' => 1637,
                'state_id' => NULL,
                'name' => 'Baixo Mondego',
              ),
            137 => 
            array (
                'id' => 1638,
                'state_id' => NULL,
                'name' => 'Alentejo Central',
              ),
            138 => 
            array (
                'id' => 1639,
                'state_id' => NULL,
                'name' => 'Ehime',
            ),
            139 => 
            array (
                'id' => 1640,
                'state_id' => NULL,
                'name' => 'Aichi',
            ),
            140 => 
            array (
                'id' => 1641,
                'state_id' => NULL,
                'name' => 'Hokkaido',
            ),
            141 => 
            array (
                'id' => 1642,
                'state_id' => NULL,
                'name' => 'Hyogo',
            ),
            142 => 
            array (
                'id' => 1643,
                'state_id' => NULL,
                'name' => 'Okinawa',
            ),
            143 => 
            array (
                'id' => 1644,
                'state_id' => NULL,
                'name' => 'Ibaraki',
            ),
            144 => 
            array (
                'id' => 1645,
                'state_id' => NULL,
                'name' => 'Osaka',
            ),
            145 => 
            array (
                'id' => 1646,
                'state_id' => NULL,
                'name' => 'Oita',
            ),
            146 => 
            array (
                'id' => 1647,
                'state_id' => NULL,
                'name' => 'Shimane',
            ),
            147 => 
            array (
                'id' => 1648,
                'state_id' => NULL,
                'name' => 'Tokushima',
            ),
            148 => 
            array (
                'id' => 1649,
                'state_id' => NULL,
                'name' => 'Tokyo',
            ),
            149 => 
            array (
                'id' => 1650,
                'state_id' => NULL,
                'name' => 'Fukushima',
            ),
            150 => 
            array (
                'id' => 1651,
                'state_id' => NULL,
                'name' => 'Fukuoka',
            ),
            151 => 
            array (
                'id' => 1652,
                'state_id' => NULL,
                'name' => 'Fukui',
            ),
            152 => 
            array (
                'id' => 1653,
                'state_id' => NULL,
                'name' => 'Toyama',
            ),
            153 => 
            array (
                'id' => 1654,
                'state_id' => NULL,
                'name' => 'Okayama',
            ),
            154 => 
            array (
                'id' => 1655,
                'state_id' => NULL,
                'name' => 'Kochi',
            ),
            155 => 
            array (
                'id' => 1656,
                'state_id' => NULL,
                'name' => 'Miyagi',
            ),
            156 => 
            array (
                'id' => 1657,
                'state_id' => NULL,
                'name' => 'Miyazaki',
            ),
            157 => 
            array (
                'id' => 1658,
                'state_id' => NULL,
                'name' => 'Hiroshima',
            ),
            158 => 
            array (
                'id' => 1659,
                'state_id' => NULL,
                'name' => 'Wakayama',
            ),
            159 => 
            array (
                'id' => 1660,
                'state_id' => NULL,
                'name' => 'Kyoto',
            ),
            160 => 
            array (
                'id' => 1661,
                'state_id' => NULL,
                'name' => 'Shizuoka',
            ),
            161 => 
            array (
                'id' => 1662,
                'state_id' => NULL,
                'name' => 'Tochigi',
            ),
            162 => 
            array (
                'id' => 1663,
                'state_id' => NULL,
                'name' => 'Kagoshima',
            ),
            163 => 
            array (
                'id' => 1664,
                'state_id' => NULL,
                'name' => 'Nara',
            ),
            164 => 
            array (
                'id' => 1665,
                'state_id' => NULL,
                'name' => 'Tottori',
            ),
            165 => 
            array (
                'id' => 1666,
                'state_id' => NULL,
                'name' => 'Gifu',
            ),
            166 => 
            array (
                'id' => 1667,
                'state_id' => NULL,
                'name' => 'Saitama',
            ),
            167 => 
            array (
                'id' => 1668,
                'state_id' => NULL,
                'name' => 'Chiba',
            ),
            168 => 
            array (
                'id' => 1669,
                'state_id' => NULL,
                'name' => 'Aomori',
            ),
            169 => 
            array (
                'id' => 1670,
                'state_id' => NULL,
                'name' => 'Akita',
            ),
            170 => 
            array (
                'id' => 1671,
                'state_id' => NULL,
                'name' => 'Gunma',
            ),
            171 => 
            array (
                'id' => 1672,
                'state_id' => NULL,
                'name' => 'Mie',
            ),
            172 => 
            array (
                'id' => 1673,
                'state_id' => NULL,
                'name' => 'Yamaguchi',
            ),
            173 => 
            array (
                'id' => 1674,
                'state_id' => NULL,
                'name' => 'Yamanashi',
            ),
            174 => 
            array (
                'id' => 1675,
                'state_id' => NULL,
                'name' => 'Yamagata',
            ),
            175 => 
            array (
                'id' => 1676,
                'state_id' => NULL,
                'name' => 'Kanagawa',
            ),
            176 => 
            array (
                'id' => 1677,
                'state_id' => NULL,
                'name' => 'Ishikawa',
            ),
            177 => 
            array (
                'id' => 1678,
                'state_id' => NULL,
                'name' => 'Kagawa',
            ),
            178 => 
            array (
                'id' => 1679,
                'state_id' => NULL,
                'name' => 'Niigata',
            ),
            179 => 
            array (
                'id' => 1680,
                'state_id' => NULL,
                'name' => 'Kumamoto',
            ),
            180 => 
            array (
                'id' => 1681,
                'state_id' => NULL,
                'name' => 'Iwate',
            ),
            181 => 
            array (
                'id' => 1682,
                'state_id' => NULL,
                'name' => 'Nagasaki',
            ),
            182 => 
            array (
                'id' => 1683,
                'state_id' => NULL,
                'name' => 'Nagano',
            ),
            183 => 
            array (
                'id' => 1684,
                'state_id' => NULL,
                'name' => 'Shiga',
            ),
            184 => 
            array (
                'id' => 1685,
                'state_id' => NULL,
                'name' => 'Saga',
            ),
            185 => 
            array (
                'id' => 1686,
                'state_id' => NULL,
                'name' => 'Norrbottens',
            ),
            186 => 
            array (
                'id' => 1687,
                'state_id' => NULL,
                'name' => 'Blekinge',
            ),
            187 => 
            array (
                'id' => 1688,
                'state_id' => NULL,
                'name' => 'Dalarnas',
              ),
            188 => 
            array (
                'id' => 1689,
                'state_id' => NULL,
                'name' => 'Ustergotland',
              ),
            189 => 
            array (
                'id' => 1690,
                'state_id' => NULL,
                'name' => 'Orebro',
            ),
            190 => 
            array (
                'id' => 1691,
                'state_id' => NULL,
                'name' => 'Gotlands',
            ),
            191 => 
            array (
                'id' => 1692,
                'state_id' => NULL,
                'name' => 'Hallands',
            ),
            192 => 
            array (
                'id' => 1693,
                'state_id' => NULL,
                'name' => 'Kalmar',
            ),
            193 => 
            array (
                'id' => 1694,
                'state_id' => NULL,
                'name' => 'Kronobergs',
            ),
            194 => 
            array (
                'id' => 1695,
                'state_id' => NULL,
                'name' => 'Sodermanlands',
            ),
            195 => 
            array (
                'id' => 1696,
                'state_id' => NULL,
                'name' => 'Stockholms',
            ),
            196 => 
            array (
                'id' => 1697,
                'state_id' => NULL,
                'name' => 'Skane',
            ),
            197 => 
            array (
                'id' => 1698,
                'state_id' => NULL,
                'name' => 'Varmlands',
            ),
            198 => 
            array (
                'id' => 1699,
                'state_id' => NULL,
                'name' => 'Uppsala',
            ),
            199 => 
            array (
                'id' => 1700,
                'state_id' => NULL,
                'name' => 'Vasterbottens',
            ),
            200 => 
            array (
                'id' => 1701,
                'state_id' => NULL,
                'name' => 'Vastmanlands',
            ),
            201 => 
            array (
                'id' => 1702,
                'state_id' => NULL,
                'name' => 'Vasternorrlands',
            ),
            202 => 
            array (
                'id' => 1703,
                'state_id' => NULL,
                'name' => 'Vastra Gotalands',
            ),
            203 => 
            array (
                'id' => 1704,
                'state_id' => NULL,
                'name' => 'Jonkopings',
            ),
            204 => 
            array (
                'id' => 1705,
                'state_id' => NULL,
                'name' => 'Gavleborgs',
            ),
            205 => 
            array (
                'id' => 1706,
                'state_id' => NULL,
                'name' => 'Jamtlands',
            ),
            206 => 
            array (
                'id' => 1707,
                'state_id' => NULL,
                'name' => 'Aargau',
            ),
            207 => 
            array (
                'id' => 1708,
                'state_id' => NULL,
                'name' => 'Basel－Sstadt',
            ),
            208 => 
            array (
                'id' => 1709,
                'state_id' => NULL,
                'name' => 'Basel Landschaft',
            ),
            209 => 
            array (
                'id' => 1710,
                'state_id' => NULL,
                'name' => 'Bern',
            ),
            210 => 
            array (
                'id' => 1711,
                'state_id' => NULL,
                'name' => 'Zug',
            ),
            211 => 
            array (
                'id' => 1712,
                'state_id' => NULL,
                'name' => 'Freiburg',
            ),
            212 => 
            array (
                'id' => 1713,
                'state_id' => NULL,
                'name' => 'Glarus',
            ),
            213 => 
            array (
                'id' => 1714,
                'state_id' => NULL,
                'name' => 'Graubünden',
            ),
            214 => 
            array (
                'id' => 1715,
                'state_id' => NULL,
                'name' => 'Luzern',
            ),
            215 => 
            array (
                'id' => 1716,
                'state_id' => NULL,
                'name' => 'Lausanne',
            ),
            216 => 
            array (
                'id' => 1717,
                'state_id' => NULL,
                'name' => 'Neuchatel',
            ),
            217 => 
            array (
                'id' => 1718,
                'state_id' => NULL,
                'name' => 'Appenzell Innerrhodn',
            ),
            218 => 
            array (
                'id' => 1719,
                'state_id' => NULL,
                'name' => 'Geneve',
            ),
            219 => 
            array (
                'id' => 1720,
                'state_id' => NULL,
                'name' => 'Jura',
            ),
            220 => 
            array (
                'id' => 1721,
                'state_id' => NULL,
                'name' => 'Schaffhausen',
            ),
            221 => 
            array (
                'id' => 1722,
                'state_id' => NULL,
                'name' => 'Obwalden',
            ),
            222 => 
            array (
                'id' => 1723,
                'state_id' => NULL,
                'name' => 'St.Gallen',
            ),
            223 => 
            array (
                'id' => 1724,
                'state_id' => NULL,
                'name' => 'Schwyz',
            ),
            224 => 
            array (
                'id' => 1725,
                'state_id' => NULL,
                'name' => 'Zurich',
            ),
            225 => 
            array (
                'id' => 1726,
                'state_id' => NULL,
                'name' => 'Solothurn',
            ),
            226 => 
            array (
                'id' => 1727,
                'state_id' => NULL,
                'name' => 'Ticino',
            ),
            227 => 
            array (
                'id' => 1728,
                'state_id' => NULL,
                'name' => 'Thurgau',
            ),
            228 => 
            array (
                'id' => 1729,
                'state_id' => NULL,
                'name' => 'Wallis',
            ),
            229 => 
            array (
                'id' => 1730,
                'state_id' => NULL,
                'name' => 'Appenzell Ausserrhon',
            ),
            230 => 
            array (
                'id' => 1731,
                'state_id' => NULL,
                'name' => 'Vaud',
            ),
            231 => 
            array (
                'id' => 1732,
                'state_id' => NULL,
                'name' => 'Uri',
            ),
            232 => 
            array (
                'id' => 1733,
                'state_id' => NULL,
                'name' => 'Nidwalden',
            ),
            233 => 
            array (
                'id' => 1734,
                'state_id' => NULL,
                'name' => 'Apopa',
              ),
            234 => 
            array (
                'id' => 1735,
                'state_id' => NULL,
                'name' => 'Ahuachapan',
            ),
            235 => 
            array (
                'id' => 1736,
                'state_id' => NULL,
                'name' => 'Litoral',
            ),
            236 => 
            array (
                'id' => 1737,
                'state_id' => NULL,
                'name' => 'Chalatenango',
            ),
            237 => 
            array (
                'id' => 1738,
                'state_id' => NULL,
                'name' => 'Delgado',
            ),
            238 => 
            array (
                'id' => 1739,
                'state_id' => NULL,
                'name' => 'Kie-Ntem',
            ),
            239 => 
            array (
                'id' => 1740,
                'state_id' => NULL,
                'name' => 'Cabanas',
            ),
            240 => 
            array (
                'id' => 1741,
                'state_id' => NULL,
                'name' => 'Cuscatlan',
            ),
            241 => 
            array (
                'id' => 1742,
                'state_id' => NULL,
                'name' => 'La Paz',
            ),
            242 => 
            array (
                'id' => 1743,
                'state_id' => NULL,
                'name' => 'La Libertad',
            ),
            243 => 
            array (
                'id' => 1744,
                'state_id' => NULL,
                'name' => 'La Union',
            ),
            244 => 
            array (
                'id' => 1745,
                'state_id' => NULL,
                'name' => 'Mejicanos',
              ),
            245 => 
            array (
                'id' => 1746,
                'state_id' => NULL,
                'name' => 'Morazan',
            ),
            246 => 
            array (
                'id' => 1747,
                'state_id' => NULL,
                'name' => 'Santa Ana',
            ),
            247 => 
            array (
                'id' => 1748,
                'state_id' => NULL,
                'name' => 'San Miguel',
            ),
            248 => 
            array (
                'id' => 1749,
                'state_id' => NULL,
                'name' => 'San Salvador',
            ),
            249 => 
            array (
                'id' => 1750,
                'state_id' => NULL,
                'name' => 'San Vicente',
            ),
            250 => 
            array (
                'id' => 1751,
                'state_id' => NULL,
                'name' => 'Sonsonate',
            ),
            251 => 
            array (
                'id' => 1752,
                'state_id' => NULL,
                'name' => 'Soyapango',
              ),
            252 => 
            array (
                'id' => 1753,
                'state_id' => NULL,
                'name' => 'Wele-Nzas',
            ),
            253 => 
            array (
                'id' => 1754,
                'state_id' => NULL,
                'name' => 'Usulutan',
            ),
            254 => 
            array (
                'id' => 1755,
                'state_id' => NULL,
                'name' => 'Ilopango',
            ),
            255 => 
            array (
                'id' => 1756,
                'state_id' => NULL,
                'name' => 'Centro Sur',
            ),
            256 => 
            array (
                'id' => 1757,
                'state_id' => NULL,
                'name' => 'Beograd',
              ),
            257 => 
            array (
                'id' => 1758,
                'state_id' => NULL,
                'name' => 'Podgorica',
              ),
            258 => 
            array (
                'id' => 1759,
                'state_id' => NULL,
                'name' => 'Kragujevac',
              ),
            259 => 
            array (
                'id' => 1760,
                'state_id' => NULL,
                'name' => 'Nis',
              ),
            260 => 
            array (
                'id' => 1761,
                'state_id' => NULL,
                'name' => 'Novi Sad',
              ),
            261 => 
            array (
                'id' => 1762,
                'state_id' => NULL,
                'name' => 'Pristina',
              ),
            262 => 
            array (
                'id' => 1763,
                'state_id' => NULL,
                'name' => 'Subotica',
              ),
            263 => 
            array (
                'id' => 1764,
                'state_id' => NULL,
                'name' => 'Zemun',
              ),
            264 => 
            array (
                'id' => 1765,
                'state_id' => NULL,
                'name' => 'Northern',
            ),
            265 => 
            array (
                'id' => 1766,
                'state_id' => NULL,
                'name' => 'Eastern',
            ),
            266 => 
            array (
                'id' => 1767,
                'state_id' => NULL,
                'name' => 'Southern',
            ),
            267 => 
            array (
                'id' => 1768,
                'state_id' => NULL,
                'name' => 'Western',
            ),
            268 => 
            array (
                'id' => 1769,
                'state_id' => NULL,
                'name' => 'Dakar',
            ),
            269 => 
            array (
                'id' => 1770,
                'state_id' => NULL,
                'name' => 'Fatick',
            ),
            270 => 
            array (
                'id' => 1771,
                'state_id' => NULL,
                'name' => 'Ziguinchor',
            ),
            271 => 
            array (
                'id' => 1772,
                'state_id' => NULL,
                'name' => 'Thies',
            ),
            272 => 
            array (
                'id' => 1773,
                'state_id' => NULL,
                'name' => 'Diourbel',
            ),
            273 => 
            array (
                'id' => 1774,
                'state_id' => NULL,
                'name' => 'Kaolack',
            ),
            274 => 
            array (
                'id' => 1775,
                'state_id' => NULL,
                'name' => 'Kolda',
            ),
            275 => 
            array (
                'id' => 1776,
                'state_id' => NULL,
                'name' => 'Louga',
            ),
            276 => 
            array (
                'id' => 1777,
                'state_id' => NULL,
                'name' => 'Matam',
            ),
            277 => 
            array (
                'id' => 1778,
                'state_id' => NULL,
                'name' => 'Saint-Louis',
            ),
            278 => 
            array (
                'id' => 1779,
                'state_id' => NULL,
                'name' => 'Tambacounda',
            ),
            279 => 
            array (
                'id' => 1780,
                'state_id' => NULL,
                'name' => 'Famagusta',
            ),
            280 => 
            array (
                'id' => 1781,
                'state_id' => NULL,
                'name' => 'Kyrenia',
            ),
            281 => 
            array (
                'id' => 1782,
                'state_id' => NULL,
                'name' => 'Larnaca',
            ),
            282 => 
            array (
                'id' => 1783,
                'state_id' => NULL,
                'name' => 'Limassol',
            ),
            283 => 
            array (
                'id' => 1784,
                'state_id' => NULL,
                'name' => 'Nicosia',
            ),
            284 => 
            array (
                'id' => 1785,
                'state_id' => NULL,
                'name' => 'Pafos',
            ),
            285 => 
            array (
                'id' => 1786,
                'state_id' => NULL,
                'name' => 'Arar',
              ),
            286 => 
            array (
                'id' => 1787,
                'state_id' => NULL,
                'name' => 'Abha',
              ),
            287 => 
            array (
                'id' => 1788,
                'state_id' => NULL,
                'name' => 'Al Bahah',
            ),
            288 => 
            array (
                'id' => 1789,
                'state_id' => NULL,
                'name' => 'Buraydah',
              ),
            289 => 
            array (
                'id' => 1790,
                'state_id' => NULL,
                'name' => 'Dammam',
              ),
            290 => 
            array (
                'id' => 1791,
                'state_id' => NULL,
                'name' => 'Hafar al-Batin',
              ),
            291 => 
            array (
                'id' => 1792,
                'state_id' => NULL,
                'name' => 'Hail',
            ),
            292 => 
            array (
                'id' => 1793,
                'state_id' => NULL,
                'name' => 'Khamis Mushayt',
              ),
            293 => 
            array (
                'id' => 1794,
                'state_id' => NULL,
                'name' => 'Al-Kharj',
              ),
            294 => 
            array (
                'id' => 1795,
                'state_id' => NULL,
                'name' => 'Al-Hufuf',
              ),
            295 => 
            array (
                'id' => 1796,
                'state_id' => NULL,
                'name' => 'Jiddah',
              ),
            296 => 
            array (
                'id' => 1797,
                'state_id' => NULL,
                'name' => 'Jizan',
            ),
            297 => 
            array (
                'id' => 1798,
                'state_id' => NULL,
                'name' => 'Riyad',
            ),
            298 => 
            array (
                'id' => 1799,
                'state_id' => NULL,
                'name' => 'Medina',
              ),
            299 => 
            array (
                'id' => 1800,
                'state_id' => NULL,
                'name' => 'Makkah',
            ),
            300 => 
            array (
                'id' => 1801,
                'state_id' => NULL,
                'name' => 'Al-Mubarraz',
              ),
            301 => 
            array (
                'id' => 1802,
                'state_id' => NULL,
                'name' => 'Najran',
            ),
            302 => 
            array (
                'id' => 1803,
                'state_id' => NULL,
                'name' => 'Sakaka',
              ),
            303 => 
            array (
                'id' => 1804,
                'state_id' => NULL,
                'name' => 'Tabuk',
            ),
            304 => 
            array (
                'id' => 1805,
                'state_id' => NULL,
                'name' => 'At Tarif',
              ),
            305 => 
            array (
                'id' => 1806,
                'state_id' => NULL,
                'name' => 'Yanbu al-Bahr',
              ),
            306 => 
            array (
                'id' => 1807,
                'state_id' => NULL,
                'name' => 'Al-Jubayl',
              ),
            307 => 
            array (
                'id' => 1808,
                'state_id' => NULL,
                'name' => 'Anuradhapura',
              ),
            308 => 
            array (
                'id' => 1809,
                'state_id' => NULL,
                'name' => 'Ampara',
              ),
            309 => 
            array (
                'id' => 1810,
                'state_id' => NULL,
                'name' => 'Badulla',
              ),
            310 => 
            array (
                'id' => 1811,
                'state_id' => NULL,
                'name' => 'Batticaloa',
              ),
            311 => 
            array (
                'id' => 1812,
                'state_id' => NULL,
                'name' => 'Polonnaruwa',
              ),
            312 => 
            array (
                'id' => 1813,
                'state_id' => NULL,
                'name' => 'Hambantota',
              ),
            313 => 
            array (
                'id' => 1814,
                'state_id' => NULL,
                'name' => 'Kilinochchi',
              ),
            314 => 
            array (
                'id' => 1815,
                'state_id' => NULL,
                'name' => 'Galle',
              ),
            315 => 
            array (
                'id' => 1816,
                'state_id' => NULL,
                'name' => 'Gampaha',
              ),
            316 => 
            array (
                'id' => 1817,
                'state_id' => NULL,
                'name' => 'Jaffna',
              ),
            317 => 
            array (
                'id' => 1818,
                'state_id' => NULL,
                'name' => 'Kalutara',
              ),
            318 => 
            array (
                'id' => 1819,
                'state_id' => NULL,
                'name' => 'Kegalle',
              ),
            319 => 
            array (
                'id' => 1820,
                'state_id' => NULL,
                'name' => 'Kandy',
              ),
            320 => 
            array (
                'id' => 1821,
                'state_id' => NULL,
                'name' => 'Colombo',
              ),
            321 => 
            array (
                'id' => 1822,
                'state_id' => NULL,
                'name' => 'Kurunegala',
              ),
            322 => 
            array (
                'id' => 1823,
                'state_id' => NULL,
                'name' => 'Ratnapura',
              ),
            323 => 
            array (
                'id' => 1824,
                'state_id' => NULL,
                'name' => 'Mannar',
              ),
            324 => 
            array (
                'id' => 1825,
                'state_id' => NULL,
                'name' => 'Matale',
              ),
            325 => 
            array (
                'id' => 1826,
                'state_id' => NULL,
                'name' => 'Matara',
              ),
            326 => 
            array (
                'id' => 1827,
                'state_id' => NULL,
                'name' => 'Monaragala',
              ),
            327 => 
            array (
                'id' => 1828,
                'state_id' => NULL,
                'name' => 'Mullathivu',
              ),
            328 => 
            array (
                'id' => 1829,
                'state_id' => NULL,
                'name' => 'Nuwara Eliya',
              ),
            329 => 
            array (
                'id' => 1830,
                'state_id' => NULL,
                'name' => 'Puttalam',
              ),
            330 => 
            array (
                'id' => 1831,
                'state_id' => NULL,
                'name' => 'Trincomalee',
              ),
            331 => 
            array (
                'id' => 1832,
                'state_id' => NULL,
                'name' => 'Vavuniya',
              ),
            332 => 
            array (
                'id' => 1833,
                'state_id' => NULL,
                'name' => 'Banskobystricky',
              ),
            333 => 
            array (
                'id' => 1834,
                'state_id' => NULL,
                'name' => 'Bratislavsky',
              ),
            334 => 
            array (
                'id' => 1835,
                'state_id' => NULL,
                'name' => 'Koricky',
              ),
            335 => 
            array (
                'id' => 1836,
                'state_id' => NULL,
                'name' => 'Nitriansky',
              ),
            336 => 
            array (
                'id' => 1837,
                'state_id' => NULL,
                'name' => 'Prerovsky',
              ),
            337 => 
            array (
                'id' => 1838,
                'state_id' => NULL,
                'name' => 'Rilinsky',
              ),
            338 => 
            array (
                'id' => 1839,
                'state_id' => NULL,
                'name' => 'Trnavsky',
              ),
            339 => 
            array (
                'id' => 1840,
                'state_id' => NULL,
                'name' => 'Trenriansky',
              ),
            340 => 
            array (
                'id' => 1841,
                'state_id' => NULL,
                'name' => 'Obalno-kraska',
              ),
            341 => 
            array (
                'id' => 1842,
                'state_id' => NULL,
                'name' => 'Osrednjeslovenska',
              ),
            342 => 
            array (
                'id' => 1843,
                'state_id' => NULL,
                'name' => 'Podravska',
              ),
            343 => 
            array (
                'id' => 1844,
                'state_id' => NULL,
                'name' => 'Pomurska',
              ),
            344 => 
            array (
                'id' => 1845,
                'state_id' => NULL,
                'name' => 'Dolenjska',
              ),
            345 => 
            array (
                'id' => 1846,
                'state_id' => NULL,
                'name' => 'Gorenjska',
              ),
            346 => 
            array (
                'id' => 1847,
                'state_id' => NULL,
                'name' => 'Goriska',
              ),
            347 => 
            array (
                'id' => 1848,
                'state_id' => NULL,
                'name' => 'Koroska',
              ),
            348 => 
            array (
                'id' => 1849,
                'state_id' => NULL,
                'name' => 'Notranjsko-kraska',
              ),
            349 => 
            array (
                'id' => 1850,
                'state_id' => NULL,
                'name' => 'Savinjska',
              ),
            350 => 
            array (
                'id' => 1851,
                'state_id' => NULL,
                'name' => 'Spodnjeposavska',
              ),
            351 => 
            array (
                'id' => 1852,
                'state_id' => NULL,
                'name' => 'Zasavska',
              ),
            352 => 
            array (
                'id' => 1853,
                'state_id' => NULL,
                'name' => 'Ash-Shamaliyah',
              ),
            353 => 
            array (
                'id' => 1854,
                'state_id' => NULL,
                'name' => 'Al-Istiwaiyah',
              ),
            354 => 
            array (
                'id' => 1855,
                'state_id' => NULL,
                'name' => 'Darfur',
              ),
            355 => 
            array (
                'id' => 1856,
                'state_id' => NULL,
                'name' => 'Ash-Sharqiyah',
              ),
            356 => 
            array (
                'id' => 1857,
                'state_id' => NULL,
                'name' => 'Bahr al-Ghazal',
              ),
            357 => 
            array (
                'id' => 1858,
                'state_id' => NULL,
                'name' => 'Al-Khartum',
              ),
            358 => 
            array (
                'id' => 1859,
                'state_id' => NULL,
                'name' => 'Kurdufan',
              ),
            359 => 
            array (
                'id' => 1860,
                'state_id' => NULL,
                'name' => 'Aali an-Nil',
              ),
            360 => 
            array (
                'id' => 1861,
                'state_id' => NULL,
                'name' => 'Al Wasta',
              ),
            361 => 
            array (
                'id' => 1862,
                'state_id' => NULL,
                'name' => 'Brokopondo',
            ),
            362 => 
            array (
                'id' => 1863,
                'state_id' => NULL,
                'name' => 'Coronie',
            ),
            363 => 
            array (
                'id' => 1864,
                'state_id' => NULL,
                'name' => 'Commewijne',
            ),
            364 => 
            array (
                'id' => 1865,
                'state_id' => NULL,
                'name' => 'Marowijne',
            ),
            365 => 
            array (
                'id' => 1866,
                'state_id' => NULL,
                'name' => 'Nickerie',
            ),
            366 => 
            array (
                'id' => 1867,
                'state_id' => NULL,
                'name' => 'Para',
            ),
            367 => 
            array (
                'id' => 1868,
                'state_id' => NULL,
                'name' => 'Paramaribo',
            ),
            368 => 
            array (
                'id' => 1869,
                'state_id' => NULL,
                'name' => 'Saramacca',
            ),
            369 => 
            array (
                'id' => 1870,
                'state_id' => NULL,
                'name' => 'Wanica',
            ),
            370 => 
            array (
                'id' => 1871,
                'state_id' => NULL,
                'name' => 'Sipaliwini',
            ),
            371 => 
            array (
                'id' => 1872,
                'state_id' => NULL,
                'name' => 'Guadalcanal',
            ),
            372 => 
            array (
                'id' => 1873,
                'state_id' => NULL,
                'name' => 'Honiara',
            ),
            373 => 
            array (
                'id' => 1874,
                'state_id' => NULL,
                'name' => 'Rennell and Bellona',
            ),
            374 => 
            array (
                'id' => 1875,
                'state_id' => NULL,
                'name' => 'Makira',
            ),
            375 => 
            array (
                'id' => 1876,
                'state_id' => NULL,
                'name' => 'Malaita',
            ),
            376 => 
            array (
                'id' => 1877,
                'state_id' => NULL,
                'name' => 'Choiseul',
            ),
            377 => 
            array (
                'id' => 1878,
                'state_id' => NULL,
                'name' => 'Temotu',
            ),
            378 => 
            array (
                'id' => 1879,
                'state_id' => NULL,
                'name' => 'Western',
            ),
            379 => 
            array (
                'id' => 1880,
                'state_id' => NULL,
                'name' => 'Isabel',
            ),
            380 => 
            array (
                'id' => 1881,
                'state_id' => NULL,
                'name' => 'Central Islands',
            ),
            381 => 
            array (
                'id' => 1882,
                'state_id' => NULL,
                'name' => 'Dushanbe',
              ),
            382 => 
            array (
                'id' => 1883,
                'state_id' => NULL,
                'name' => 'Khorugh',
              ),
            383 => 
            array (
                'id' => 1884,
                'state_id' => NULL,
                'name' => 'Kanibadam',
              ),
            384 => 
            array (
                'id' => 1885,
                'state_id' => NULL,
                'name' => 'Kofarnihon',
              ),
            385 => 
            array (
                'id' => 1886,
                'state_id' => NULL,
                'name' => 'Khujand',
              ),
            386 => 
            array (
                'id' => 1887,
                'state_id' => NULL,
                'name' => 'Kurgan-Tjube',
              ),
            387 => 
            array (
                'id' => 1888,
                'state_id' => NULL,
                'name' => 'Kulob',
              ),
            388 => 
            array (
                'id' => 1889,
                'state_id' => NULL,
                'name' => 'Rogun',
              ),
            389 => 
            array (
                'id' => 1890,
                'state_id' => NULL,
                'name' => 'Nurek',
              ),
            390 => 
            array (
                'id' => 1891,
                'state_id' => NULL,
                'name' => 'Pendzhikent',
              ),
            391 => 
            array (
                'id' => 1892,
                'state_id' => NULL,
                'name' => 'Sarband',
              ),
            392 => 
            array (
                'id' => 1893,
                'state_id' => NULL,
                'name' => 'Taboshar',
              ),
            393 => 
            array (
                'id' => 1894,
                'state_id' => NULL,
                'name' => 'Tursunzade',
              ),
            394 => 
            array (
                'id' => 1895,
                'state_id' => NULL,
                'name' => 'Ura-Tjube',
              ),
            395 => 
            array (
                'id' => 1896,
                'state_id' => NULL,
                'name' => 'Isfara',
              ),
            396 => 
            array (
                'id' => 1897,
                'state_id' => NULL,
                'name' => 'Amnat Charoen',
            ),
            397 => 
            array (
                'id' => 1898,
                'state_id' => NULL,
                'name' => 'Prachuap Khiri Khan',
            ),
            398 => 
            array (
                'id' => 1899,
                'state_id' => NULL,
                'name' => 'Pathum Thani',
            ),
            399 => 
            array (
                'id' => 1900,
                'state_id' => NULL,
                'name' => 'Prachin Buri',
            ),
            400 => 
            array (
                'id' => 1901,
                'state_id' => NULL,
                'name' => 'Kanchanaburi',
            ),
            401 => 
            array (
                'id' => 1902,
                'state_id' => NULL,
                'name' => 'Saraburi',
            ),
            402 => 
            array (
                'id' => 1903,
                'state_id' => NULL,
                'name' => 'Pattani',
            ),
            403 => 
            array (
                'id' => 1904,
                'state_id' => NULL,
                'name' => 'Samut Prakan',
            ),
            404 => 
            array (
                'id' => 1905,
                'state_id' => NULL,
                'name' => 'Nakhon Sawan',
            ),
            405 => 
            array (
                'id' => 1906,
                'state_id' => NULL,
                'name' => 'Chachoengsao',
            ),
            406 => 
            array (
                'id' => 1907,
                'state_id' => NULL,
                'name' => 'Phetchabun',
            ),
            407 => 
            array (
                'id' => 1908,
                'state_id' => NULL,
                'name' => 'Phatthalung',
            ),
            408 => 
            array (
                'id' => 1909,
                'state_id' => NULL,
                'name' => 'Chai Nat',
            ),
            409 => 
            array (
                'id' => 1910,
                'state_id' => NULL,
                'name' => 'Chaiyaphum',
            ),
            410 => 
            array (
                'id' => 1911,
                'state_id' => NULL,
                'name' => 'Uttaradit',
            ),
            411 => 
            array (
                'id' => 1912,
                'state_id' => NULL,
                'name' => 'Chumphon',
            ),
            412 => 
            array (
                'id' => 1913,
                'state_id' => NULL,
                'name' => 'Chon Buri',
            ),
            413 => 
            array (
                'id' => 1914,
                'state_id' => NULL,
                'name' => 'Tak',
            ),
            414 => 
            array (
                'id' => 1915,
                'state_id' => NULL,
                'name' => 'Trat',
            ),
            415 => 
            array (
                'id' => 1916,
                'state_id' => NULL,
                'name' => 'Phra Nakhon Si Ayutthaya',
            ),
            416 => 
            array (
                'id' => 1917,
                'state_id' => NULL,
                'name' => 'Trang',
            ),
            417 => 
            array (
                'id' => 1918,
                'state_id' => NULL,
                'name' => 'Phetchaburi',
            ),
            418 => 
            array (
                'id' => 1919,
                'state_id' => NULL,
                'name' => 'Nakhon Pathom',
            ),
            419 => 
            array (
                'id' => 1920,
                'state_id' => NULL,
                'name' => 'Kamphaeng Phet',
            ),
            420 => 
            array (
                'id' => 1921,
                'state_id' => NULL,
                'name' => 'Ang Thong',
            ),
            421 => 
            array (
                'id' => 1922,
                'state_id' => NULL,
                'name' => 'Lop Buri',
            ),
            422 => 
            array (
                'id' => 1923,
                'state_id' => NULL,
                'name' => 'Kalasin',
            ),
            423 => 
            array (
                'id' => 1924,
                'state_id' => NULL,
                'name' => 'Krabi',
            ),
            424 => 
            array (
                'id' => 1925,
                'state_id' => NULL,
                'name' => 'Chanthaburi',
            ),
            425 => 
            array (
                'id' => 1926,
                'state_id' => NULL,
                'name' => 'Khon Kaen',
            ),
            426 => 
            array (
                'id' => 1927,
                'state_id' => NULL,
                'name' => 'Rayong',
            ),
            427 => 
            array (
                'id' => 1928,
                'state_id' => NULL,
                'name' => 'Nong Khai',
            ),
            428 => 
            array (
                'id' => 1929,
                'state_id' => NULL,
                'name' => 'Nong Bua Lamphu',
            ),
            429 => 
            array (
                'id' => 1930,
                'state_id' => NULL,
                'name' => 'Ratchaburi',
            ),
            430 => 
            array (
                'id' => 1931,
                'state_id' => NULL,
                'name' => 'Loei',
            ),
            431 => 
            array (
                'id' => 1932,
                'state_id' => NULL,
                'name' => 'Roi Et',
            ),
            432 => 
            array (
                'id' => 1933,
                'state_id' => NULL,
                'name' => 'Samut Sakhon',
            ),
            433 => 
            array (
                'id' => 1934,
                'state_id' => NULL,
                'name' => 'Ranong',
            ),
            434 => 
            array (
                'id' => 1935,
                'state_id' => NULL,
                'name' => 'Nakhon Si Thammarat',
            ),
            435 => 
            array (
                'id' => 1936,
                'state_id' => NULL,
                'name' => 'Maha Sarakham',
            ),
            436 => 
            array (
                'id' => 1937,
                'state_id' => NULL,
                'name' => 'Bangkok',
            ),
            437 => 
            array (
                'id' => 1938,
                'state_id' => NULL,
                'name' => 'Mukdahan',
            ),
            438 => 
            array (
                'id' => 1939,
                'state_id' => NULL,
                'name' => 'Nakhon Nayok',
            ),
            439 => 
            array (
                'id' => 1940,
                'state_id' => NULL,
                'name' => 'Nakhon Phanom',
            ),
            440 => 
            array (
                'id' => 1941,
                'state_id' => NULL,
                'name' => 'Nan',
            ),
            441 => 
            array (
                'id' => 1942,
                'state_id' => NULL,
                'name' => 'Lamphun',
            ),
            442 => 
            array (
                'id' => 1943,
                'state_id' => NULL,
                'name' => 'Nonthaburi',
            ),
            443 => 
            array (
                'id' => 1944,
                'state_id' => NULL,
                'name' => 'Phrae',
            ),
            444 => 
            array (
                'id' => 1945,
                'state_id' => NULL,
                'name' => 'Phayao',
            ),
            445 => 
            array (
                'id' => 1946,
                'state_id' => NULL,
                'name' => 'Phangnga',
            ),
            446 => 
            array (
                'id' => 1947,
                'state_id' => NULL,
                'name' => 'Phitsanulok',
            ),
            447 => 
            array (
                'id' => 1948,
                'state_id' => NULL,
                'name' => 'Phichit',
            ),
            448 => 
            array (
                'id' => 1949,
                'state_id' => NULL,
                'name' => 'Phuket',
            ),
            449 => 
            array (
                'id' => 1950,
                'state_id' => NULL,
                'name' => 'Chiang Rai',
            ),
            450 => 
            array (
                'id' => 1951,
                'state_id' => NULL,
                'name' => 'Chiang Mai',
            ),
            451 => 
            array (
                'id' => 1952,
                'state_id' => NULL,
                'name' => 'Sakon Nakhon',
            ),
            452 => 
            array (
                'id' => 1953,
                'state_id' => NULL,
                'name' => 'Satun',
            ),
            453 => 
            array (
                'id' => 1954,
                'state_id' => NULL,
                'name' => 'Sa Kaeo',
            ),
            454 => 
            array (
                'id' => 1955,
                'state_id' => NULL,
                'name' => 'Si sa ket',
            ),
            455 => 
            array (
                'id' => 1956,
                'state_id' => NULL,
                'name' => 'Songkhla',
            ),
            456 => 
            array (
                'id' => 1957,
                'state_id' => NULL,
                'name' => 'Sukhothai',
            ),
            457 => 
            array (
                'id' => 1958,
                'state_id' => NULL,
                'name' => 'Surat Thani',
            ),
            458 => 
            array (
                'id' => 1959,
                'state_id' => NULL,
                'name' => 'Surin',
            ),
            459 => 
            array (
                'id' => 1960,
                'state_id' => NULL,
                'name' => 'Suphan Buri',
            ),
            460 => 
            array (
                'id' => 1961,
                'state_id' => NULL,
                'name' => 'Narathiwat',
            ),
            461 => 
            array (
                'id' => 1962,
                'state_id' => NULL,
                'name' => 'Udon Thani',
            ),
            462 => 
            array (
                'id' => 1963,
                'state_id' => NULL,
                'name' => 'Uthai Thani',
            ),
            463 => 
            array (
                'id' => 1964,
                'state_id' => NULL,
                'name' => 'Ubon Ratchathani',
            ),
            464 => 
            array (
                'id' => 1965,
                'state_id' => NULL,
                'name' => 'Buri Ram',
            ),
            465 => 
            array (
                'id' => 1966,
                'state_id' => NULL,
                'name' => 'Sing Buri',
            ),
            466 => 
            array (
                'id' => 1967,
                'state_id' => NULL,
                'name' => 'Yasothon',
            ),
            467 => 
            array (
                'id' => 1968,
                'state_id' => NULL,
                'name' => 'Yala',
            ),
            468 => 
            array (
                'id' => 1969,
                'state_id' => NULL,
                'name' => 'Mae Hong Son',
            ),
            469 => 
            array (
                'id' => 1970,
                'state_id' => NULL,
                'name' => 'Samut Songkhram',
            ),
            470 => 
            array (
                'id' => 1971,
                'state_id' => NULL,
                'name' => 'Arusha',
            ),
            471 => 
            array (
                'id' => 1972,
                'state_id' => NULL,
                'name' => 'Kaskazini Pemba',
            ),
            472 => 
            array (
                'id' => 1973,
                'state_id' => NULL,
                'name' => 'Kusini Pemba',
            ),
            473 => 
            array (
                'id' => 1974,
                'state_id' => NULL,
                'name' => 'Pwani',
            ),
            474 => 
            array (
                'id' => 1975,
                'state_id' => NULL,
                'name' => 'Dar es Salaam',
            ),
            475 => 
            array (
                'id' => 1976,
                'state_id' => NULL,
                'name' => 'Dodoma',
            ),
            476 => 
            array (
                'id' => 1977,
                'state_id' => NULL,
                'name' => 'Kigoma',
            ),
            477 => 
            array (
                'id' => 1978,
                'state_id' => NULL,
                'name' => 'Kagera',
            ),
            478 => 
            array (
                'id' => 1979,
                'state_id' => NULL,
                'name' => 'Lindi',
            ),
            479 => 
            array (
                'id' => 1980,
                'state_id' => NULL,
                'name' => 'Rukwa',
            ),
            480 => 
            array (
                'id' => 1981,
                'state_id' => NULL,
                'name' => 'Ruvuma',
            ),
            481 => 
            array (
                'id' => 1982,
                'state_id' => NULL,
                'name' => 'Mara',
            ),
            482 => 
            array (
                'id' => 1983,
                'state_id' => NULL,
                'name' => 'Manyara',
            ),
            483 => 
            array (
                'id' => 1984,
                'state_id' => NULL,
                'name' => 'Morogoro',
            ),
            484 => 
            array (
                'id' => 1985,
                'state_id' => NULL,
                'name' => 'Mbeya',
            ),
            485 => 
            array (
                'id' => 1986,
                'state_id' => NULL,
                'name' => 'Mtwara',
            ),
            486 => 
            array (
                'id' => 1987,
                'state_id' => NULL,
                'name' => 'Mwanza',
            ),
            487 => 
            array (
                'id' => 1988,
                'state_id' => NULL,
                'name' => 'Kilimanjaro',
            ),
            488 => 
            array (
                'id' => 1989,
                'state_id' => NULL,
                'name' => 'Zanzibar',
            ),
            489 => 
            array (
                'id' => 1990,
                'state_id' => NULL,
                'name' => 'Kaskazini Unguja',
            ),
            490 => 
            array (
                'id' => 1991,
                'state_id' => NULL,
                'name' => 'Kusini Unguja',
            ),
            491 => 
            array (
                'id' => 1992,
                'state_id' => NULL,
                'name' => 'Mjini Magharibi',
            ),
            492 => 
            array (
                'id' => 1993,
                'state_id' => NULL,
                'name' => 'Tabora',
            ),
            493 => 
            array (
                'id' => 1994,
                'state_id' => NULL,
                'name' => 'Tanga',
            ),
            494 => 
            array (
                'id' => 1995,
                'state_id' => NULL,
                'name' => 'Singida',
            ),
            495 => 
            array (
                'id' => 1996,
                'state_id' => NULL,
                'name' => 'Shinyanga',
            ),
            496 => 
            array (
                'id' => 1997,
                'state_id' => NULL,
                'name' => 'Iringa',
            ),
            497 => 
            array (
                'id' => 1998,
                'state_id' => NULL,
                'name' => 'Eua',
            ),
            498 => 
            array (
                'id' => 1999,
                'state_id' => NULL,
                'name' => 'Haapai',
            ),
            499 => 
            array (
                'id' => 2000,
                'state_id' => NULL,
                'name' => 'Niuas',
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 2001,
                'state_id' => NULL,
                'name' => 'Tongatapu',
            ),
            1 => 
            array (
                'id' => 2002,
                'state_id' => NULL,
                'name' => 'Vavau',
            ),
            2 => 
            array (
                'id' => 2003,
                'state_id' => NULL,
                'name' => 'Ariana',
            ),
            3 => 
            array (
                'id' => 2004,
                'state_id' => NULL,
                'name' => 'Beja',
            ),
            4 => 
            array (
                'id' => 2005,
                'state_id' => NULL,
                'name' => 'Ben Arous',
            ),
            5 => 
            array (
                'id' => 2006,
                'state_id' => NULL,
                'name' => 'Bizerte',
            ),
            6 => 
            array (
                'id' => 2007,
                'state_id' => NULL,
                'name' => 'Kebili',
            ),
            7 => 
            array (
                'id' => 2008,
                'state_id' => NULL,
                'name' => 'Gabes',
            ),
            8 => 
            array (
                'id' => 2009,
                'state_id' => NULL,
                'name' => 'Gafsa',
            ),
            9 => 
            array (
                'id' => 2010,
                'state_id' => NULL,
                'name' => 'Jendouba',
            ),
            10 => 
            array (
                'id' => 2011,
                'state_id' => NULL,
                'name' => 'Le Kef',
            ),
            11 => 
            array (
                'id' => 2012,
                'state_id' => NULL,
                'name' => 'Kasserine',
            ),
            12 => 
            array (
                'id' => 2013,
                'state_id' => NULL,
                'name' => 'Kairouan',
            ),
            13 => 
            array (
                'id' => 2014,
                'state_id' => NULL,
                'name' => 'Mahdia',
            ),
            14 => 
            array (
                'id' => 2015,
                'state_id' => NULL,
                'name' => 'Manouba',
            ),
            15 => 
            array (
                'id' => 2016,
                'state_id' => NULL,
                'name' => 'Medenine',
            ),
            16 => 
            array (
                'id' => 2017,
                'state_id' => NULL,
                'name' => 'Monastir',
            ),
            17 => 
            array (
                'id' => 2018,
                'state_id' => NULL,
                'name' => 'Nabeul',
            ),
            18 => 
            array (
                'id' => 2019,
                'state_id' => NULL,
                'name' => 'Sfax',
            ),
            19 => 
            array (
                'id' => 2020,
                'state_id' => NULL,
                'name' => 'Sousse',
            ),
            20 => 
            array (
                'id' => 2021,
                'state_id' => NULL,
                'name' => 'Tataouine',
            ),
            21 => 
            array (
                'id' => 2022,
                'state_id' => NULL,
                'name' => 'Tunis',
            ),
            22 => 
            array (
                'id' => 2023,
                'state_id' => NULL,
                'name' => 'Tozeur',
            ),
            23 => 
            array (
                'id' => 2024,
                'state_id' => NULL,
                'name' => 'Sidi Bouzid',
            ),
            24 => 
            array (
                'id' => 2025,
                'state_id' => NULL,
                'name' => 'Siliana',
            ),
            25 => 
            array (
                'id' => 2026,
                'state_id' => NULL,
                'name' => 'Zaghouan',
            ),
            26 => 
            array (
                'id' => 2027,
                'state_id' => NULL,
                'name' => 'Adana',
              ),
            27 => 
            array (
                'id' => 2028,
                'state_id' => NULL,
                'name' => 'Adiyaman',
              ),
            28 => 
            array (
                'id' => 2029,
                'state_id' => NULL,
                'name' => 'Ardahan',
              ),
            29 => 
            array (
                'id' => 2030,
                'state_id' => NULL,
                'name' => 'Artvin',
              ),
            30 => 
            array (
                'id' => 2031,
                'state_id' => NULL,
                'name' => 'Afyon',
              ),
            31 => 
            array (
                'id' => 2032,
                'state_id' => NULL,
                'name' => 'Aksaray',
              ),
            32 => 
            array (
                'id' => 2033,
                'state_id' => NULL,
                'name' => 'Agri',
              ),
            33 => 
            array (
                'id' => 2034,
                'state_id' => NULL,
                'name' => 'Amasya',
              ),
            34 => 
            array (
                'id' => 2035,
                'state_id' => NULL,
                'name' => 'Edirne',
              ),
            35 => 
            array (
                'id' => 2036,
                'state_id' => NULL,
                'name' => 'Erzincan',
              ),
            36 => 
            array (
                'id' => 2037,
                'state_id' => NULL,
                'name' => 'Erzurum',
              ),
            37 => 
            array (
                'id' => 2038,
                'state_id' => NULL,
                'name' => 'Elazig',
              ),
            38 => 
            array (
                'id' => 2039,
                'state_id' => NULL,
                'name' => 'Eskisehir',
              ),
            39 => 
            array (
                'id' => 2040,
                'state_id' => NULL,
                'name' => 'Aydin',
              ),
            40 => 
            array (
                'id' => 2041,
                'state_id' => NULL,
                'name' => 'Ankara',
              ),
            41 => 
            array (
                'id' => 2042,
                'state_id' => NULL,
                'name' => 'Antalya',
              ),
            42 => 
            array (
                'id' => 2043,
                'state_id' => NULL,
                'name' => 'Ordu',
              ),
            43 => 
            array (
                'id' => 2044,
                'state_id' => NULL,
                'name' => 'Bartin',
              ),
            44 => 
            array (
                'id' => 2045,
                'state_id' => NULL,
                'name' => 'Balikesir',
              ),
            45 => 
            array (
                'id' => 2046,
                'state_id' => NULL,
                'name' => 'Batman',
              ),
            46 => 
            array (
                'id' => 2047,
                'state_id' => NULL,
                'name' => 'Bayburt',
              ),
            47 => 
            array (
                'id' => 2048,
                'state_id' => NULL,
                'name' => 'Bilecik',
              ),
            48 => 
            array (
                'id' => 2049,
                'state_id' => NULL,
                'name' => 'Bitlis',
              ),
            49 => 
            array (
                'id' => 2050,
                'state_id' => NULL,
                'name' => 'Bingol',
              ),
            50 => 
            array (
                'id' => 2051,
                'state_id' => NULL,
                'name' => 'Bolu',
              ),
            51 => 
            array (
                'id' => 2052,
                'state_id' => NULL,
                'name' => 'Burdur',
              ),
            52 => 
            array (
                'id' => 2053,
                'state_id' => NULL,
                'name' => 'Bursa',
              ),
            53 => 
            array (
                'id' => 2054,
                'state_id' => NULL,
                'name' => 'Cankiri',
              ),
            54 => 
            array (
                'id' => 2055,
                'state_id' => NULL,
                'name' => 'Denizli',
              ),
            55 => 
            array (
                'id' => 2056,
                'state_id' => NULL,
                'name' => 'Diyarbakir',
              ),
            56 => 
            array (
                'id' => 2057,
                'state_id' => NULL,
                'name' => 'Van',
              ),
            57 => 
            array (
                'id' => 2058,
                'state_id' => NULL,
                'name' => 'Hakkari',
              ),
            58 => 
            array (
                'id' => 2059,
                'state_id' => NULL,
                'name' => 'Hatay',
              ),
            59 => 
            array (
                'id' => 2060,
                'state_id' => NULL,
                'name' => 'Kilis',
              ),
            60 => 
            array (
                'id' => 2061,
                'state_id' => NULL,
                'name' => 'Giresun',
              ),
            61 => 
            array (
                'id' => 2062,
                'state_id' => NULL,
                'name' => 'Gaziantep',
              ),
            62 => 
            array (
                'id' => 2063,
                'state_id' => NULL,
                'name' => 'Gumushane',
              ),
            63 => 
            array (
                'id' => 2064,
                'state_id' => NULL,
                'name' => 'Kars',
              ),
            64 => 
            array (
                'id' => 2065,
                'state_id' => NULL,
                'name' => 'Kahraman Maras',
              ),
            65 => 
            array (
                'id' => 2066,
                'state_id' => NULL,
                'name' => 'Karabuk',
              ),
            66 => 
            array (
                'id' => 2067,
                'state_id' => NULL,
                'name' => 'Karaman',
              ),
            67 => 
            array (
                'id' => 2068,
                'state_id' => NULL,
                'name' => 'Kastamonu',
              ),
            68 => 
            array (
                'id' => 2069,
                'state_id' => NULL,
                'name' => 'Kayseri',
              ),
            69 => 
            array (
                'id' => 2070,
                'state_id' => NULL,
                'name' => 'Kocaeli',
              ),
            70 => 
            array (
                'id' => 2071,
                'state_id' => NULL,
                'name' => 'Kirklareli',
              ),
            71 => 
            array (
                'id' => 2072,
                'state_id' => NULL,
                'name' => 'Konya',
              ),
            72 => 
            array (
                'id' => 2073,
                'state_id' => NULL,
                'name' => 'Kirsehir',
              ),
            73 => 
            array (
                'id' => 2074,
                'state_id' => NULL,
                'name' => 'Kirikkale',
              ),
            74 => 
            array (
                'id' => 2075,
                'state_id' => NULL,
                'name' => 'Urfa',
              ),
            75 => 
            array (
                'id' => 2076,
                'state_id' => NULL,
                'name' => 'Rize',
              ),
            76 => 
            array (
                'id' => 2077,
                'state_id' => NULL,
                'name' => 'Mardin',
              ),
            77 => 
            array (
                'id' => 2078,
                'state_id' => NULL,
                'name' => 'Malatya',
              ),
            78 => 
            array (
                'id' => 2079,
                'state_id' => NULL,
                'name' => 'Manisa',
              ),
            79 => 
            array (
                'id' => 2080,
                'state_id' => NULL,
                'name' => 'Mugla',
              ),
            80 => 
            array (
                'id' => 2081,
                'state_id' => NULL,
                'name' => 'Mus',
              ),
            81 => 
            array (
                'id' => 2082,
                'state_id' => NULL,
                'name' => 'Nevsehir',
              ),
            82 => 
            array (
                'id' => 2083,
                'state_id' => NULL,
                'name' => 'Nigde',
              ),
            83 => 
            array (
                'id' => 2084,
                'state_id' => NULL,
                'name' => 'Canakkale',
              ),
            84 => 
            array (
                'id' => 2085,
                'state_id' => NULL,
                'name' => 'Corum',
              ),
            85 => 
            array (
                'id' => 2086,
                'state_id' => NULL,
                'name' => 'Kutahya',
              ),
            86 => 
            array (
                'id' => 2087,
                'state_id' => NULL,
                'name' => 'Sakarya',
              ),
            87 => 
            array (
                'id' => 2088,
                'state_id' => NULL,
                'name' => 'Samsun',
              ),
            88 => 
            array (
                'id' => 2089,
                'state_id' => NULL,
                'name' => 'Tekirdag',
              ),
            89 => 
            array (
                'id' => 2090,
                'state_id' => NULL,
                'name' => 'Trabzon',
              ),
            90 => 
            array (
                'id' => 2091,
                'state_id' => NULL,
                'name' => 'Tunceli',
              ),
            91 => 
            array (
                'id' => 2092,
                'state_id' => NULL,
                'name' => 'Tokat',
              ),
            92 => 
            array (
                'id' => 2093,
                'state_id' => NULL,
                'name' => 'Usak',
              ),
            93 => 
            array (
                'id' => 2094,
                'state_id' => NULL,
                'name' => 'Sirnak',
              ),
            94 => 
            array (
                'id' => 2095,
                'state_id' => NULL,
                'name' => 'Siirt',
              ),
            95 => 
            array (
                'id' => 2096,
                'state_id' => NULL,
                'name' => 'Sinop',
              ),
            96 => 
            array (
                'id' => 2097,
                'state_id' => NULL,
                'name' => 'Sivas',
              ),
            97 => 
            array (
                'id' => 2098,
                'state_id' => NULL,
                'name' => 'Igdir',
              ),
            98 => 
            array (
                'id' => 2099,
                'state_id' => NULL,
                'name' => 'Icel',
              ),
            99 => 
            array (
                'id' => 2100,
                'state_id' => NULL,
                'name' => 'Isparta',
              ),
            100 => 
            array (
                'id' => 2101,
                'state_id' => NULL,
                'name' => 'Istanbul',
              ),
            101 => 
            array (
                'id' => 2102,
                'state_id' => NULL,
                'name' => 'Izmir',
              ),
            102 => 
            array (
                'id' => 2103,
                'state_id' => NULL,
                'name' => 'Yozgat',
              ),
            103 => 
            array (
                'id' => 2104,
                'state_id' => NULL,
                'name' => 'Zonguldak',
              ),
            104 => 
            array (
                'id' => 2105,
                'state_id' => NULL,
                'name' => 'Ahal',
            ),
            105 => 
            array (
                'id' => 2106,
                'state_id' => NULL,
                'name' => 'Ashgabat',
              ),
            106 => 
            array (
                'id' => 2107,
                'state_id' => NULL,
                'name' => 'Balkan',
            ),
            107 => 
            array (
                'id' => 2108,
                'state_id' => NULL,
                'name' => 'Dashoguz',
            ),
            108 => 
            array (
                'id' => 2109,
                'state_id' => NULL,
                'name' => 'Lebap',
            ),
            109 => 
            array (
                'id' => 2110,
                'state_id' => NULL,
                'name' => 'Mary',
            ),
            110 => 
            array (
                'id' => 2111,
                'state_id' => NULL,
                'name' => 'Nebitdag',
              ),
            111 => 
            array (
                'id' => 2112,
                'state_id' => NULL,
                'name' => 'Malampa',
            ),
            112 => 
            array (
                'id' => 2113,
                'state_id' => NULL,
                'name' => 'Penama',
            ),
            113 => 
            array (
                'id' => 2114,
                'state_id' => NULL,
                'name' => 'Sanma',
            ),
            114 => 
            array (
                'id' => 2115,
                'state_id' => NULL,
                'name' => 'Tafea',
            ),
            115 => 
            array (
                'id' => 2116,
                'state_id' => NULL,
                'name' => 'Torba',
            ),
            116 => 
            array (
                'id' => 2117,
                'state_id' => NULL,
                'name' => 'Shefa',
            ),
            117 => 
            array (
                'id' => 2118,
                'state_id' => NULL,
                'name' => 'El Progreso',
            ),
            118 => 
            array (
                'id' => 2119,
                'state_id' => NULL,
                'name' => 'Escuintla',
            ),
            119 => 
            array (
                'id' => 2120,
                'state_id' => NULL,
                'name' => 'Jalapa',
            ),
            120 => 
            array (
                'id' => 2121,
                'state_id' => NULL,
                'name' => 'Jutiapa',
            ),
            121 => 
            array (
                'id' => 2122,
                'state_id' => NULL,
                'name' => 'Quiche',
            ),
            122 => 
            array (
                'id' => 2123,
                'state_id' => NULL,
                'name' => 'Quetzaltenango',
            ),
            123 => 
            array (
                'id' => 2124,
                'state_id' => NULL,
                'name' => 'Retalhuleu',
            ),
            124 => 
            array (
                'id' => 2125,
                'state_id' => NULL,
                'name' => 'Mixco',
              ),
            125 => 
            array (
                'id' => 2126,
                'state_id' => NULL,
                'name' => 'Peten',
            ),
            126 => 
            array (
                'id' => 2127,
                'state_id' => NULL,
                'name' => 'Chiquimula',
            ),
            127 => 
            array (
                'id' => 2128,
                'state_id' => NULL,
                'name' => 'Chimaltenango',
            ),
            128 => 
            array (
                'id' => 2129,
                'state_id' => NULL,
                'name' => 'Zacapa',
            ),
            129 => 
            array (
                'id' => 2130,
                'state_id' => NULL,
                'name' => 'Sacatepequez',
            ),
            130 => 
            array (
                'id' => 2131,
                'state_id' => NULL,
                'name' => 'Alta Verapaz',
            ),
            131 => 
            array (
                'id' => 2132,
                'state_id' => NULL,
                'name' => 'Santa Rosa',
            ),
            132 => 
            array (
                'id' => 2133,
                'state_id' => NULL,
                'name' => 'San Marcos',
            ),
            133 => 
            array (
                'id' => 2134,
                'state_id' => NULL,
                'name' => 'Suchitepequez',
            ),
            134 => 
            array (
                'id' => 2135,
                'state_id' => NULL,
                'name' => 'Solola',
            ),
            135 => 
            array (
                'id' => 2136,
                'state_id' => NULL,
                'name' => 'Totonicapan',
            ),
            136 => 
            array (
                'id' => 2137,
                'state_id' => NULL,
                'name' => 'Guatemala',
            ),
            137 => 
            array (
                'id' => 2138,
                'state_id' => NULL,
                'name' => 'Huehuetenango',
            ),
            138 => 
            array (
                'id' => 2139,
                'state_id' => NULL,
                'name' => 'Baja Verapaz',
            ),
            139 => 
            array (
                'id' => 2140,
                'state_id' => NULL,
                'name' => 'Villa Nueva',
              ),
            140 => 
            array (
                'id' => 2141,
                'state_id' => NULL,
                'name' => 'Izabal',
            ),
            141 => 
            array (
                'id' => 2142,
                'state_id' => NULL,
                'name' => 'Aragua',
            ),
            142 => 
            array (
                'id' => 2143,
                'state_id' => NULL,
                'name' => 'Delta Amacuro',
            ),
            143 => 
            array (
                'id' => 2144,
                'state_id' => NULL,
                'name' => 'Apure',
            ),
            144 => 
            array (
                'id' => 2145,
                'state_id' => NULL,
                'name' => 'Anzoategui',
            ),
            145 => 
            array (
                'id' => 2146,
                'state_id' => NULL,
                'name' => 'Barinas',
            ),
            146 => 
            array (
                'id' => 2147,
                'state_id' => NULL,
                'name' => 'Bolivar',
            ),
            147 => 
            array (
                'id' => 2148,
                'state_id' => NULL,
                'name' => 'Portuguesa',
            ),
            148 => 
            array (
                'id' => 2149,
                'state_id' => NULL,
                'name' => 'Falcon',
            ),
            149 => 
            array (
                'id' => 2150,
                'state_id' => NULL,
                'name' => 'Guarico',
            ),
            150 => 
            array (
                'id' => 2151,
                'state_id' => NULL,
                'name' => 'Caracas',
            ),
            151 => 
            array (
                'id' => 2152,
                'state_id' => NULL,
                'name' => 'Carabobo',
            ),
            152 => 
            array (
                'id' => 2153,
                'state_id' => NULL,
                'name' => 'Cojedes',
            ),
            153 => 
            array (
                'id' => 2154,
                'state_id' => NULL,
                'name' => 'Lara',
            ),
            154 => 
            array (
                'id' => 2155,
                'state_id' => NULL,
                'name' => 'Dependencias Federales',
            ),
            155 => 
            array (
                'id' => 2156,
                'state_id' => NULL,
                'name' => 'Merida',
            ),
            156 => 
            array (
                'id' => 2157,
                'state_id' => NULL,
                'name' => 'Miranda',
            ),
            157 => 
            array (
                'id' => 2158,
                'state_id' => NULL,
                'name' => 'Monagas',
            ),
            158 => 
            array (
                'id' => 2159,
                'state_id' => NULL,
                'name' => 'Sucre',
            ),
            159 => 
            array (
                'id' => 2160,
                'state_id' => NULL,
                'name' => 'Zulia',
            ),
            160 => 
            array (
                'id' => 2161,
                'state_id' => NULL,
                'name' => 'Tachira',
            ),
            161 => 
            array (
                'id' => 2162,
                'state_id' => NULL,
                'name' => 'Trujillo',
            ),
            162 => 
            array (
                'id' => 2163,
                'state_id' => NULL,
                'name' => 'Estado Nueva Esparta',
            ),
            163 => 
            array (
                'id' => 2164,
                'state_id' => NULL,
                'name' => 'Yaracuy',
            ),
            164 => 
            array (
                'id' => 2165,
                'state_id' => NULL,
                'name' => 'Amazonas',
            ),
            165 => 
            array (
                'id' => 2166,
                'state_id' => NULL,
                'name' => 'Arua',
              ),
            166 => 
            array (
                'id' => 2167,
                'state_id' => NULL,
                'name' => 'Apac',
              ),
            167 => 
            array (
                'id' => 2168,
                'state_id' => NULL,
                'name' => 'Adjumani',
              ),
            168 => 
            array (
                'id' => 2169,
                'state_id' => NULL,
                'name' => 'Bundibugyo',
              ),
            169 => 
            array (
                'id' => 2170,
                'state_id' => NULL,
                'name' => 'Bugiri',
              ),
            170 => 
            array (
                'id' => 2171,
                'state_id' => NULL,
                'name' => 'Busia',
              ),
            171 => 
            array (
                'id' => 2172,
                'state_id' => NULL,
                'name' => 'Bushenyi',
              ),
            172 => 
            array (
                'id' => 2173,
                'state_id' => NULL,
                'name' => 'Ntungamo',
              ),
            173 => 
            array (
                'id' => 2174,
                'state_id' => NULL,
                'name' => 'Gulu',
              ),
            174 => 
            array (
                'id' => 2175,
                'state_id' => NULL,
                'name' => 'Hoima',
              ),
            175 => 
            array (
                'id' => 2176,
                'state_id' => NULL,
                'name' => 'Kibaale',
              ),
            176 => 
            array (
                'id' => 2177,
                'state_id' => NULL,
                'name' => 'Kiboga',
              ),
            177 => 
            array (
                'id' => 2178,
                'state_id' => NULL,
                'name' => 'Kyenjojo',
              ),
            178 => 
            array (
                'id' => 2179,
                'state_id' => NULL,
                'name' => 'Kisoro',
              ),
            179 => 
            array (
                'id' => 2180,
                'state_id' => NULL,
                'name' => 'Kitgum',
              ),
            180 => 
            array (
                'id' => 2181,
                'state_id' => NULL,
                'name' => 'Jinja',
              ),
            181 => 
            array (
                'id' => 2182,
                'state_id' => NULL,
                'name' => 'Kabale',
              ),
            182 => 
            array (
                'id' => 2183,
                'state_id' => NULL,
                'name' => 'Kabarole',
              ),
            183 => 
            array (
                'id' => 2184,
                'state_id' => NULL,
                'name' => 'Kaberamaido',
              ),
            184 => 
            array (
                'id' => 2185,
                'state_id' => NULL,
                'name' => 'Kalangala',
              ),
            185 => 
            array (
                'id' => 2186,
                'state_id' => NULL,
                'name' => 'Kamwenge',
              ),
            186 => 
            array (
                'id' => 2187,
                'state_id' => NULL,
                'name' => 'Kamuli',
              ),
            187 => 
            array (
                'id' => 2188,
                'state_id' => NULL,
                'name' => 'Kanungu',
              ),
            188 => 
            array (
                'id' => 2189,
                'state_id' => NULL,
                'name' => 'Kapchorwa',
              ),
            189 => 
            array (
                'id' => 2190,
                'state_id' => NULL,
                'name' => 'Kasese',
              ),
            190 => 
            array (
                'id' => 2191,
                'state_id' => NULL,
                'name' => 'Katakwi',
              ),
            191 => 
            array (
                'id' => 2192,
                'state_id' => NULL,
                'name' => 'Kayunga',
              ),
            192 => 
            array (
                'id' => 2193,
                'state_id' => NULL,
                'name' => 'Kampala',
              ),
            193 => 
            array (
                'id' => 2194,
                'state_id' => NULL,
                'name' => 'Kotido',
              ),
            194 => 
            array (
                'id' => 2195,
                'state_id' => NULL,
                'name' => 'Kumi',
              ),
            195 => 
            array (
                'id' => 2196,
                'state_id' => NULL,
                'name' => 'Rakai',
              ),
            196 => 
            array (
                'id' => 2197,
                'state_id' => NULL,
                'name' => 'Lira',
              ),
            197 => 
            array (
                'id' => 2198,
                'state_id' => NULL,
                'name' => 'Luwero',
              ),
            198 => 
            array (
                'id' => 2199,
                'state_id' => NULL,
                'name' => 'Rukungiri',
              ),
            199 => 
            array (
                'id' => 2200,
                'state_id' => NULL,
                'name' => 'Masaka',
              ),
            200 => 
            array (
                'id' => 2201,
                'state_id' => NULL,
                'name' => 'Masindi',
              ),
            201 => 
            array (
                'id' => 2202,
                'state_id' => NULL,
                'name' => 'Mayuge',
              ),
            202 => 
            array (
                'id' => 2203,
                'state_id' => NULL,
                'name' => 'Moroto',
              ),
            203 => 
            array (
                'id' => 2204,
                'state_id' => NULL,
                'name' => 'Moyo',
              ),
            204 => 
            array (
                'id' => 2205,
                'state_id' => NULL,
                'name' => 'Mbarara',
              ),
            205 => 
            array (
                'id' => 2206,
                'state_id' => NULL,
                'name' => 'Mbale',
              ),
            206 => 
            array (
                'id' => 2207,
                'state_id' => NULL,
                'name' => 'Mpigi',
              ),
            207 => 
            array (
                'id' => 2208,
                'state_id' => NULL,
                'name' => 'Mubende',
              ),
            208 => 
            array (
                'id' => 2209,
                'state_id' => NULL,
                'name' => 'Mukono',
              ),
            209 => 
            array (
                'id' => 2210,
                'state_id' => NULL,
                'name' => 'Nakapiripirit',
              ),
            210 => 
            array (
                'id' => 2211,
                'state_id' => NULL,
                'name' => 'Nakasongola',
              ),
            211 => 
            array (
                'id' => 2212,
                'state_id' => NULL,
                'name' => 'Nebbi',
              ),
            212 => 
            array (
                'id' => 2213,
                'state_id' => NULL,
                'name' => 'Pader',
              ),
            213 => 
            array (
                'id' => 2214,
                'state_id' => NULL,
                'name' => 'Pallisa',
              ),
            214 => 
            array (
                'id' => 2215,
                'state_id' => NULL,
                'name' => 'Sembabule',
              ),
            215 => 
            array (
                'id' => 2216,
                'state_id' => NULL,
                'name' => 'Soroti',
              ),
            216 => 
            array (
                'id' => 2217,
                'state_id' => NULL,
                'name' => 'Tororo',
              ),
            217 => 
            array (
                'id' => 2218,
                'state_id' => NULL,
                'name' => 'Wakiso',
              ),
            218 => 
            array (
                'id' => 2219,
                'state_id' => NULL,
                'name' => 'Sironko',
              ),
            219 => 
            array (
                'id' => 2220,
                'state_id' => NULL,
                'name' => 'Iganga',
              ),
            220 => 
            array (
                'id' => 2221,
                'state_id' => NULL,
                'name' => 'Yumbe',
              ),
            221 => 
            array (
                'id' => 2222,
                'state_id' => NULL,
                'name' => 'Odessa',
            ),
            222 => 
            array (
                'id' => 2223,
                'state_id' => NULL,
                'name' => 'Poltava',
            ),
            223 => 
            array (
                'id' => 2224,
                'state_id' => NULL,
                'name' => 'Dnipropetrovsk',
            ),
            224 => 
            array (
                'id' => 2225,
                'state_id' => NULL,
                'name' => 'Donetsk',
            ),
            225 => 
            array (
                'id' => 2226,
                'state_id' => NULL,
                'name' => 'Kharkiv',
            ),
            226 => 
            array (
                'id' => 2227,
                'state_id' => NULL,
                'name' => 'Khersonsrka',
            ),
            227 => 
            array (
                'id' => 2228,
                'state_id' => NULL,
                'name' => 'Khmelnytsky',
            ),
            228 => 
            array (
                'id' => 2229,
                'state_id' => NULL,
                'name' => 'Kyiv',
            ),
            229 => 
            array (
                'id' => 2230,
                'state_id' => NULL,
                'name' => 'Kirovohrad',
            ),
            230 => 
            array (
                'id' => 2231,
                'state_id' => NULL,
                'name' => 'Ternopil',
            ),
            231 => 
            array (
                'id' => 2232,
                'state_id' => NULL,
                'name' => 'Respublika Krym',
            ),
            232 => 
            array (
                'id' => 2233,
                'state_id' => NULL,
                'name' => 'Lviv',
            ),
            233 => 
            array (
                'id' => 2234,
                'state_id' => NULL,
                'name' => 'Luhansk',
            ),
            234 => 
            array (
                'id' => 2235,
                'state_id' => NULL,
                'name' => 'Rivne',
            ),
            235 => 
            array (
                'id' => 2236,
                'state_id' => NULL,
                'name' => 'Mykolayiv',
            ),
            236 => 
            array (
                'id' => 2237,
                'state_id' => NULL,
                'name' => 'Cherkasy',
            ),
            237 => 
            array (
                'id' => 2238,
                'state_id' => NULL,
                'name' => 'Chernihiv',
            ),
            238 => 
            array (
                'id' => 2239,
                'state_id' => NULL,
                'name' => 'Chernivtsi',
            ),
            239 => 
            array (
                'id' => 2240,
                'state_id' => NULL,
                'name' => 'Zhytomyr',
            ),
            240 => 
            array (
                'id' => 2241,
                'state_id' => NULL,
                'name' => 'Sumy',
            ),
            241 => 
            array (
                'id' => 2242,
                'state_id' => NULL,
                'name' => 'Zakarpatska',
            ),
            242 => 
            array (
                'id' => 2243,
                'state_id' => NULL,
                'name' => 'Vinnytsya',
            ),
            243 => 
            array (
                'id' => 2244,
                'state_id' => NULL,
                'name' => 'Volyn',
            ),
            244 => 
            array (
                'id' => 2245,
                'state_id' => NULL,
                'name' => 'Ivano-Frankivsk',
            ),
            245 => 
            array (
                'id' => 2246,
                'state_id' => NULL,
                'name' => 'Zaporizhzhya',
            ),
            246 => 
            array (
                'id' => 2247,
                'state_id' => NULL,
                'name' => 'Artigas',
            ),
            247 => 
            array (
                'id' => 2248,
                'state_id' => NULL,
                'name' => 'Durazno',
            ),
            248 => 
            array (
                'id' => 2249,
                'state_id' => NULL,
                'name' => 'Florida',
            ),
            249 => 
            array (
                'id' => 2250,
                'state_id' => NULL,
                'name' => 'Flores',
            ),
            250 => 
            array (
                'id' => 2251,
                'state_id' => NULL,
                'name' => 'Canelones',
            ),
            251 => 
            array (
                'id' => 2252,
                'state_id' => NULL,
                'name' => 'Colonia',
            ),
            252 => 
            array (
                'id' => 2253,
                'state_id' => NULL,
                'name' => 'Lavalleja',
            ),
            253 => 
            array (
                'id' => 2254,
                'state_id' => NULL,
                'name' => 'Rivera',
            ),
            254 => 
            array (
                'id' => 2255,
                'state_id' => NULL,
                'name' => 'Rocha',
            ),
            255 => 
            array (
                'id' => 2256,
                'state_id' => NULL,
                'name' => 'Maldonado',
            ),
            256 => 
            array (
                'id' => 2257,
                'state_id' => NULL,
                'name' => 'Montevideo',
            ),
            257 => 
            array (
                'id' => 2258,
                'state_id' => NULL,
                'name' => 'Rio Negro',
            ),
            258 => 
            array (
                'id' => 2259,
                'state_id' => NULL,
                'name' => 'Paysandu',
            ),
            259 => 
            array (
                'id' => 2260,
                'state_id' => NULL,
                'name' => 'Salto',
            ),
            260 => 
            array (
                'id' => 2261,
                'state_id' => NULL,
                'name' => 'Cerro Largo',
            ),
            261 => 
            array (
                'id' => 2262,
                'state_id' => NULL,
                'name' => 'Treinta y Tres',
            ),
            262 => 
            array (
                'id' => 2263,
                'state_id' => NULL,
                'name' => 'San Jose',
            ),
            263 => 
            array (
                'id' => 2264,
                'state_id' => NULL,
                'name' => 'Soriano',
            ),
            264 => 
            array (
                'id' => 2265,
                'state_id' => NULL,
                'name' => 'Tacuarembo',
              ),
            265 => 
            array (
                'id' => 2266,
                'state_id' => NULL,
                'name' => 'Andijon',
            ),
            266 => 
            array (
                'id' => 2267,
                'state_id' => NULL,
                'name' => 'Buxoro',
            ),
            267 => 
            array (
                'id' => 2268,
                'state_id' => NULL,
                'name' => 'Fargona',
            ),
            268 => 
            array (
                'id' => 2269,
                'state_id' => NULL,
                'name' => 'Xorazm',
            ),
            269 => 
            array (
                'id' => 2270,
                'state_id' => NULL,
                'name' => 'Jizzax',
            ),
            270 => 
            array (
                'id' => 2271,
                'state_id' => NULL,
                'name' => 'Qoraqalpogiston',
            ),
            271 => 
            array (
                'id' => 2272,
                'state_id' => NULL,
                'name' => 'Qasqadaryo',
            ),
            272 => 
            array (
                'id' => 2273,
                'state_id' => NULL,
                'name' => 'Namangan',
            ),
            273 => 
            array (
                'id' => 2274,
                'state_id' => NULL,
                'name' => 'Navoiy',
            ),
            274 => 
            array (
                'id' => 2275,
                'state_id' => NULL,
                'name' => 'Samarqand',
            ),
            275 => 
            array (
                'id' => 2276,
                'state_id' => NULL,
                'name' => 'Surxondaryo',
            ),
            276 => 
            array (
                'id' => 2277,
                'state_id' => NULL,
                'name' => 'Toshkent',
            ),
            277 => 
            array (
                'id' => 2278,
                'state_id' => NULL,
                'name' => 'Toshkent Shahri',
            ),
            278 => 
            array (
                'id' => 2279,
                'state_id' => NULL,
                'name' => 'Sirdaryo',
            ),
            279 => 
            array (
                'id' => 2280,
                'state_id' => NULL,
                'name' => 'Almeria',
              ),
            280 => 
            array (
                'id' => 2281,
                'state_id' => NULL,
                'name' => 'Albacete',
              ),
            281 => 
            array (
                'id' => 2282,
                'state_id' => NULL,
                'name' => 'Alava',
              ),
            282 => 
            array (
                'id' => 2283,
                'state_id' => NULL,
                'name' => 'Alicante',
              ),
            283 => 
            array (
                'id' => 2284,
                'state_id' => NULL,
                'name' => 'Asturias',
              ),
            284 => 
            array (
                'id' => 2285,
                'state_id' => NULL,
                'name' => 'Avila',
              ),
            285 => 
            array (
                'id' => 2286,
                'state_id' => NULL,
                'name' => 'Orense',
              ),
            286 => 
            array (
                'id' => 2287,
                'state_id' => NULL,
                'name' => 'Badajoz',
              ),
            287 => 
            array (
                'id' => 2288,
                'state_id' => NULL,
                'name' => 'Baleares',
              ),
            288 => 
            array (
                'id' => 2289,
                'state_id' => NULL,
                'name' => 'Valladolid',
              ),
            289 => 
            array (
                'id' => 2290,
                'state_id' => NULL,
                'name' => 'Valencia',
              ),
            290 => 
            array (
                'id' => 2291,
                'state_id' => NULL,
                'name' => 'Barcelona',
              ),
            291 => 
            array (
                'id' => 2292,
                'state_id' => NULL,
                'name' => 'Vizcaya',
              ),
            292 => 
            array (
                'id' => 2293,
                'state_id' => NULL,
                'name' => 'Burgos',
              ),
            293 => 
            array (
                'id' => 2294,
                'state_id' => NULL,
                'name' => 'Granada',
              ),
            294 => 
            array (
                'id' => 2295,
                'state_id' => NULL,
                'name' => 'Guadalajara',
              ),
            295 => 
            array (
                'id' => 2296,
                'state_id' => NULL,
                'name' => 'Jaen',
              ),
            296 => 
            array (
                'id' => 2297,
                'state_id' => NULL,
                'name' => 'Gerona',
              ),
            297 => 
            array (
                'id' => 2298,
                'state_id' => NULL,
                'name' => 'Guipuzcoa',
              ),
            298 => 
            array (
                'id' => 2299,
                'state_id' => NULL,
                'name' => 'Cadiz',
              ),
            299 => 
            array (
                'id' => 2300,
                'state_id' => NULL,
                'name' => 'Caceres',
              ),
            300 => 
            array (
                'id' => 2301,
                'state_id' => NULL,
                'name' => 'Cludad Real',
              ),
            301 => 
            array (
                'id' => 2302,
                'state_id' => NULL,
                'name' => 'Castellon',
              ),
            302 => 
            array (
                'id' => 2303,
                'state_id' => NULL,
                'name' => 'Cordoba',
              ),
            303 => 
            array (
                'id' => 2304,
                'state_id' => NULL,
                'name' => 'Cuenca',
              ),
            304 => 
            array (
                'id' => 2305,
                'state_id' => NULL,
                'name' => 'La Coruna',
              ),
            305 => 
            array (
                'id' => 2306,
                'state_id' => NULL,
                'name' => 'La Rioja',
              ),
            306 => 
            array (
                'id' => 2307,
                'state_id' => NULL,
                'name' => 'Las Palmas',
              ),
            307 => 
            array (
                'id' => 2308,
                'state_id' => NULL,
                'name' => 'Leon',
              ),
            308 => 
            array (
                'id' => 2309,
                'state_id' => NULL,
                'name' => 'Lleida',
              ),
            309 => 
            array (
                'id' => 2310,
                'state_id' => NULL,
                'name' => 'Provincia de Lugo',
              ),
            310 => 
            array (
                'id' => 2311,
                'state_id' => NULL,
                'name' => 'Madrid',
              ),
            311 => 
            array (
                'id' => 2312,
                'state_id' => NULL,
                'name' => 'Malaga',
              ),
            312 => 
            array (
                'id' => 2313,
                'state_id' => NULL,
                'name' => 'Murcia',
              ),
            313 => 
            array (
                'id' => 2314,
                'state_id' => NULL,
                'name' => 'Navarra',
              ),
            314 => 
            array (
                'id' => 2315,
                'state_id' => NULL,
                'name' => 'Palencia',
              ),
            315 => 
            array (
                'id' => 2316,
                'state_id' => NULL,
                'name' => 'Provincia de Pontevedra',
              ),
            316 => 
            array (
                'id' => 2317,
                'state_id' => NULL,
                'name' => 'Zaragoza',
              ),
            317 => 
            array (
                'id' => 2318,
                'state_id' => NULL,
                'name' => 'Salamanca',
              ),
            318 => 
            array (
                'id' => 2319,
                'state_id' => NULL,
                'name' => 'Zamora',
              ),
            319 => 
            array (
                'id' => 2320,
                'state_id' => NULL,
                'name' => 'Segovia',
              ),
            320 => 
            array (
                'id' => 2321,
                'state_id' => NULL,
                'name' => 'Sevilla',
              ),
            321 => 
            array (
                'id' => 2322,
                'state_id' => NULL,
                'name' => 'Santander',
              ),
            322 => 
            array (
                'id' => 2323,
                'state_id' => NULL,
                'name' => 'Santa Cruz de Tenerife',
              ),
            323 => 
            array (
                'id' => 2324,
                'state_id' => NULL,
                'name' => 'Soria',
              ),
            324 => 
            array (
                'id' => 2325,
                'state_id' => NULL,
                'name' => 'Tarragona',
              ),
            325 => 
            array (
                'id' => 2326,
                'state_id' => NULL,
                'name' => 'Teruel',
              ),
            326 => 
            array (
                'id' => 2327,
                'state_id' => NULL,
                'name' => 'Toledo',
              ),
            327 => 
            array (
                'id' => 2328,
                'state_id' => NULL,
                'name' => 'Huelva',
              ),
            328 => 
            array (
                'id' => 2329,
                'state_id' => NULL,
                'name' => 'Huesca',
              ),
            329 => 
            array (
                'id' => 2330,
                'state_id' => NULL,
                'name' => 'Peiraievs',
              ),
            330 => 
            array (
                'id' => 2331,
                'state_id' => NULL,
                'name' => 'Dodecanese',
            ),
            331 => 
            array (
                'id' => 2332,
                'state_id' => NULL,
                'name' => 'Chanion',
              ),
            332 => 
            array (
                'id' => 2333,
                'state_id' => NULL,
                'name' => 'Cyclades',
            ),
            333 => 
            array (
                'id' => 2334,
                'state_id' => NULL,
                'name' => 'Lasithiou',
              ),
            334 => 
            array (
                'id' => 2335,
                'state_id' => NULL,
                'name' => 'Lesbos',
              ),
            335 => 
            array (
                'id' => 2336,
                'state_id' => NULL,
                'name' => 'Rethymnis',
              ),
            336 => 
            array (
                'id' => 2337,
                'state_id' => NULL,
                'name' => 'Samos',
              ),
            337 => 
            array (
                'id' => 2338,
                'state_id' => NULL,
                'name' => 'Athens',
              ),
            338 => 
            array (
                'id' => 2339,
                'state_id' => NULL,
                'name' => 'Irakleiou',
              ),
            339 => 
            array (
                'id' => 2340,
                'state_id' => NULL,
                'name' => 'Auckland',
              ),
            340 => 
            array (
                'id' => 2341,
                'state_id' => NULL,
                'name' => 'North Shore',
              ),
            341 => 
            array (
                'id' => 2342,
                'state_id' => NULL,
                'name' => 'Palmerston North',
              ),
            342 => 
            array (
                'id' => 2343,
                'state_id' => NULL,
                'name' => 'Far North',
              ),
            343 => 
            array (
                'id' => 2344,
                'state_id' => NULL,
                'name' => 'Blenheim',
              ),
            344 => 
            array (
                'id' => 2345,
                'state_id' => NULL,
                'name' => 'Dunedin',
              ),
            345 => 
            array (
                'id' => 2346,
                'state_id' => NULL,
                'name' => 'Greymouth',
              ),
            346 => 
            array (
                'id' => 2347,
                'state_id' => NULL,
                'name' => 'Hamilton',
              ),
            347 => 
            array (
                'id' => 2348,
                'state_id' => NULL,
                'name' => 'Hastings',
              ),
            348 => 
            array (
                'id' => 2349,
                'state_id' => NULL,
                'name' => 'Waitakere',
              ),
            349 => 
            array (
                'id' => 2350,
                'state_id' => NULL,
                'name' => 'Gisborne',
              ),
            350 => 
            array (
                'id' => 2351,
                'state_id' => NULL,
                'name' => 'Kaipara',
              ),
            351 => 
            array (
                'id' => 2352,
                'state_id' => NULL,
                'name' => 'Christchurch',
              ),
            352 => 
            array (
                'id' => 2353,
                'state_id' => NULL,
                'name' => 'Richmond',
              ),
            353 => 
            array (
                'id' => 2354,
                'state_id' => NULL,
                'name' => 'Manukau',
              ),
            354 => 
            array (
                'id' => 2355,
                'state_id' => NULL,
                'name' => 'Nelson',
              ),
            355 => 
            array (
                'id' => 2356,
                'state_id' => NULL,
                'name' => 'Napier',
              ),
            356 => 
            array (
                'id' => 2357,
                'state_id' => NULL,
                'name' => 'Stratford',
              ),
            357 => 
            array (
                'id' => 2358,
                'state_id' => NULL,
                'name' => 'Taumarunui',
              ),
            358 => 
            array (
                'id' => 2359,
                'state_id' => NULL,
                'name' => 'Whakatane',
              ),
            359 => 
            array (
                'id' => 2360,
                'state_id' => NULL,
                'name' => 'Whangarei',
              ),
            360 => 
            array (
                'id' => 2361,
                'state_id' => NULL,
                'name' => 'Wanganui',
              ),
            361 => 
            array (
                'id' => 2362,
                'state_id' => NULL,
                'name' => 'New Plymouth',
              ),
            362 => 
            array (
                'id' => 2363,
                'state_id' => NULL,
                'name' => 'Invercargill',
              ),
            363 => 
            array (
                'id' => 2364,
                'state_id' => NULL,
                'name' => 'Baranya',
            ),
            364 => 
            array (
                'id' => 2365,
                'state_id' => NULL,
                'name' => 'Bacs-Kiskun',
            ),
            365 => 
            array (
                'id' => 2366,
                'state_id' => NULL,
                'name' => 'Borsod-Abauj-Zemplen',
            ),
            366 => 
            array (
                'id' => 2367,
                'state_id' => NULL,
                'name' => 'Bekes',
            ),
            367 => 
            array (
                'id' => 2368,
                'state_id' => NULL,
                'name' => 'Budapest',
            ),
            368 => 
            array (
                'id' => 2369,
                'state_id' => NULL,
                'name' => 'Fejer',
            ),
            369 => 
            array (
                'id' => 2370,
                'state_id' => NULL,
                'name' => 'Hajdu-Bihar',
            ),
            370 => 
            array (
                'id' => 2371,
                'state_id' => NULL,
                'name' => 'Heves',
            ),
            371 => 
            array (
                'id' => 2372,
                'state_id' => NULL,
                'name' => 'Jasz-Nagykun-Szolnok',
            ),
            372 => 
            array (
                'id' => 2373,
                'state_id' => NULL,
                'name' => 'Gyor-Moson-Sopron',
            ),
            373 => 
            array (
                'id' => 2374,
                'state_id' => NULL,
                'name' => 'Komarom-Esztergom',
            ),
            374 => 
            array (
                'id' => 2375,
                'state_id' => NULL,
                'name' => 'Nograd',
            ),
            375 => 
            array (
                'id' => 2376,
                'state_id' => NULL,
                'name' => 'Pest',
            ),
            376 => 
            array (
                'id' => 2377,
                'state_id' => NULL,
                'name' => 'Csongrad',
            ),
            377 => 
            array (
                'id' => 2378,
                'state_id' => NULL,
                'name' => 'Somogy',
            ),
            378 => 
            array (
                'id' => 2379,
                'state_id' => NULL,
                'name' => 'Szabolcs-Szatmar-Bereg',
            ),
            379 => 
            array (
                'id' => 2380,
                'state_id' => NULL,
                'name' => 'Tolna',
            ),
            380 => 
            array (
                'id' => 2381,
                'state_id' => NULL,
                'name' => 'Veszprem',
            ),
            381 => 
            array (
                'id' => 2382,
                'state_id' => NULL,
                'name' => 'Vas',
            ),
            382 => 
            array (
                'id' => 2383,
                'state_id' => NULL,
                'name' => 'Zala',
            ),
            383 => 
            array (
                'id' => 2384,
                'state_id' => NULL,
                'name' => 'Halab',
            ),
            384 => 
            array (
                'id' => 2385,
                'state_id' => NULL,
                'name' => 'Rif Dimashq',
            ),
            385 => 
            array (
                'id' => 2386,
                'state_id' => NULL,
                'name' => 'Madinat Dimashq',
            ),
            386 => 
            array (
                'id' => 2387,
                'state_id' => NULL,
                'name' => 'Dayr az Zawr',
            ),
            387 => 
            array (
                'id' => 2388,
                'state_id' => NULL,
                'name' => 'Dara',
            ),
            388 => 
            array (
                'id' => 2389,
                'state_id' => NULL,
                'name' => 'Hamah',
            ),
            389 => 
            array (
                'id' => 2390,
                'state_id' => NULL,
                'name' => 'Al Hasakah',
            ),
            390 => 
            array (
                'id' => 2391,
                'state_id' => NULL,
                'name' => 'Hims',
            ),
            391 => 
            array (
                'id' => 2392,
                'state_id' => NULL,
                'name' => 'Al Ghab',
            ),
            392 => 
            array (
                'id' => 2393,
                'state_id' => NULL,
                'name' => 'Al-Qamishli',
            ),
            393 => 
            array (
                'id' => 2394,
                'state_id' => NULL,
                'name' => 'Al Qunaytirah',
            ),
            394 => 
            array (
                'id' => 2395,
                'state_id' => NULL,
                'name' => 'Ar Raqqah',
            ),
            395 => 
            array (
                'id' => 2396,
                'state_id' => NULL,
                'name' => 'Al Ladhiqiyah',
            ),
            396 => 
            array (
                'id' => 2397,
                'state_id' => NULL,
                'name' => 'As Suwayda',
            ),
            397 => 
            array (
                'id' => 2398,
                'state_id' => NULL,
                'name' => 'Tartus',
            ),
            398 => 
            array (
                'id' => 2399,
                'state_id' => NULL,
                'name' => 'Idlib',
            ),
            399 => 
            array (
                'id' => 2400,
                'state_id' => NULL,
                'name' => 'Portland',
              ),
            400 => 
            array (
                'id' => 2401,
                'state_id' => NULL,
                'name' => 'Hanover',
              ),
            401 => 
            array (
                'id' => 2402,
                'state_id' => NULL,
                'name' => 'Kingston',
              ),
            402 => 
            array (
                'id' => 2403,
                'state_id' => NULL,
                'name' => 'Clarendon',
              ),
            403 => 
            array (
                'id' => 2404,
                'state_id' => NULL,
                'name' => 'Manchester',
              ),
            404 => 
            array (
                'id' => 2405,
                'state_id' => NULL,
                'name' => 'St. Andrews',
              ),
            405 => 
            array (
                'id' => 2406,
                'state_id' => NULL,
                'name' => 'St. Ann',
              ),
            406 => 
            array (
                'id' => 2407,
                'state_id' => NULL,
                'name' => 'St. Catherine',
              ),
            407 => 
            array (
                'id' => 2408,
                'state_id' => NULL,
                'name' => 'St. Mary',
              ),
            408 => 
            array (
                'id' => 2409,
                'state_id' => NULL,
                'name' => 'St. Thomas',
              ),
            409 => 
            array (
                'id' => 2410,
                'state_id' => NULL,
                'name' => 'St. Elizabeth',
              ),
            410 => 
            array (
                'id' => 2411,
                'state_id' => NULL,
                'name' => 'St. James',
              ),
            411 => 
            array (
                'id' => 2412,
                'state_id' => NULL,
                'name' => 'Trelawny',
              ),
            412 => 
            array (
                'id' => 2413,
                'state_id' => NULL,
                'name' => 'Westmoreland',
              ),
            413 => 
            array (
                'id' => 2414,
                'state_id' => NULL,
                'name' => 'Armavir',
              ),
            414 => 
            array (
                'id' => 2415,
                'state_id' => NULL,
                'name' => 'Aragacotn',
              ),
            415 => 
            array (
                'id' => 2416,
                'state_id' => NULL,
                'name' => 'Ararat',
              ),
            416 => 
            array (
                'id' => 2417,
                'state_id' => NULL,
                'name' => 'Yerevan',
              ),
            417 => 
            array (
                'id' => 2418,
                'state_id' => NULL,
                'name' => 'Gelarkunik',
              ),
            418 => 
            array (
                'id' => 2419,
                'state_id' => NULL,
                'name' => 'Kotayk',
              ),
            419 => 
            array (
                'id' => 2420,
                'state_id' => NULL,
                'name' => 'Lorri',
              ),
            420 => 
            array (
                'id' => 2421,
                'state_id' => NULL,
                'name' => 'Tavus',
              ),
            421 => 
            array (
                'id' => 2422,
                'state_id' => NULL,
                'name' => 'VayocJor',
              ),
            422 => 
            array (
                'id' => 2423,
                'state_id' => NULL,
                'name' => 'Shirak',
              ),
            423 => 
            array (
                'id' => 2424,
                'state_id' => NULL,
                'name' => 'Syunik',
              ),
            424 => 
            array (
                'id' => 2425,
                'state_id' => NULL,
                'name' => 'Abyan',
            ),
            425 => 
            array (
                'id' => 2426,
                'state_id' => NULL,
                'name' => 'Amran Sana',
            ),
            426 => 
            array (
                'id' => 2427,
                'state_id' => NULL,
                'name' => 'Al-Bayda',
            ),
            427 => 
            array (
                'id' => 2428,
                'state_id' => NULL,
                'name' => 'Ad-Dali',
            ),
            428 => 
            array (
                'id' => 2429,
                'state_id' => NULL,
                'name' => 'Hadramawt',
            ),
            429 => 
            array (
                'id' => 2430,
                'state_id' => NULL,
                'name' => 'Hajjah',
            ),
            430 => 
            array (
                'id' => 2431,
                'state_id' => NULL,
                'name' => 'Al-Hudaydah',
            ),
            431 => 
            array (
                'id' => 2432,
                'state_id' => NULL,
                'name' => 'Al-Jawf',
            ),
            432 => 
            array (
                'id' => 2433,
                'state_id' => NULL,
                'name' => 'Lahij',
            ),
            433 => 
            array (
                'id' => 2434,
                'state_id' => NULL,
                'name' => 'Marib',
            ),
            434 => 
            array (
                'id' => 2435,
                'state_id' => NULL,
                'name' => 'Al-Mahrah',
            ),
            435 => 
            array (
                'id' => 2436,
                'state_id' => NULL,
                'name' => 'Al-Mahwit',
            ),
            436 => 
            array (
                'id' => 2437,
                'state_id' => NULL,
                'name' => 'Sadah',
            ),
            437 => 
            array (
                'id' => 2438,
                'state_id' => NULL,
                'name' => 'Sana',
            ),
            438 => 
            array (
                'id' => 2439,
                'state_id' => NULL,
                'name' => 'Seiyun',
              ),
            439 => 
            array (
                'id' => 2440,
                'state_id' => NULL,
                'name' => 'Shabwah',
            ),
            440 => 
            array (
                'id' => 2441,
                'state_id' => NULL,
                'name' => 'Taizz',
            ),
            441 => 
            array (
                'id' => 2442,
                'state_id' => NULL,
                'name' => 'Ash-Shihr',
              ),
            442 => 
            array (
                'id' => 2443,
                'state_id' => NULL,
                'name' => 'Adan',
            ),
            443 => 
            array (
                'id' => 2444,
                'state_id' => NULL,
                'name' => 'Ibb',
            ),
            444 => 
            array (
                'id' => 2445,
                'state_id' => NULL,
                'name' => 'Dhamar',
            ),
            445 => 
            array (
                'id' => 2446,
                'state_id' => NULL,
                'name' => 'Ashdod',
              ),
            446 => 
            array (
                'id' => 2447,
                'state_id' => NULL,
                'name' => 'Beersheba',
              ),
            447 => 
            array (
                'id' => 2448,
                'state_id' => NULL,
                'name' => 'Bat Yam',
              ),
            448 => 
            array (
                'id' => 2449,
                'state_id' => NULL,
                'name' => 'Haifa',
              ),
            449 => 
            array (
                'id' => 2450,
                'state_id' => NULL,
                'name' => 'Holon',
              ),
            450 => 
            array (
                'id' => 2451,
                'state_id' => NULL,
                'name' => 'Netanya',
              ),
            451 => 
            array (
                'id' => 2452,
                'state_id' => NULL,
                'name' => 'Tel Aviv-Yafo',
              ),
            452 => 
            array (
                'id' => 2453,
                'state_id' => NULL,
                'name' => 'Jerusalem',
            ),
            453 => 
            array (
                'id' => 2454,
                'state_id' => NULL,
                'name' => 'Asti',
              ),
            454 => 
            array (
                'id' => 2455,
                'state_id' => NULL,
                'name' => 'Ascoli Piceno',
              ),
            455 => 
            array (
                'id' => 2456,
                'state_id' => NULL,
                'name' => 'Ancona',
              ),
            456 => 
            array (
                'id' => 2457,
                'state_id' => NULL,
                'name' => 'Olbia-Tempio',
              ),
            457 => 
            array (
                'id' => 2458,
                'state_id' => NULL,
                'name' => 'Oristano',
              ),
            458 => 
            array (
                'id' => 2459,
                'state_id' => NULL,
                'name' => 'Aosta',
              ),
            459 => 
            array (
                'id' => 2460,
                'state_id' => NULL,
                'name' => 'Palermo',
              ),
            460 => 
            array (
                'id' => 2461,
                'state_id' => NULL,
                'name' => 'Bari',
              ),
            461 => 
            array (
                'id' => 2462,
                'state_id' => NULL,
                'name' => 'Bergamo',
              ),
            462 => 
            array (
                'id' => 2463,
                'state_id' => NULL,
                'name' => 'Benevento',
              ),
            463 => 
            array (
                'id' => 2464,
                'state_id' => NULL,
                'name' => 'Pisa',
              ),
            464 => 
            array (
                'id' => 2465,
                'state_id' => NULL,
                'name' => 'Pordenone',
              ),
            465 => 
            array (
                'id' => 2466,
                'state_id' => NULL,
                'name' => 'Potenza',
              ),
            466 => 
            array (
                'id' => 2467,
                'state_id' => NULL,
                'name' => 'Bologna',
              ),
            467 => 
            array (
                'id' => 2468,
                'state_id' => NULL,
                'name' => 'Biella',
              ),
            468 => 
            array (
                'id' => 2469,
                'state_id' => NULL,
                'name' => 'Brescia',
              ),
            469 => 
            array (
                'id' => 2470,
                'state_id' => NULL,
                'name' => 'Brindisi',
              ),
            470 => 
            array (
                'id' => 2471,
                'state_id' => NULL,
                'name' => 'Trieste',
              ),
            471 => 
            array (
                'id' => 2472,
                'state_id' => NULL,
                'name' => 'Turin',
              ),
            472 => 
            array (
                'id' => 2473,
                'state_id' => NULL,
                'name' => 'Ferrara',
              ),
            473 => 
            array (
                'id' => 2474,
                'state_id' => NULL,
                'name' => 'Firenze',
              ),
            474 => 
            array (
                'id' => 2475,
                'state_id' => NULL,
                'name' => 'Foggia',
              ),
            475 => 
            array (
                'id' => 2476,
                'state_id' => NULL,
                'name' => 'Cagliari',
              ),
            476 => 
            array (
                'id' => 2477,
                'state_id' => NULL,
                'name' => 'Caserta',
              ),
            477 => 
            array (
                'id' => 2478,
                'state_id' => NULL,
                'name' => 'Catania',
              ),
            478 => 
            array (
                'id' => 2479,
                'state_id' => NULL,
                'name' => 'Catanzaro',
              ),
            479 => 
            array (
                'id' => 2480,
                'state_id' => NULL,
                'name' => 'Campobasso',
              ),
            480 => 
            array (
                'id' => 2481,
                'state_id' => NULL,
                'name' => 'Como',
              ),
            481 => 
            array (
                'id' => 2482,
                'state_id' => NULL,
                'name' => 'Cosenza',
              ),
            482 => 
            array (
                'id' => 2483,
                'state_id' => NULL,
                'name' => 'Crotone',
              ),
            483 => 
            array (
                'id' => 2484,
                'state_id' => NULL,
                'name' => 'Cuneo',
              ),
            484 => 
            array (
                'id' => 2485,
                'state_id' => NULL,
                'name' => 'L\'Aquila',
              ),
            485 => 
            array (
                'id' => 2486,
                'state_id' => NULL,
                'name' => 'La Spezia',
              ),
            486 => 
            array (
                'id' => 2487,
                'state_id' => NULL,
                'name' => 'Lecco',
              ),
            487 => 
            array (
                'id' => 2488,
                'state_id' => NULL,
                'name' => 'Lecce',
              ),
            488 => 
            array (
                'id' => 2489,
                'state_id' => NULL,
                'name' => 'Reggio Emilia',
              ),
            489 => 
            array (
                'id' => 2490,
                'state_id' => NULL,
                'name' => 'Reggio Calabria',
              ),
            490 => 
            array (
                'id' => 2491,
                'state_id' => NULL,
                'name' => 'Livorno',
              ),
            491 => 
            array (
                'id' => 2492,
                'state_id' => NULL,
                'name' => 'Roma',
              ),
            492 => 
            array (
                'id' => 2493,
                'state_id' => NULL,
                'name' => 'Massa-Carrara',
              ),
            493 => 
            array (
                'id' => 2494,
                'state_id' => NULL,
                'name' => 'Matera',
              ),
            494 => 
            array (
                'id' => 2495,
                'state_id' => NULL,
                'name' => 'Monza e Brianza',
              ),
            495 => 
            array (
                'id' => 2496,
                'state_id' => NULL,
                'name' => 'Milano',
              ),
            496 => 
            array (
                'id' => 2497,
                'state_id' => NULL,
                'name' => 'Modena',
              ),
            497 => 
            array (
                'id' => 2498,
                'state_id' => NULL,
                'name' => 'Messina',
              ),
            498 => 
            array (
                'id' => 2499,
                'state_id' => NULL,
                'name' => 'Naples',
              ),
            499 => 
            array (
                'id' => 2500,
                'state_id' => NULL,
                'name' => 'Nuoro',
              ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 2501,
                'state_id' => NULL,
                'name' => 'Novara',
              ),
            1 => 
            array (
                'id' => 2502,
                'state_id' => NULL,
                'name' => 'Parma',
              ),
            2 => 
            array (
                'id' => 2503,
                'state_id' => NULL,
                'name' => 'Pavia',
              ),
            3 => 
            array (
                'id' => 2504,
                'state_id' => NULL,
                'name' => 'Perugia',
              ),
            4 => 
            array (
                'id' => 2505,
                'state_id' => NULL,
                'name' => 'Genova',
              ),
            5 => 
            array (
                'id' => 2506,
                'state_id' => NULL,
                'name' => 'Salerno',
              ),
            6 => 
            array (
                'id' => 2507,
                'state_id' => NULL,
                'name' => 'Sassari',
              ),
            7 => 
            array (
                'id' => 2508,
                'state_id' => NULL,
                'name' => 'Savona',
              ),
            8 => 
            array (
                'id' => 2509,
                'state_id' => NULL,
                'name' => 'Taranto',
              ),
            9 => 
            array (
                'id' => 2510,
                'state_id' => NULL,
                'name' => 'Trapani',
              ),
            10 => 
            array (
                'id' => 2511,
                'state_id' => NULL,
                'name' => 'Trento',
              ),
            11 => 
            array (
                'id' => 2512,
                'state_id' => NULL,
                'name' => 'Venice',
              ),
            12 => 
            array (
                'id' => 2513,
                'state_id' => NULL,
                'name' => 'Vercelli',
              ),
            13 => 
            array (
                'id' => 2514,
                'state_id' => NULL,
                'name' => 'Viterbo',
              ),
            14 => 
            array (
                'id' => 2515,
                'state_id' => NULL,
                'name' => 'Udine',
              ),
            15 => 
            array (
                'id' => 2516,
                'state_id' => NULL,
                'name' => 'Syracuse',
              ),
            16 => 
            array (
                'id' => 2517,
                'state_id' => NULL,
                'name' => 'Siena',
              ),
            17 => 
            array (
                'id' => 2518,
                'state_id' => NULL,
                'name' => 'Alessandria',
              ),
            18 => 
            array (
                'id' => 2519,
                'state_id' => NULL,
                'name' => 'Isernia',
              ),
            19 => 
            array (
                'id' => 2520,
                'state_id' => NULL,
                'name' => 'Aizawl',
              ),
            20 => 
            array (
                'id' => 2521,
                'state_id' => NULL,
                'name' => 'Bangalore',
              ),
            21 => 
            array (
                'id' => 2522,
                'state_id' => NULL,
                'name' => 'Pondicherry',
              ),
            22 => 
            array (
                'id' => 2523,
                'state_id' => NULL,
                'name' => 'Bhopal',
              ),
            23 => 
            array (
                'id' => 2524,
                'state_id' => NULL,
                'name' => 'Bhubaneswar',
              ),
            24 => 
            array (
                'id' => 2525,
                'state_id' => NULL,
                'name' => 'Chandigarh',
              ),
            25 => 
            array (
                'id' => 2526,
                'state_id' => NULL,
                'name' => 'Daman',
              ),
            26 => 
            array (
                'id' => 2527,
                'state_id' => NULL,
                'name' => 'Diu',
              ),
            27 => 
            array (
                'id' => 2528,
                'state_id' => NULL,
                'name' => 'Gangtok',
              ),
            28 => 
            array (
                'id' => 2529,
                'state_id' => NULL,
                'name' => 'Coimbatore',
              ),
            29 => 
            array (
                'id' => 2530,
                'state_id' => NULL,
                'name' => 'Calcutta',
              ),
            30 => 
            array (
                'id' => 2531,
                'state_id' => NULL,
                'name' => 'Karaikal',
              ),
            31 => 
            array (
                'id' => 2532,
                'state_id' => NULL,
                'name' => 'Jabalpur',
              ),
            32 => 
            array (
                'id' => 2533,
                'state_id' => NULL,
                'name' => 'Jalandhar',
              ),
            33 => 
            array (
                'id' => 2534,
                'state_id' => NULL,
                'name' => 'Jodhpur',
              ),
            34 => 
            array (
                'id' => 2535,
                'state_id' => NULL,
                'name' => 'Chennai',
              ),
            35 => 
            array (
                'id' => 2536,
                'state_id' => NULL,
                'name' => 'Kavaratti',
              ),
            36 => 
            array (
                'id' => 2537,
                'state_id' => NULL,
                'name' => 'Kohima',
              ),
            37 => 
            array (
                'id' => 2538,
                'state_id' => NULL,
                'name' => 'Mahe',
              ),
            38 => 
            array (
                'id' => 2539,
                'state_id' => NULL,
                'name' => 'Madurai',
              ),
            39 => 
            array (
                'id' => 2540,
                'state_id' => NULL,
                'name' => 'Sambalpur',
              ),
            40 => 
            array (
                'id' => 2541,
                'state_id' => NULL,
                'name' => 'Trivandrum',
              ),
            41 => 
            array (
                'id' => 2542,
                'state_id' => NULL,
                'name' => 'Udaipur',
              ),
            42 => 
            array (
                'id' => 2543,
                'state_id' => NULL,
                'name' => 'Shillong',
              ),
            43 => 
            array (
                'id' => 2544,
                'state_id' => NULL,
                'name' => 'Silvassa',
              ),
            44 => 
            array (
                'id' => 2545,
                'state_id' => NULL,
                'name' => 'New Delhi',
              ),
            45 => 
            array (
                'id' => 2546,
                'state_id' => NULL,
                'name' => 'Yanam',
              ),
            46 => 
            array (
                'id' => 2547,
                'state_id' => NULL,
                'name' => 'Imphal',
              ),
            47 => 
            array (
                'id' => 2548,
                'state_id' => NULL,
                'name' => 'Indore',
              ),
            48 => 
            array (
                'id' => 2549,
                'state_id' => NULL,
                'name' => 'Jaipur',
              ),
            49 => 
            array (
                'id' => 2550,
                'state_id' => NULL,
                'name' => 'Bali',
            ),
            50 => 
            array (
                'id' => 2551,
                'state_id' => NULL,
                'name' => 'Kepulauan Bangka Belitung',
            ),
            51 => 
            array (
                'id' => 2552,
                'state_id' => NULL,
                'name' => 'Sulawesi Utara',
            ),
            52 => 
            array (
                'id' => 2553,
                'state_id' => NULL,
                'name' => 'Sumatera Utara',
            ),
            53 => 
            array (
                'id' => 2554,
                'state_id' => NULL,
                'name' => 'Daerah Tingkat I Kalimantan Barat',
            ),
            54 => 
            array (
                'id' => 2555,
                'state_id' => NULL,
                'name' => 'Kalimantan Timur',
            ),
            55 => 
            array (
                'id' => 2556,
                'state_id' => NULL,
                'name' => 'Sulawesi Tenggara',
            ),
            56 => 
            array (
                'id' => 2557,
                'state_id' => NULL,
                'name' => 'Nusa Tenggara Timur',
            ),
            57 => 
            array (
                'id' => 2558,
                'state_id' => NULL,
                'name' => 'Java Timur',
            ),
            58 => 
            array (
                'id' => 2559,
                'state_id' => NULL,
                'name' => 'Riau',
            ),
            59 => 
            array (
                'id' => 2560,
                'state_id' => NULL,
                'name' => 'Maluku',
            ),
            60 => 
            array (
                'id' => 2561,
                'state_id' => NULL,
                'name' => 'Bengkulu',
            ),
            61 => 
            array (
                'id' => 2562,
                'state_id' => NULL,
                'name' => 'Lampung',
            ),
            62 => 
            array (
                'id' => 2563,
                'state_id' => NULL,
                'name' => 'Kalimantan Selatan',
            ),
            63 => 
            array (
                'id' => 2564,
                'state_id' => NULL,
                'name' => 'Sulawesi Selatan',
            ),
            64 => 
            array (
                'id' => 2565,
                'state_id' => NULL,
                'name' => 'Sumatera Selatan',
            ),
            65 => 
            array (
                'id' => 2566,
                'state_id' => NULL,
                'name' => 'Daerah Istimewa Yogyakarta',
            ),
            66 => 
            array (
                'id' => 2567,
                'state_id' => NULL,
                'name' => 'Banten',
            ),
            67 => 
            array (
                'id' => 2568,
                'state_id' => NULL,
                'name' => 'Nusa Tenggara Barat',
            ),
            68 => 
            array (
                'id' => 2569,
                'state_id' => NULL,
                'name' => 'Sumatera Barat',
            ),
            69 => 
            array (
                'id' => 2570,
                'state_id' => NULL,
                'name' => 'Java Barat',
            ),
            70 => 
            array (
                'id' => 2571,
                'state_id' => NULL,
                'name' => 'Jakarta Raya',
            ),
            71 => 
            array (
                'id' => 2572,
                'state_id' => NULL,
                'name' => 'Aceh',
            ),
            72 => 
            array (
                'id' => 2573,
                'state_id' => NULL,
                'name' => 'Irian Jaya',
            ),
            73 => 
            array (
                'id' => 2574,
                'state_id' => NULL,
                'name' => 'Jambi',
            ),
            74 => 
            array (
                'id' => 2575,
                'state_id' => NULL,
                'name' => 'Kalimantan Tengah',
            ),
            75 => 
            array (
                'id' => 2576,
                'state_id' => NULL,
                'name' => 'Sulawesi Tengah',
            ),
            76 => 
            array (
                'id' => 2577,
                'state_id' => NULL,
                'name' => 'Java Tengah',
            ),
            77 => 
            array (
                'id' => 2578,
                'state_id' => NULL,
                'name' => 'Allun',
            ),
            78 => 
            array (
                'id' => 2579,
                'state_id' => NULL,
                'name' => 'Amman',
            ),
            79 => 
            array (
                'id' => 2580,
                'state_id' => NULL,
                'name' => 'Balqa',
            ),
            80 => 
            array (
                'id' => 2581,
                'state_id' => NULL,
                'name' => 'Jarash',
            ),
            81 => 
            array (
                'id' => 2582,
                'state_id' => NULL,
                'name' => 'Karak',
            ),
            82 => 
            array (
                'id' => 2583,
                'state_id' => NULL,
                'name' => 'Rusayfah',
            ),
            83 => 
            array (
                'id' => 2584,
                'state_id' => NULL,
                'name' => 'Maan',
            ),
            84 => 
            array (
                'id' => 2585,
                'state_id' => NULL,
                'name' => 'Madaba',
            ),
            85 => 
            array (
                'id' => 2586,
                'state_id' => NULL,
                'name' => 'Mafraq',
            ),
            86 => 
            array (
                'id' => 2587,
                'state_id' => NULL,
                'name' => 'Tafiela',
            ),
            87 => 
            array (
                'id' => 2588,
                'state_id' => NULL,
                'name' => 'Aqaba',
            ),
            88 => 
            array (
                'id' => 2589,
                'state_id' => NULL,
                'name' => 'Irbid',
            ),
            89 => 
            array (
                'id' => 2590,
                'state_id' => NULL,
                'name' => 'Zarqa',
            ),
            90 => 
            array (
                'id' => 2591,
                'state_id' => NULL,
                'name' => 'Haiphong',
            ),
            91 => 
            array (
                'id' => 2592,
                'state_id' => NULL,
                'name' => 'Hanoi',
            ),
            92 => 
            array (
                'id' => 2593,
                'state_id' => NULL,
                'name' => 'Ho Chi Minh City',
            ),
            93 => 
            array (
                'id' => 2594,
                'state_id' => NULL,
                'name' => 'Northern',
            ),
            94 => 
            array (
                'id' => 2595,
                'state_id' => NULL,
                'name' => 'Eastern',
            ),
            95 => 
            array (
                'id' => 2596,
                'state_id' => NULL,
                'name' => 'Luapula',
            ),
            96 => 
            array (
                'id' => 2597,
                'state_id' => NULL,
                'name' => 'Lusaka',
            ),
            97 => 
            array (
                'id' => 2598,
                'state_id' => NULL,
                'name' => 'Southern',
            ),
            98 => 
            array (
                'id' => 2599,
                'state_id' => NULL,
                'name' => 'Copperbelt',
            ),
            99 => 
            array (
                'id' => 2600,
                'state_id' => NULL,
                'name' => 'North-Western',
            ),
            100 => 
            array (
                'id' => 2601,
                'state_id' => NULL,
                'name' => 'Western',
            ),
            101 => 
            array (
                'id' => 2602,
                'state_id' => NULL,
                'name' => 'Central',
            ),
            102 => 
            array (
                'id' => 2603,
                'state_id' => NULL,
                'name' => 'Region de la Araucania',
            ),
            103 => 
            array (
                'id' => 2604,
                'state_id' => NULL,
                'name' => 'Region de Atacama',
            ),
            104 => 
            array (
                'id' => 2605,
                'state_id' => NULL,
                'name' => 'Region de Antofagasta',
            ),
            105 => 
            array (
                'id' => 2606,
                'state_id' => NULL,
                'name' => 'Region del Biobio',
            ),
            106 => 
            array (
                'id' => 2607,
                'state_id' => NULL,
                'name' => 'Libertador',
            ),
            107 => 
            array (
                'id' => 2608,
                'state_id' => NULL,
                'name' => 'Region de los Lagos',
            ),
            108 => 
            array (
                'id' => 2609,
                'state_id' => NULL,
                'name' => 'Region de Coquimbo',
            ),
            109 => 
            array (
                'id' => 2610,
                'state_id' => NULL,
                'name' => 'Region del Maule',
            ),
            110 => 
            array (
                'id' => 2611,
                'state_id' => NULL,
                'name' => 'Magallanes y Antartica Chilena',
            ),
            111 => 
            array (
                'id' => 2612,
                'state_id' => NULL,
                'name' => 'Metropolitana de Santiago',
            ),
            112 => 
            array (
                'id' => 2613,
                'state_id' => NULL,
                'name' => 'Region de Tarapaca',
            ),
            113 => 
            array (
                'id' => 2614,
                'state_id' => NULL,
                'name' => 'Region de Valparaiso',
            ),
            114 => 
            array (
                'id' => 2615,
                'state_id' => NULL,
                'name' => 'Region de Alsen del General Carlos Ibanez del',
            ),
            115 => 
            array (
                'id' => 2616,
                'state_id' => NULL,
                'name' => 'Bamingui-Bangoran',
            ),
            116 => 
            array (
                'id' => 2617,
                'state_id' => NULL,
                'name' => 'Bangui',
              ),
            117 => 
            array (
                'id' => 2618,
                'state_id' => NULL,
                'name' => 'Bimbo',
            ),
            118 => 
            array (
                'id' => 2619,
                'state_id' => NULL,
                'name' => 'Kemo',
            ),
            119 => 
            array (
                'id' => 2620,
                'state_id' => NULL,
                'name' => 'Lobaye',
            ),
            120 => 
            array (
                'id' => 2621,
                'state_id' => NULL,
                'name' => 'Mambere-Kadei',
            ),
            121 => 
            array (
                'id' => 2622,
                'state_id' => NULL,
                'name' => 'Mbomou',
            ),
            122 => 
            array (
                'id' => 2623,
                'state_id' => NULL,
                'name' => 'Nana-Gribizi',
            ),
            123 => 
            array (
                'id' => 2624,
                'state_id' => NULL,
                'name' => 'Nana-Mambere',
            ),
            124 => 
            array (
                'id' => 2625,
                'state_id' => NULL,
                'name' => 'Sangha-Mbaere',
            ),
            125 => 
            array (
                'id' => 2626,
                'state_id' => NULL,
                'name' => 'Haute-Kotto',
            ),
            126 => 
            array (
                'id' => 2627,
                'state_id' => NULL,
                'name' => 'Haut-Mbomou',
            ),
            127 => 
            array (
                'id' => 2628,
                'state_id' => NULL,
                'name' => 'Ouaka',
            ),
            128 => 
            array (
                'id' => 2629,
                'state_id' => NULL,
                'name' => 'Vakaga',
            ),
            129 => 
            array (
                'id' => 2630,
                'state_id' => NULL,
                'name' => 'Ouham',
            ),
            130 => 
            array (
                'id' => 2631,
                'state_id' => NULL,
                'name' => 'Ouham-Pende',
            ),
            131 => 
            array (
                'id' => 2632,
                'state_id' => NULL,
                'name' => 'Ombella-Mpoko',
            ),
            132 => 
            array (
                'id' => 2633,
                'state_id' => NULL,
                'name' => 'Basse-Kotto',
            ),
            133 => 
            array (
                'id' => 2634,
                'state_id' => 26,
                'name' => 'Dongcheng',
            ),
            134 => 
            array (
                'id' => 2635,
                'state_id' => 26,
                'name' => 'Xicheng',
            ),
            135 => 
            array (
                'id' => 2636,
                'state_id' => 26,
                'name' => 'Chaoyang',
            ),
            136 => 
            array (
                'id' => 2637,
                'state_id' => 26,
                'name' => 'Fengtai',
            ),
            137 => 
            array (
                'id' => 2638,
                'state_id' => 26,
                'name' => 'Shijingshan',
            ),
            138 => 
            array (
                'id' => 2639,
                'state_id' => 26,
                'name' => 'Haidian',
            ),
            139 => 
            array (
                'id' => 2640,
                'state_id' => 26,
                'name' => 'Mentougou',
            ),
            140 => 
            array (
                'id' => 2641,
                'state_id' => 26,
                'name' => 'Fangshan',
            ),
            141 => 
            array (
                'id' => 2642,
                'state_id' => 26,
                'name' => 'Tongzhou',
            ),
            142 => 
            array (
                'id' => 2643,
                'state_id' => 26,
                'name' => 'Shunyi',
            ),
            143 => 
            array (
                'id' => 2644,
                'state_id' => 26,
                'name' => 'Changping',
            ),
            144 => 
            array (
                'id' => 2645,
                'state_id' => 26,
                'name' => 'Daxing',
            ),
            145 => 
            array (
                'id' => 2646,
                'state_id' => 26,
                'name' => 'Pinggu',
            ),
            146 => 
            array (
                'id' => 2647,
                'state_id' => 26,
                'name' => 'Huairou',
            ),
            147 => 
            array (
                'id' => 2648,
                'state_id' => 26,
                'name' => 'Miyun',
            ),
            148 => 
            array (
                'id' => 2649,
                'state_id' => 26,
                'name' => 'Yanqing',
            ),
            149 => 
            array (
                'id' => 2650,
                'state_id' => 52,
                'name' => 'Heping',
            ),
            150 => 
            array (
                'id' => 2651,
                'state_id' => 52,
                'name' => 'Hedong',
            ),
            151 => 
            array (
                'id' => 2652,
                'state_id' => 52,
                'name' => 'Hexi',
            ),
            152 => 
            array (
                'id' => 2653,
                'state_id' => 52,
                'name' => 'Nankai',
            ),
            153 => 
            array (
                'id' => 2654,
                'state_id' => 52,
                'name' => 'Hebei',
            ),
            154 => 
            array (
                'id' => 2655,
                'state_id' => 52,
                'name' => 'Hongqiao',
            ),
            155 => 
            array (
                'id' => 2656,
                'state_id' => 52,
                'name' => 'Binghaixinqu',
            ),
            156 => 
            array (
                'id' => 2657,
                'state_id' => 52,
                'name' => 'Dongli',
            ),
            157 => 
            array (
                'id' => 2658,
                'state_id' => 52,
                'name' => 'Xiqing',
            ),
            158 => 
            array (
                'id' => 2659,
                'state_id' => 52,
                'name' => 'Jinnan',
            ),
            159 => 
            array (
                'id' => 2660,
                'state_id' => 52,
                'name' => 'Beichen',
            ),
            160 => 
            array (
                'id' => 2661,
                'state_id' => 52,
                'name' => 'Ninghe',
            ),
            161 => 
            array (
                'id' => 2662,
                'state_id' => 52,
                'name' => 'Wuqing',
            ),
            162 => 
            array (
                'id' => 2663,
                'state_id' => 52,
                'name' => 'Jinghai',
            ),
            163 => 
            array (
                'id' => 2664,
                'state_id' => 52,
                'name' => 'Baodi',
            ),
            164 => 
            array (
                'id' => 2665,
                'state_id' => 52,
                'name' => 'Jixian',
            ),
            165 => 
            array (
                'id' => 2666,
                'state_id' => 34,
                'name' => 'Shijiazhuang',
            ),
            166 => 
            array (
                'id' => 2667,
                'state_id' => 34,
                'name' => 'Tangshan',
            ),
            167 => 
            array (
                'id' => 2668,
                'state_id' => 34,
                'name' => 'Qinhuangdao',
            ),
            168 => 
            array (
                'id' => 2669,
                'state_id' => 34,
                'name' => 'Handan',
            ),
            169 => 
            array (
                'id' => 2670,
                'state_id' => 34,
                'name' => 'Xingtai',
            ),
            170 => 
            array (
                'id' => 2671,
                'state_id' => 34,
                'name' => 'Baoding',
            ),
            171 => 
            array (
                'id' => 2672,
                'state_id' => 34,
                'name' => 'Zhangjiakou',
            ),
            172 => 
            array (
                'id' => 2673,
                'state_id' => 34,
                'name' => 'Chengde',
            ),
            173 => 
            array (
                'id' => 2674,
                'state_id' => 34,
                'name' => 'Cangzhou',
            ),
            174 => 
            array (
                'id' => 2675,
                'state_id' => 34,
                'name' => 'Langfang',
            ),
            175 => 
            array (
                'id' => 2676,
                'state_id' => 34,
                'name' => 'Hengshui',
            ),
            176 => 
            array (
                'id' => 2677,
                'state_id' => 49,
                'name' => 'Taiyuan',
            ),
            177 => 
            array (
                'id' => 2678,
                'state_id' => 49,
                'name' => 'Datong',
            ),
            178 => 
            array (
                'id' => 2679,
                'state_id' => 49,
                'name' => 'Yangquan',
            ),
            179 => 
            array (
                'id' => 2680,
                'state_id' => 49,
                'name' => 'Changzhi',
            ),
            180 => 
            array (
                'id' => 2681,
                'state_id' => 49,
                'name' => 'Jincheng',
            ),
            181 => 
            array (
                'id' => 2682,
                'state_id' => 49,
                'name' => 'Shuozhou',
            ),
            182 => 
            array (
                'id' => 2683,
                'state_id' => 49,
                'name' => 'Jinzhong',
            ),
            183 => 
            array (
                'id' => 2684,
                'state_id' => 49,
                'name' => 'Yuncheng',
            ),
            184 => 
            array (
                'id' => 2685,
                'state_id' => 49,
                'name' => 'Xinzhou',
            ),
            185 => 
            array (
                'id' => 2686,
                'state_id' => 49,
                'name' => 'Linfen',
            ),
            186 => 
            array (
                'id' => 2687,
                'state_id' => 49,
                'name' => 'luliang',
            ),
            187 => 
            array (
                'id' => 2688,
                'state_id' => 39,
                'name' => 'Hohhot',
            ),
            188 => 
            array (
                'id' => 2689,
                'state_id' => 39,
                'name' => 'Baotou',
            ),
            189 => 
            array (
                'id' => 2690,
                'state_id' => 39,
                'name' => 'Wuhai',
            ),
            190 => 
            array (
                'id' => 2691,
                'state_id' => 39,
                'name' => 'Chifeng',
            ),
            191 => 
            array (
                'id' => 2692,
                'state_id' => 39,
                'name' => 'Tongliao',
            ),
            192 => 
            array (
                'id' => 2693,
                'state_id' => 39,
                'name' => 'Ordos',
            ),
            193 => 
            array (
                'id' => 2694,
                'state_id' => 39,
                'name' => 'Hulun Buir',
            ),
            194 => 
            array (
                'id' => 2695,
                'state_id' => 39,
                'name' => 'Bayannur',
            ),
            195 => 
            array (
                'id' => 2696,
                'state_id' => 39,
                'name' => 'Ulan Qab',
            ),
            196 => 
            array (
                'id' => 2697,
                'state_id' => 39,
                'name' => 'Xing\'an',
            ),
            197 => 
            array (
                'id' => 2698,
                'state_id' => 39,
                'name' => 'Xilin Gol',
            ),
            198 => 
            array (
                'id' => 2699,
                'state_id' => 39,
                'name' => 'Alxa',
            ),
            199 => 
            array (
                'id' => 2700,
                'state_id' => 43,
                'name' => 'Shenyang',
            ),
            200 => 
            array (
                'id' => 2701,
                'state_id' => 43,
                'name' => 'Dalian',
            ),
            201 => 
            array (
                'id' => 2702,
                'state_id' => 43,
                'name' => 'Anshan',
            ),
            202 => 
            array (
                'id' => 2703,
                'state_id' => 43,
                'name' => 'Fushun',
            ),
            203 => 
            array (
                'id' => 2704,
                'state_id' => 43,
                'name' => 'Benxi',
            ),
            204 => 
            array (
                'id' => 2705,
                'state_id' => 43,
                'name' => 'Dandong',
            ),
            205 => 
            array (
                'id' => 2706,
                'state_id' => 43,
                'name' => 'Jinzhou',
            ),
            206 => 
            array (
                'id' => 2707,
                'state_id' => 43,
                'name' => 'Yingkou',
            ),
            207 => 
            array (
                'id' => 2708,
                'state_id' => 43,
                'name' => 'Fuxin',
            ),
            208 => 
            array (
                'id' => 2709,
                'state_id' => 43,
                'name' => 'Liaoyang',
            ),
            209 => 
            array (
                'id' => 2710,
                'state_id' => 43,
                'name' => 'Panjin',
            ),
            210 => 
            array (
                'id' => 2711,
                'state_id' => 43,
                'name' => 'Tieling',
            ),
            211 => 
            array (
                'id' => 2712,
                'state_id' => 43,
                'name' => 'Chaoyang',
            ),
            212 => 
            array (
                'id' => 2713,
                'state_id' => 43,
                'name' => 'Huludao',
            ),
            213 => 
            array (
                'id' => 2714,
                'state_id' => 42,
                'name' => 'Changchun',
            ),
            214 => 
            array (
                'id' => 2715,
                'state_id' => 42,
                'name' => 'Jilin',
            ),
            215 => 
            array (
                'id' => 2716,
                'state_id' => 42,
                'name' => 'Siping',
            ),
            216 => 
            array (
                'id' => 2717,
                'state_id' => 42,
                'name' => 'Liaoyuan',
            ),
            217 => 
            array (
                'id' => 2718,
                'state_id' => 42,
                'name' => 'Tonghua',
            ),
            218 => 
            array (
                'id' => 2719,
                'state_id' => 42,
                'name' => 'Baishan',
            ),
            219 => 
            array (
                'id' => 2720,
                'state_id' => 42,
                'name' => 'Songyuan',
            ),
            220 => 
            array (
                'id' => 2721,
                'state_id' => 42,
                'name' => 'Baicheng',
            ),
            221 => 
            array (
                'id' => 2722,
                'state_id' => 42,
                'name' => 'Yanbian Korean Autonomous Prefecture',
            ),
            222 => 
            array (
                'id' => 2723,
                'state_id' => 35,
                'name' => 'Harbin',
            ),
            223 => 
            array (
                'id' => 2724,
                'state_id' => 35,
                'name' => 'Qiqihar',
            ),
            224 => 
            array (
                'id' => 2725,
                'state_id' => 35,
                'name' => 'Jixi',
            ),
            225 => 
            array (
                'id' => 2726,
                'state_id' => 35,
                'name' => 'Hegang',
            ),
            226 => 
            array (
                'id' => 2727,
                'state_id' => 35,
                'name' => 'Shuangyashan',
            ),
            227 => 
            array (
                'id' => 2728,
                'state_id' => 35,
                'name' => 'Daqing',
            ),
            228 => 
            array (
                'id' => 2729,
                'state_id' => 35,
                'name' => 'Yichun',
            ),
            229 => 
            array (
                'id' => 2730,
                'state_id' => 35,
                'name' => 'Jiamusi',
            ),
            230 => 
            array (
                'id' => 2731,
                'state_id' => 35,
                'name' => 'Qitaihe',
            ),
            231 => 
            array (
                'id' => 2732,
                'state_id' => 35,
                'name' => 'Mudanjiang',
            ),
            232 => 
            array (
                'id' => 2733,
                'state_id' => 35,
                'name' => 'Heihe',
            ),
            233 => 
            array (
                'id' => 2734,
                'state_id' => 35,
                'name' => 'Suihua',
            ),
            234 => 
            array (
                'id' => 2735,
                'state_id' => 35,
                'name' => 'Da Hinggan Ling',
            ),
            235 => 
            array (
                'id' => 2736,
                'state_id' => 48,
                'name' => 'Huangpu',
            ),
            236 => 
            array (
                'id' => 2737,
                'state_id' => 48,
                'name' => 'Luwan',
            ),
            237 => 
            array (
                'id' => 2738,
                'state_id' => 48,
                'name' => 'Xuhui',
            ),
            238 => 
            array (
                'id' => 2739,
                'state_id' => 48,
                'name' => 'Changning',
            ),
            239 => 
            array (
                'id' => 2740,
                'state_id' => 48,
                'name' => 'Jing\'an',
            ),
            240 => 
            array (
                'id' => 2741,
                'state_id' => 48,
                'name' => 'Putuo',
            ),
            241 => 
            array (
                'id' => 2742,
                'state_id' => 48,
                'name' => 'Zhabei',
            ),
            242 => 
            array (
                'id' => 2743,
                'state_id' => 48,
                'name' => 'Hongkou',
            ),
            243 => 
            array (
                'id' => 2744,
                'state_id' => 48,
                'name' => 'Yangpu',
            ),
            244 => 
            array (
                'id' => 2745,
                'state_id' => 48,
                'name' => 'Minhang',
            ),
            245 => 
            array (
                'id' => 2746,
                'state_id' => 48,
                'name' => 'Baoshan',
            ),
            246 => 
            array (
                'id' => 2747,
                'state_id' => 48,
                'name' => 'Jiading',
            ),
            247 => 
            array (
                'id' => 2748,
                'state_id' => 48,
                'name' => 'Pudong New Area',
            ),
            248 => 
            array (
                'id' => 2749,
                'state_id' => 48,
                'name' => 'Jinshan',
            ),
            249 => 
            array (
                'id' => 2750,
                'state_id' => 48,
                'name' => 'Songjiang',
            ),
            250 => 
            array (
                'id' => 2751,
                'state_id' => 48,
                'name' => 'Fengxian',
            ),
            251 => 
            array (
                'id' => 2752,
                'state_id' => 48,
                'name' => 'Qingpu',
            ),
            252 => 
            array (
                'id' => 2753,
                'state_id' => 48,
                'name' => 'Chongming',
            ),
            253 => 
            array (
                'id' => 2754,
                'state_id' => 40,
                'name' => 'Nanjing',
            ),
            254 => 
            array (
                'id' => 2755,
                'state_id' => 40,
                'name' => 'Wuxi',
            ),
            255 => 
            array (
                'id' => 2756,
                'state_id' => 40,
                'name' => 'Xuzhou',
            ),
            256 => 
            array (
                'id' => 2757,
                'state_id' => 40,
                'name' => 'Changzhou',
            ),
            257 => 
            array (
                'id' => 2758,
                'state_id' => 40,
                'name' => 'Suzhou',
            ),
            258 => 
            array (
                'id' => 2759,
                'state_id' => 40,
                'name' => 'Nantong',
            ),
            259 => 
            array (
                'id' => 2760,
                'state_id' => 40,
                'name' => 'Lianyungang',
            ),
            260 => 
            array (
                'id' => 2761,
                'state_id' => 40,
                'name' => 'Huai\'an',
            ),
            261 => 
            array (
                'id' => 2762,
                'state_id' => 40,
                'name' => 'Yancheng',
            ),
            262 => 
            array (
                'id' => 2763,
                'state_id' => 40,
                'name' => 'Yangzhou',
            ),
            263 => 
            array (
                'id' => 2764,
                'state_id' => 40,
                'name' => 'Zhenjiang',
            ),
            264 => 
            array (
                'id' => 2765,
                'state_id' => 40,
                'name' => 'Taizhou',
            ),
            265 => 
            array (
                'id' => 2766,
                'state_id' => 40,
                'name' => 'Suqian',
            ),
            266 => 
            array (
                'id' => 2767,
                'state_id' => 56,
                'name' => 'Hangzhou',
            ),
            267 => 
            array (
                'id' => 2768,
                'state_id' => 56,
                'name' => 'Ningbo',
            ),
            268 => 
            array (
                'id' => 2769,
                'state_id' => 56,
                'name' => 'Wenzhou',
            ),
            269 => 
            array (
                'id' => 2770,
                'state_id' => 56,
                'name' => 'Jiaxing',
            ),
            270 => 
            array (
                'id' => 2771,
                'state_id' => 56,
                'name' => 'Huzhou',
            ),
            271 => 
            array (
                'id' => 2772,
                'state_id' => 56,
                'name' => 'Shaoxing',
            ),
            272 => 
            array (
                'id' => 2773,
                'state_id' => 56,
                'name' => 'Jinhua',
            ),
            273 => 
            array (
                'id' => 2774,
                'state_id' => 56,
                'name' => 'Quzhou',
            ),
            274 => 
            array (
                'id' => 2775,
                'state_id' => 56,
                'name' => 'Zhoushan',
            ),
            275 => 
            array (
                'id' => 2776,
                'state_id' => 56,
                'name' => 'Taizhou',
            ),
            276 => 
            array (
                'id' => 2777,
                'state_id' => 56,
                'name' => 'Lishui',
            ),
            277 => 
            array (
                'id' => 2778,
                'state_id' => 25,
                'name' => 'Hefei',
            ),
            278 => 
            array (
                'id' => 2779,
                'state_id' => 25,
                'name' => 'Wuhu',
            ),
            279 => 
            array (
                'id' => 2780,
                'state_id' => 25,
                'name' => 'Bengbu',
            ),
            280 => 
            array (
                'id' => 2781,
                'state_id' => 25,
                'name' => 'Huainan',
            ),
            281 => 
            array (
                'id' => 2782,
                'state_id' => 25,
                'name' => 'Ma\'anshan',
            ),
            282 => 
            array (
                'id' => 2783,
                'state_id' => 25,
                'name' => 'Huaibei',
            ),
            283 => 
            array (
                'id' => 2784,
                'state_id' => 25,
                'name' => 'Tongling',
            ),
            284 => 
            array (
                'id' => 2785,
                'state_id' => 25,
                'name' => 'Anqing',
            ),
            285 => 
            array (
                'id' => 2786,
                'state_id' => 25,
                'name' => 'Huangshan',
            ),
            286 => 
            array (
                'id' => 2787,
                'state_id' => 25,
                'name' => 'Chuzhou',
            ),
            287 => 
            array (
                'id' => 2788,
                'state_id' => 25,
                'name' => 'Fuyang',
            ),
            288 => 
            array (
                'id' => 2789,
                'state_id' => 25,
                'name' => 'Suzhou',
            ),
            289 => 
            array (
                'id' => 2790,
                'state_id' => 25,
                'name' => 'Lu\'an',
            ),
            290 => 
            array (
                'id' => 2791,
                'state_id' => 25,
                'name' => 'Bozhou',
            ),
            291 => 
            array (
                'id' => 2792,
                'state_id' => 25,
                'name' => 'Chizhou',
            ),
            292 => 
            array (
                'id' => 2793,
                'state_id' => 25,
                'name' => 'Xuancheng',
            ),
            293 => 
            array (
                'id' => 2794,
                'state_id' => 28,
                'name' => 'Fuzhou',
            ),
            294 => 
            array (
                'id' => 2795,
                'state_id' => 28,
                'name' => 'Xiamen',
            ),
            295 => 
            array (
                'id' => 2796,
                'state_id' => 28,
                'name' => 'Putian',
            ),
            296 => 
            array (
                'id' => 2797,
                'state_id' => 28,
                'name' => 'Sanming',
            ),
            297 => 
            array (
                'id' => 2798,
                'state_id' => 28,
                'name' => 'Quanzhou',
            ),
            298 => 
            array (
                'id' => 2799,
                'state_id' => 28,
                'name' => 'Zhangzhou',
            ),
            299 => 
            array (
                'id' => 2800,
                'state_id' => 28,
                'name' => 'Nanping',
            ),
            300 => 
            array (
                'id' => 2801,
                'state_id' => 28,
                'name' => 'Longyan',
            ),
            301 => 
            array (
                'id' => 2802,
                'state_id' => 28,
                'name' => 'Ningde',
            ),
            302 => 
            array (
                'id' => 2803,
                'state_id' => 41,
                'name' => 'Nanchang',
            ),
            303 => 
            array (
                'id' => 2804,
                'state_id' => 41,
                'name' => 'Jingdezhen',
            ),
            304 => 
            array (
                'id' => 2805,
                'state_id' => 41,
                'name' => 'Pingxiang',
            ),
            305 => 
            array (
                'id' => 2806,
                'state_id' => 41,
                'name' => 'Jiujiang',
            ),
            306 => 
            array (
                'id' => 2807,
                'state_id' => 41,
                'name' => 'Xinyu',
            ),
            307 => 
            array (
                'id' => 2808,
                'state_id' => 41,
                'name' => 'Yingtan',
            ),
            308 => 
            array (
                'id' => 2809,
                'state_id' => 41,
                'name' => 'Ganzhou',
            ),
            309 => 
            array (
                'id' => 2810,
                'state_id' => 41,
                'name' => 'Ji\'an',
            ),
            310 => 
            array (
                'id' => 2811,
                'state_id' => 41,
                'name' => 'Yichun',
            ),
            311 => 
            array (
                'id' => 2812,
                'state_id' => 41,
                'name' => 'Fuzhou',
            ),
            312 => 
            array (
                'id' => 2813,
                'state_id' => 41,
                'name' => 'Shangrao',
            ),
            313 => 
            array (
                'id' => 2814,
                'state_id' => 47,
                'name' => 'Jinan',
            ),
            314 => 
            array (
                'id' => 2815,
                'state_id' => 47,
                'name' => 'Qingdao',
            ),
            315 => 
            array (
                'id' => 2816,
                'state_id' => 47,
                'name' => 'Zibo',
            ),
            316 => 
            array (
                'id' => 2817,
                'state_id' => 47,
                'name' => 'Zaozhuang',
            ),
            317 => 
            array (
                'id' => 2818,
                'state_id' => 47,
                'name' => 'Dongying',
            ),
            318 => 
            array (
                'id' => 2819,
                'state_id' => 47,
                'name' => 'Yantai',
            ),
            319 => 
            array (
                'id' => 2820,
                'state_id' => 47,
                'name' => 'Weifang',
            ),
            320 => 
            array (
                'id' => 2821,
                'state_id' => 47,
                'name' => 'Jining',
            ),
            321 => 
            array (
                'id' => 2822,
                'state_id' => 47,
                'name' => 'Tai\'an',
            ),
            322 => 
            array (
                'id' => 2823,
                'state_id' => 47,
                'name' => 'Weihai',
            ),
            323 => 
            array (
                'id' => 2824,
                'state_id' => 47,
                'name' => 'Rizhao',
            ),
            324 => 
            array (
                'id' => 2825,
                'state_id' => 47,
                'name' => 'Laiwu',
            ),
            325 => 
            array (
                'id' => 2826,
                'state_id' => 47,
                'name' => 'Linyi',
            ),
            326 => 
            array (
                'id' => 2827,
                'state_id' => 47,
                'name' => 'Dezhou',
            ),
            327 => 
            array (
                'id' => 2828,
                'state_id' => 47,
                'name' => 'Liaocheng',
            ),
            328 => 
            array (
                'id' => 2829,
                'state_id' => 47,
                'name' => 'Binzhou',
            ),
            329 => 
            array (
                'id' => 2830,
                'state_id' => 47,
                'name' => 'Heze',
            ),
            330 => 
            array (
                'id' => 2831,
                'state_id' => 36,
                'name' => 'Zhengzhou',
            ),
            331 => 
            array (
                'id' => 2832,
                'state_id' => 36,
                'name' => 'Kaifeng',
            ),
            332 => 
            array (
                'id' => 2833,
                'state_id' => 36,
                'name' => 'Luoyang',
            ),
            333 => 
            array (
                'id' => 2834,
                'state_id' => 36,
                'name' => 'Pingdingshan',
            ),
            334 => 
            array (
                'id' => 2835,
                'state_id' => 36,
                'name' => 'Anyang',
            ),
            335 => 
            array (
                'id' => 2836,
                'state_id' => 36,
                'name' => 'Hebi',
            ),
            336 => 
            array (
                'id' => 2837,
                'state_id' => 36,
                'name' => 'Xinxiang',
            ),
            337 => 
            array (
                'id' => 2838,
                'state_id' => 36,
                'name' => 'Jiaozuo',
            ),
            338 => 
            array (
                'id' => 2839,
                'state_id' => 36,
                'name' => 'Puyang',
            ),
            339 => 
            array (
                'id' => 2840,
                'state_id' => 36,
                'name' => 'Xuchang',
            ),
            340 => 
            array (
                'id' => 2841,
                'state_id' => 36,
                'name' => 'Luohe',
            ),
            341 => 
            array (
                'id' => 2842,
                'state_id' => 36,
                'name' => 'Sanmenxia',
            ),
            342 => 
            array (
                'id' => 2843,
                'state_id' => 36,
                'name' => 'Nanyang',
            ),
            343 => 
            array (
                'id' => 2844,
                'state_id' => 36,
                'name' => 'Shangqiu',
            ),
            344 => 
            array (
                'id' => 2845,
                'state_id' => 36,
                'name' => 'Xinyang',
            ),
            345 => 
            array (
                'id' => 2846,
                'state_id' => 36,
                'name' => 'Zhoukou',
            ),
            346 => 
            array (
                'id' => 2847,
                'state_id' => 36,
                'name' => 'Zhumadian',
            ),
            347 => 
            array (
                'id' => 2848,
                'state_id' => 36,
                'name' => 'Jiyuan',
            ),
            348 => 
            array (
                'id' => 2849,
                'state_id' => 37,
                'name' => 'Wuhan',
            ),
            349 => 
            array (
                'id' => 2850,
                'state_id' => 37,
                'name' => 'Huangshi',
            ),
            350 => 
            array (
                'id' => 2851,
                'state_id' => 37,
                'name' => 'Shiyan',
            ),
            351 => 
            array (
                'id' => 2852,
                'state_id' => 37,
                'name' => 'Yichang',
            ),
            352 => 
            array (
                'id' => 2853,
                'state_id' => 37,
                'name' => 'Xiangyang',
            ),
            353 => 
            array (
                'id' => 2854,
                'state_id' => 37,
                'name' => 'Ezhou',
            ),
            354 => 
            array (
                'id' => 2855,
                'state_id' => 37,
                'name' => 'Jingmen',
            ),
            355 => 
            array (
                'id' => 2856,
                'state_id' => 37,
                'name' => 'Xiaogan',
            ),
            356 => 
            array (
                'id' => 2857,
                'state_id' => 37,
                'name' => 'Jingzhou',
            ),
            357 => 
            array (
                'id' => 2858,
                'state_id' => 37,
                'name' => 'Huanggang',
            ),
            358 => 
            array (
                'id' => 2859,
                'state_id' => 37,
                'name' => 'Xianning',
            ),
            359 => 
            array (
                'id' => 2860,
                'state_id' => 37,
                'name' => 'Suizhou',
            ),
            360 => 
            array (
                'id' => 2861,
                'state_id' => 37,
                'name' => 'Enshi Tujia-Miao Autonomous Prefecture',
            ),
            361 => 
            array (
                'id' => 2862,
                'state_id' => 37,
                'name' => 'Xiantao',
            ),
            362 => 
            array (
                'id' => 2863,
                'state_id' => 37,
                'name' => 'Qianjiang',
            ),
            363 => 
            array (
                'id' => 2864,
                'state_id' => 37,
                'name' => 'Tianmen',
            ),
            364 => 
            array (
                'id' => 2865,
                'state_id' => 37,
                'name' => 'Shennongjia',
              ),
            365 => 
            array (
                'id' => 2866,
                'state_id' => 38,
                'name' => 'Changsha',
            ),
            366 => 
            array (
                'id' => 2867,
                'state_id' => 38,
                'name' => 'Zhuzhou',
            ),
            367 => 
            array (
                'id' => 2868,
                'state_id' => 38,
                'name' => 'Xiangtan',
            ),
            368 => 
            array (
                'id' => 2869,
                'state_id' => 38,
                'name' => 'Hengyang',
            ),
            369 => 
            array (
                'id' => 2870,
                'state_id' => 38,
                'name' => 'Shaoyang',
            ),
            370 => 
            array (
                'id' => 2871,
                'state_id' => 38,
                'name' => 'Yueyang',
            ),
            371 => 
            array (
                'id' => 2872,
                'state_id' => 38,
                'name' => 'Changde',
            ),
            372 => 
            array (
                'id' => 2873,
                'state_id' => 38,
                'name' => 'Zhangjiajie',
            ),
            373 => 
            array (
                'id' => 2874,
                'state_id' => 38,
                'name' => 'Yiyang',
            ),
            374 => 
            array (
                'id' => 2875,
                'state_id' => 38,
                'name' => 'Chenzhou',
            ),
            375 => 
            array (
                'id' => 2876,
                'state_id' => 38,
                'name' => 'Yongzhou',
            ),
            376 => 
            array (
                'id' => 2877,
                'state_id' => 38,
                'name' => 'Huaihua',
            ),
            377 => 
            array (
                'id' => 2878,
                'state_id' => 38,
                'name' => 'Loudi',
            ),
            378 => 
            array (
                'id' => 2879,
                'state_id' => 38,
                'name' => 'Xiangxi Tujia-Miao Autonomous Prefecture',
            ),
            379 => 
            array (
                'id' => 2880,
                'state_id' => 30,
                'name' => 'Guangzhou',
            ),
            380 => 
            array (
                'id' => 2881,
                'state_id' => 30,
                'name' => 'Shaoguan',
            ),
            381 => 
            array (
                'id' => 2882,
                'state_id' => 30,
                'name' => 'Shenzhen',
            ),
            382 => 
            array (
                'id' => 2883,
                'state_id' => 30,
                'name' => 'Zhuhai',
            ),
            383 => 
            array (
                'id' => 2884,
                'state_id' => 30,
                'name' => 'Shantou',
            ),
            384 => 
            array (
                'id' => 2885,
                'state_id' => 30,
                'name' => 'Foshan',
            ),
            385 => 
            array (
                'id' => 2886,
                'state_id' => 30,
                'name' => 'Jiangmen',
            ),
            386 => 
            array (
                'id' => 2887,
                'state_id' => 30,
                'name' => 'Zhanjiang',
            ),
            387 => 
            array (
                'id' => 2888,
                'state_id' => 30,
                'name' => 'Maoming',
            ),
            388 => 
            array (
                'id' => 2889,
                'state_id' => 30,
                'name' => 'Zhaoqing',
            ),
            389 => 
            array (
                'id' => 2890,
                'state_id' => 30,
                'name' => 'Huizhou',
            ),
            390 => 
            array (
                'id' => 2891,
                'state_id' => 30,
                'name' => 'Meizhou',
            ),
            391 => 
            array (
                'id' => 2892,
                'state_id' => 30,
                'name' => 'Shanwei',
            ),
            392 => 
            array (
                'id' => 2893,
                'state_id' => 30,
                'name' => 'Heyuan',
            ),
            393 => 
            array (
                'id' => 2894,
                'state_id' => 30,
                'name' => 'Yangjiang',
            ),
            394 => 
            array (
                'id' => 2895,
                'state_id' => 30,
                'name' => 'Qingyuan',
            ),
            395 => 
            array (
                'id' => 2896,
                'state_id' => 30,
                'name' => 'Dongguan',
            ),
            396 => 
            array (
                'id' => 2897,
                'state_id' => 30,
                'name' => 'Zhongshan',
            ),
            397 => 
            array (
                'id' => 2898,
                'state_id' => 30,
                'name' => 'Chaozhou',
            ),
            398 => 
            array (
                'id' => 2899,
                'state_id' => 30,
                'name' => 'Jieyang',
            ),
            399 => 
            array (
                'id' => 2900,
                'state_id' => 30,
                'name' => 'Yunfu',
            ),
            400 => 
            array (
                'id' => 2901,
                'state_id' => 31,
                'name' => 'Nanning',
            ),
            401 => 
            array (
                'id' => 2902,
                'state_id' => 31,
                'name' => 'Liuzhou',
            ),
            402 => 
            array (
                'id' => 2903,
                'state_id' => 31,
                'name' => 'Guilin',
            ),
            403 => 
            array (
                'id' => 2904,
                'state_id' => 31,
                'name' => 'Wuzhou',
            ),
            404 => 
            array (
                'id' => 2905,
                'state_id' => 31,
                'name' => 'Beihai',
            ),
            405 => 
            array (
                'id' => 2906,
                'state_id' => 31,
                'name' => 'Fangchenggang',
            ),
            406 => 
            array (
                'id' => 2907,
                'state_id' => 31,
                'name' => 'Qinzhou',
            ),
            407 => 
            array (
                'id' => 2908,
                'state_id' => 31,
                'name' => 'Guigang',
            ),
            408 => 
            array (
                'id' => 2909,
                'state_id' => 31,
                'name' => 'Yulin',
            ),
            409 => 
            array (
                'id' => 2910,
                'state_id' => 31,
                'name' => 'Baise',
            ),
            410 => 
            array (
                'id' => 2911,
                'state_id' => 31,
                'name' => 'Hezhou',
            ),
            411 => 
            array (
                'id' => 2912,
                'state_id' => 31,
                'name' => 'Hechi',
            ),
            412 => 
            array (
                'id' => 2913,
                'state_id' => 31,
                'name' => 'Laibin',
            ),
            413 => 
            array (
                'id' => 2914,
                'state_id' => 31,
                'name' => 'Chongzuo',
            ),
            414 => 
            array (
                'id' => 2915,
                'state_id' => 33,
                'name' => 'Haikou',
            ),
            415 => 
            array (
                'id' => 2916,
                'state_id' => 33,
                'name' => 'Sanya',
            ),
            416 => 
            array (
                'id' => 2917,
                'state_id' => 33,
                'name' => 'Shansha',
            ),
            417 => 
            array (
                'id' => 2918,
                'state_id' => 33,
                'name' => 'Wuzhishan',
            ),
            418 => 
            array (
                'id' => 2919,
                'state_id' => 33,
                'name' => 'Qionghai',
            ),
            419 => 
            array (
                'id' => 2920,
                'state_id' => 33,
                'name' => 'Danzhou',
            ),
            420 => 
            array (
                'id' => 2921,
                'state_id' => 33,
                'name' => 'Wenchang',
            ),
            421 => 
            array (
                'id' => 2922,
                'state_id' => 33,
                'name' => 'Wanning',
            ),
            422 => 
            array (
                'id' => 2923,
                'state_id' => 33,
                'name' => 'Dongfang',
            ),
            423 => 
            array (
                'id' => 2924,
                'state_id' => 33,
                'name' => 'Ding\'an',
              ),
            424 => 
            array (
                'id' => 2925,
                'state_id' => 33,
                'name' => 'Tunchang',
              ),
            425 => 
            array (
                'id' => 2926,
                'state_id' => 33,
                'name' => 'Cengmai',
              ),
            426 => 
            array (
                'id' => 2927,
                'state_id' => 33,
                'name' => 'Lingao',
              ),
            427 => 
            array (
                'id' => 2928,
                'state_id' => 33,
                'name' => 'Baisha Li Autonomous County',
              ),
            428 => 
            array (
                'id' => 2929,
                'state_id' => 33,
                'name' => 'Jiang Li Autonomous County',
              ),
            429 => 
            array (
                'id' => 2930,
                'state_id' => 33,
                'name' => 'Ledong Li Autonomous County',
              ),
            430 => 
            array (
                'id' => 2931,
                'state_id' => 33,
                'name' => 'Lingshui Li Autonomous County',
              ),
            431 => 
            array (
                'id' => 2932,
                'state_id' => 33,
                'name' => 'Baoting Li-Miao Autonomous County',
              ),
            432 => 
            array (
                'id' => 2933,
                'state_id' => 33,
                'name' => 'Qiongzhong Li-Miao Autonomous County',
              ),
            433 => 
            array (
                'id' => 2934,
                'state_id' => 27,
                'name' => 'Wanzhou',
            ),
            434 => 
            array (
                'id' => 2935,
                'state_id' => 27,
                'name' => 'Fuling',
            ),
            435 => 
            array (
                'id' => 2936,
                'state_id' => 27,
                'name' => 'Yuzhong',
            ),
            436 => 
            array (
                'id' => 2937,
                'state_id' => 27,
                'name' => 'Dadukou',
            ),
            437 => 
            array (
                'id' => 2938,
                'state_id' => 27,
                'name' => 'Jiangbei',
            ),
            438 => 
            array (
                'id' => 2939,
                'state_id' => 27,
                'name' => 'Shapingba',
            ),
            439 => 
            array (
                'id' => 2940,
                'state_id' => 27,
                'name' => 'Jiulongpo',
            ),
            440 => 
            array (
                'id' => 2941,
                'state_id' => 27,
                'name' => 'Nan\'an',
            ),
            441 => 
            array (
                'id' => 2942,
                'state_id' => 27,
                'name' => 'Beibei',
            ),
            442 => 
            array (
                'id' => 2943,
                'state_id' => 27,
                'name' => 'Liangjiang',
            ),
            443 => 
            array (
                'id' => 2944,
                'state_id' => 27,
                'name' => 'Wansheng',
            ),
            444 => 
            array (
                'id' => 2945,
                'state_id' => 27,
                'name' => 'Shuangqiao',
            ),
            445 => 
            array (
                'id' => 2946,
                'state_id' => 27,
                'name' => 'Yubei',
            ),
            446 => 
            array (
                'id' => 2947,
                'state_id' => 27,
                'name' => 'Ba\'nan',
            ),
            447 => 
            array (
                'id' => 2948,
                'state_id' => 27,
                'name' => 'Changshou',
            ),
            448 => 
            array (
                'id' => 2949,
                'state_id' => 27,
                'name' => 'Qijiang',
            ),
            449 => 
            array (
                'id' => 2950,
                'state_id' => 27,
                'name' => 'Tongnan',
            ),
            450 => 
            array (
                'id' => 2951,
                'state_id' => 27,
                'name' => 'Tongliang',
            ),
            451 => 
            array (
                'id' => 2952,
                'state_id' => 27,
                'name' => 'Dazu',
            ),
            452 => 
            array (
                'id' => 2953,
                'state_id' => 27,
                'name' => 'Rongchang',
            ),
            453 => 
            array (
                'id' => 2954,
                'state_id' => 27,
                'name' => 'Bishan',
            ),
            454 => 
            array (
                'id' => 2955,
                'state_id' => 27,
                'name' => 'Liangping',
            ),
            455 => 
            array (
                'id' => 2956,
                'state_id' => 27,
                'name' => 'Chengkou',
            ),
            456 => 
            array (
                'id' => 2957,
                'state_id' => 27,
                'name' => 'Fengdu',
            ),
            457 => 
            array (
                'id' => 2958,
                'state_id' => 27,
                'name' => 'Dianjiang',
            ),
            458 => 
            array (
                'id' => 2959,
                'state_id' => 27,
                'name' => 'Wulong',
            ),
            459 => 
            array (
                'id' => 2960,
                'state_id' => 27,
                'name' => 'Zhongxian',
            ),
            460 => 
            array (
                'id' => 2961,
                'state_id' => 27,
                'name' => 'Kaixian',
            ),
            461 => 
            array (
                'id' => 2962,
                'state_id' => 27,
                'name' => 'Yunyang',
            ),
            462 => 
            array (
                'id' => 2963,
                'state_id' => 27,
                'name' => 'Fengjie',
            ),
            463 => 
            array (
                'id' => 2964,
                'state_id' => 27,
                'name' => 'Wushan',
            ),
            464 => 
            array (
                'id' => 2965,
                'state_id' => 27,
                'name' => 'Wuxi',
            ),
            465 => 
            array (
                'id' => 2966,
                'state_id' => 27,
                'name' => 'Qianjiang',
            ),
            466 => 
            array (
                'id' => 2967,
                'state_id' => 27,
                'name' => 'Shizhu Tujia Autonomous Country',
            ),
            467 => 
            array (
                'id' => 2968,
                'state_id' => 27,
                'name' => 'Xiushan Tujia-Miao Autonomous Country',
            ),
            468 => 
            array (
                'id' => 2969,
                'state_id' => 27,
                'name' => 'Youyang Tujia-Miao Autonomous Country',
            ),
            469 => 
            array (
                'id' => 2970,
                'state_id' => 27,
                'name' => 'Pengshui Miao-Tujia Autonomous Country',
            ),
            470 => 
            array (
                'id' => 2971,
                'state_id' => 27,
                'name' => 'Jiangjin',
            ),
            471 => 
            array (
                'id' => 2972,
                'state_id' => 27,
                'name' => 'Hechuan',
            ),
            472 => 
            array (
                'id' => 2973,
                'state_id' => 27,
                'name' => 'Yongchuan',
            ),
            473 => 
            array (
                'id' => 2974,
                'state_id' => 27,
                'name' => 'Liangjiangxinqu',
            ),
            474 => 
            array (
                'id' => 2975,
                'state_id' => 50,
                'name' => 'Chengdu',
            ),
            475 => 
            array (
                'id' => 2976,
                'state_id' => 50,
                'name' => 'Zigong',
            ),
            476 => 
            array (
                'id' => 2977,
                'state_id' => 50,
                'name' => 'Panzhihua',
            ),
            477 => 
            array (
                'id' => 2978,
                'state_id' => 50,
                'name' => 'Luzhou',
            ),
            478 => 
            array (
                'id' => 2979,
                'state_id' => 50,
                'name' => 'Deyang',
            ),
            479 => 
            array (
                'id' => 2980,
                'state_id' => 50,
                'name' => 'Mianyang',
            ),
            480 => 
            array (
                'id' => 2981,
                'state_id' => 50,
                'name' => 'Guangyuan',
            ),
            481 => 
            array (
                'id' => 2982,
                'state_id' => 50,
                'name' => 'Suining',
            ),
            482 => 
            array (
                'id' => 2983,
                'state_id' => 50,
                'name' => 'Neijiang',
            ),
            483 => 
            array (
                'id' => 2984,
                'state_id' => 50,
                'name' => 'Leshan',
            ),
            484 => 
            array (
                'id' => 2985,
                'state_id' => 50,
                'name' => 'Nanchong',
            ),
            485 => 
            array (
                'id' => 2986,
                'state_id' => 50,
                'name' => 'Meishan',
            ),
            486 => 
            array (
                'id' => 2987,
                'state_id' => 50,
                'name' => 'Yibin',
            ),
            487 => 
            array (
                'id' => 2988,
                'state_id' => 50,
                'name' => 'Guang\'an',
            ),
            488 => 
            array (
                'id' => 2989,
                'state_id' => 50,
                'name' => 'Dazhou',
            ),
            489 => 
            array (
                'id' => 2990,
                'state_id' => 50,
                'name' => 'Ya\'an',
            ),
            490 => 
            array (
                'id' => 2991,
                'state_id' => 50,
                'name' => 'Bazhong',
            ),
            491 => 
            array (
                'id' => 2992,
                'state_id' => 50,
                'name' => 'Ziyang',
            ),
            492 => 
            array (
                'id' => 2993,
                'state_id' => 50,
                'name' => 'Aba Tibetan-Qiang Autonomous Prefecture',
            ),
            493 => 
            array (
                'id' => 2994,
                'state_id' => 50,
                'name' => 'Garze Tibetan Autonomous Prefecture',
            ),
            494 => 
            array (
                'id' => 2995,
                'state_id' => 50,
                'name' => 'Liangshan Yi Autonomous Prefecture',
            ),
            495 => 
            array (
                'id' => 2996,
                'state_id' => 32,
                'name' => 'Guiyang',
            ),
            496 => 
            array (
                'id' => 2997,
                'state_id' => 32,
                'name' => 'Liupanshui',
            ),
            497 => 
            array (
                'id' => 2998,
                'state_id' => 32,
                'name' => 'Zunyi',
            ),
            498 => 
            array (
                'id' => 2999,
                'state_id' => 32,
                'name' => 'Anshun',
            ),
            499 => 
            array (
                'id' => 3000,
                'state_id' => 32,
                'name' => 'Tongren',
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 3001,
                'state_id' => 32,
                'name' => 'Qianxinan Buyi-Miao Autonomous Prefecture',
            ),
            1 => 
            array (
                'id' => 3002,
                'state_id' => 32,
                'name' => 'Bijie',
            ),
            2 => 
            array (
                'id' => 3003,
                'state_id' => 32,
                'name' => 'Qiandongnan Miao-Dong Autonomous Prefecture',
            ),
            3 => 
            array (
                'id' => 3004,
                'state_id' => 32,
                'name' => 'Qiannan Buyi Autonomous Prefecture',
            ),
            4 => 
            array (
                'id' => 3005,
                'state_id' => 55,
                'name' => 'Kunming',
            ),
            5 => 
            array (
                'id' => 3006,
                'state_id' => 55,
                'name' => 'Qujing',
            ),
            6 => 
            array (
                'id' => 3007,
                'state_id' => 55,
                'name' => 'Yuxi',
            ),
            7 => 
            array (
                'id' => 3008,
                'state_id' => 55,
                'name' => 'Baoshan',
            ),
            8 => 
            array (
                'id' => 3009,
                'state_id' => 55,
                'name' => 'Zhaotong',
            ),
            9 => 
            array (
                'id' => 3010,
                'state_id' => 55,
                'name' => 'Lijiang',
            ),
            10 => 
            array (
                'id' => 3011,
                'state_id' => 55,
                'name' => 'Pu\'er',
            ),
            11 => 
            array (
                'id' => 3012,
                'state_id' => 55,
                'name' => 'Lincang',
            ),
            12 => 
            array (
                'id' => 3013,
                'state_id' => 55,
                'name' => 'Chuxiong Yi Autonomous Prefecture',
            ),
            13 => 
            array (
                'id' => 3014,
                'state_id' => 55,
                'name' => 'Honghe Hani-Yi Autonomous Prefecture',
            ),
            14 => 
            array (
                'id' => 3015,
                'state_id' => 55,
                'name' => 'Wenshan Zhuang-Miao Autonomous Prefecture',
            ),
            15 => 
            array (
                'id' => 3016,
                'state_id' => 55,
                'name' => 'Xishuangbanna Dai Autonomous Prefecture',
            ),
            16 => 
            array (
                'id' => 3017,
                'state_id' => 55,
                'name' => 'Dali Bai Autonomous Prefecture',
            ),
            17 => 
            array (
                'id' => 3018,
                'state_id' => 55,
                'name' => 'Dehong Dai-Jingpo Autonomous Prefecture',
            ),
            18 => 
            array (
                'id' => 3019,
                'state_id' => 55,
                'name' => 'Nujiang Lisu Autonomous Prefecture',
            ),
            19 => 
            array (
                'id' => 3020,
                'state_id' => 55,
                'name' => 'Diqing Tibetan Autonomous Prefecture',
            ),
            20 => 
            array (
                'id' => 3021,
                'state_id' => 53,
                'name' => 'Lhasa',
            ),
            21 => 
            array (
                'id' => 3022,
                'state_id' => 53,
                'name' => 'Qamdo',
            ),
            22 => 
            array (
                'id' => 3023,
                'state_id' => 53,
                'name' => 'Shannan',
            ),
            23 => 
            array (
                'id' => 3024,
                'state_id' => 53,
                'name' => 'Xigaze',
            ),
            24 => 
            array (
                'id' => 3025,
                'state_id' => 53,
                'name' => 'Nagqu',
            ),
            25 => 
            array (
                'id' => 3026,
                'state_id' => 53,
                'name' => 'Ngari',
            ),
            26 => 
            array (
                'id' => 3027,
                'state_id' => 53,
                'name' => 'Nyingchi',
            ),
            27 => 
            array (
                'id' => 3028,
                'state_id' => 46,
                'name' => 'Xi\'an',
            ),
            28 => 
            array (
                'id' => 3029,
                'state_id' => 46,
                'name' => 'Tongchuan',
            ),
            29 => 
            array (
                'id' => 3030,
                'state_id' => 46,
                'name' => 'Baoji',
            ),
            30 => 
            array (
                'id' => 3031,
                'state_id' => 46,
                'name' => 'Xianyang',
            ),
            31 => 
            array (
                'id' => 3032,
                'state_id' => 46,
                'name' => 'Weinan',
            ),
            32 => 
            array (
                'id' => 3033,
                'state_id' => 46,
                'name' => 'Yan\'an',
            ),
            33 => 
            array (
                'id' => 3034,
                'state_id' => 46,
                'name' => 'Hanzhong',
            ),
            34 => 
            array (
                'id' => 3035,
                'state_id' => 46,
                'name' => 'Yulin',
            ),
            35 => 
            array (
                'id' => 3036,
                'state_id' => 46,
                'name' => 'Ankang',
            ),
            36 => 
            array (
                'id' => 3037,
                'state_id' => 46,
                'name' => 'Shangluo',
            ),
            37 => 
            array (
                'id' => 3038,
                'state_id' => 29,
                'name' => 'Lanzhou',
            ),
            38 => 
            array (
                'id' => 3039,
                'state_id' => 29,
                'name' => 'Jinchang',
            ),
            39 => 
            array (
                'id' => 3040,
                'state_id' => 29,
                'name' => 'Baiyin',
            ),
            40 => 
            array (
                'id' => 3041,
                'state_id' => 29,
                'name' => 'Tianshui',
            ),
            41 => 
            array (
                'id' => 3042,
                'state_id' => 29,
                'name' => 'Jiayuguan',
            ),
            42 => 
            array (
                'id' => 3043,
                'state_id' => 29,
                'name' => 'Wuwei',
            ),
            43 => 
            array (
                'id' => 3044,
                'state_id' => 29,
                'name' => 'Zhangye',
            ),
            44 => 
            array (
                'id' => 3045,
                'state_id' => 29,
                'name' => 'Pingliang',
            ),
            45 => 
            array (
                'id' => 3046,
                'state_id' => 29,
                'name' => 'Jiuquan',
            ),
            46 => 
            array (
                'id' => 3047,
                'state_id' => 29,
                'name' => 'Qingyang',
            ),
            47 => 
            array (
                'id' => 3048,
                'state_id' => 29,
                'name' => 'Dingxi',
            ),
            48 => 
            array (
                'id' => 3049,
                'state_id' => 29,
                'name' => 'Longnan',
            ),
            49 => 
            array (
                'id' => 3050,
                'state_id' => 29,
                'name' => 'Linxia Hui Autonomous Prefecture',
            ),
            50 => 
            array (
                'id' => 3051,
                'state_id' => 29,
                'name' => 'Gannan Tibetan Autonomous Prefecture',
            ),
            51 => 
            array (
                'id' => 3052,
                'state_id' => 45,
                'name' => 'Xining',
            ),
            52 => 
            array (
                'id' => 3053,
                'state_id' => 45,
                'name' => 'Haidong',
            ),
            53 => 
            array (
                'id' => 3054,
                'state_id' => 45,
                'name' => 'Haibei Tibetan Autonomous Prefecture',
            ),
            54 => 
            array (
                'id' => 3055,
                'state_id' => 45,
                'name' => 'Huangnan Tibetan Autonomous Prefecture',
            ),
            55 => 
            array (
                'id' => 3056,
                'state_id' => 45,
                'name' => 'Hainan Tibetan Autonomous Prefecture',
            ),
            56 => 
            array (
                'id' => 3057,
                'state_id' => 45,
                'name' => 'Guoluo Tibetan Autonomous Prefecture',
            ),
            57 => 
            array (
                'id' => 3058,
                'state_id' => 45,
                'name' => 'Yushu Tibetan Autonomous Prefecture',
            ),
            58 => 
            array (
                'id' => 3059,
                'state_id' => 45,
                'name' => 'Haixi Mongol-Tibetan Autonomous Prefecture',
            ),
            59 => 
            array (
                'id' => 3060,
                'state_id' => 44,
                'name' => 'Yinchuan',
            ),
            60 => 
            array (
                'id' => 3061,
                'state_id' => 44,
                'name' => 'Shizuishan',
            ),
            61 => 
            array (
                'id' => 3062,
                'state_id' => 44,
                'name' => 'Wuzhong',
            ),
            62 => 
            array (
                'id' => 3063,
                'state_id' => 44,
                'name' => 'Guyuan',
            ),
            63 => 
            array (
                'id' => 3064,
                'state_id' => 44,
                'name' => 'Zhongwei',
            ),
            64 => 
            array (
                'id' => 3065,
                'state_id' => 54,
                'name' => 'Urumqi',
            ),
            65 => 
            array (
                'id' => 3066,
                'state_id' => 54,
                'name' => 'Karamay',
            ),
            66 => 
            array (
                'id' => 3067,
                'state_id' => 54,
                'name' => 'Turpan',
            ),
            67 => 
            array (
                'id' => 3068,
                'state_id' => 54,
                'name' => 'Hami',
            ),
            68 => 
            array (
                'id' => 3069,
                'state_id' => 54,
                'name' => 'Changji Hui Autonomous Prefecture',
            ),
            69 => 
            array (
                'id' => 3070,
                'state_id' => 54,
                'name' => 'Bortala Mongol Autonomous Prefecture',
            ),
            70 => 
            array (
                'id' => 3071,
                'state_id' => 54,
                'name' => 'Bayingolin Mongol Autonomous Prefecture',
            ),
            71 => 
            array (
                'id' => 3072,
                'state_id' => 54,
                'name' => 'Aksu',
            ),
            72 => 
            array (
                'id' => 3073,
                'state_id' => 54,
                'name' => 'Kizilsu Kirgiz Autonomous Prefecture',
            ),
            73 => 
            array (
                'id' => 3074,
                'state_id' => 54,
                'name' => 'Kashi',
            ),
            74 => 
            array (
                'id' => 3075,
                'state_id' => 54,
                'name' => 'Hotan',
            ),
            75 => 
            array (
                'id' => 3076,
                'state_id' => 54,
                'name' => 'Ili Kazakh Autonomous Prefecture',
            ),
            76 => 
            array (
                'id' => 3077,
                'state_id' => 54,
                'name' => 'Tacheng',
            ),
            77 => 
            array (
                'id' => 3078,
                'state_id' => 54,
                'name' => 'Altay',
            ),
            78 => 
            array (
                'id' => 3079,
                'state_id' => 54,
                'name' => 'Shihezi',
            ),
            79 => 
            array (
                'id' => 3080,
                'state_id' => 54,
                'name' => 'Alar',
            ),
            80 => 
            array (
                'id' => 3081,
                'state_id' => 54,
                'name' => 'Tumsuk',
            ),
            81 => 
            array (
                'id' => 3082,
                'state_id' => 54,
                'name' => 'Wujiaqu',
            ),
            82 => 
            array (
                'id' => 3083,
                'state_id' => 54,
                'name' => 'Beitun',
            ),
            83 => 
            array (
                'id' => 3084,
                'state_id' => 51,
                'name' => 'Taipei City',
            ),
            84 => 
            array (
                'id' => 3085,
                'state_id' => 51,
                'name' => 'Kaohsiung City',
            ),
            85 => 
            array (
                'id' => 3086,
                'state_id' => 51,
                'name' => 'Keelung City',
            ),
            86 => 
            array (
                'id' => 3087,
                'state_id' => 51,
                'name' => 'Taichung City',
            ),
            87 => 
            array (
                'id' => 3088,
                'state_id' => 51,
                'name' => 'Tainan City',
            ),
            88 => 
            array (
                'id' => 3089,
                'state_id' => 51,
                'name' => 'Hsinchu City',
            ),
            89 => 
            array (
                'id' => 3090,
                'state_id' => 51,
                'name' => 'Chiayi City',
            ),
            90 => 
            array (
                'id' => 3091,
                'state_id' => 51,
                'name' => 'Taipei County',
            ),
            91 => 
            array (
                'id' => 3092,
                'state_id' => 51,
                'name' => 'Ilan County',
            ),
            92 => 
            array (
                'id' => 3093,
                'state_id' => 51,
                'name' => 'Taoyuan County',
            ),
            93 => 
            array (
                'id' => 3094,
                'state_id' => 51,
                'name' => 'Hsinchu County',
            ),
            94 => 
            array (
                'id' => 3095,
                'state_id' => 51,
                'name' => 'Miaoli County',
            ),
            95 => 
            array (
                'id' => 3096,
                'state_id' => 51,
                'name' => 'Taichung County',
            ),
            96 => 
            array (
                'id' => 3097,
                'state_id' => 51,
                'name' => 'Changhwa County',
            ),
            97 => 
            array (
                'id' => 3098,
                'state_id' => 51,
                'name' => 'Nantou County',
            ),
            98 => 
            array (
                'id' => 3099,
                'state_id' => 51,
                'name' => 'Yunnlin County',
            ),
            99 => 
            array (
                'id' => 3100,
                'state_id' => 51,
                'name' => 'Chiayi County',
            ),
            100 => 
            array (
                'id' => 3101,
                'state_id' => 51,
                'name' => 'Tainan County',
            ),
            101 => 
            array (
                'id' => 3102,
                'state_id' => 51,
                'name' => 'Kaohsiung County',
            ),
            102 => 
            array (
                'id' => 3103,
                'state_id' => 51,
                'name' => 'Pingtung County',
            ),
            103 => 
            array (
                'id' => 3104,
                'state_id' => 51,
                'name' => 'Taitung County',
            ),
            104 => 
            array (
                'id' => 3105,
                'state_id' => 51,
                'name' => 'Hualian County',
            ),
            105 => 
            array (
                'id' => 3106,
                'state_id' => 51,
                'name' => 'Penghu County',
            ),
            106 => 
            array (
                'id' => 3107,
                'state_id' => 114,
                'name' => 'Palmerston',
              ),
            107 => 
            array (
                'id' => 3108,
                'state_id' => 114,
                'name' => 'Darwin',
              ),
            108 => 
            array (
                'id' => 3109,
                'state_id' => 112,
                'name' => 'Canberra',
              ),
            109 => 
            array (
                'id' => 3110,
                'state_id' => 115,
                'name' => 'Brisbane',
              ),
            110 => 
            array (
                'id' => 3111,
                'state_id' => 115,
                'name' => 'Gold Coast',
              ),
            111 => 
            array (
                'id' => 3112,
                'state_id' => 115,
                'name' => 'Cairns',
              ),
            112 => 
            array (
                'id' => 3113,
                'state_id' => 115,
                'name' => 'Caloundra',
              ),
            113 => 
            array (
                'id' => 3114,
                'state_id' => 115,
                'name' => 'Townsville',
              ),
            114 => 
            array (
                'id' => 3115,
                'state_id' => 115,
                'name' => 'Toowoomba',
              ),
            115 => 
            array (
                'id' => 3116,
                'state_id' => 116,
                'name' => 'Adelaide',
              ),
            116 => 
            array (
                'id' => 3117,
                'state_id' => 116,
                'name' => 'Port Augusta',
              ),
            117 => 
            array (
                'id' => 3118,
                'state_id' => 116,
                'name' => 'Mount Gambier',
              ),
            118 => 
            array (
                'id' => 3119,
                'state_id' => 116,
                'name' => 'Whyalla',
              ),
            119 => 
            array (
                'id' => 3120,
                'state_id' => 116,
                'name' => 'Port Lincoln',
              ),
            120 => 
            array (
                'id' => 3121,
                'state_id' => 116,
                'name' => 'Murray Bridge',
              ),
            121 => 
            array (
                'id' => 3122,
                'state_id' => 116,
                'name' => 'Port Pirie',
              ),
            122 => 
            array (
                'id' => 3123,
                'state_id' => 116,
                'name' => 'Victor Harbor',
              ),
            123 => 
            array (
                'id' => 3124,
                'state_id' => 117,
                'name' => 'Burnie',
              ),
            124 => 
            array (
                'id' => 3125,
                'state_id' => 117,
                'name' => 'Devonport',
              ),
            125 => 
            array (
                'id' => 3126,
                'state_id' => 117,
                'name' => 'Hobart',
              ),
            126 => 
            array (
                'id' => 3127,
                'state_id' => 117,
                'name' => 'Launceston',
              ),
            127 => 
            array (
                'id' => 3128,
                'state_id' => 118,
                'name' => 'Geelong',
              ),
            128 => 
            array (
                'id' => 3129,
                'state_id' => 118,
                'name' => 'Melbourne',
              ),
            129 => 
            array (
                'id' => 3130,
                'state_id' => 119,
                'name' => 'Albany',
              ),
            130 => 
            array (
                'id' => 3131,
                'state_id' => 119,
                'name' => 'Bunbury',
              ),
            131 => 
            array (
                'id' => 3132,
                'state_id' => 119,
                'name' => 'Fremantle',
              ),
            132 => 
            array (
                'id' => 3133,
                'state_id' => 119,
                'name' => 'Geraldton',
              ),
            133 => 
            array (
                'id' => 3134,
                'state_id' => 119,
                'name' => 'Kalgoorlie',
              ),
            134 => 
            array (
                'id' => 3135,
                'state_id' => 119,
                'name' => 'Mandurah',
              ),
            135 => 
            array (
                'id' => 3136,
                'state_id' => 119,
                'name' => 'Perth',
              ),
            136 => 
            array (
                'id' => 3137,
                'state_id' => 113,
                'name' => 'Newcastle',
              ),
            137 => 
            array (
                'id' => 3138,
                'state_id' => 113,
                'name' => 'Wollongong',
              ),
            138 => 
            array (
                'id' => 3139,
                'state_id' => 113,
                'name' => 'Sydney',
              ),
            139 => 
            array (
                'id' => 3140,
                'state_id' => 3,
                'name' => 'Dalseong-gun',
              ),
            140 => 
            array (
                'id' => 3141,
                'state_id' => 3,
                'name' => 'Daegu',
              ),
            141 => 
            array (
                'id' => 3142,
                'state_id' => 3,
                'name' => 'Suseong-gu',
              ),
            142 => 
            array (
                'id' => 3143,
                'state_id' => 4,
                'name' => 'Chuncheon',
              ),
            143 => 
            array (
                'id' => 3144,
                'state_id' => 4,
                'name' => 'Donghae',
              ),
            144 => 
            array (
                'id' => 3145,
                'state_id' => 4,
                'name' => 'Goseong County',
              ),
            145 => 
            array (
                'id' => 3146,
                'state_id' => 4,
                'name' => 'Hoengseong County',
              ),
            146 => 
            array (
                'id' => 3147,
                'state_id' => 4,
                'name' => 'Hongcheon County',
              ),
            147 => 
            array (
                'id' => 3148,
                'state_id' => 4,
                'name' => 'Hwacheon County',
              ),
            148 => 
            array (
                'id' => 3149,
                'state_id' => 4,
                'name' => 'Gangneung',
              ),
            149 => 
            array (
                'id' => 3150,
                'state_id' => 4,
                'name' => 'Jeongseon County',
              ),
            150 => 
            array (
                'id' => 3151,
                'state_id' => 4,
                'name' => 'Inje County',
              ),
            151 => 
            array (
                'id' => 3152,
                'state_id' => 4,
                'name' => 'Yeongwol County',
              ),
            152 => 
            array (
                'id' => 3153,
                'state_id' => 4,
                'name' => 'Pyeongchang County',
              ),
            153 => 
            array (
                'id' => 3154,
                'state_id' => 4,
                'name' => 'Samcheok',
              ),
            154 => 
            array (
                'id' => 3155,
                'state_id' => 4,
                'name' => 'Sokcho',
              ),
            155 => 
            array (
                'id' => 3156,
                'state_id' => 4,
                'name' => 'Taebaek',
              ),
            156 => 
            array (
                'id' => 3157,
                'state_id' => 4,
                'name' => 'Cheorwon County',
              ),
            157 => 
            array (
                'id' => 3158,
                'state_id' => 4,
                'name' => 'Yangyang County',
              ),
            158 => 
            array (
                'id' => 3159,
                'state_id' => 4,
                'name' => 'Yanggu County',
              ),
            159 => 
            array (
                'id' => 3160,
                'state_id' => 4,
                'name' => 'Wonju',
              ),
            160 => 
            array (
                'id' => 3161,
                'state_id' => 5,
                'name' => 'Anseong',
              ),
            161 => 
            array (
                'id' => 3162,
                'state_id' => 5,
                'name' => 'Ansan',
              ),
            162 => 
            array (
                'id' => 3163,
                'state_id' => 5,
                'name' => 'Anyang',
              ),
            163 => 
            array (
                'id' => 3164,
                'state_id' => 5,
                'name' => 'Pocheon',
              ),
            164 => 
            array (
                'id' => 3165,
                'state_id' => 5,
                'name' => 'Seongnam',
              ),
            165 => 
            array (
                'id' => 3166,
                'state_id' => 5,
                'name' => 'Dongducheon',
              ),
            166 => 
            array (
                'id' => 3167,
                'state_id' => 5,
                'name' => 'Bucheon',
              ),
            167 => 
            array (
                'id' => 3168,
                'state_id' => 5,
                'name' => 'Goyang',
              ),
            168 => 
            array (
                'id' => 3169,
                'state_id' => 5,
                'name' => 'Gwangmyeong',
              ),
            169 => 
            array (
                'id' => 3170,
                'state_id' => 5,
                'name' => 'Gwangju',
              ),
            170 => 
            array (
                'id' => 3171,
                'state_id' => 5,
                'name' => 'Gwacheon',
              ),
            171 => 
            array (
                'id' => 3172,
                'state_id' => 5,
                'name' => 'Hanam',
              ),
            172 => 
            array (
                'id' => 3173,
                'state_id' => 5,
                'name' => 'Hwaseong',
              ),
            173 => 
            array (
                'id' => 3174,
                'state_id' => 5,
                'name' => 'Gapyeong County',
              ),
            174 => 
            array (
                'id' => 3175,
                'state_id' => 5,
                'name' => 'Gimpo',
              ),
            175 => 
            array (
                'id' => 3176,
                'state_id' => 5,
                'name' => 'Guri',
              ),
            176 => 
            array (
                'id' => 3177,
                'state_id' => 5,
                'name' => 'Gunpo',
              ),
            177 => 
            array (
                'id' => 3178,
                'state_id' => 5,
                'name' => 'Yeoju County',
              ),
            178 => 
            array (
                'id' => 3179,
                'state_id' => 5,
                'name' => 'Icheon',
              ),
            179 => 
            array (
                'id' => 3180,
                'state_id' => 5,
                'name' => 'Yeoncheon County',
              ),
            180 => 
            array (
                'id' => 3181,
                'state_id' => 5,
                'name' => 'Yongin',
              ),
            181 => 
            array (
                'id' => 3182,
                'state_id' => 5,
                'name' => 'Namyangju',
              ),
            182 => 
            array (
                'id' => 3183,
                'state_id' => 5,
                'name' => 'Pyeongtaek',
              ),
            183 => 
            array (
                'id' => 3184,
                'state_id' => 5,
                'name' => 'Paju',
              ),
            184 => 
            array (
                'id' => 3185,
                'state_id' => 5,
                'name' => 'Siheung',
              ),
            185 => 
            array (
                'id' => 3186,
                'state_id' => 5,
                'name' => 'Suwon',
              ),
            186 => 
            array (
                'id' => 3187,
                'state_id' => 5,
                'name' => 'Osan',
              ),
            187 => 
            array (
                'id' => 3188,
                'state_id' => 5,
                'name' => 'Yangpyeong County',
              ),
            188 => 
            array (
                'id' => 3189,
                'state_id' => 5,
                'name' => 'Yangju',
              ),
            189 => 
            array (
                'id' => 3190,
                'state_id' => 5,
                'name' => 'Uiwang',
              ),
            190 => 
            array (
                'id' => 3191,
                'state_id' => 5,
                'name' => 'Uijeongbu',
              ),
            191 => 
            array (
                'id' => 3192,
                'state_id' => 1,
                'name' => 'Andong',
              ),
            192 => 
            array (
                'id' => 3193,
                'state_id' => 1,
                'name' => 'Bonghwa County',
              ),
            193 => 
            array (
                'id' => 3194,
                'state_id' => 1,
                'name' => 'Goryeong County',
              ),
            194 => 
            array (
                'id' => 3195,
                'state_id' => 1,
                'name' => 'Gumi',
              ),
            195 => 
            array (
                'id' => 3196,
                'state_id' => 1,
                'name' => 'Gimcheon',
              ),
            196 => 
            array (
                'id' => 3197,
                'state_id' => 1,
                'name' => 'Gunwi County',
              ),
            197 => 
            array (
                'id' => 3198,
                'state_id' => 1,
                'name' => 'Yecheon County',
              ),
            198 => 
            array (
                'id' => 3199,
                'state_id' => 1,
                'name' => 'Pohang',
              ),
            199 => 
            array (
                'id' => 3200,
                'state_id' => 1,
                'name' => 'Chilgok County',
              ),
            200 => 
            array (
                'id' => 3201,
                'state_id' => 1,
                'name' => 'Cheongdo County',
              ),
            201 => 
            array (
                'id' => 3202,
                'state_id' => 1,
                'name' => 'Cheongsong County',
              ),
            202 => 
            array (
                'id' => 3203,
                'state_id' => 1,
                'name' => 'Gyeongsan',
              ),
            203 => 
            array (
                'id' => 3204,
                'state_id' => 1,
                'name' => 'Gyeongju',
              ),
            204 => 
            array (
                'id' => 3205,
                'state_id' => 1,
                'name' => 'Yeongju',
              ),
            205 => 
            array (
                'id' => 3206,
                'state_id' => 1,
                'name' => 'Sangju',
              ),
            206 => 
            array (
                'id' => 3207,
                'state_id' => 1,
                'name' => 'Uljin County',
              ),
            207 => 
            array (
                'id' => 3208,
                'state_id' => 1,
                'name' => 'Mungyeong',
              ),
            208 => 
            array (
                'id' => 3209,
                'state_id' => 1,
                'name' => 'Seongju County',
              ),
            209 => 
            array (
                'id' => 3210,
                'state_id' => 1,
                'name' => 'Uiseong County',
              ),
            210 => 
            array (
                'id' => 3211,
                'state_id' => 1,
                'name' => 'Yeongyang County',
              ),
            211 => 
            array (
                'id' => 3212,
                'state_id' => 1,
                'name' => 'Yeongdeok County',
              ),
            212 => 
            array (
                'id' => 3213,
                'state_id' => 1,
                'name' => 'Yeongcheon',
              ),
            213 => 
            array (
                'id' => 3214,
                'state_id' => 1,
                'name' => 'Ulleung County',
              ),
            214 => 
            array (
                'id' => 3215,
                'state_id' => 2,
                'name' => 'Changnyeong County',
              ),
            215 => 
            array (
                'id' => 3216,
                'state_id' => 2,
                'name' => 'Changwon',
              ),
            216 => 
            array (
                'id' => 3217,
                'state_id' => 2,
                'name' => 'Goseong County',
              ),
            217 => 
            array (
                'id' => 3218,
                'state_id' => 2,
                'name' => 'Hadong County',
              ),
            218 => 
            array (
                'id' => 3219,
                'state_id' => 2,
                'name' => 'Gimhae',
              ),
            219 => 
            array (
                'id' => 3220,
                'state_id' => 2,
                'name' => 'Jinju',
              ),
            220 => 
            array (
                'id' => 3221,
                'state_id' => 2,
                'name' => 'Geochang County',
              ),
            221 => 
            array (
                'id' => 3222,
                'state_id' => 2,
                'name' => 'Geoje',
              ),
            222 => 
            array (
                'id' => 3223,
                'state_id' => 2,
                'name' => 'Yangsan',
              ),
            223 => 
            array (
                'id' => 3224,
                'state_id' => 2,
                'name' => 'Masan',
              ),
            224 => 
            array (
                'id' => 3225,
                'state_id' => 2,
                'name' => 'Miryang',
              ),
            225 => 
            array (
                'id' => 3226,
                'state_id' => 2,
                'name' => 'Namhae County',
              ),
            226 => 
            array (
                'id' => 3227,
                'state_id' => 2,
                'name' => 'Sancheong County',
              ),
            227 => 
            array (
                'id' => 3228,
                'state_id' => 2,
                'name' => 'Sacheon',
              ),
            228 => 
            array (
                'id' => 3229,
                'state_id' => 2,
                'name' => 'Tongyeong',
              ),
            229 => 
            array (
                'id' => 3230,
                'state_id' => 2,
                'name' => 'Hapcheon County',
              ),
            230 => 
            array (
                'id' => 3231,
                'state_id' => 2,
                'name' => 'Haman County',
              ),
            231 => 
            array (
                'id' => 3232,
                'state_id' => 2,
                'name' => 'Hamyang County',
              ),
            232 => 
            array (
                'id' => 3233,
                'state_id' => 2,
                'name' => 'Uiryeong County',
              ),
            233 => 
            array (
                'id' => 3234,
                'state_id' => 2,
                'name' => 'Jinhae',
              ),
            234 => 
            array (
                'id' => 3235,
                'state_id' => 6,
                'name' => 'Sunchang County',
              ),
            235 => 
            array (
                'id' => 3236,
                'state_id' => 6,
                'name' => 'Buan County',
              ),
            236 => 
            array (
                'id' => 3237,
                'state_id' => 6,
                'name' => 'Gochang County',
              ),
            237 => 
            array (
                'id' => 3238,
                'state_id' => 6,
                'name' => 'Gimje',
              ),
            238 => 
            array (
                'id' => 3239,
                'state_id' => 6,
                'name' => 'Jeongeup',
              ),
            239 => 
            array (
                'id' => 3240,
                'state_id' => 6,
                'name' => 'Muju County',
              ),
            240 => 
            array (
                'id' => 3241,
                'state_id' => 6,
                'name' => 'Namwon',
              ),
            241 => 
            array (
                'id' => 3242,
                'state_id' => 6,
                'name' => 'Jeonju',
              ),
            242 => 
            array (
                'id' => 3243,
                'state_id' => 6,
                'name' => 'Gunsan',
              ),
            243 => 
            array (
                'id' => 3244,
                'state_id' => 6,
                'name' => 'Imsil County',
              ),
            244 => 
            array (
                'id' => 3245,
                'state_id' => 6,
                'name' => 'Wanju County',
              ),
            245 => 
            array (
                'id' => 3246,
                'state_id' => 6,
                'name' => 'Iksan',
              ),
            246 => 
            array (
                'id' => 3247,
                'state_id' => 6,
                'name' => 'Jangsu County',
              ),
            247 => 
            array (
                'id' => 3248,
                'state_id' => 6,
                'name' => 'Jinan County',
              ),
            248 => 
            array (
                'id' => 3249,
                'state_id' => 7,
                'name' => 'Boseong County',
              ),
            249 => 
            array (
                'id' => 3250,
                'state_id' => 7,
                'name' => 'Goheung County',
              ),
            250 => 
            array (
                'id' => 3251,
                'state_id' => 7,
                'name' => 'Gokseong County',
              ),
            251 => 
            array (
                'id' => 3252,
                'state_id' => 7,
                'name' => 'Wando County',
              ),
            252 => 
            array (
                'id' => 3253,
                'state_id' => 7,
                'name' => 'Gwangyang',
              ),
            253 => 
            array (
                'id' => 3254,
                'state_id' => 7,
                'name' => 'Haenam County',
              ),
            254 => 
            array (
                'id' => 3255,
                'state_id' => 7,
                'name' => 'Hwasun County',
              ),
            255 => 
            array (
                'id' => 3256,
                'state_id' => 7,
                'name' => 'Gangjin County',
              ),
            256 => 
            array (
                'id' => 3257,
                'state_id' => 7,
                'name' => 'Yeosu',
              ),
            257 => 
            array (
                'id' => 3258,
                'state_id' => 7,
                'name' => 'Yeonggwang County',
              ),
            258 => 
            array (
                'id' => 3259,
                'state_id' => 7,
                'name' => 'Yeongam County',
              ),
            259 => 
            array (
                'id' => 3260,
                'state_id' => 7,
                'name' => 'Naju',
              ),
            260 => 
            array (
                'id' => 3261,
                'state_id' => 7,
                'name' => 'Mokpo',
              ),
            261 => 
            array (
                'id' => 3262,
                'state_id' => 7,
                'name' => 'Gurye County',
              ),
            262 => 
            array (
                'id' => 3263,
                'state_id' => 7,
                'name' => 'Suncheon',
              ),
            263 => 
            array (
                'id' => 3264,
                'state_id' => 7,
                'name' => 'Damyang County',
              ),
            264 => 
            array (
                'id' => 3265,
                'state_id' => 7,
                'name' => 'Muan County',
              ),
            265 => 
            array (
                'id' => 3266,
                'state_id' => 7,
                'name' => 'Hampyeong County',
              ),
            266 => 
            array (
                'id' => 3267,
                'state_id' => 7,
                'name' => 'Sinan County',
              ),
            267 => 
            array (
                'id' => 3268,
                'state_id' => 7,
                'name' => 'Jangseong County',
              ),
            268 => 
            array (
                'id' => 3269,
                'state_id' => 7,
                'name' => 'Jangheung County',
              ),
            269 => 
            array (
                'id' => 3270,
                'state_id' => 7,
                'name' => 'Jindo County',
              ),
            270 => 
            array (
                'id' => 3271,
                'state_id' => 8,
                'name' => 'Boeun County',
              ),
            271 => 
            array (
                'id' => 3272,
                'state_id' => 8,
                'name' => 'Jeungpyeong County',
              ),
            272 => 
            array (
                'id' => 3273,
                'state_id' => 8,
                'name' => 'Danyang County',
              ),
            273 => 
            array (
                'id' => 3274,
                'state_id' => 8,
                'name' => 'Jecheon',
              ),
            274 => 
            array (
                'id' => 3275,
                'state_id' => 8,
                'name' => 'Goesan County',
              ),
            275 => 
            array (
                'id' => 3276,
                'state_id' => 8,
                'name' => 'Cheongwon County',
              ),
            276 => 
            array (
                'id' => 3277,
                'state_id' => 8,
                'name' => 'Cheongju',
              ),
            277 => 
            array (
                'id' => 3278,
                'state_id' => 8,
                'name' => 'Okcheon County',
              ),
            278 => 
            array (
                'id' => 3279,
                'state_id' => 8,
                'name' => 'Eumseong County',
              ),
            279 => 
            array (
                'id' => 3280,
                'state_id' => 8,
                'name' => 'Yeongdong County',
              ),
            280 => 
            array (
                'id' => 3281,
                'state_id' => 8,
                'name' => 'Jincheon County',
              ),
            281 => 
            array (
                'id' => 3282,
                'state_id' => 8,
                'name' => 'Chungju',
              ),
            282 => 
            array (
                'id' => 3283,
                'state_id' => 9,
                'name' => 'Boryeong',
              ),
            283 => 
            array (
                'id' => 3284,
                'state_id' => 9,
                'name' => 'Buyeo County',
              ),
            284 => 
            array (
                'id' => 3285,
                'state_id' => 9,
                'name' => 'Gongju',
              ),
            285 => 
            array (
                'id' => 3286,
                'state_id' => 9,
                'name' => 'Hongseong County',
              ),
            286 => 
            array (
                'id' => 3287,
                'state_id' => 9,
                'name' => 'Gyeryong',
              ),
            287 => 
            array (
                'id' => 3288,
                'state_id' => 9,
                'name' => 'Geumsan County',
              ),
            288 => 
            array (
                'id' => 3289,
                'state_id' => 9,
                'name' => 'Yesan County',
              ),
            289 => 
            array (
                'id' => 3290,
                'state_id' => 9,
                'name' => 'Nonsan',
              ),
            290 => 
            array (
                'id' => 3291,
                'state_id' => 9,
                'name' => 'Cheongyang County',
              ),
            291 => 
            array (
                'id' => 3292,
                'state_id' => 9,
                'name' => 'Seosan',
              ),
            292 => 
            array (
                'id' => 3293,
                'state_id' => 9,
                'name' => 'Seocheon County',
              ),
            293 => 
            array (
                'id' => 3294,
                'state_id' => 9,
                'name' => 'Taean County',
              ),
            294 => 
            array (
                'id' => 3295,
                'state_id' => 9,
                'name' => 'Dangjin County',
              ),
            295 => 
            array (
                'id' => 3296,
                'state_id' => 9,
                'name' => 'Cheonan',
              ),
            296 => 
            array (
                'id' => 3297,
                'state_id' => 9,
                'name' => 'Asan',
              ),
            297 => 
            array (
                'id' => 3298,
                'state_id' => 9,
                'name' => 'Yeongi County',
              ),
            298 => 
            array (
                'id' => 3299,
                'state_id' => 20,
                'name' => 'Butterworth',
              ),
            299 => 
            array (
                'id' => 3300,
                'state_id' => 20,
                'name' => 'George Town',
              ),
            300 => 
            array (
                'id' => 3301,
                'state_id' => 20,
                'name' => 'Bukit Mertajam',
              ),
            301 => 
            array (
                'id' => 3302,
                'state_id' => 20,
                'name' => 'Nibong Tebal',
              ),
            302 => 
            array (
                'id' => 3303,
                'state_id' => 19,
                'name' => 'Kangar',
              ),
            303 => 
            array (
                'id' => 3304,
                'state_id' => 24,
                'name' => 'Kemaman',
              ),
            304 => 
            array (
                'id' => 3305,
                'state_id' => 24,
                'name' => 'Kuala Terengganu',
              ),
            305 => 
            array (
                'id' => 3306,
                'state_id' => 24,
                'name' => 'Dungun',
              ),
            306 => 
            array (
                'id' => 3307,
                'state_id' => 24,
                'name' => 'Marang',
              ),
            307 => 
            array (
                'id' => 3308,
                'state_id' => 24,
                'name' => 'Setiu',
              ),
            308 => 
            array (
                'id' => 3309,
                'state_id' => 24,
                'name' => 'Hulu',
              ),
            309 => 
            array (
                'id' => 3310,
                'state_id' => 24,
                'name' => 'Besut',
              ),
            310 => 
            array (
                'id' => 3311,
                'state_id' => 11,
                'name' => 'Padang Terap',
              ),
            311 => 
            array (
                'id' => 3312,
                'state_id' => 11,
                'name' => 'Pendang',
              ),
            312 => 
            array (
                'id' => 3313,
                'state_id' => 11,
                'name' => 'Langkawi',
              ),
            313 => 
            array (
                'id' => 3314,
                'state_id' => 11,
                'name' => 'Kota Setar',
              ),
            314 => 
            array (
                'id' => 3315,
                'state_id' => 11,
                'name' => 'Kubang Pasu',
              ),
            315 => 
            array (
                'id' => 3316,
                'state_id' => 11,
                'name' => 'Kuala Muda',
              ),
            316 => 
            array (
                'id' => 3317,
                'state_id' => 11,
                'name' => 'Baling',
              ),
            317 => 
            array (
                'id' => 3318,
                'state_id' => 11,
                'name' => 'Kulim',
              ),
            318 => 
            array (
                'id' => 3319,
                'state_id' => 11,
                'name' => 'Bandar Baharu',
              ),
            319 => 
            array (
                'id' => 3320,
                'state_id' => 12,
                'name' => 'Pasir Putih',
              ),
            320 => 
            array (
                'id' => 3321,
                'state_id' => 12,
                'name' => 'Pasir Mas',
              ),
            321 => 
            array (
                'id' => 3322,
                'state_id' => 12,
                'name' => 'Tanah Merah',
              ),
            322 => 
            array (
                'id' => 3323,
                'state_id' => 12,
                'name' => 'Tumpat',
              ),
            323 => 
            array (
                'id' => 3324,
                'state_id' => 12,
                'name' => 'Bachok',
              ),
            324 => 
            array (
                'id' => 3325,
                'state_id' => 12,
                'name' => 'Kota Bharu',
              ),
            325 => 
            array (
                'id' => 3326,
                'state_id' => 12,
                'name' => 'Kuala Krai',
              ),
            326 => 
            array (
                'id' => 3327,
                'state_id' => 12,
                'name' => 'Gua Musang',
              ),
            327 => 
            array (
                'id' => 3328,
                'state_id' => 12,
                'name' => 'Machang',
              ),
            328 => 
            array (
                'id' => 3329,
                'state_id' => 12,
                'name' => 'Jeli',
              ),
            329 => 
            array (
                'id' => 3330,
                'state_id' => 13,
                'name' => 'Kuala Lumpur',
              ),
            330 => 
            array (
                'id' => 3331,
                'state_id' => 15,
                'name' => 'Melaka',
              ),
            331 => 
            array (
                'id' => 3332,
                'state_id' => 15,
                'name' => 'Alor Gajah',
              ),
            332 => 
            array (
                'id' => 3333,
                'state_id' => 15,
                'name' => 'Jasin',
              ),
            333 => 
            array (
                'id' => 3334,
                'state_id' => 14,
                'name' => 'Labuan',
              ),
            334 => 
            array (
                'id' => 3335,
                'state_id' => 14,
                'name' => 'Victoria',
              ),
            335 => 
            array (
                'id' => 3336,
                'state_id' => 17,
                'name' => 'Bera',
              ),
            336 => 
            array (
                'id' => 3337,
                'state_id' => 17,
                'name' => 'Pekan',
              ),
            337 => 
            array (
                'id' => 3338,
                'state_id' => 17,
                'name' => 'Temerloh',
              ),
            338 => 
            array (
                'id' => 3339,
                'state_id' => 17,
                'name' => 'Jerantut',
              ),
            339 => 
            array (
                'id' => 3340,
                'state_id' => 17,
                'name' => 'Kuantan',
              ),
            340 => 
            array (
                'id' => 3341,
                'state_id' => 17,
                'name' => 'Cameron Highlands',
              ),
            341 => 
            array (
                'id' => 3342,
                'state_id' => 17,
                'name' => 'Raub',
              ),
            342 => 
            array (
                'id' => 3343,
                'state_id' => 17,
                'name' => 'Kuala Lipis',
              ),
            343 => 
            array (
                'id' => 3344,
                'state_id' => 17,
                'name' => 'Maran',
              ),
            344 => 
            array (
                'id' => 3345,
                'state_id' => 17,
                'name' => 'Bentong',
              ),
            345 => 
            array (
                'id' => 3346,
                'state_id' => 17,
                'name' => 'Rompin',
              ),
            346 => 
            array (
                'id' => 3347,
                'state_id' => 18,
                'name' => 'Teluk Intan',
              ),
            347 => 
            array (
                'id' => 3348,
                'state_id' => 18,
                'name' => 'Tanjung Malim',
              ),
            348 => 
            array (
                'id' => 3349,
                'state_id' => 18,
                'name' => 'Sungai Siput',
              ),
            349 => 
            array (
                'id' => 3350,
                'state_id' => 18,
                'name' => 'Lumut',
              ),
            350 => 
            array (
                'id' => 3351,
                'state_id' => 18,
                'name' => 'Batu Gajah',
              ),
            351 => 
            array (
                'id' => 3352,
                'state_id' => 18,
                'name' => 'Kuala Kangsar',
              ),
            352 => 
            array (
                'id' => 3353,
                'state_id' => 18,
                'name' => 'Taiping',
              ),
            353 => 
            array (
                'id' => 3354,
                'state_id' => 18,
                'name' => 'Ipoh',
              ),
            354 => 
            array (
                'id' => 3355,
                'state_id' => 10,
                'name' => 'Pontian',
              ),
            355 => 
            array (
                'id' => 3356,
                'state_id' => 10,
                'name' => 'Mersing',
              ),
            356 => 
            array (
                'id' => 3357,
                'state_id' => 10,
                'name' => 'Kota Tinggi',
              ),
            357 => 
            array (
                'id' => 3358,
                'state_id' => 10,
                'name' => 'Kluang',
              ),
            358 => 
            array (
                'id' => 3359,
                'state_id' => 10,
                'name' => 'Batu Pahat',
              ),
            359 => 
            array (
                'id' => 3360,
                'state_id' => 10,
                'name' => 'Muar',
              ),
            360 => 
            array (
                'id' => 3361,
                'state_id' => 10,
                'name' => 'Segamat',
              ),
            361 => 
            array (
                'id' => 3362,
                'state_id' => 10,
                'name' => 'Johor Bahru',
              ),
            362 => 
            array (
                'id' => 3363,
                'state_id' => 16,
                'name' => 'Port Dickson',
              ),
            363 => 
            array (
                'id' => 3364,
                'state_id' => 16,
                'name' => 'Tampin',
              ),
            364 => 
            array (
                'id' => 3365,
                'state_id' => 16,
                'name' => 'Seremban',
              ),
            365 => 
            array (
                'id' => 3366,
                'state_id' => 16,
                'name' => 'Kuala Pilah',
              ),
            366 => 
            array (
                'id' => 3367,
                'state_id' => 16,
                'name' => 'Rembau',
              ),
            367 => 
            array (
                'id' => 3368,
                'state_id' => 16,
                'name' => 'Jempol',
              ),
            368 => 
            array (
                'id' => 3369,
                'state_id' => 16,
                'name' => 'Jelebu',
              ),
            369 => 
            array (
                'id' => 3370,
                'state_id' => 21,
                'name' => 'Papar',
              ),
            370 => 
            array (
                'id' => 3371,
                'state_id' => 21,
                'name' => 'Beaufort',
              ),
            371 => 
            array (
                'id' => 3372,
                'state_id' => 21,
                'name' => 'Beluran',
              ),
            372 => 
            array (
                'id' => 3373,
                'state_id' => 21,
                'name' => 'Pitas',
              ),
            373 => 
            array (
                'id' => 3374,
                'state_id' => 21,
                'name' => 'Penampang',
              ),
            374 => 
            array (
                'id' => 3375,
                'state_id' => 21,
                'name' => 'Tambunan',
              ),
            375 => 
            array (
                'id' => 3376,
                'state_id' => 21,
                'name' => 'Tenom',
              ),
            376 => 
            array (
                'id' => 3377,
                'state_id' => 21,
                'name' => 'Tawau',
              ),
            377 => 
            array (
                'id' => 3378,
                'state_id' => 21,
                'name' => 'Tuaran',
              ),
            378 => 
            array (
                'id' => 3379,
                'state_id' => 21,
                'name' => 'Kota Kinabalu',
              ),
            379 => 
            array (
                'id' => 3380,
                'state_id' => 21,
                'name' => 'Kota Marudu',
              ),
            380 => 
            array (
                'id' => 3381,
                'state_id' => 21,
                'name' => 'Keningau',
              ),
            381 => 
            array (
                'id' => 3382,
                'state_id' => 21,
                'name' => 'Kudat',
              ),
            382 => 
            array (
                'id' => 3383,
                'state_id' => 21,
                'name' => 'Kota Belud',
              ),
            383 => 
            array (
                'id' => 3384,
                'state_id' => 21,
                'name' => 'Kunak',
              ),
            384 => 
            array (
                'id' => 3385,
                'state_id' => 21,
                'name' => 'Kuala Penyu',
              ),
            385 => 
            array (
                'id' => 3386,
                'state_id' => 21,
                'name' => 'Kinabatangan',
              ),
            386 => 
            array (
                'id' => 3387,
                'state_id' => 21,
                'name' => 'Ranau',
              ),
            387 => 
            array (
                'id' => 3388,
                'state_id' => 21,
                'name' => 'Lahad Datu',
              ),
            388 => 
            array (
                'id' => 3389,
                'state_id' => 21,
                'name' => 'Nabawan',
              ),
            389 => 
            array (
                'id' => 3390,
                'state_id' => 21,
                'name' => 'Sandakan',
              ),
            390 => 
            array (
                'id' => 3391,
                'state_id' => 21,
                'name' => 'Sipitang',
              ),
            391 => 
            array (
                'id' => 3392,
                'state_id' => 21,
                'name' => 'Semporna',
              ),
            392 => 
            array (
                'id' => 3393,
                'state_id' => 22,
                'name' => 'Kuching',
              ),
            393 => 
            array (
                'id' => 3394,
                'state_id' => 22,
                'name' => 'Kapit',
              ),
            394 => 
            array (
                'id' => 3395,
                'state_id' => 22,
                'name' => 'Limbang',
              ),
            395 => 
            array (
                'id' => 3396,
                'state_id' => 22,
                'name' => 'Miri',
              ),
            396 => 
            array (
                'id' => 3397,
                'state_id' => 22,
                'name' => 'Bintulu',
              ),
            397 => 
            array (
                'id' => 3398,
                'state_id' => 22,
                'name' => 'Mukah',
              ),
            398 => 
            array (
                'id' => 3399,
                'state_id' => 22,
                'name' => 'Betong',
              ),
            399 => 
            array (
                'id' => 3400,
                'state_id' => 22,
                'name' => 'Samarahan',
              ),
            400 => 
            array (
                'id' => 3401,
                'state_id' => 22,
                'name' => 'Sri Aman',
              ),
            401 => 
            array (
                'id' => 3402,
                'state_id' => 22,
                'name' => 'Sarikei',
              ),
            402 => 
            array (
                'id' => 3403,
                'state_id' => 22,
                'name' => 'Sibu',
              ),
            403 => 
            array (
                'id' => 3404,
                'state_id' => 23,
                'name' => 'Petaling',
              ),
            404 => 
            array (
                'id' => 3405,
                'state_id' => 23,
                'name' => 'Gombak',
              ),
            405 => 
            array (
                'id' => 3406,
                'state_id' => 23,
                'name' => 'Kuala Langat',
              ),
            406 => 
            array (
                'id' => 3407,
                'state_id' => 23,
                'name' => 'Kuala Selangor',
              ),
            407 => 
            array (
                'id' => 3408,
                'state_id' => 23,
                'name' => 'Sabak Bernam',
              ),
            408 => 
            array (
                'id' => 3409,
                'state_id' => 23,
                'name' => 'Hulu Langat',
              ),
            409 => 
            array (
                'id' => 3410,
                'state_id' => 23,
                'name' => 'Hulu Selangor',
              ),
            410 => 
            array (
                'id' => 3411,
                'state_id' => 23,
                'name' => 'Sepang',
              ),
            411 => 
            array (
                'id' => 3412,
                'state_id' => 64,
                'name' => 'Fayetteville',
              ),
            412 => 
            array (
                'id' => 3413,
                'state_id' => 64,
                'name' => 'Fort Smith',
              ),
            413 => 
            array (
                'id' => 3414,
                'state_id' => 64,
                'name' => 'Little Rock',
              ),
            414 => 
            array (
                'id' => 3415,
                'state_id' => 61,
                'name' => 'Birmingham',
              ),
            415 => 
            array (
                'id' => 3416,
                'state_id' => 61,
                'name' => 'Montgomery',
              ),
            416 => 
            array (
                'id' => 3417,
                'state_id' => 61,
                'name' => 'Mobile',
              ),
            417 => 
            array (
                'id' => 3418,
                'state_id' => 62,
                'name' => 'Anchorage',
              ),
            418 => 
            array (
                'id' => 3419,
                'state_id' => 62,
                'name' => 'Fairbanks',
              ),
            419 => 
            array (
                'id' => 3420,
                'state_id' => 62,
                'name' => 'Juneau',
              ),
            420 => 
            array (
                'id' => 3421,
                'state_id' => 73,
                'name' => 'Idaho Falls',
              ),
            421 => 
            array (
                'id' => 3422,
                'state_id' => 73,
                'name' => 'Pocatello',
              ),
            422 => 
            array (
                'id' => 3423,
                'state_id' => 73,
                'name' => 'Boise',
              ),
            423 => 
            array (
                'id' => 3424,
                'state_id' => 73,
                'name' => 'Blackfoot',
              ),
            424 => 
            array (
                'id' => 3425,
                'state_id' => 73,
                'name' => 'Coeur d\'Alene',
              ),
            425 => 
            array (
                'id' => 3426,
                'state_id' => 73,
                'name' => 'Lewiston',
              ),
            426 => 
            array (
                'id' => 3427,
                'state_id' => 73,
                'name' => 'Moscow',
              ),
            427 => 
            array (
                'id' => 3428,
                'state_id' => 73,
                'name' => 'Murphy',
              ),
            428 => 
            array (
                'id' => 3429,
                'state_id' => 73,
                'name' => 'Nampa',
              ),
            429 => 
            array (
                'id' => 3430,
                'state_id' => 73,
                'name' => 'Ketchum',
              ),
            430 => 
            array (
                'id' => 3431,
                'state_id' => 73,
                'name' => 'Sun Valley',
              ),
            431 => 
            array (
                'id' => 3432,
                'state_id' => 73,
                'name' => 'American Falls',
              ),
            432 => 
            array (
                'id' => 3433,
                'state_id' => 76,
                'name' => 'Davenport',
              ),
            433 => 
            array (
                'id' => 3434,
                'state_id' => 76,
                'name' => 'Des Moines',
              ),
            434 => 
            array (
                'id' => 3435,
                'state_id' => 76,
                'name' => 'Cedar Rapids',
              ),
            435 => 
            array (
                'id' => 3436,
                'state_id' => 95,
                'name' => 'Bismarck',
              ),
            436 => 
            array (
                'id' => 3437,
                'state_id' => 95,
                'name' => 'Grand Forks',
              ),
            437 => 
            array (
                'id' => 3438,
                'state_id' => 95,
                'name' => 'Fargo',
              ),
            438 => 
            array (
                'id' => 3439,
                'state_id' => 95,
                'name' => 'Minot',
              ),
            439 => 
            array (
                'id' => 3440,
                'state_id' => 94,
                'name' => 'Asheville',
              ),
            440 => 
            array (
                'id' => 3441,
                'state_id' => 94,
                'name' => 'Durham',
              ),
            441 => 
            array (
                'id' => 3442,
                'state_id' => 94,
                'name' => 'Greensboro',
              ),
            442 => 
            array (
                'id' => 3443,
                'state_id' => 94,
                'name' => 'Chapel Hill',
              ),
            443 => 
            array (
                'id' => 3444,
                'state_id' => 94,
                'name' => 'Raleigh',
              ),
            444 => 
            array (
                'id' => 3445,
                'state_id' => 94,
                'name' => 'Raleigh-Durham',
              ),
            445 => 
            array (
                'id' => 3446,
                'state_id' => 94,
                'name' => 'Charlotte',
              ),
            446 => 
            array (
                'id' => 3447,
                'state_id' => 99,
                'name' => 'Allentown',
              ),
            447 => 
            array (
                'id' => 3448,
                'state_id' => 99,
                'name' => 'Philadephia',
              ),
            448 => 
            array (
                'id' => 3449,
                'state_id' => 99,
                'name' => 'Pittsburgh',
              ),
            449 => 
            array (
                'id' => 3450,
                'state_id' => 104,
                'name' => 'El Paso',
              ),
            450 => 
            array (
                'id' => 3451,
                'state_id' => 104,
                'name' => 'Austin',
              ),
            451 => 
            array (
                'id' => 3452,
                'state_id' => 104,
                'name' => 'Dallas',
              ),
            452 => 
            array (
                'id' => 3453,
                'state_id' => 104,
                'name' => 'Corpus Christi',
              ),
            453 => 
            array (
                'id' => 3454,
                'state_id' => 104,
                'name' => 'Galveston',
              ),
            454 => 
            array (
                'id' => 3455,
                'state_id' => 104,
                'name' => 'Laredo',
              ),
            455 => 
            array (
                'id' => 3456,
                'state_id' => 104,
                'name' => 'McAllen',
              ),
            456 => 
            array (
                'id' => 3457,
                'state_id' => 104,
                'name' => 'San Antonio',
              ),
            457 => 
            array (
                'id' => 3458,
                'state_id' => 104,
                'name' => 'Houston',
              ),
            458 => 
            array (
                'id' => 3459,
                'state_id' => 96,
                'name' => 'Dayton',
              ),
            459 => 
            array (
                'id' => 3460,
                'state_id' => 96,
                'name' => 'Columbus',
              ),
            460 => 
            array (
                'id' => 3461,
                'state_id' => 96,
                'name' => 'Cleveland',
              ),
            461 => 
            array (
                'id' => 3462,
                'state_id' => 96,
                'name' => 'Toledo',
              ),
            462 => 
            array (
                'id' => 3463,
                'state_id' => 96,
                'name' => 'Cincinnati',
              ),
            463 => 
            array (
                'id' => 3464,
                'state_id' => 97,
                'name' => 'Oklahoma City',
              ),
            464 => 
            array (
                'id' => 3465,
                'state_id' => 97,
                'name' => 'Norman',
              ),
            465 => 
            array (
                'id' => 3466,
                'state_id' => 97,
                'name' => 'Tulsa',
              ),
            466 => 
            array (
                'id' => 3467,
                'state_id' => 98,
                'name' => 'Bend',
              ),
            467 => 
            array (
                'id' => 3468,
                'state_id' => 98,
                'name' => 'Portland',
              ),
            468 => 
            array (
                'id' => 3469,
                'state_id' => 98,
                'name' => 'The Dalles',
              ),
            469 => 
            array (
                'id' => 3470,
                'state_id' => 98,
                'name' => 'Dallas',
              ),
            470 => 
            array (
                'id' => 3471,
                'state_id' => 98,
                'name' => 'Tillamook',
              ),
            471 => 
            array (
                'id' => 3472,
                'state_id' => 98,
                'name' => 'Grant\'s Pass',
              ),
            472 => 
            array (
                'id' => 3473,
                'state_id' => 98,
                'name' => 'Hood River',
              ),
            473 => 
            array (
                'id' => 3474,
                'state_id' => 98,
                'name' => 'Crater Lake',
              ),
            474 => 
            array (
                'id' => 3475,
                'state_id' => 98,
                'name' => 'Corvallis',
              ),
            475 => 
            array (
                'id' => 3476,
                'state_id' => 98,
                'name' => 'Coos Bay',
              ),
            476 => 
            array (
                'id' => 3477,
                'state_id' => 98,
                'name' => 'Medford',
              ),
            477 => 
            array (
                'id' => 3478,
                'state_id' => 98,
                'name' => 'Salem',
              ),
            478 => 
            array (
                'id' => 3479,
                'state_id' => 98,
                'name' => 'St Helens',
              ),
            479 => 
            array (
                'id' => 3480,
                'state_id' => 98,
                'name' => 'Springfield',
              ),
            480 => 
            array (
                'id' => 3481,
                'state_id' => 98,
                'name' => 'Eugene',
              ),
            481 => 
            array (
                'id' => 3482,
                'state_id' => 70,
                'name' => 'Orlando',
              ),
            482 => 
            array (
                'id' => 3483,
                'state_id' => 70,
                'name' => 'Key West',
              ),
            483 => 
            array (
                'id' => 3484,
                'state_id' => 70,
                'name' => 'Jacksonville',
              ),
            484 => 
            array (
                'id' => 3485,
                'state_id' => 70,
                'name' => 'Cape Canaveral',
              ),
            485 => 
            array (
                'id' => 3486,
                'state_id' => 70,
                'name' => 'Fort Lauderdale',
              ),
            486 => 
            array (
                'id' => 3487,
                'state_id' => 70,
                'name' => 'Miami',
              ),
            487 => 
            array (
                'id' => 3488,
                'state_id' => 70,
                'name' => 'St. Petersburg',
              ),
            488 => 
            array (
                'id' => 3489,
                'state_id' => 70,
                'name' => 'Tallahassee',
              ),
            489 => 
            array (
                'id' => 3490,
                'state_id' => 70,
                'name' => 'Tampa',
              ),
            490 => 
            array (
                'id' => 3491,
                'state_id' => 106,
                'name' => 'Burlington',
              ),
            491 => 
            array (
                'id' => 3492,
                'state_id' => 106,
                'name' => 'Rutland',
              ),
            492 => 
            array (
                'id' => 3493,
                'state_id' => 106,
                'name' => 'South Burlington',
              ),
            493 => 
            array (
                'id' => 3494,
                'state_id' => 69,
                'name' => 'Washington D.C.',
              ),
            494 => 
            array (
                'id' => 3495,
                'state_id' => 108,
                'name' => 'Spokane',
              ),
            495 => 
            array (
                'id' => 3496,
                'state_id' => 108,
                'name' => 'Tacoma',
              ),
            496 => 
            array (
                'id' => 3497,
                'state_id' => 108,
                'name' => 'Seattle',
              ),
            497 => 
            array (
                'id' => 3498,
                'state_id' => 111,
                'name' => 'Evanston',
              ),
            498 => 
            array (
                'id' => 3499,
                'state_id' => 111,
                'name' => 'Casper',
              ),
            499 => 
            array (
                'id' => 3500,
                'state_id' => 111,
                'name' => 'Laramie',
              ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 3501,
                'state_id' => 111,
                'name' => 'Rock Springs',
              ),
            1 => 
            array (
                'id' => 3502,
                'state_id' => 111,
                'name' => 'Cheyenne',
              ),
            2 => 
            array (
                'id' => 3503,
                'state_id' => 111,
                'name' => 'Sheridan',
              ),
            3 => 
            array (
                'id' => 3504,
                'state_id' => 65,
                'name' => 'San Francisco',
              ),
            4 => 
            array (
                'id' => 3505,
                'state_id' => 65,
                'name' => 'Los Angeles',
              ),
            5 => 
            array (
                'id' => 3506,
                'state_id' => 65,
                'name' => 'San Diego',
              ),
            6 => 
            array (
                'id' => 3507,
                'state_id' => 65,
                'name' => 'San Jose',
              ),
            7 => 
            array (
                'id' => 3508,
                'state_id' => 77,
                'name' => 'Abilene',
              ),
            8 => 
            array (
                'id' => 3509,
                'state_id' => 77,
                'name' => 'Overland Park',
              ),
            9 => 
            array (
                'id' => 3510,
                'state_id' => 77,
                'name' => 'Hutchinson',
              ),
            10 => 
            array (
                'id' => 3511,
                'state_id' => 77,
                'name' => 'Kansas City',
              ),
            11 => 
            array (
                'id' => 3512,
                'state_id' => 77,
                'name' => 'Leavenworth',
              ),
            12 => 
            array (
                'id' => 3513,
                'state_id' => 77,
                'name' => 'Lawrence',
              ),
            13 => 
            array (
                'id' => 3514,
                'state_id' => 77,
                'name' => 'Manhattan',
              ),
            14 => 
            array (
                'id' => 3515,
                'state_id' => 77,
                'name' => 'Topeka',
              ),
            15 => 
            array (
                'id' => 3516,
                'state_id' => 77,
                'name' => 'Wichita',
              ),
            16 => 
            array (
                'id' => 3517,
                'state_id' => 67,
                'name' => 'Bridgeport',
              ),
            17 => 
            array (
                'id' => 3518,
                'state_id' => 67,
                'name' => 'Darien',
              ),
            18 => 
            array (
                'id' => 3519,
                'state_id' => 67,
                'name' => 'Greenwich',
              ),
            19 => 
            array (
                'id' => 3520,
                'state_id' => 67,
                'name' => 'Hartford',
              ),
            20 => 
            array (
                'id' => 3521,
                'state_id' => 67,
                'name' => 'Middletown',
              ),
            21 => 
            array (
                'id' => 3522,
                'state_id' => 67,
                'name' => 'New Haven',
              ),
            22 => 
            array (
                'id' => 3523,
                'state_id' => 67,
                'name' => 'Westport',
              ),
            23 => 
            array (
                'id' => 3524,
                'state_id' => 67,
                'name' => 'Waterbury',
              ),
            24 => 
            array (
                'id' => 3525,
                'state_id' => 67,
                'name' => 'New Britain',
              ),
            25 => 
            array (
                'id' => 3526,
                'state_id' => 66,
                'name' => 'Aspen',
              ),
            26 => 
            array (
                'id' => 3527,
                'state_id' => 66,
                'name' => 'Aurora',
              ),
            27 => 
            array (
                'id' => 3528,
                'state_id' => 66,
                'name' => 'Boulder',
              ),
            28 => 
            array (
                'id' => 3529,
                'state_id' => 66,
                'name' => 'Grand Junction',
              ),
            29 => 
            array (
                'id' => 3530,
                'state_id' => 66,
                'name' => 'Denver',
              ),
            30 => 
            array (
                'id' => 3531,
                'state_id' => 66,
                'name' => 'Fort Collins',
              ),
            31 => 
            array (
                'id' => 3532,
                'state_id' => 66,
                'name' => 'Colorado Springs',
              ),
            32 => 
            array (
                'id' => 3533,
                'state_id' => 66,
                'name' => 'Vail',
              ),
            33 => 
            array (
                'id' => 3534,
                'state_id' => 78,
                'name' => 'Lexington',
              ),
            34 => 
            array (
                'id' => 3535,
                'state_id' => 78,
                'name' => 'Louisville',
              ),
            35 => 
            array (
                'id' => 3536,
                'state_id' => 78,
                'name' => 'Owensboro',
              ),
            36 => 
            array (
                'id' => 3537,
                'state_id' => 79,
                'name' => 'Baton Rouge',
              ),
            37 => 
            array (
                'id' => 3538,
                'state_id' => 79,
                'name' => 'Shreveport',
              ),
            38 => 
            array (
                'id' => 3539,
                'state_id' => 79,
                'name' => 'New Orleans',
              ),
            39 => 
            array (
                'id' => 3540,
                'state_id' => 100,
                'name' => 'Pawtucket',
              ),
            40 => 
            array (
                'id' => 3541,
                'state_id' => 100,
                'name' => 'Cranston',
              ),
            41 => 
            array (
                'id' => 3542,
                'state_id' => 100,
                'name' => 'Newport',
              ),
            42 => 
            array (
                'id' => 3543,
                'state_id' => 100,
                'name' => 'Providence',
              ),
            43 => 
            array (
                'id' => 3544,
                'state_id' => 100,
                'name' => 'Westerly',
              ),
            44 => 
            array (
                'id' => 3545,
                'state_id' => 100,
                'name' => 'Woonsocket',
              ),
            45 => 
            array (
                'id' => 3546,
                'state_id' => 100,
                'name' => 'Warwick',
              ),
            46 => 
            array (
                'id' => 3547,
                'state_id' => 81,
                'name' => 'Balitmore',
              ),
            47 => 
            array (
                'id' => 3548,
                'state_id' => 81,
                'name' => 'Gaithersburg',
              ),
            48 => 
            array (
                'id' => 3549,
                'state_id' => 81,
                'name' => 'Rockville',
              ),
            49 => 
            array (
                'id' => 3550,
                'state_id' => 82,
                'name' => 'Boston',
              ),
            50 => 
            array (
                'id' => 3551,
                'state_id' => 82,
                'name' => 'Springfield',
              ),
            51 => 
            array (
                'id' => 3552,
                'state_id' => 82,
                'name' => 'Worcester',
              ),
            52 => 
            array (
                'id' => 3553,
                'state_id' => 87,
                'name' => 'Billings',
              ),
            53 => 
            array (
                'id' => 3554,
                'state_id' => 87,
                'name' => 'Great Falls',
              ),
            54 => 
            array (
                'id' => 3555,
                'state_id' => 87,
                'name' => 'Missoula',
              ),
            55 => 
            array (
                'id' => 3556,
                'state_id' => 86,
                'name' => 'Columbia',
              ),
            56 => 
            array (
                'id' => 3557,
                'state_id' => 86,
                'name' => 'Jefferson City',
              ),
            57 => 
            array (
                'id' => 3558,
                'state_id' => 86,
                'name' => 'Kansas City',
              ),
            58 => 
            array (
                'id' => 3559,
                'state_id' => 86,
                'name' => 'Sanit Louis',
              ),
            59 => 
            array (
                'id' => 3560,
                'state_id' => 86,
                'name' => 'Springfield',
              ),
            60 => 
            array (
                'id' => 3561,
                'state_id' => 85,
                'name' => 'Biloxi',
              ),
            61 => 
            array (
                'id' => 3562,
                'state_id' => 85,
                'name' => 'Gulfport',
              ),
            62 => 
            array (
                'id' => 3563,
                'state_id' => 85,
                'name' => 'Greenville',
              ),
            63 => 
            array (
                'id' => 3564,
                'state_id' => 85,
                'name' => 'Hattiesburg',
              ),
            64 => 
            array (
                'id' => 3565,
                'state_id' => 85,
                'name' => 'Jackson',
              ),
            65 => 
            array (
                'id' => 3566,
                'state_id' => 85,
                'name' => 'Meridian',
              ),
            66 => 
            array (
                'id' => 3567,
                'state_id' => 85,
                'name' => 'Vicksburg',
              ),
            67 => 
            array (
                'id' => 3568,
                'state_id' => 83,
                'name' => 'Ann Arbor',
              ),
            68 => 
            array (
                'id' => 3569,
                'state_id' => 83,
                'name' => 'Battle Creek',
              ),
            69 => 
            array (
                'id' => 3570,
                'state_id' => 83,
                'name' => 'Bay City',
              ),
            70 => 
            array (
                'id' => 3571,
                'state_id' => 83,
                'name' => 'Grand Rapids',
              ),
            71 => 
            array (
                'id' => 3572,
                'state_id' => 83,
                'name' => 'Dearborn',
              ),
            72 => 
            array (
                'id' => 3573,
                'state_id' => 83,
                'name' => 'Detroit',
              ),
            73 => 
            array (
                'id' => 3574,
                'state_id' => 83,
                'name' => 'Flint',
              ),
            74 => 
            array (
                'id' => 3575,
                'state_id' => 83,
                'name' => 'Wyandotte',
              ),
            75 => 
            array (
                'id' => 3576,
                'state_id' => 83,
                'name' => 'Kalamazoo',
              ),
            76 => 
            array (
                'id' => 3577,
                'state_id' => 83,
                'name' => 'Lansing',
              ),
            77 => 
            array (
                'id' => 3578,
                'state_id' => 83,
                'name' => 'Muskegon',
              ),
            78 => 
            array (
                'id' => 3579,
                'state_id' => 83,
                'name' => 'Pontiac',
              ),
            79 => 
            array (
                'id' => 3580,
                'state_id' => 83,
                'name' => 'Saginaw',
              ),
            80 => 
            array (
                'id' => 3581,
                'state_id' => 83,
                'name' => 'Sault Ste Marie',
              ),
            81 => 
            array (
                'id' => 3582,
                'state_id' => 83,
                'name' => 'Warren',
              ),
            82 => 
            array (
                'id' => 3583,
                'state_id' => 83,
                'name' => 'Port Huron',
              ),
            83 => 
            array (
                'id' => 3584,
                'state_id' => 80,
                'name' => 'Bangor',
              ),
            84 => 
            array (
                'id' => 3585,
                'state_id' => 80,
                'name' => 'Portland',
              ),
            85 => 
            array (
                'id' => 3586,
                'state_id' => 80,
                'name' => 'Lewiston',
              ),
            86 => 
            array (
                'id' => 3587,
                'state_id' => 84,
                'name' => 'Rochester',
              ),
            87 => 
            array (
                'id' => 3588,
                'state_id' => 84,
                'name' => 'Minneapolis',
              ),
            88 => 
            array (
                'id' => 3589,
                'state_id' => 84,
                'name' => 'Saint Paul',
              ),
            89 => 
            array (
                'id' => 3590,
                'state_id' => 102,
                'name' => 'Aberdeen',
              ),
            90 => 
            array (
                'id' => 3591,
                'state_id' => 102,
                'name' => 'Rapid City',
              ),
            91 => 
            array (
                'id' => 3592,
                'state_id' => 102,
                'name' => 'Sioux Falls',
              ),
            92 => 
            array (
                'id' => 3593,
                'state_id' => 101,
                'name' => 'North Charleston',
              ),
            93 => 
            array (
                'id' => 3594,
                'state_id' => 101,
                'name' => 'Charleston',
              ),
            94 => 
            array (
                'id' => 3595,
                'state_id' => 101,
                'name' => 'Columbia',
              ),
            95 => 
            array (
                'id' => 3596,
                'state_id' => 88,
                'name' => 'Omaha',
              ),
            96 => 
            array (
                'id' => 3597,
                'state_id' => 88,
                'name' => 'Bellevue',
              ),
            97 => 
            array (
                'id' => 3598,
                'state_id' => 88,
                'name' => 'Lincoln',
              ),
            98 => 
            array (
                'id' => 3599,
                'state_id' => 89,
                'name' => 'Elko',
              ),
            99 => 
            array (
                'id' => 3600,
                'state_id' => 89,
                'name' => 'North Las Vegas',
              ),
            100 => 
            array (
                'id' => 3601,
                'state_id' => 89,
                'name' => 'Virginia City',
              ),
            101 => 
            array (
                'id' => 3602,
                'state_id' => 89,
                'name' => 'Henderson',
              ),
            102 => 
            array (
                'id' => 3603,
                'state_id' => 89,
                'name' => 'Carson City',
              ),
            103 => 
            array (
                'id' => 3604,
                'state_id' => 89,
                'name' => 'Las Vegas',
              ),
            104 => 
            array (
                'id' => 3605,
                'state_id' => 89,
                'name' => 'Reno',
              ),
            105 => 
            array (
                'id' => 3606,
                'state_id' => 89,
                'name' => 'Sparks',
              ),
            106 => 
            array (
                'id' => 3607,
                'state_id' => 93,
                'name' => 'Buffalo',
              ),
            107 => 
            array (
                'id' => 3608,
                'state_id' => 93,
                'name' => 'Rochester',
              ),
            108 => 
            array (
                'id' => 3609,
                'state_id' => 93,
                'name' => 'New York',
              ),
            109 => 
            array (
                'id' => 3610,
                'state_id' => 68,
                'name' => 'Dover',
              ),
            110 => 
            array (
                'id' => 3611,
                'state_id' => 68,
                'name' => 'Newark',
              ),
            111 => 
            array (
                'id' => 3612,
                'state_id' => 68,
                'name' => 'Wilmington',
              ),
            112 => 
            array (
                'id' => 3613,
                'state_id' => 103,
                'name' => 'Bristol',
              ),
            113 => 
            array (
                'id' => 3614,
                'state_id' => 103,
                'name' => 'Chattanooga',
              ),
            114 => 
            array (
                'id' => 3615,
                'state_id' => 103,
                'name' => 'Kingsport',
              ),
            115 => 
            array (
                'id' => 3616,
                'state_id' => 103,
                'name' => 'Memphis',
              ),
            116 => 
            array (
                'id' => 3617,
                'state_id' => 103,
                'name' => 'Nashville',
              ),
            117 => 
            array (
                'id' => 3618,
                'state_id' => 103,
                'name' => 'Knoxville',
              ),
            118 => 
            array (
                'id' => 3619,
                'state_id' => 103,
                'name' => 'Tri-City Area',
              ),
            119 => 
            array (
                'id' => 3620,
                'state_id' => 103,
                'name' => 'Smyrna',
              ),
            120 => 
            array (
                'id' => 3621,
                'state_id' => 103,
                'name' => 'Spring Hill',
              ),
            121 => 
            array (
                'id' => 3622,
                'state_id' => 103,
                'name' => 'Johnson City',
              ),
            122 => 
            array (
                'id' => 3623,
                'state_id' => 110,
                'name' => 'Appleton',
              ),
            123 => 
            array (
                'id' => 3624,
                'state_id' => 110,
                'name' => 'Oshkosh',
              ),
            124 => 
            array (
                'id' => 3625,
                'state_id' => 110,
                'name' => 'Green Bay',
              ),
            125 => 
            array (
                'id' => 3626,
                'state_id' => 110,
                'name' => 'Kenosha',
              ),
            126 => 
            array (
                'id' => 3627,
                'state_id' => 110,
                'name' => 'LaCrosse',
              ),
            127 => 
            array (
                'id' => 3628,
                'state_id' => 110,
                'name' => 'Racine',
              ),
            128 => 
            array (
                'id' => 3629,
                'state_id' => 110,
                'name' => 'Manitowoc',
              ),
            129 => 
            array (
                'id' => 3630,
                'state_id' => 110,
                'name' => 'Madison',
              ),
            130 => 
            array (
                'id' => 3631,
                'state_id' => 110,
                'name' => 'Milwaukee',
              ),
            131 => 
            array (
                'id' => 3632,
                'state_id' => 110,
                'name' => 'Eau Claire',
              ),
            132 => 
            array (
                'id' => 3633,
                'state_id' => 110,
                'name' => 'Wausau',
              ),
            133 => 
            array (
                'id' => 3634,
                'state_id' => 110,
                'name' => 'Sheboygan',
              ),
            134 => 
            array (
                'id' => 3635,
                'state_id' => 107,
                'name' => 'Virginia Beach',
              ),
            135 => 
            array (
                'id' => 3636,
                'state_id' => 107,
                'name' => 'Norfolk',
              ),
            136 => 
            array (
                'id' => 3637,
                'state_id' => 107,
                'name' => 'Chesapeake',
              ),
            137 => 
            array (
                'id' => 3638,
                'state_id' => 109,
                'name' => 'Charleston',
              ),
            138 => 
            array (
                'id' => 3639,
                'state_id' => 109,
                'name' => 'Huntington',
              ),
            139 => 
            array (
                'id' => 3640,
                'state_id' => 109,
                'name' => 'Parkersburg',
              ),
            140 => 
            array (
                'id' => 3641,
                'state_id' => 72,
                'name' => 'Kailua',
              ),
            141 => 
            array (
                'id' => 3642,
                'state_id' => 72,
                'name' => 'Honolulu',
              ),
            142 => 
            array (
                'id' => 3643,
                'state_id' => 72,
                'name' => 'Hilo',
              ),
            143 => 
            array (
                'id' => 3644,
                'state_id' => 90,
                'name' => 'Concord',
              ),
            144 => 
            array (
                'id' => 3645,
                'state_id' => 90,
                'name' => 'Manchester',
              ),
            145 => 
            array (
                'id' => 3646,
                'state_id' => 90,
                'name' => 'Nashua',
              ),
            146 => 
            array (
                'id' => 3647,
                'state_id' => 92,
                'name' => 'Albuquerque',
              ),
            147 => 
            array (
                'id' => 3648,
                'state_id' => 92,
                'name' => 'Las Cruces',
              ),
            148 => 
            array (
                'id' => 3649,
                'state_id' => 92,
                'name' => 'Roswell',
              ),
            149 => 
            array (
                'id' => 3650,
                'state_id' => 92,
                'name' => 'Santa Fe',
              ),
            150 => 
            array (
                'id' => 3651,
                'state_id' => 91,
                'name' => 'Newark',
              ),
            151 => 
            array (
                'id' => 3652,
                'state_id' => 91,
                'name' => 'Paterson',
              ),
            152 => 
            array (
                'id' => 3653,
                'state_id' => 91,
                'name' => 'Jersey City',
              ),
            153 => 
            array (
                'id' => 3654,
                'state_id' => 63,
                'name' => 'Phoenix',
              ),
            154 => 
            array (
                'id' => 3655,
                'state_id' => 63,
                'name' => 'Glendale',
              ),
            155 => 
            array (
                'id' => 3656,
                'state_id' => 63,
                'name' => 'Mesa',
              ),
            156 => 
            array (
                'id' => 3657,
                'state_id' => 63,
                'name' => 'Scottsdale',
              ),
            157 => 
            array (
                'id' => 3658,
                'state_id' => 63,
                'name' => 'Tempe',
              ),
            158 => 
            array (
                'id' => 3659,
                'state_id' => 63,
                'name' => 'Tucson',
              ),
            159 => 
            array (
                'id' => 3660,
                'state_id' => 63,
                'name' => 'Yuma',
              ),
            160 => 
            array (
                'id' => 3661,
                'state_id' => 74,
                'name' => 'Alton',
              ),
            161 => 
            array (
                'id' => 3662,
                'state_id' => 74,
                'name' => 'Aurora',
              ),
            162 => 
            array (
                'id' => 3663,
                'state_id' => 74,
                'name' => 'Bloomington',
              ),
            163 => 
            array (
                'id' => 3664,
                'state_id' => 74,
                'name' => 'Danville',
              ),
            164 => 
            array (
                'id' => 3665,
                'state_id' => 74,
                'name' => 'De Kalb',
              ),
            165 => 
            array (
                'id' => 3666,
                'state_id' => 74,
                'name' => 'Decatur',
              ),
            166 => 
            array (
                'id' => 3667,
                'state_id' => 74,
                'name' => 'East St Louis',
              ),
            167 => 
            array (
                'id' => 3668,
                'state_id' => 74,
                'name' => 'Champaign-Urbana',
              ),
            168 => 
            array (
                'id' => 3669,
                'state_id' => 74,
                'name' => 'Galesburg',
              ),
            169 => 
            array (
                'id' => 3670,
                'state_id' => 74,
                'name' => 'Carbondale',
              ),
            170 => 
            array (
                'id' => 3671,
                'state_id' => 74,
                'name' => 'Rock Island',
              ),
            171 => 
            array (
                'id' => 3672,
                'state_id' => 74,
                'name' => 'Rockford',
              ),
            172 => 
            array (
                'id' => 3673,
                'state_id' => 74,
                'name' => 'Normal',
              ),
            173 => 
            array (
                'id' => 3674,
                'state_id' => 74,
                'name' => 'Peoria',
              ),
            174 => 
            array (
                'id' => 3675,
                'state_id' => 74,
                'name' => 'Centralia',
              ),
            175 => 
            array (
                'id' => 3676,
                'state_id' => 74,
                'name' => 'Springfield',
              ),
            176 => 
            array (
                'id' => 3677,
                'state_id' => 74,
                'name' => 'Waukegan',
              ),
            177 => 
            array (
                'id' => 3678,
                'state_id' => 74,
                'name' => 'Chicago',
              ),
            178 => 
            array (
                'id' => 3679,
                'state_id' => 75,
                'name' => 'Evansville',
              ),
            179 => 
            array (
                'id' => 3680,
                'state_id' => 75,
                'name' => 'Fort Wayne',
              ),
            180 => 
            array (
                'id' => 3681,
                'state_id' => 75,
                'name' => 'Indianapolis',
              ),
            181 => 
            array (
                'id' => 3682,
                'state_id' => 105,
                'name' => 'Ogden',
              ),
            182 => 
            array (
                'id' => 3683,
                'state_id' => 105,
                'name' => 'Layton',
              ),
            183 => 
            array (
                'id' => 3684,
                'state_id' => 105,
                'name' => 'Orem',
              ),
            184 => 
            array (
                'id' => 3685,
                'state_id' => 105,
                'name' => 'Park City',
              ),
            185 => 
            array (
                'id' => 3686,
                'state_id' => 105,
                'name' => 'Provo',
              ),
            186 => 
            array (
                'id' => 3687,
                'state_id' => 105,
                'name' => 'St.George',
              ),
            187 => 
            array (
                'id' => 3688,
                'state_id' => 105,
                'name' => 'West Valley City',
              ),
            188 => 
            array (
                'id' => 3689,
                'state_id' => 105,
                'name' => 'Salt Lake City',
              ),
            189 => 
            array (
                'id' => 3690,
                'state_id' => 71,
                'name' => 'Augusta',
              ),
            190 => 
            array (
                'id' => 3691,
                'state_id' => 71,
                'name' => 'Columbus',
              ),
            191 => 
            array (
                'id' => 3692,
                'state_id' => 71,
                'name' => 'Macon',
              ),
            192 => 
            array (
                'id' => 3693,
                'state_id' => 71,
                'name' => 'Savannah',
              ),
            193 => 
            array (
                'id' => 3694,
                'state_id' => 71,
                'name' => 'Atlanta',
              ),
            194 => 
            array (
                'id' => 3695,
                'state_id' => 58,
                'name' => 'Belfast',
              ),
            195 => 
            array (
                'id' => 3696,
                'state_id' => 58,
                'name' => 'Derry',
              ),
            196 => 
            array (
                'id' => 3697,
                'state_id' => 58,
                'name' => 'Lisburn',
              ),
            197 => 
            array (
                'id' => 3698,
                'state_id' => 58,
                'name' => 'Newry',
              ),
            198 => 
            array (
                'id' => 3699,
                'state_id' => 59,
                'name' => 'Aberdeen',
              ),
            199 => 
            array (
                'id' => 3700,
                'state_id' => 59,
                'name' => 'Edinburgh',
              ),
            200 => 
            array (
                'id' => 3701,
                'state_id' => 59,
                'name' => 'Dundee',
              ),
            201 => 
            array (
                'id' => 3702,
                'state_id' => 59,
                'name' => 'Glasgow',
              ),
            202 => 
            array (
                'id' => 3703,
                'state_id' => 59,
                'name' => 'Stirling',
              ),
            203 => 
            array (
                'id' => 3704,
                'state_id' => 59,
                'name' => 'Inverness',
              ),
            204 => 
            array (
                'id' => 3705,
                'state_id' => 60,
                'name' => 'Bangor',
              ),
            205 => 
            array (
                'id' => 3706,
                'state_id' => 60,
                'name' => 'Cardiff',
              ),
            206 => 
            array (
                'id' => 3707,
                'state_id' => 60,
                'name' => 'Newport',
              ),
            207 => 
            array (
                'id' => 3708,
                'state_id' => 60,
                'name' => 'Swansea',
              ),
            208 => 
            array (
                'id' => 3709,
                'state_id' => 57,
                'name' => 'Exeter',
              ),
            209 => 
            array (
                'id' => 3710,
                'state_id' => 57,
                'name' => 'Bath',
              ),
            210 => 
            array (
                'id' => 3711,
                'state_id' => 57,
                'name' => 'Peterborough',
              ),
            211 => 
            array (
                'id' => 3712,
                'state_id' => 57,
                'name' => 'Birmingham',
              ),
            212 => 
            array (
                'id' => 3713,
                'state_id' => 57,
                'name' => 'Bradford',
              ),
            213 => 
            array (
                'id' => 3714,
                'state_id' => 57,
                'name' => 'Brighton & Hove',
              ),
            214 => 
            array (
                'id' => 3715,
                'state_id' => 57,
                'name' => 'Bristol',
              ),
            215 => 
            array (
                'id' => 3716,
                'state_id' => 57,
                'name' => 'Derby',
              ),
            216 => 
            array (
                'id' => 3717,
                'state_id' => 57,
                'name' => 'Durham',
              ),
            217 => 
            array (
                'id' => 3718,
                'state_id' => 57,
                'name' => 'Gloucester',
              ),
            218 => 
            array (
                'id' => 3719,
                'state_id' => 57,
                'name' => 'Kingston upon Hull',
              ),
            219 => 
            array (
                'id' => 3720,
                'state_id' => 57,
                'name' => 'Hereford',
              ),
            220 => 
            array (
                'id' => 3721,
                'state_id' => 57,
                'name' => 'Cambridge',
              ),
            221 => 
            array (
                'id' => 3722,
                'state_id' => 57,
                'name' => 'Carlisle',
              ),
            222 => 
            array (
                'id' => 3723,
                'state_id' => 57,
                'name' => 'Canterbury',
              ),
            223 => 
            array (
                'id' => 3724,
                'state_id' => 57,
                'name' => 'Coventry',
              ),
            224 => 
            array (
                'id' => 3725,
                'state_id' => 57,
                'name' => 'Lancaster',
              ),
            225 => 
            array (
                'id' => 3726,
                'state_id' => 57,
                'name' => 'Ripon',
              ),
            226 => 
            array (
                'id' => 3727,
                'state_id' => 57,
                'name' => 'Lichfield',
              ),
            227 => 
            array (
                'id' => 3728,
                'state_id' => 57,
                'name' => 'Liverpool',
              ),
            228 => 
            array (
                'id' => 3729,
                'state_id' => 57,
                'name' => 'Leeds',
              ),
            229 => 
            array (
                'id' => 3730,
                'state_id' => 57,
                'name' => 'Leicester',
              ),
            230 => 
            array (
                'id' => 3731,
                'state_id' => 57,
                'name' => 'Lincoln',
              ),
            231 => 
            array (
                'id' => 3732,
                'state_id' => 57,
                'name' => 'London',
              ),
            232 => 
            array (
                'id' => 3733,
                'state_id' => 57,
                'name' => 'Manchester',
              ),
            233 => 
            array (
                'id' => 3734,
                'state_id' => 57,
                'name' => 'Southampton',
              ),
            234 => 
            array (
                'id' => 3735,
                'state_id' => 57,
                'name' => 'Oxford',
              ),
            235 => 
            array (
                'id' => 3736,
                'state_id' => 57,
                'name' => 'Newcastle',
              ),
            236 => 
            array (
                'id' => 3737,
                'state_id' => 57,
                'name' => 'Nottingham',
              ),
            237 => 
            array (
                'id' => 3738,
                'state_id' => 57,
                'name' => 'Norwich',
              ),
            238 => 
            array (
                'id' => 3739,
                'state_id' => 57,
                'name' => 'Portsmouth',
              ),
            239 => 
            array (
                'id' => 3740,
                'state_id' => 57,
                'name' => 'Preston',
              ),
            240 => 
            array (
                'id' => 3741,
                'state_id' => 57,
                'name' => 'Plymouth',
              ),
            241 => 
            array (
                'id' => 3742,
                'state_id' => 57,
                'name' => 'Chichester',
              ),
            242 => 
            array (
                'id' => 3743,
                'state_id' => 57,
                'name' => 'Chester',
              ),
            243 => 
            array (
                'id' => 3744,
                'state_id' => 57,
                'name' => 'Sunderland',
              ),
            244 => 
            array (
                'id' => 3745,
                'state_id' => 57,
                'name' => 'Saint Albans',
              ),
            245 => 
            array (
                'id' => 3746,
                'state_id' => 57,
                'name' => 'Salisbury',
              ),
            246 => 
            array (
                'id' => 3747,
                'state_id' => 57,
                'name' => 'Salford',
              ),
            247 => 
            array (
                'id' => 3748,
                'state_id' => 57,
                'name' => 'Truro',
              ),
            248 => 
            array (
                'id' => 3749,
                'state_id' => 57,
                'name' => 'Stoke-on-Trent',
              ),
            249 => 
            array (
                'id' => 3750,
                'state_id' => 57,
                'name' => 'Wells',
              ),
            250 => 
            array (
                'id' => 3751,
                'state_id' => 57,
                'name' => 'Wakefield',
              ),
            251 => 
            array (
                'id' => 3752,
                'state_id' => 57,
                'name' => 'Winchester',
              ),
            252 => 
            array (
                'id' => 3753,
                'state_id' => 57,
                'name' => 'Wolverhampton',
              ),
            253 => 
            array (
                'id' => 3754,
                'state_id' => 57,
                'name' => 'Worcester',
              ),
            254 => 
            array (
                'id' => 3755,
                'state_id' => 57,
                'name' => 'Sheffield',
              ),
            255 => 
            array (
                'id' => 3756,
                'state_id' => 57,
                'name' => 'Ely',
              ),
            256 => 
            array (
                'id' => 3757,
                'state_id' => 57,
                'name' => 'York',
              ),
        ));
        
        
    }
}