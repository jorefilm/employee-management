<?php


namespace App\Repositories;

interface CountryRepositoryInterface
{
    public function listSearchResult($search = null, $perPage = 10);
    public function getCountryWithCities();
    public function getCountryWithStates();
}