<?php

namespace App\Http\Controllers;

// App
use App\Repositories\CountryRepositoryInterface;

use App\Http\Requests\CountryRequest;

// Laravel
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Country Repository implementation 
     */ 
    private $country;

     /**
     * Create new controller instance
     * 
     * @return void 
     */ 
    public function __construct(
        CountryRepositoryInterface $country
    )
    {
        $this->country = $country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = $this->country->listAllPaginated();

        $data = [
            'countries' => $countries
        ];
        return view('countries.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('countries.edit_create');
    }

    private function saveCountryForm($request, $country = null)
    {
        $data = $request->all();

        if($country) {
            return $this->country->update($country, $data);
        }
        return $this->country->create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        $country = $this->saveCountryForm($request);
        
        flash(__('alerts.success'))->success();
        return redirect()->route('country.show', $country->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = $this->country->findById($id);

        $data = [
            'country' => $country
        ];
        return view('countries.edit_create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, $id)
    {
        $country = $this->country->findById($id);
        $this->saveCountryForm($request, $country);

        flash(__('alerts.success'))->success();
        return redirect()->route('country.show', $country->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->country->delete($id);

        flash(__('alerts.success'))->success();
        return redirect()->route('country.index');
    }
}
