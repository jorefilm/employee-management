<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(route('state.index')); ?>">Back to States</a>
    <h1><?php echo e(isset($state) ? 'Edit' : 'Create'); ?> State</h1>

    <div class="row">
        <div class="col-lg-6">
            
            <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <form action="<?php echo e(isset($state) ? route('state.update', $state->id) : route('state.store')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php echo method_field(isset($state) ? 'PUT' : 'POST'); ?>
        
                <div class="form-group">
                    <label for="">State Name</label>
                    <input type="text" name="name" value="<?php echo e(old('name', (isset($state) ? $state->name : '' ))); ?>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Country</label>
                    <select name="country_id" class="form-control">
                        <option value="">Select Country</option>

                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option 
                                value="<?php echo e($country->id); ?>"
                                <?php if(old('country_id')): ?>
                                    <?php echo e(old('country_id') == $country->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($state) ? ( $state->country_id == $country->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                <?php echo e($country->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
        
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>