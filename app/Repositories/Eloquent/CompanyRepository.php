<?php

namespace App\Repositories\Eloquent;

// App
use App\Company;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\CompanyRepositoryInterface;

class CompanyRepository extends AbstractRepository implements CompanyRepositoryInterface
{
    /**
     * Create mew instance of repository
     * 
     * @return void
     */ 
    public function __construct(Company $company)
    {
        $this->model = $company;
    }

}