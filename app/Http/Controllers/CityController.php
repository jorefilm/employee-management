<?php

namespace App\Http\Controllers;

// App
use App\Repositories\CityRepositoryInterface;
use App\Repositories\StateRepositoryInterface;
use App\Repositories\CountryRepositoryInterface;


use App\Http\Requests\CityRequest;

// laravel
use Illuminate\Http\Request;

class CityController extends Controller
{

    /**
     * City Repository implementation 
     */ 
    private $city;

    /**
     * State Repository implementation 
     */ 
    private $state;

    /**
     * Country Repository implementation 
     */ 
    private $country;

    /**
     * Create new instance of controller
     * 
     * @return void
     */
    public function __construct(
        CityRepositoryInterface $city,
        StateRepositoryInterface $state,
        CountryRepositoryInterface $country
        )
    {
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = $this->city->listSearchResult($request->input('search'))
            ->appends($request->except(['page','_token']));
        $data = [
            'cities' => $cities
        ];
        return view('cities.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->showCityForm();
    }

    /**
     * Return the Edit or Create city page
     */ 
    public function showCityForm($city = null)
    {
        $countries = $this->country->getCountryWithStates();
        $states = $this->state->getAll();

        $data = [
            'countries' => $countries,
            'states' => $states
        ];

        if($city) {
            $data['city'] = $city;
        }

        return view('cities.edit_create', $data);
    }

    /**
     * Save or Update an Employee
     * 
     * @return mixed
     */ 
    private function saveCityForm($request, $city = null)
    {
        $data = $request->all();

        if($city) {
            return $this->city->update($city, $data);
        }
        
        return $this->city->create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        $city = $this->saveCityForm($request);

        flash(__('alerts.success'))->success();
        return redirect()->route('city.show', $city->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = $this->city->findById($id);
        return $this->showCityForm($city);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $id)
    {
        $city = $this->city->findById($id);
        $this->saveCityForm($request, $city);

        flash(__('alerts.success'))->success();
        return redirect()->route('city.show', $city->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->city->delete($id);

        flash(__('alerts.success'))->success();
        return redirect()->route('city.index');
    }
}
