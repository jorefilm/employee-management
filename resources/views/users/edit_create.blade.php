@extends('layouts.app')

@section('content')
    <a href="{{ route('user.index') }}">Back to Users</a>
    <h1>{{ isset($user) ? 'Edit' : 'Create' }} User</h1>

    <div class="row">
        <div class="col-lg-6">
            
            @include('partials.alerts')

            <form action="{{ isset($user) ? route('user.update', $user->id) : route('user.store') }}" method="POST">
                @csrf
                @method(isset($user) ? 'PUT' : 'POST')
                
                @if(isset($user))
                <input type="hidden" name="id" value="{{ $user->id }}">
                @endif
                
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" name="username" value="{{ old('username', (isset($user) ? $user->username : '' )) }}" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Firstname</label>
                    <input type="text" name="firstname" value="{{ old('firstname', (isset($user) ? $user->firstname : '' )) }}" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Lastname</label>
                    <input type="text" name="lastname" value="{{ old('lastname', (isset($user) ? $user->lastname : '' )) }}" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" name="email" value="{{ old('email', (isset($user) ? $user->email : '' )) }}" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" name="password" value="" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Confirm Password</label>
                    <input type="password" name="password_confirmation" value="" class="form-control">
                </div>
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
@endsection