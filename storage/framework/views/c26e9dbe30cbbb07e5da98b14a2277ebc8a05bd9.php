<?php $__env->startSection('content'); ?>
    <h1>Cities</h1>
    <a class="mb-3 d-block" href="<?php echo e(route('city.create')); ?>">Add</a>
    
    <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <form>
        <div class="input-group mb-2">
            <input type="text" name="search" placeholder="Search city name" class="form-control" value="<?php echo e(Request::input('search')); ?>">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>State</th>
            <th>Country</th>
            <th></th>
        </thead>
        <?php if($cities): ?>
            <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($city->id); ?></td>
                    <td><?php echo e($city->name); ?></td>
                    <td><?php echo e($city->state ? $city->state->name : ''); ?></td>
                    <td><?php echo e($city->state ? ($city->state->country ? $city->state->country->name : '' ) : ''); ?></td>
                    <td class="text-center">
                        <a href="<?php echo e(route('city.show', $city->id)); ?>">Edit</a>
                        <form action="<?php echo e(route('city.destroy', $city->id)); ?>" method="POST" class="d-inline action-delete">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('DELETE'); ?>
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </table>

    <?php echo e($cities->links()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>