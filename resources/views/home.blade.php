@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <h1>Dashboard</h1>

            You are logged in!
        </div>
    </div>
</div>
@endsection
