<?php

namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;

class AbstractRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function listAllPaginated($perPage = 20)
    {
        return $this->model->paginate($perPage);
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function update($entity, $data)
    {
        return $entity->fill($data)
            ->save();
    }
    
    public function delete($id)
    {
        $row = $this->findById($id);
        return $row->delete();
    }
}