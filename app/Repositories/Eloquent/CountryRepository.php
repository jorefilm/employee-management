<?php

namespace App\Repositories\Eloquent;

// App
use App\Country;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\CountryRepositoryInterface;

class CountryRepository extends AbstractRepository implements CountryRepositoryInterface
{
    /**
     * Create mew instance of repository
     * 
     * @return void
     */ 
    public function __construct(Country $country)
    {
        $this->model = $country;
    }

    /**
     * Return the List of City
     * 
     * @return \Illuminate\Pagination\Paginator|Array
     */ 
    public function listSearchResult($search = null, $perPage = 10)
    {
        return $this->model->when($search, function($query) use($search) {
            $query->where(function($query) use($search) {
                $query->where('salary',  'like', "%" . $search . "%")
                    ->orWhere('country_code',  'like', "%" . $search . "%");
            });
        })
        ->paginate(20);
    }

    /**
     * Return all countries that has city under it
     * 
     * @return mixed
     */ 
    public function getCountryWithCities()
    {
        return $this->model->whereHas('states.cities')->get();
    }

    /**
     * Return all countries that has states under it
     * 
     * @return mixed
     */ 
    public function getCountryWithStates()
    {
        return $this->model->whereHas('states')->get();
    }
}