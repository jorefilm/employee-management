<?php

namespace App\Http\Controllers;

// App
use App\Employee;

use App\Repositories\EmployeeRepositoryInterface;
use App\Repositories\CountryRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\StateRepositoryInterface;
use App\Repositories\DivisionRepositoryInterface;
use App\Repositories\DepartmentRepositoryInterface;
use App\Repositories\CompanyRepositoryInterface;

use App\Http\Requests\EmployeeRequest;

// Laravel
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Employee Repository implementation 
     */ 
    private $employee;

    /**
     * Country Repository implementation 
     */ 
    private $country;

    /**
     * City Repository implementation 
     */ 
    private $city;

    /**
     * State Repository implementation 
     */ 
    private $state;

    /**
     * Division Repository implementation 
     */ 
    private $division;

    /**
     * Department Repository implementation 
     */ 
    private $department;

    /**
     * Department Repository implementation 
     */ 
    private $company;

    /**
     * Create new controller instance
     * 
     * @return void 
     */ 
    public function __construct(
        EmployeeRepositoryInterface $employee,
        CountryRepositoryInterface $country,
        CityRepositoryInterface $city,
        StateRepositoryInterface $state,
        DivisionRepositoryInterface $division,
        DepartmentRepositoryInterface $department,
        CompanyRepositoryInterface $company
    )
    {
        $this->employee = $employee;
        $this->country = $country;
        $this->city = $city;
        $this->state = $state;
        $this->division = $division;
        $this->department = $department;
        $this->company = $company;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employees = $this->employee->listSearchResult($request->input('search'))
            ->appends($request->except(['page','_token']));
        
        $data = [
            'employees' => $employees
        ];
        return view('employees.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->showEmployeeForm();
    }
    
    /**
     * Return the Edit or Create employee page
     */ 
    public function showEmployeeForm($employee = null)
    {
        $cities = $this->city->getAll();
        $states = $this->state->getAll();
        $countries = $this->country->getCountryWithCities();
        $divisions = $this->division->getAll();
        $departments = $this->department->getAll();
        $companies = $this->company->getAll();

        $data = [
            'cities'        => $cities,
            'states'        => $states,
            'countries'     => $countries,
            'divisions'     => $divisions,
            'departments'   => $departments,
            'companies'     => $companies,
        ];

        if($employee) {
            $data['employee'] = $employee;
        }

        return view('employees.edit_create', $data);
    }

    /**
     * Save or Update an Employee
     * 
     * @return mixed
     */ 
    public function saveEmployeeForm($request, $employee = null)
    {
        $data = $request->except(['_token', 'picture']);

        // If uploaded an image
        if($request->hasFile('picture')) {
            // Generate filename
            $getimageName = time().'.'.$request->picture->getClientOriginalExtension();
            // Upload
            $request->picture->move(public_path('uploads'), $getimageName);
            
            $data['picture'] = $getimageName;
        }

        if($employee) {
            return $this->employee->update($employee, $data);
        }
        
        return $this->employee->create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        // Save the employee
        $employee = $this->saveEmployeeForm($request);

        flash(__('alerts.success'))->success();
        return redirect()->route('employee.show', $employee->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = $this->employee->findById($id);
        return $this->showEmployeeForm($employee);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        $employee = $this->employee->findById($id);
        $this->saveEmployeeForm($request, $employee);

        flash(__('alerts.success'))->success();
        return redirect()->route('employee.show', $employee->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = $this->employee->findById($id);

        $this->employee->delete($id);

        flash(__('alerts.success'))->success();
        return redirect()->route('employee.index');
    }

    public function report(Request $request)
    {
        // @TODO
        $employees = $this->employee->getEmployeeReport($request);

        $data = [
            'employees' => $employees
        ];
        return view('employees.report', $data);
    }
}
