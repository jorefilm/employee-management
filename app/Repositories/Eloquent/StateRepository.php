<?php

namespace App\Repositories\Eloquent;

// App
use App\State;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\StateRepositoryInterface;

class StateRepository extends AbstractRepository implements StateRepositoryInterface
{
    /**
     * Create mew instance of repository
     * 
     * @return void
     */ 
    public function __construct(State $state)
    {
        $this->model = $state;
    }

}