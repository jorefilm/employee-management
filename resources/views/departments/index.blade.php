@extends('layouts.app')

@section('content')
    <h1>Departments</h1>
    <a class="mb-3 d-block" href="{{ route('department.create') }}">Add</a>

    @include('partials.alerts')

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th></th>
        </thead>
        @if($departments)
            @foreach($departments as $department)
                <tr>
                    <td>{{ $department->id }}</td>
                    <td>{{ $department->name }}</td>
                    <td class="text-center">
                        <a href="{{ route('department.show', $department->id) }}">Edit</a>

                        <form action="{{ route('department.destroy', $department->id) }}" method="POST" class="d-inline action-delete">
                            @csrf
                            @method('DELETE')
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    {{ $departments->links() }}
@endsection