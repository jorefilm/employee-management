@extends('layouts.app')

@section('content')
    <a href="{{ route('department.index') }}">Back to Departments</a>
    <h1>{{ isset($department) ? 'Edit' : 'Create' }} Department</h1>

    <div class="row">
        <div class="col-lg-6">
            
            @include('partials.alerts')

            <form action="{{ isset($department) ? route('department.update', $department->id) : route('department.store') }}" method="POST">
                @csrf
                @method(isset($department) ? 'PUT' : 'POST')
        
                <div class="form-group">
                    <label for="">Department Name</label>
                    <input type="text" name="name" value="{{ old('name', (isset($department) ? $department->name : '' )) }}" class="form-control">
                </div>
        

        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
@endsection