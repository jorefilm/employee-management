<?php

namespace App\Repositories\Eloquent;

// App
use App\User;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\UserRepositoryInterface;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    /**
     * Create new instance of repository
     * 
     * @return void
     */ 
    public function __construct(User $user)
    {
        $this->model = $user;
    }

     /**
     * Retrieve the List of Users
     * 
     * @return \Illuminate\Pagination\Paginator|Array
     */ 
    public function listSearchResult($search = null, $perPage = 10)
    {
        return $this->model->where('id', '!=', auth()->user()->id)
        ->when($search, function($query) use($search) {
            $query->where(function($query) use($search) {
                $query->orWhere('username', 'like', "%" . $search . "%");
                $query->orWhere('firstname',  'like', "%" . $search . "%");
                $query->orWhere('lastname',   'like', "%" . $search . "%");
            });
        })
        ->paginate($perPage);
    }

}