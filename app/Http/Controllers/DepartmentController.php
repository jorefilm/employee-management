<?php

namespace App\Http\Controllers;

// App
use App\Repositories\DepartmentRepositoryInterface;
use App\Http\Requests\DepartmentRequest;

// Laravel
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Department Repository implementation 
     */ 
    private $department;

     /**
     * Create new controller instance
     * 
     * @return void 
     */ 
    public function __construct(DepartmentRepositoryInterface $department)
    {
        $this->department = $department;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = $this->department->listAllPaginated();

        $data = [
            'departments' => $departments
        ];
        return view('departments.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.edit_create');
    }

    private function saveDepartmentForm($request, $department = null)
    {
        $data = $request->all();

        if($department) {
            return $this->department->update($department, $data);
        }

        return $this->department->create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        $department = $this->saveDepartmentForm($request);

        flash(__('alerts.success'))->success();
        return redirect()->route('department.show', $department->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = $this->department->findById($id);

        $data = [
            'department' => $department
        ];
        return view('departments.edit_create', $data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentRequest $request, $id)
    {
        $department = $this->department->findById($id);
        $this->saveDepartmentForm($request, $department);

        flash(__('alerts.success'))->success();
        return redirect()->route('department.show', $department->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->department->delete($id);

        flash(__('alerts.success'))->success();
        return redirect()->route('department.index');
    }
}
