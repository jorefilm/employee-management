@extends('layouts.app')

@section('content')
    <a href="{{ route('division.index') }}">Back to Divisions</a>
    <h1>{{ isset($division) ? 'Edit' : 'Create' }} Division</h1>

    <div class="row">
        <div class="col-lg-6">
            
            @include('partials.alerts')

            <form action="{{ isset($division) ? route('division.update', $division->id) : route('division.store') }}" method="POST">
                @csrf
                @method(isset($division) ? 'PUT' : 'POST')
        
                <div class="form-group">
                    <label for="">Division Name</label>
                    <input type="text" name="name" value="{{ old('name', (isset($division) ? $division->name : '' )) }}" class="form-control">
                </div>
        

        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
@endsection