
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app'
// });


$(function() {

    datePicker();
    countrySelect();
    confirmDelete();
    pictureUpload();


    function datePicker() {
        $('.datepicker').datepicker(
            { 
                dateFormat: 'yy-mm-dd',
                maxDate: 0
            }
        );
    }

    function countrySelect() {
        $('select[name="state_id"], select[name="city_id"]').find('option').hide();

        $('select[name="country_id"]').change(function() {
            // $('select[name="state_id"], select[name="city_id"]').parents('.form-group').show();
            console.log('country changed');
            var countryId = $(this).val();
            $('select[name="state_id"]').val('');
            $('select[name="city_id"]').val('');
            $('select[name="state_id"]').find('option').hide();
            $('select[name="state_id"]').find('option[data-country-id="' + countryId  + '"]').show();
        });

        $('select[name="state_id"]').change(function() {
            // $('select[name="state_id"], select[name="city_id"]').parents('.form-group').show();
            console.log('state changed');
            var stateId = $(this).val();
            $('select[name="city_id"]').val('');
            $('select[name="city_id"]').find('option').hide();
            $('select[name="city_id"]').find('option[data-state-id="' + stateId  + '"]').show();
        });
    }

    function confirmDelete() {
        $('form.action-delete').submit(function(e) {
            if(!confirm('Are you sure?')) {
                return e.preventDefault();
            }
        });
    }

    function pictureUpload() {
        $('input[name="picture"').change(function() {
            if (this.files && this.files[0]) {
                console.log('changed')
                var reader = new FileReader();
            
                var that = this;
                reader.onload = function(e) {
                  $(that).parents('.form-group').find('.picture-preview').attr('src', e.target.result);
                }
            
                reader.readAsDataURL(that.files[0]);
            }
        });
    }

});