<?php

namespace App\Http\Controllers;

// App
use App\Repositories\EmployeeRepositoryInterface;
use App\Repositories\SalaryRepositoryInterface;
use App\Http\Requests\SalaryRequest;

// Laravel
use Illuminate\Http\Request;

class SalaryController extends Controller
{
    /**
     * Employee Repository implementation 
     */ 
    private $employee;

    /**
     * Salary Repository implementation 
     */ 
    private $salary;

     /**
     * Create new controller instance
     * 
     * @return void 
     */ 
    public function __construct(EmployeeRepositoryInterface $employee, SalaryRepositoryInterface $salary)
    {
        $this->employee = $employee;
        $this->salary = $salary;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employees = $this->employee->listSearchSalaryResult($request->input('search'))
            ->appends($request->except(['page','_token']));

        $data = [
            'employees' => $employees
        ];
        return view('salaries.index', $data);
    }

    private function saveSalaryForm($request, $employee)
    {
        $data = $request->all();

        // check if employee has already related salary 
        if($employee->salary)
        {
            return $this->salary->update($employee->salary, $data);
        }
        
        return $this->employee->addSalaryToEmployee($employee, $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalaryRequest $request)
    {
        $employee = $this->employee->findById($request->input('employee_id'));
        $this->saveSalaryForm($request, $employee);

        flash(__('alerts.success'))->success();
        return redirect()->route('salary.show', $employee->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $employee = $this->employee->findById($id);

        $data = [
            'employee' => $employee,
            'salary' => $employee->salary
        ];
        return view('salaries.edit_create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function update(SalaryRequest $request, $id)
    {
        $salary = $this->salary->findById($id);
        $this->salary->update($salary, $request->all());

        flash(__('alerts.success'))->success();
        return redirect()->route('salary.show', $salary->employee_id);
    }

    public function report()
    {
        $employees = $this->employee->getAllEmployeeSalaryReport();
            
        $data = [
            'employees' => $employees
        ];
        return view('salaries.report', $data);
    }
}
