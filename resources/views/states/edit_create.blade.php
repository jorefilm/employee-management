@extends('layouts.app')

@section('content')
    <a href="{{ route('state.index') }}">Back to States</a>
    <h1>{{ isset($state) ? 'Edit' : 'Create' }} State</h1>

    <div class="row">
        <div class="col-lg-6">
            
            @include('partials.alerts')

            <form action="{{ isset($state) ? route('state.update', $state->id) : route('state.store') }}" method="POST">
                @csrf
                @method(isset($state) ? 'PUT' : 'POST')
        
                <div class="form-group">
                    <label for="">State Name</label>
                    <input type="text" name="name" value="{{ old('name', (isset($state) ? $state->name : '' )) }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Country</label>
                    <select name="country_id" class="form-control">
                        <option value="">Select Country</option>

                        @foreach($countries as $country)
                            <option 
                                value="{{ $country->id }}"
                                @if(old('country_id'))
                                    {{ old('country_id') == $country->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($state) ? ( $state->country_id == $country->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                {{ $country->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
        
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
@endsection