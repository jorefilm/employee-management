<?php $__env->startSection('content'); ?>
    <h1>Departments</h1>
    <a class="mb-3 d-block" href="<?php echo e(route('department.create')); ?>">Add</a>

    <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th></th>
        </thead>
        <?php if($departments): ?>
            <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($department->id); ?></td>
                    <td><?php echo e($department->name); ?></td>
                    <td class="text-center">
                        <a href="<?php echo e(route('department.show', $department->id)); ?>">Edit</a>

                        <form action="<?php echo e(route('department.destroy', $department->id)); ?>" method="POST" class="d-inline action-delete">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('DELETE'); ?>
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </table>

    <?php echo e($departments->links()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>