<?php $__env->startSection('content'); ?>
    <h1>Employees</h1>
    <div class="mb-3 d-block">
        <a class="" href="<?php echo e(route('employee.create')); ?>">Add</a> | <a class="" href="<?php echo e(route('salary.index')); ?>">Salaries</a> | <a class="" href="<?php echo e(route('report.employees')); ?>">Reports</a>
    </div>

    <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <form>
        <div class="input-group mb-2">
            <input type="text" name="search" placeholder="Search employee by name and department." class="form-control" value="<?php echo e(Request::input('search')); ?>">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Department</th>
            <th>Company</th>
            <th>Location</th>
            <th>Date Hired</th>
            <th></th>
        </thead>
        <?php if($employees): ?>
            <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($employee->id); ?></td>
                    <td><?php echo e($employee->fullname); ?></td>
                    <td><?php echo e($employee->department->name); ?></td>
                    <td><?php echo e($employee->company->name); ?></td>
                    <td><?php echo e($employee->state ? $employee->state->name . ', ' : ''); ?><?php echo e($employee->country ? $employee->country->name : ''); ?></td>
                    <td><?php echo e($employee->hired_date); ?></td>
                    <td class="text-center">
                        <a href="<?php echo e(route('employee.show', $employee->id)); ?>">Edit</a>
                        <form action="<?php echo e(route('employee.destroy', $employee->id)); ?>" method="POST" class="d-inline action-delete">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('DELETE'); ?>
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>

                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </table>

    <?php echo e($employees->links()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>