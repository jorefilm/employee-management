<?php

namespace App\Http\Controllers;

// App
use App\Http\Requests\UserRequest;

use App\Repositories\UserRepositoryInterface;

// Laravel
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * User Repository implementation 
     */ 
    private $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $auth_user = auth()->user();

        $users = $this->user->listSearchResult($request->input('search'))
            ->appends($request->except(['page','_token']));

        $data = [
            'users' => $users
        ];

        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.edit_create');
    }

    public function saveUserForm($request, $user = null)
    {
        $data = $request->only(['username', 'firstname', 'lastname', 'email']);

        // Check if password is changed
        if($request->input('password')) {
            $data['password'] = bcrypt($request->input('password'));
        }

        if($user) {
            return $this->user->update($user, $data);
        }

        return $this->user->create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->saveUserForm($request);

        flash(__('alerts.success'))->success();
        return redirect()->route('user.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->user->findById($id);

        $data = [
            'user' => $user
        ];
        return view('users.edit_create', $data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = $this->user->findById($id);
        $this->saveUserForm($request, $user);

        flash(__('alerts.success'))->success();
        return redirect()->route('user.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user->delete($id);

        flash(__('alerts.success'))->success();
        return redirect()->route('user.index');
    }
}
