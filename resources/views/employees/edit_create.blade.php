@extends('layouts.app')

@section('content')
    <a href="{{ route('employee.index') }}">Back to Employees</a>
    <h1>{{ isset($employee) ? 'Edit' : 'Create' }} Employee</h1>

    <div class="row">
        <div class="col-lg-6">
            @include('partials.alerts')

    
            <form action="{{ isset($employee) ? route('employee.update', $employee->id) : route('employee.store') }}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method(isset($employee) ? 'PUT' : 'POST')
                
                <h4>Basic Info</h4>
                <div class="form-group">
                    <label for="">Firstname</label>
                    <input type="text" name="firstname" value="{{ old('firstname', (isset($employee) ? $employee->firstname : '' )) }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Middlename</label>
                    <input type="text" name="middlename" value="{{ old('middlename', (isset($employee) ? $employee->middlename : '' )) }}" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Lastname</label>
                    <input type="text" name="lastname" value="{{ old('lastname', (isset($employee) ? $employee->lastname : '' )) }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Date of Birth</label>
                    <input type="text" name="birthdate" value="{{ old('birthdate', (isset($employee) ? $employee->bday : '' )) }}" class="form-control datepicker"  autocomplete="off">
                </div>
                @if(isset($employee))
                    <div class="form-group">
                        <label for="">Age</label>
                        <input type="text" readonly value="{{ $employee->age }}" class="form-control">
                    </div>
                @endif
                <div class="form-group">
                    <label for="">Photo</label>
                    @if(isset($employee))
                        <img class="img-fluid picture-preview" src="{{ $employee->picture ? asset('uploads/' . $employee->picture) : 'https://www.chaarat.com/wp-content/uploads/2017/08/placeholder-user.png' }}">
                    @else
                        <img class="img-fluid picture-preview" src="https://www.chaarat.com/wp-content/uploads/2017/08/placeholder-user.png">
                    @endif
                    <input type="file" name="picture" value="{{ old('picture', (isset($employee) ? $employee->picture : '' )) }}" class="form-control">
                </div>

                <h4>Address</h4>

                <div class="form-group">
                    <label for="">Address</label>
                    <input type="text" name="address" value="{{ old('address', (isset($employee) ? $employee->address : '' )) }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Country</label>
                    <select name="country_id" class="form-control">
                        <option value="">Select Country</option>

                        @foreach($countries as $country)
                            <option 
                                value="{{ $country->id }}"
                                @if(old('country_id'))
                                    {{ old('country_id') == $country->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($employee) ? ( $employee->country_id == $country->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                {{ $country->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="">State</label>
                    <select name="state_id" class="form-control">
                        <option value="">Select State</option>

                        @foreach($states as $state)
                            <option
                                value="{{ $state->id }}"
                                data-country-id="{{ $state->country_id }}"
                                @if(old('state_id'))
                                    {{ old('state_id') == $state->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($employee) ? ( $employee->state_id == $state->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                {{ $state->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="">City</label>
                    <select name="city_id" class="form-control">
                        <option value="">Select City</option>

                        @foreach($cities as $city)
                            <option
                                value="{{ $city->id }}"
                                data-state-id="{{ $city->state_id }}"
                                data-country-id="{{ $city->state->country->id }}"
                                @if(old('city_id'))
                                    {{ old('city_id') == $city->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($employee) ? ( $employee->city_id == $city->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                    {{ $city->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                

                <div class="form-group">
                    <label for="">ZIP</label>
                        <input type="text" name="zip" value="{{ old('zip', (isset($employee) ? $employee->zip : '' )) }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Department</label>
                    <select name="department_id" class="form-control">
                        <option value="">Select Department</option>

                        @foreach($departments as $department)
                            <option 
                                value="{{ $department->id }}"
                                @if(old('department_id'))
                                    {{ old('department_id') == $department->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($employee) ? ( $employee->department_id == $department->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                {{ $department->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Division</label>
                    <select name="division_id" class="form-control">
                        <option value="">Select division</option>

                        @foreach($divisions as $division)
                            <option 
                                value="{{ $division->id }}"
                                @if(old('division_id'))
                                    {{ old('division_id') == $division->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($employee) ? ( $employee->division_id == $division->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                {{ $division->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Company</label>
                    <select name="company_id" class="form-control">
                        <option value="">Select company</option>

                        @foreach($companies as $company)
                            <option 
                                value="{{ $company->id }}"
                                @if(old('company_id'))
                                    {{ old('company_id') == $company->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($employee) ? ( $employee->company_id == $company->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                {{ $company->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Date Hired</label>
                    <input type="text" name="date_hired" value="{{ old('date_hired', (isset($employee) ? $employee->hired_date : '' )) }}" class="form-control datepicker" autocomplete="off">
                </div>


        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
@endsection