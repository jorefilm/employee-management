<?php

// App
use App\City;
use App\State;
use App\Country;
use App\Department;
use App\Division;
use App\Company;
use App\Employee;
// Laravel
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $companies = Company::all()->pluck('id');
        $departments = Department::all()->pluck('id');
        $divisions = Division::all()->pluck('id');

        $countries = Country::whereHas('states.cities')
            ->with('states.cities')
            ->get();

        $bday = Carbon::createFromDate(2000, 1, 1);
        $today = Carbon::create();
        $startHired = Carbon::createFromDate(2012, 1, 1);

        

        $dateHired  = rand($startHired->timestamp, $today->timestamp);
        $country       = $countries->random();

        factory(Employee::class, 50)->create([
            'city_id'       => $country->states->random()->cities->random()->id,
            'state_id'      => $country->states->random()->id,
            'country_id'    => $country->id,
            'birthdate'     => $bday->toDateString(),
            // 'age'           => $today->diffInYears($bday),
            'date_hired'    => Carbon::createFromTimeStamp($dateHired),
            'department_id' => $departments->random(),
            'division_id'   => $divisions->random(),
            'company_id'    => $companies->random(),
        ]);
        
        
    }
}
