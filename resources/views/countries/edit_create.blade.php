@extends('layouts.app')

@section('content')
    <a href="{{ route('country.index') }}">Back to Countries</a>
    <h1>{{ isset($country) ? 'Edit' : 'Create' }} Country</h1>

    <div class="row">
        <div class="col-lg-6">
            
            @include('partials.alerts')

            <form action="{{ isset($country) ? route('country.update', $country->id) : route('country.store') }}" method="POST">
                @csrf
                @method(isset($country) ? 'PUT' : 'POST')
        
                <div class="form-group">
                    <label for="">Country Name</label>
                    <input type="text" name="name" value="{{ old('name', (isset($country) ? $country->name : '' )) }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Country Code</label>
                    <input type="text" name="country_code" value="{{ old('country_code', (isset($country) ? $country->country_code : '' )) }}" class="form-control">
                </div>
        
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
@endsection