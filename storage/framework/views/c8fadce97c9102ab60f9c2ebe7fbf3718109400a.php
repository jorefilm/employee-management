<?php $__env->startSection('content'); ?>
    <h1>Divisions</h1>

    <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th></th>
        </thead>
        <?php if($divisions): ?>
            <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($division->id); ?></td>
                    <td><?php echo e($division->name); ?></td>
                    <td class="text-center">
                        <a href="<?php echo e(route('division.show', $division->id)); ?>">Edit</a>
                        <form action="<?php echo e(route('division.destroy', $division->id)); ?>" method="POST" class="d-inline action-delete">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('DELETE'); ?>
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </table>

    <?php echo e($divisions->links()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>