<!-- Sidebar -->
@if(config('menu.sidebar'))
    <ul class="sidebar navbar-nav">
        @foreach(config('menu.sidebar') as $menuItem)
            <li class="nav-item dropdown">
                <a 
                    class="nav-link {{ $menuItem['children'] ? 'dropdown-toggle' : '' }}"
                    href="{{ count($menuItem['children']) ? '#' : route($menuItem['route'])  }}"
                    role="button"
                    {{ count($menuItem['children']) ? 'data-toggle=dropdown' : '' }}
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    <i class="fas fa-fw {{ $menuItem['icon'] }}"></i>
                    <span>{{ $menuItem['label'] }}</span>
                </a>

                @if($menuItem['children'])
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        @foreach($menuItem['children'] as $childItem)
                            <a class="dropdown-item" href="{{ route($childItem['route']) }}">{{ $childItem['label'] }}</a>
                        @endforeach
                    </div>
                @endif
            </li>
        @endforeach
    </ul>
@endif