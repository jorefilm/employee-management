<?php

namespace App;

// App

// Laravel
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'firstname',
        'lastname',
        'middlename',
        'address',
        'city_id',
        'state_id',
        'country_id',
        'zip',
        'birthdate',
        'date_hired',
        'department_id',
        'division_id',
        'company_id',
        'picture'
    ];

    protected $dates = ['date_hired', 'birthdate'];
    protected $appends = ['age'];

    // Relations

    public function salary()
    {
        return $this->hasOne(Salary::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    // Mutators

    public function getFullnameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getAgeAttribute()
    {
        return Carbon::create()->diffInYears($this->birthdate);
    }
    public function getBdayAttribute()
    {
        return $this->birthdate->format('Y-m-d');
    }

    public function getHiredDateAttribute()
    {
        return $this->date_hired->format('Y-m-d');
    }

    public function getSalaryAmountAttribute()
    {
        return null != $this->salary ? $this->salary->salary : null;
    }
}
