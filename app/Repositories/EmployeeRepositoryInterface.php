<?php


namespace App\Repositories;

interface EmployeeRepositoryInterface
{
    public function listSearchResult($search = null, $perPage = 10);
    public function listSearchSalaryResult($search = null, $perPage = 10);
    public function getAllEmployeeSalaryReport();
    public function addSalaryToEmployee($employee, $data);
}