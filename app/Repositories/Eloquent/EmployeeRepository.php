<?php

namespace App\Repositories\Eloquent;

// App
use App\Employee;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\EmployeeRepositoryInterface;

class EmployeeRepository extends AbstractRepository implements EmployeeRepositoryInterface
{
    /**
     * Create mew instance of repository
     * 
     * @return void
     */ 
    public function __construct(Employee $employee)
    {
        $this->model = $employee;
    }

    /**
     * Retrieve the List of Employees
     * 
     * @return \Illuminate\Pagination\Paginator|Array
     */ 
    public function listSearchResult($search = null, $perPage = 10)
    {
        return $this->model->select('employees.*')
            ->join('departments', 'departments.id', '=',  'employees.department_id')
            ->latest()
            ->when($search, function($query) use($search) {
                $query->where(function($query) use($search) {
                    $query->where('employees.firstname',  'like', "%" . $search . "%");
                    $query->orWhere('employees.middlename', 'like', "%" . $search . "%");
                    $query->orWhere('employees.lastname',   'like', "%" . $search . "%");
                    $query->orWhere('departments.name',   'like', "%" . $search . "%");
                });
            })
            ->paginate(20);
    }

    /**
     * Retrieve the List of Employee Salaries
     * 
     * @return \Illuminate\Pagination\Paginator|Array
     */ 
    public function listSearchSalaryResult($search = null, $perPage = 10)
    {
        return $this->model->select('employees.*')
            ->leftJoin('salaries', 'salaries.employee_id', '=',  'employees.id')
            ->leftJoin('departments', 'departments.id', '=', 'employees.department_id')
            ->when($search, function($query) use($search) {
                $query->where(function($query) use($search) {
                    $query->where('employees.firstname',  'like', "%" . $search . "%");
                    $query->orWhere('employees.middlename', 'like', "%" . $search . "%");
                    $query->orWhere('employees.lastname',   'like', "%" . $search . "%");
                    $query->orWhere('departments.name',   'like', "%" . $search . "%");
                    $query->orWhere('salaries.salary',   'like', "%" . $search . "%");
                });
            })
            ->with('salary')
            ->paginate(20);
    }

    public function getAllEmployeeSalaryReport()
    {
        return $this->model->select('employees.*')
            ->whereNotNull('salaries.salary')
            ->leftJoin('salaries', 'salaries.employee_id', '=',  'employees.id')
            ->orderBy('salaries.salary', 'DESC')
            ->with('salary')
            ->get();
    }

    public function addSalaryToEmployee($employee, $data)
    {
        return $employee->salary()->create($data)->save();
    }

    public function getEmployeeReport($request)
    {
        return $this->model->select('employees.*')
            ->join('departments', 'departments.id', '=',  'employees.department_id')
            ->latest()
            ->when($request->input('date_start'), function($query) use($request) {
               $query->whereDate('date_hired', '>', $request->input('date_start'));
            })
            ->when($request->input('date_end'), function($query) use($request) {
                $query->where('date_hired', '<', $request->input('date_end'));
            })
            ->get();
        
    }
}