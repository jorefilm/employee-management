@extends('layouts.app')

@section('content')
    <h1>Reports - Salaries</h1>
    <div class="mb-3 d-block">
        <a class="" href="{{ route('report.salaries') }}">Salaries</a> | <a class="" href="{{ route('report.employees') }}">Reports</a>
    </div>

    @include('partials.alerts')

    <div id="customers">
        <table class="table table-borderless">
            <colgroup>
                <col width="10%">
                <col width="45%">
                <col width="45%">
            </colgroup>
            <thead>
                <th>ID</th>
                <th>Employee</th>
                <th>Salary</th>
            </thead>
            @if($employees)
                @foreach($employees as $employee)
                    <tr>
                        <td>{{ $employee->id }}</td>
                        <td>{{ $employee->fullname }}</td>
                        <td>{{ $employee->salary_amount }}</td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" ></script>
    
    <script>
        $(function() {
            $(document).ready(function() {
                $('#customers table').DataTable( {
                    dom: 'Bfrtip',
                    searching: false,
                    ordering: false,
                    info: false,
                    paging: false,
                    buttons: [
                        'excelHtml5',
                        'pdfHtml5'
                    ]
                } );
            } );
        });
    </script>
@endpush