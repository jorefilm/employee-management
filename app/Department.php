<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name',
    ];

    // Relations

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
}
