@extends('layouts.app')

@section('content')
    <h1>Divisions</h1>

    @include('partials.alerts')

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th></th>
        </thead>
        @if($divisions)
            @foreach($divisions as $division)
                <tr>
                    <td>{{ $division->id }}</td>
                    <td>{{ $division->name }}</td>
                    <td class="text-center">
                        <a href="{{ route('division.show', $division->id) }}">Edit</a>
                        <form action="{{ route('division.destroy', $division->id) }}" method="POST" class="d-inline action-delete">
                            @csrf
                            @method('DELETE')
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    {{ $divisions->links() }}
@endsection