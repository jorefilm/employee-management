<?php

namespace App\Repositories;

interface CityRepositoryInterface
{
    public function listSearchResult($search = null, $perPage = 10);
}