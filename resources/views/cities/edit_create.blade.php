@extends('layouts.app')

@section('content')
    <a href="{{ route('city.index') }}">Back to Cities</a>
    <h1>{{ isset($city) ? 'Edit' : 'Create' }} City</h1>

    <div class="row">
        <div class="col-lg-6">
            
            @include('partials.alerts')

            <form action="{{ isset($city) ? route('city.update', $city->id) : route('city.store') }}" method="POST">
                @csrf
                @method(isset($city) ? 'PUT' : 'POST')
        
                <div class="form-group">
                    <label for="">City Name</label>
                    <input type="text" name="name" value="{{ old('name', (isset($city) ? $city->name : '' )) }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Country</label>
                    <select name="country_id" class="form-control">
                        <option value="">Select Country</option>

                        @foreach($countries as $country)
                            <option 
                                value="{{ $country->id }}"
                                @if(old('country_id'))
                                    {{ old('country_id') == $country->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($city) ? ( $city->state->country_id == $country->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                {{ $country->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="">State</label>
                    <select name="state_id" class="form-control">
                        <option value="">Select State</option>

                        @foreach($states as $state)
                            <option 
                                value="{{ $state->id }}"
                                data-country-id="{{ $state->country_id }}"
                                @if(old('state_id'))
                                    {{ old('state_id') == $state->id ? ' selected="selected"' : '' }}
                                @else
                                    {{ isset($city) ? ( $city->state_id == $state->id ? ' selected="selected"' : ''  ) : '' }}
                                @endif
                            >
                                {{ $state->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
        
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
@endsection