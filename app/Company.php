<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name',
    ];

    // Relations
    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
}
