<?php $__env->startSection('content'); ?>
    <h1>Reports - Employees</h1>
    <div class="mb-3 d-block">
        <a class="" href="<?php echo e(route('report.salaries')); ?>">Salaries</a> | <a class="" href="<?php echo e(route('report.employees')); ?>">Reports</a>
    </div>

    <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <form>
        <div class="input-group mb-2">
            <input type="text" name="date_start" value="<?php echo e(Request::input('date_start')); ?>" class="form-control datepicker" autocomplete="off">
            <input type="text" name="date_end" value="<?php echo e(Request::input('date_end')); ?>" class="form-control datepicker"  autocomplete="off">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <div id="customers">
        <table class="table table-borderless">
            <colgroup>
                <col width="5%">
                <col width="20%">
                <col width="20%">
                <col width="20%">
                <col width="20%">
                <col width="15%">
            </colgroup>
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Department</th>
                <th>Company</th>
                <th>Location</th>
                <th>Date Hired</th>
            </thead>
            <?php if($employees): ?>
                <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($employee->id); ?></td>
                        <td><?php echo e($employee->fullname); ?></td>
                        <td><?php echo e($employee->department->name); ?></td>
                        <td><?php echo e($employee->company->name); ?></td>
                        <td><?php echo e($employee->state ? $employee->state->name . ',' : ''); ?> <?php echo e($employee->country ? $employee->country->name : ''); ?></td>
                        <td><?php echo e($employee->hired_date); ?></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </table>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" ></script>
    
    <script>
        $(function() {
            $(document).ready(function() {
                $('#customers table').DataTable( {
                    dom: 'Bfrtip',
                    searching: false,
                    buttons: [
                        'excelHtml5',
                        'pdfHtml5'
                    ]
                } );
            } );
        });
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>