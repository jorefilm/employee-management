@extends('layouts.app')

@section('content')
    <h1>Reports - Employees</h1>
    <div class="mb-3 d-block">
        <a class="" href="{{ route('report.salaries') }}">Salaries</a> | <a class="" href="{{ route('report.employees') }}">Reports</a>
    </div>

    @include('partials.alerts')

    <form>
        <div class="input-group mb-2">
            <input type="text" name="date_start" value="{{ Request::input('date_start') }}" class="form-control datepicker" autocomplete="off">
            <input type="text" name="date_end" value="{{ Request::input('date_end') }}" class="form-control datepicker"  autocomplete="off">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <div id="customers">
        <table class="table table-borderless">
            <colgroup>
                <col width="5%">
                <col width="20%">
                <col width="20%">
                <col width="20%">
                <col width="20%">
                <col width="15%">
            </colgroup>
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Department</th>
                <th>Company</th>
                <th>Location</th>
                <th>Date Hired</th>
            </thead>
            @if($employees)
                @foreach($employees as $employee)
                    <tr>
                        <td>{{ $employee->id }}</td>
                        <td>{{ $employee->fullname }}</td>
                        <td>{{ $employee->department->name }}</td>
                        <td>{{ $employee->company->name }}</td>
                        <td>{{ $employee->state ? $employee->state->name . ',' : '' }} {{ $employee->country ? $employee->country->name : '' }}</td>
                        <td>{{ $employee->hired_date }}</td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" ></script>
    
    <script>
        $(function() {
            $(document).ready(function() {
                $('#customers table').DataTable( {
                    dom: 'Bfrtip',
                    searching: false,
                    buttons: [
                        'excelHtml5',
                        'pdfHtml5'
                    ]
                } );
            } );
        });
    </script>
@endpush