<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(route('user.index')); ?>">Back to Users</a>
    <h1><?php echo e(isset($user) ? 'Edit' : 'Create'); ?> User</h1>

    <div class="row">
        <div class="col-lg-6">
            
            <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <form action="<?php echo e(isset($user) ? route('user.update', $user->id) : route('user.store')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php echo method_field(isset($user) ? 'PUT' : 'POST'); ?>
                
                <?php if(isset($user)): ?>
                <input type="hidden" name="id" value="<?php echo e($user->id); ?>">
                <?php endif; ?>
                
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" name="username" value="<?php echo e(old('username', (isset($user) ? $user->username : '' ))); ?>" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Firstname</label>
                    <input type="text" name="firstname" value="<?php echo e(old('firstname', (isset($user) ? $user->firstname : '' ))); ?>" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Lastname</label>
                    <input type="text" name="lastname" value="<?php echo e(old('lastname', (isset($user) ? $user->lastname : '' ))); ?>" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" name="email" value="<?php echo e(old('email', (isset($user) ? $user->email : '' ))); ?>" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" name="password" value="" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Confirm Password</label>
                    <input type="password" name="password_confirmation" value="" class="form-control">
                </div>
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>