@extends('layouts.app')

@section('content')
    <h1>Employees</h1>
    <div class="mb-3 d-block">
        <a class="" href="{{ route('employee.create') }}">Add</a> | <a class="" href="{{ route('salary.index') }}">Salaries</a> | <a class="" href="{{ route('report.employees') }}">Reports</a>
    </div>

    @include('partials.alerts')

    <form>
        <div class="input-group mb-2">
            <input type="text" name="search" placeholder="Search employee by name and department." class="form-control" value="{{ Request::input('search') }}">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Department</th>
            <th>Company</th>
            <th>Location</th>
            <th>Date Hired</th>
            <th></th>
        </thead>
        @if($employees)
            @foreach($employees as $employee)
                <tr>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->fullname }}</td>
                    <td>{{ $employee->department->name }}</td>
                    <td>{{ $employee->company->name }}</td>
                    <td>{{ $employee->state ? $employee->state->name . ', ' : '' }}{{ $employee->country ? $employee->country->name : '' }}</td>
                    <td>{{ $employee->hired_date }}</td>
                    <td class="text-center">
                        <a href="{{ route('employee.show', $employee->id) }}">Edit</a>
                        <form action="{{ route('employee.destroy', $employee->id) }}" method="POST" class="d-inline action-delete">
                            @csrf
                            @method('DELETE')
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>

                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    {{ $employees->links() }}
@endsection