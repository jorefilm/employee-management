@extends('layouts.app')

@section('content')
    <h1>Countries</h1>
    <a class="mb-3 d-block" href="{{ route('country.create') }}">Add</a>

    @include('partials.alerts')

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Code</th>
            <th></th>
        </thead>
        @if($countries)
            @foreach($countries as $country)
                <tr>
                    <td>{{ $country->id }}</td>
                    <td>{{ $country->name }}</td>
                    <td>{{ $country->country_code }}</td>
                    <td class="text-center">
                        <a href="{{ route('country.show', $country->id) }}">Edit</a>

                        <form action="{{ route('country.destroy', $country->id) }}" method="POST" class="d-inline action-delete">
                            @csrf
                            @method('DELETE')
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    {{ $countries->links() }}
@endsection