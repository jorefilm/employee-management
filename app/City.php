<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class City extends Model
{
    protected $fillable = [
        'name',
        'state_id'
    ];

    protected $with = ['state'];
    
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('state_id', function (Builder $builder) {
            $builder->whereNotNull('state_id');
            $builder->orderBy('name', 'ASC');
        });
    }

    // Relations
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    
}
