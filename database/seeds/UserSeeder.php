<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete all data first
        App\User::truncate();

        factory(App\User::class, 50)->create();

        // Add default admin use
        factory(App\User::class)->create([
            'username' => 'admin',
            'email'    =>  'admin@admin.com',
            'password' => bcrypt('admin')
        ]);

        $this->command->info('-------------------------');
        $this->command->info('Creted Admin User');
        $this->command->info('email: admin@admin.com');
        $this->command->info('password: admin');
        $this->command->info('-------------------------');
    }
}
