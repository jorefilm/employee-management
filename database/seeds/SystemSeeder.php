<?php

use Illuminate\Database\Seeder;
use App\Department;
use App\Division;
use App\Company;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        Division::truncate();
        Department::truncate();
        Company::truncate();

        factory(Division::class, 30)->create();
        factory(Department::class, 30)->create();
        factory(Company::class, 30)->create();

    }
}
