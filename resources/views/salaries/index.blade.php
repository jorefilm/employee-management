@extends('layouts.app')

@section('content')
    <h1>Salaries</h1>

    @include('partials.alerts')

    <form>
        <div class="input-group mb-2">
            <input type="text" name="search" placeholder="Search by employee name, department and salary." class="form-control" value="{{ Request::input('search') }}">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Employee</th>
            <th>Department</th>
            <th>Salary</th>
            <th></th>
        </thead>
        @if($employees)
            @foreach($employees as $employee)
                <tr>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->fullname }}</td>
                    <td>{{ $employee->department->name }}</td>
                    <td>{{ $employee->salary_amount }}</td>
                    <td class="text-center">
                        <a href="{{ route('salary.show', $employee->id) }}">Edit</a>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    {{ $employees->links() }}
@endsection