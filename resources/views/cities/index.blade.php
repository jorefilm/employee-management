@extends('layouts.app')

@section('content')
    <h1>Cities</h1>
    <a class="mb-3 d-block" href="{{ route('city.create') }}">Add</a>
    
    @include('partials.alerts')

    <form>
        <div class="input-group mb-2">
            <input type="text" name="search" placeholder="Search city name" class="form-control" value="{{ Request::input('search') }}">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>State</th>
            <th>Country</th>
            <th></th>
        </thead>
        @if($cities)
            @foreach($cities as $city)
                <tr>
                    <td>{{ $city->id }}</td>
                    <td>{{ $city->name }}</td>
                    <td>{{ $city->state ? $city->state->name : '' }}</td>
                    <td>{{ $city->state ? ($city->state->country ? $city->state->country->name : '' ) : '' }}</td>
                    <td class="text-center">
                        <a href="{{ route('city.show', $city->id) }}">Edit</a>
                        <form action="{{ route('city.destroy', $city->id) }}" method="POST" class="d-inline action-delete">
                            @csrf
                            @method('DELETE')
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    {{ $cities->links() }}
@endsection