<?php

// App
use App\Country;
use App\City;
use App\State;

// Laravel
use Illuminate\Database\Seeder;

class GeoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete all data first
        App\City::truncate();
        App\State::truncate();
        App\Country::truncate();

        $this->call(WorldCountriesTableSeeder::class);
        $this->call(WorldDivisionsTableSeeder::class);
        $this->call(WorldCitiesTableSeeder::class);
    }


}
