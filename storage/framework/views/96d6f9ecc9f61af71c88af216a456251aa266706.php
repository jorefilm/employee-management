<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(route('salary.index')); ?>">Back to List</a>
    <h1>Salary <?php echo e(isset($employee) ? '- ' . $employee->fullname : ''); ?></h1>

    <div class="row">
        <div class="col-lg-6">
            
            <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <form action="<?php echo e(isset($salary) ? route('salary.update', $salary->id) : route('salary.store')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php echo method_field(isset($salary) ? 'PUT' : 'POST'); ?>

                <input type="hidden" name="employee_id" value="<?php echo e($employee->id); ?>">
        
                <div class="form-group">
                    <label for="">Salary Amount</label>
                    <input type="text" name="salary" value="<?php echo e(old('salary', (isset($salary) ? $salary->salary : '' ))); ?>" class="form-control">
                </div>

        
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>