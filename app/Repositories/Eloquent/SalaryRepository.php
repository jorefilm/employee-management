<?php

namespace App\Repositories\Eloquent;

// App
use App\Salary;
use App\Employee;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\SalaryRepositoryInterface;

class SalaryRepository extends AbstractRepository implements SalaryRepositoryInterface
{
    /**
     * Create mew instance of repository
     * 
     * @return void
     */ 
    public function __construct(Salary $salary)
    {
        $this->model = $salary;
    }

    

}