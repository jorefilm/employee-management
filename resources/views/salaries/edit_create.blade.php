@extends('layouts.app')

@section('content')
    <a href="{{ route('salary.index') }}">Back to List</a>
    <h1>Salary {{ isset($employee) ? '- ' . $employee->fullname : '' }}</h1>

    <div class="row">
        <div class="col-lg-6">
            
            @include('partials.alerts')

            <form action="{{ isset($salary) ? route('salary.update', $salary->id) : route('salary.store') }}" method="POST">
                @csrf
                @method(isset($salary) ? 'PUT' : 'POST')

                <input type="hidden" name="employee_id" value="{{ $employee->id }}">
        
                <div class="form-group">
                    <label for="">Salary Amount</label>
                    <input type="text" name="salary" value="{{ old('salary', (isset($salary) ? $salary->salary : '' )) }}" class="form-control">
                </div>

        
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
@endsection