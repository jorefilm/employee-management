<?php


namespace App\Repositories;

interface UserRepositoryInterface
{
    public function listSearchResult($search = null, $perPage = 10);
}