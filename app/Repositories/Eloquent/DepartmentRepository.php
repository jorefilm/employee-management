<?php

namespace App\Repositories\Eloquent;

// App
use App\Department;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\DepartmentRepositoryInterface;

class DepartmentRepository extends AbstractRepository implements DepartmentRepositoryInterface
{
    /**
     * Create mew instance of repository
     * 
     * @return void
     */ 
    public function __construct(Department $department)
    {
        $this->model = $department;
    }

}