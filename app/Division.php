<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = [
        'name',
    ];

    // Relations
    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
}
