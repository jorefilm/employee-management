<?php

namespace App\Http\Controllers;

// App
use App\Repositories\DivisionRepositoryInterface;
use App\Http\Requests\DivisionRequest;

// Laravel
use Illuminate\Http\Request;

class DivisionController extends Controller
{
    /**
     * Division Repository implementation 
     */ 
    private $division;

    /**
     * Create new controller instance
     * 
     * @return void 
     */ 
    public function __construct(DivisionRepositoryInterface $division)
    {
        $this->division = $division;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $divisions = $this->division->listAllPaginated();
        $data = [
            'divisions' => $divisions
        ];
        return view('divisions.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('divisions.edit_create');
    }

    public function saveDivisionForm($request, $division = null)
    {
        $data = $request->all();

        if($division) {
            return $this->division->update($division, $data);
        }
        return $this->division->create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DivisionRequest $request)
    {
        $division = $this->saveDivisionForm($request);

        flash(__('alerts.success'))->success();
        return redirect()->route('division.show', $division->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $division = $this->division->findById($id);
        $data = [
            'division' => $division
        ];
        return view('divisions.edit_create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function update(DivisionRequest $request, $id)
    {
        $division = $this->division->findById($id);
        $this->saveDivisionForm($request, $division);

        flash(__('alerts.success'))->success();
        return redirect()->route('division.show', $division->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->division->delete($id);

        flash(__('alerts.success'))->success();
        return redirect()->route('division.index');
    }
}
