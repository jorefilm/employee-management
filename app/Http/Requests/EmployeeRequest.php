<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'     => 'required|max:60',
            'middlename'    => 'required|max:60',
            'lastname'      => 'required|max:60',
            'birthdate'     => 'required|date_format:Y-m-d',
            'address'       => 'required|max:120',
            'city_id'       => 'required|exists:cities,id',
            'state_id'      => 'required|exists:states,id',
            'country_id'    => 'required|exists:countries,id',
            'zip'           => 'required|max:10',
            'division_id'   => 'required|exists:divisions,id',
            'department_id' => 'required|exists:departments,id',
            'company_id'    => 'required|exists:companies,id',
            'picture'       => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
        ];
    }
}
