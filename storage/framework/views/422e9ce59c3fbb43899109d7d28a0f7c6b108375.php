<?php $__env->startSection('content'); ?>
    <h1>Salaries</h1>

    <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <form>
        <div class="input-group mb-2">
            <input type="text" name="search" placeholder="Search by employee name, department and salary." class="form-control" value="<?php echo e(Request::input('search')); ?>">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Employee</th>
            <th>Department</th>
            <th>Salary</th>
            <th></th>
        </thead>
        <?php if($employees): ?>
            <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($employee->id); ?></td>
                    <td><?php echo e($employee->fullname); ?></td>
                    <td><?php echo e($employee->department->name); ?></td>
                    <td><?php echo e($employee->salary_amount); ?></td>
                    <td class="text-center">
                        <a href="<?php echo e(route('salary.show', $employee->id)); ?>">Edit</a>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </table>

    <?php echo e($employees->links()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>