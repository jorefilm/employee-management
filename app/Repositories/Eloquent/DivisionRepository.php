<?php

namespace App\Repositories\Eloquent;

// App
use App\Division;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\DivisionRepositoryInterface;

class DivisionRepository extends AbstractRepository implements DivisionRepositoryInterface
{
    /**
     * Create mew instance of repository
     * 
     * @return void
     */ 
    public function __construct(Division $division)
    {
        $this->model = $division;
    }

}