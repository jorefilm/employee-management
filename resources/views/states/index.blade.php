@extends('layouts.app')

@section('content')
    <h1>States</h1>
    <a class="mb-3 d-block" href="{{ route('state.create') }}">Add</a>
    
    @include('partials.alerts')

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Country</th>
            <th></th>
        </thead>
        @if($states)
            @foreach($states as $state)
                <tr>
                    <td>{{ $state->id }}</td>
                    <td>{{ $state->name }}</td>
                    <td>{{ $state->country->name }}</td>
                    <td class="text-center">
                        <a href="{{ route('state.show', $state->id) }}">Edit</a>
                        <form action="{{ route('state.destroy', $state->id) }}" method="POST" class="d-inline action-delete">
                            @csrf
                            @method('DELETE')
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    {{ $states->links() }}
@endsection