<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(route('department.index')); ?>">Back to Departments</a>
    <h1><?php echo e(isset($department) ? 'Edit' : 'Create'); ?> Department</h1>

    <div class="row">
        <div class="col-lg-6">
            
            <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <form action="<?php echo e(isset($department) ? route('department.update', $department->id) : route('department.store')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php echo method_field(isset($department) ? 'PUT' : 'POST'); ?>
        
                <div class="form-group">
                    <label for="">Department Name</label>
                    <input type="text" name="name" value="<?php echo e(old('name', (isset($department) ? $department->name : '' ))); ?>" class="form-control">
                </div>
        

        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>