<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $fillable = ['salary'];

    // Relations
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    
}
