<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $mappings = $this->getRepositoryMapping();

        // Iterate each mapping and bind the implementation
        foreach($mappings as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }

    /**
     * Return mapping of binding for Repositories
     *
     * @return Array
     */
    private function getRepositoryMapping()
    {
        return [
            'App\Repositories\EmployeeRepositoryInterface' => 'App\Repositories\Eloquent\EmployeeRepository',
            'App\Repositories\SalaryRepositoryInterface' => 'App\Repositories\Eloquent\SalaryRepository',
            'App\Repositories\CountryRepositoryInterface' => 'App\Repositories\Eloquent\CountryRepository',
            'App\Repositories\CityRepositoryInterface' => 'App\Repositories\Eloquent\CityRepository',
            'App\Repositories\StateRepositoryInterface' => 'App\Repositories\Eloquent\StateRepository',
            'App\Repositories\DivisionRepositoryInterface' => 'App\Repositories\Eloquent\DivisionRepository',
            'App\Repositories\DepartmentRepositoryInterface' => 'App\Repositories\Eloquent\DepartmentRepository',
            'App\Repositories\CompanyRepositoryInterface' => 'App\Repositories\Eloquent\CompanyRepository',
            
            'App\Repositories\UserRepositoryInterface' => 'App\Repositories\Eloquent\UserRepository',
        ];
    }
}
