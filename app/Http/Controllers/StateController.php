<?php

namespace App\Http\Controllers;

// App
use App\Repositories\StateRepositoryInterface;
use App\Repositories\CountryRepositoryInterface;

use App\Http\Requests\StateRequest;

// Laravel
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * State Repository implementation 
     */ 
    private $state;

    /**
     * Country Repository implementation 
     */ 
    private $country;

    /**
     * Create new controller instance
     * 
     * @return void 
     */ 
    public function __construct(StateRepositoryInterface $state, CountryRepositoryInterface $country)
    {
        $this->state = $state;
        $this->country = $country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $states = $this->state->listAllPaginated()
            ->appends($request->except(['page','_token']));
        $data = [
            'states' => $states
        ];
        return view('states.index', $data);
    }

    private function showStateForm($state = null)
    {
        $countries = $this->country->getAll();

        $data = [
            'countries' => $countries
        ];

        if($state) {
            $data['state'] = $state;
        }
        return view('states.edit_create', $data);
    }

    private function saveStateForm($request, $state = null)
    {
        $data = $request->all();

        if($state) {
            return $this->state->update($state, $data);
        }

        return $this->state->create($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->showStateForm();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StateRequest $request)
    {
        $state = $this->saveStateForm($request);

        flash(__('alerts.success'))->success();
        return redirect()->route('state.show', $state->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $state = $this->state->findById($id);
        return $this->showStateForm($state);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(StateRequest $request, $id)
    {
        $state = $this->state->findById($id);
        $this->saveStateForm($request, $state);

        flash(__('alerts.success'))->success();
        return redirect()->route('state.show', $state->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->state->delete($id);

        flash(__('alerts.success'))->success();
        return redirect()->route('state.index');
    }
}
