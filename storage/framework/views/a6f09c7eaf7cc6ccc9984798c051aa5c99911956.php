<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(route('city.index')); ?>">Back to Cities</a>
    <h1><?php echo e(isset($city) ? 'Edit' : 'Create'); ?> City</h1>

    <div class="row">
        <div class="col-lg-6">
            
            <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <form action="<?php echo e(isset($city) ? route('city.update', $city->id) : route('city.store')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php echo method_field(isset($city) ? 'PUT' : 'POST'); ?>
        
                <div class="form-group">
                    <label for="">City Name</label>
                    <input type="text" name="name" value="<?php echo e(old('name', (isset($city) ? $city->name : '' ))); ?>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Country</label>
                    <select name="country_id" class="form-control">
                        <option value="">Select Country</option>

                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option 
                                value="<?php echo e($country->id); ?>"
                                <?php if(old('country_id')): ?>
                                    <?php echo e(old('country_id') == $country->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($city) ? ( $city->state->country_id == $country->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                <?php echo e($country->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">State</label>
                    <select name="state_id" class="form-control">
                        <option value="">Select State</option>

                        <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option 
                                value="<?php echo e($state->id); ?>"
                                data-country-id="<?php echo e($state->country_id); ?>"
                                <?php if(old('state_id')): ?>
                                    <?php echo e(old('state_id') == $state->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($city) ? ( $city->state_id == $state->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                <?php echo e($state->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
        
        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>