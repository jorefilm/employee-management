<?php $__env->startSection('content'); ?>
    <h1>States</h1>
    <a class="mb-3 d-block" href="<?php echo e(route('state.create')); ?>">Add</a>
    
    <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Country</th>
            <th></th>
        </thead>
        <?php if($states): ?>
            <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($state->id); ?></td>
                    <td><?php echo e($state->name); ?></td>
                    <td><?php echo e($state->country->name); ?></td>
                    <td class="text-center">
                        <a href="<?php echo e(route('state.show', $state->id)); ?>">Edit</a>
                        <form action="<?php echo e(route('state.destroy', $state->id)); ?>" method="POST" class="d-inline action-delete">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('DELETE'); ?>
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </table>

    <?php echo e($states->links()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>