<!-- Sidebar -->
<?php if(config('menu.sidebar')): ?>
    <ul class="sidebar navbar-nav">
        <?php $__currentLoopData = config('menu.sidebar'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menuItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="nav-item dropdown">
                <a 
                    class="nav-link <?php echo e($menuItem['children'] ? 'dropdown-toggle' : ''); ?>"
                    href="<?php echo e(count($menuItem['children']) ? '#' : route($menuItem['route'])); ?>"
                    role="button"
                    <?php echo e(count($menuItem['children']) ? 'data-toggle=dropdown' : ''); ?>

                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    <i class="fas fa-fw <?php echo e($menuItem['icon']); ?>"></i>
                    <span><?php echo e($menuItem['label']); ?></span>
                </a>

                <?php if($menuItem['children']): ?>
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        <?php $__currentLoopData = $menuItem['children']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $childItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a class="dropdown-item" href="<?php echo e(route($childItem['route'])); ?>"><?php echo e($childItem['label']); ?></a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endif; ?>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
<?php endif; ?>