<?php

namespace App\Repositories\Eloquent;

// App
use App\City;
use App\Repositories\Eloquent\AbstractRepository;
use App\Repositories\CityRepositoryInterface;

class CityRepository extends AbstractRepository implements CityRepositoryInterface
{
    /**
     * Create mew instance of repository
     * 
     * @return void
     */ 
    public function __construct(City $city)
    {
        $this->model = $city;
    }

    /**
     * Retrieve the List of Cities
     * 
     * @return \Illuminate\Pagination\Paginator|Array
     */ 
    public function listSearchResult($search = null, $perPage = 10)
    {
        return $this->model->when($search, function($query) use($search) {
            $query->where(function($query) use($search) {
                $query->where('name',  'like', "%" . $search . "%");
            });
        })
        -> paginate(20);
    }

}