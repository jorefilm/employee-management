@extends('layouts.app')

@section('content')
    <h1>Users</h1>
    <a class="mb-3 d-block" href="{{ route('user.create') }}">Add</a>

    @include('partials.alerts')

    <form>
        <div class="input-group mb-2">
            <input type="text" name="search" placeholder="Search user by username, first name , last name, and department." class="form-control" value="{{ Request::input('search') }}">
            <div class="input-group-append">
                <button class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Username</th>
            <th></th>
        </thead>
        @if($users)
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->fullname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->username }}</td>
                    <td class="text-center">
                        <a href="{{ route('user.show', $user->id) }}">Edit</a>
                        <form action="{{ route('user.destroy', $user->id) }}" method="POST" class="d-inline action-delete">
                            @csrf
                            @method('DELETE')
                            <button href="#" role="submit" class="text-danger btn btn-link">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    {{ $users->links() }}
@endsection