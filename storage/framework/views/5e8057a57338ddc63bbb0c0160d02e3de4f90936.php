<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(route('employee.index')); ?>">Back to Employees</a>
    <h1><?php echo e(isset($employee) ? 'Edit' : 'Create'); ?> Employee</h1>

    <div class="row">
        <div class="col-lg-6">
            <?php echo $__env->make('partials.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    
            <form action="<?php echo e(isset($employee) ? route('employee.update', $employee->id) : route('employee.store')); ?>" method="POST"  enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <?php echo method_field(isset($employee) ? 'PUT' : 'POST'); ?>
                
                <h4>Basic Info</h4>
                <div class="form-group">
                    <label for="">Firstname</label>
                    <input type="text" name="firstname" value="<?php echo e(old('firstname', (isset($employee) ? $employee->firstname : '' ))); ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Middlename</label>
                    <input type="text" name="middlename" value="<?php echo e(old('middlename', (isset($employee) ? $employee->middlename : '' ))); ?>" class="form-control">
                </div>
        
                <div class="form-group">
                    <label for="">Lastname</label>
                    <input type="text" name="lastname" value="<?php echo e(old('lastname', (isset($employee) ? $employee->lastname : '' ))); ?>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Date of Birth</label>
                    <input type="text" name="birthdate" value="<?php echo e(old('birthdate', (isset($employee) ? $employee->bday : '' ))); ?>" class="form-control datepicker"  autocomplete="off">
                </div>
                <?php if(isset($employee)): ?>
                    <div class="form-group">
                        <label for="">Age</label>
                        <input type="text" readonly value="<?php echo e($employee->age); ?>" class="form-control">
                    </div>
                <?php endif; ?>
                <div class="form-group">
                    <label for="">Photo</label>
                    <?php if(isset($employee)): ?>
                        <img class="img-fluid picture-preview" src="<?php echo e($employee->picture ? asset('uploads/' . $employee->picture) : 'https://www.chaarat.com/wp-content/uploads/2017/08/placeholder-user.png'); ?>">
                    <?php else: ?>
                        <img class="img-fluid picture-preview" src="https://www.chaarat.com/wp-content/uploads/2017/08/placeholder-user.png">
                    <?php endif; ?>
                    <input type="file" name="picture" value="<?php echo e(old('picture', (isset($employee) ? $employee->picture : '' ))); ?>" class="form-control">
                </div>

                <h4>Address</h4>

                <div class="form-group">
                    <label for="">Address</label>
                    <input type="text" name="address" value="<?php echo e(old('address', (isset($employee) ? $employee->address : '' ))); ?>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Country</label>
                    <select name="country_id" class="form-control">
                        <option value="">Select Country</option>

                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option 
                                value="<?php echo e($country->id); ?>"
                                <?php if(old('country_id')): ?>
                                    <?php echo e(old('country_id') == $country->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($employee) ? ( $employee->country_id == $country->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                <?php echo e($country->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">State</label>
                    <select name="state_id" class="form-control">
                        <option value="">Select State</option>

                        <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                                value="<?php echo e($state->id); ?>"
                                data-country-id="<?php echo e($state->country_id); ?>"
                                <?php if(old('state_id')): ?>
                                    <?php echo e(old('state_id') == $state->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($employee) ? ( $employee->state_id == $state->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                <?php echo e($state->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">City</label>
                    <select name="city_id" class="form-control">
                        <option value="">Select City</option>

                        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                                value="<?php echo e($city->id); ?>"
                                data-state-id="<?php echo e($city->state_id); ?>"
                                data-country-id="<?php echo e($city->state->country->id); ?>"
                                <?php if(old('city_id')): ?>
                                    <?php echo e(old('city_id') == $city->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($employee) ? ( $employee->city_id == $city->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                    <?php echo e($city->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                

                <div class="form-group">
                    <label for="">ZIP</label>
                        <input type="text" name="zip" value="<?php echo e(old('zip', (isset($employee) ? $employee->zip : '' ))); ?>" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Department</label>
                    <select name="department_id" class="form-control">
                        <option value="">Select Department</option>

                        <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option 
                                value="<?php echo e($department->id); ?>"
                                <?php if(old('department_id')): ?>
                                    <?php echo e(old('department_id') == $department->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($employee) ? ( $employee->department_id == $department->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                <?php echo e($department->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Division</label>
                    <select name="division_id" class="form-control">
                        <option value="">Select division</option>

                        <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option 
                                value="<?php echo e($division->id); ?>"
                                <?php if(old('division_id')): ?>
                                    <?php echo e(old('division_id') == $division->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($employee) ? ( $employee->division_id == $division->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                <?php echo e($division->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Company</label>
                    <select name="company_id" class="form-control">
                        <option value="">Select company</option>

                        <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option 
                                value="<?php echo e($company->id); ?>"
                                <?php if(old('company_id')): ?>
                                    <?php echo e(old('company_id') == $company->id ? ' selected="selected"' : ''); ?>

                                <?php else: ?>
                                    <?php echo e(isset($employee) ? ( $employee->company_id == $company->id ? ' selected="selected"' : ''  ) : ''); ?>

                                <?php endif; ?>
                            >
                                <?php echo e($company->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Date Hired</label>
                    <input type="text" name="date_hired" value="<?php echo e(old('date_hired', (isset($employee) ? $employee->hired_date : '' ))); ?>" class="form-control datepicker" autocomplete="off">
                </div>


        
                <button class="btn btn-primary btn-block" role="submit">Save</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>