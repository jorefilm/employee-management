<?php

return [
    'sidebar' => [
        [
            'label' => 'Dashboard',
            'route' => 'dashboard',
            'icon' => 'fa-home',
            'children' => []

        ],
        [
            'label' => 'Employee Management',
            'route' => 'employee.index',
            'icon' => 'fa-user',
            'children' => []
        ],
        [
            'label' => 'System Management',
            'route' => '',
            'icon' => 'fa-sliders-h',
            'children' => [
                [
                    'label' => 'Department',
                    'route' => 'department.index',
                    'icon' => '',
                    'children' => []
                ],
                [
                    'label' => 'Division',
                    'route' => 'division.index',
                    'icon' => '',
                    'children' => []
                ],
                [
                    'label' => 'Country',
                    'route' => 'country.index',
                    'icon' => '',
                    'children' => []
                ],
                [
                    'label' => 'State',
                    'route' => 'state.index',
                    'icon' => '',
                    'children' => []
                ],
                [
                    'label' => 'City',
                    'route' => 'city.index',
                    'icon' => '',
                    'children' => []
                ],
            ],
        ],
        [
            'label' => 'User Management',
            'route' => '',
            'icon' => 'fa-lock-open',
            'children' => [
                [
                    'label' => 'User',
                    'route' => 'user.index',
                    'children' => []
                ],

            ],
        ],
    ]
];