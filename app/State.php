<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class State extends Model
{
    protected $fillable = ['name', 'country_id'];
    protected $with = ['country'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('country_id', function (Builder $builder) {
            $builder->whereNotNull('country_id');
            $builder->orderBy('name', 'ASC');
        });
    }
    

    // Relations

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
